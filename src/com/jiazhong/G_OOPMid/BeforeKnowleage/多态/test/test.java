package com.jiazhong.G_OOPMid.BeforeKnowleage.多态.test;
//:现有一个继承结构如下：要求创建 1个 Person对象、2 个 Student 对象和 2 个 Teacher
//        对象,统一放在数组中，并调用每个对象 say 方法
public class test {
    public static void main(String[] args) {
        Person[] person=new Person[5];
        person[0]=new Person("大郎",45);
        person[1]=new Student("二郎",43,"小学生");
        person[2]=new Student("詹三",11,"小学生");
        person[3]=new Teacher("李思",33,"讲师");
        person[4]=new Teacher("王武",55,"教授");
        for (int i=0;i<5;i++){
            System.out.println(person[i].say());
        if (person[i] instanceof Student){
            Student student=(Student)person[i];
            System.out.println(student.study());
        }
        else if (person[i] instanceof Teacher){
            System.out.println(((Teacher)person[i]).teach());
        }
        else if (person[i] instanceof Person) {
            System.out.println("Person对象");
        }
        else {
            System.out.println("你的类型有误");
             }
        }
    }
//    public boolean equals(Object obj) {
//        if(this == obj){
//            return true;
//        }
//        if (obj instanceof Person){
//            Person person=(Person)obj;
//            return this.name.equals(test.s1)this.s1.equals(test.s1);
//        }
//        return false;
//    }
}
