package com.jiazhong.G_OOPMid.BeforeKnowleage.多态.test;

public class Student extends Person{
    private  String student;
    public Student(String name, int age,String student) {
        super(name, age);
        this.student=student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getStudent() {
        return student;
    }
    public String say(){
        return (super.say()+student);
    }
    public  String study(){
        return ("学生"+getName()+"在学习 ");
    }
}
