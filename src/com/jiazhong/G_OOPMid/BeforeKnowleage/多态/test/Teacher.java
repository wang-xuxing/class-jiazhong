package com.jiazhong.G_OOPMid.BeforeKnowleage.多态.test;

public class Teacher extends Person {
    private String teacher;
    public Teacher(String name, int age, String teacher) {
        super(name, age);
        this.teacher = teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getTeacher() {
        return teacher;
    }
    public String say(){
        return (super.say()+teacher);
    }
    public String teach(){
        return ("老师" +getName()+"在教书");
    }
}
