package com.jiazhong.G_OOPMid.BeforeKnowleage.多态.习题1;

public abstract class Animal {
    abstract void cry();
    abstract String getAnimalName();
}
class  Dog extends Animal{
    @Override
    void cry() {
        System.out.println("汪汪汪");
    }

    @Override
    String getAnimalName() {
        return "狗";
    }
}
class Cat extends Animal{
    void cry() {
        System.out.println("喵喵喵");
    }

    @Override
    String getAnimalName() {
        return "猫";
    }
}