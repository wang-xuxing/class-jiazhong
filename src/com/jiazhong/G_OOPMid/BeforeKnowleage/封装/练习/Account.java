package com.jiazhong.G_OOPMid.BeforeKnowleage.封装.练习;
//创建程序,在其中定义两个类:Account和AccountTest类体会Java的封装性。
//1. Account类要求具有属性:姓名（长度为2位3位或4位)、余额(必须>20)、密码(必须是六位)，如果不满足，则给出提示信息，并给默认值(程序员自己定)
//2.通过setXxx的方法给Account的属性赋值。
//3.在AccountTest中测试.
public class Account {
    String name;
    double yue;
    String password;
    public void setName(String name) {
    if (name.length()>=2&&name.length()<4){
        this.name = name;
    }
    else {
        System.out.println("姓名长度需在2-4位之间，请输入正确姓名");
        this.name="姓名错误";

    }
    }
    public void setYue(double yue) {
    if (yue >= 20) {
        this.yue = yue;
    }
    else {
            System.out.println("余额必须大于20");
            this.yue=0.0;
    }
    }
    public void setPassword(String password){
    if (password.length()==6){
        this.password=password;
    }else{
        System.out.println("密码必须是六位");
        this.password="000000";
        }
    }
    public String getName(String name){
        return name;
    }
    public double getYue(double yue){
        return yue;
    }
    public String getPassword(String password){
        return password;
    }
        public String print(){
        return ("姓名:"+name+"余额:"+yue+"密码:"+password);
    }

}
