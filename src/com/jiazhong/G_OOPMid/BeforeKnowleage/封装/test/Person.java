package com.jiazhong.G_OOPMid.BeforeKnowleage.封装.test;

public class Person {
    private int age;
    private String name;
    private double salary;

    public Person() {
    }
    public Person(int age, String name, double salary) {
        setAge(age);
        setName(name);
        setSalary(salary);
    }



    public void setAge(int age) {
        if (age > 0 && age < 120) {
            this.age = age;
        } else {
            System.out.println("情书输入正确的年龄");
            this.age = 18;
        }
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        if (name.length() >= 2 && name.length() <= 6) {
            this.name = name;
        } else {
            System.out.println("姓名长度在2-6字符之间");
            this.name = name;
        }
    }

    public String getName() {
        return name;
    }

    public void setSalary(double salary) {
        this.salary=salary;
    }

    public double getSalary() {
        return salary;
    }
    public String string(){
        return "姓名："+name+"年龄"+age+"工资"+salary;
    }
}
