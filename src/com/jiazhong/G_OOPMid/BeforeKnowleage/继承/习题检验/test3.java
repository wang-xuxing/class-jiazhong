package com.jiazhong.G_OOPMid.BeforeKnowleage.继承.习题检验;
class  A{
    int m;
    int getM(){
        return m;
    }
    int seeM(){
        return m;
    }
}
class B1 extends A{
    int m;

    @Override
    public int getM() {
        return m+100;
    }
}
public class test3 {
    public static void main(String[] args) {
        B1 b1=new B1();
//        b1.m=20;
        System.out.println(b1.getM());
        A a=b1;
        a.m=-100;
        System.out.println(a.getM());
        System.out.println(a.seeM());

    }
}
