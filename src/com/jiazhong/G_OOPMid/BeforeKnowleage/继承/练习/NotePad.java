package com.jiazhong.G_OOPMid.BeforeKnowleage.继承.练习;
//编写 NotePad 子类，继承 Computer 类，添加特有属性【color】
public class NotePad extends Computer{
private String color;

    public NotePad(int CPU, int neicun, int yingpan,String color) {

        super(CPU, neicun, yingpan);
        this.color=color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public String print(){
        return (super.getDetails()+"颜色"+color);
    }
}
