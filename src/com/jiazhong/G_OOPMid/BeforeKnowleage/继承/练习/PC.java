package com.jiazhong.G_OOPMid.BeforeKnowleage.继承.练习;
//编写PC子类，继承Computer类，添加特有属性【品牌brand】
public class PC extends Computer {
    private String brand;

    public PC(int CPU, int neicun, int yingpan,String barnd) {
        super(CPU, neicun, yingpan);
        this.brand =barnd;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public String print( ){
        return (getDetails()+"品牌"+ brand);
    }
}
