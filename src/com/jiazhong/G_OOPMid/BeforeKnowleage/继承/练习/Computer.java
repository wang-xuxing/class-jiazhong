package com.jiazhong.G_OOPMid.BeforeKnowleage.继承.练习;
//编写 Computer 类，包含 CPU、内存、硬盘等属性，getDetails 方 法用于返回Computer的详细信息
public class Computer {
    private int CPU;
    private int neicun;
    private int yingpan;

    public void setCPU(int CPU) {
        this.CPU = CPU;
    }
    public void setNeicun(int neicun) {
        this.neicun = neicun;
    }
    public void setYingpan(int yingpan) {
        this.yingpan = yingpan;
    }
    public int getCPU() {
        return CPU;
    }

    public int getNeicun() {
        return neicun;
    }

    public int getYingpan() {
        return yingpan;
    }
    public Computer(int CPU,int neicun,int yingpan){
    this.CPU=CPU;
    this.neicun=neicun;
    this.yingpan=yingpan;
    }

    public String getDetails() {
        return ("cpu:"+CPU+"内存容量:"+neicun+"硬盘容量:"+yingpan);
    }
}
