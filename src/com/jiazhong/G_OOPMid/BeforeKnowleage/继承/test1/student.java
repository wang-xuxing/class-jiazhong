package com.jiazhong.G_OOPMid.BeforeKnowleage.继承.test1;

public class student {
    private double score;
    private String name;
    private int age;
    public student(int age, String name, double score) {
        this.age=age;
        this.name=name;
        this.score=score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getAge() {
        return age;
    }

    public double getScore() {
        return score;
    }

    public String getName() {
        return name;
    }
    public String print(){
        return ("姓名:"+name+"年龄:"+age+"成绩:" +score);
    }
}