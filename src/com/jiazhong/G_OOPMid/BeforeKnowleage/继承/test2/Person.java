package com.jiazhong.G_forObjectMid.BeforeKnowleage.继承.test2;
//1) 编写一个 Person 类，包括属性/private（name、age），构造器、方法 say(返回自我介绍的字符串）。
public class Person {
    private int age;
    private  String name;
    public void setAge(int age) {this.age = age;}
    public void setName(String name) {this.name = name;}
    public int getAge() {return age;}
    public String getName() {return name;}
    public Person(int age,String name) {
        this.age = age;this.name=name;
    }
    public String say(){
        return ("我叫"+name+"今年"+age+"岁");
    }
}
