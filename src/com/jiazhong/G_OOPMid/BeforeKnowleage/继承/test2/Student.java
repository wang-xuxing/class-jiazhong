package com.jiazhong.G_forObjectMid.BeforeKnowleage.继承.test2;
//2) 编写一个 Student 类，继承 Person 类，增加 id、score 属性/private，以及构造器，定义 say 方法(返回自我介绍的信息)。
public class Student extends Person{
    private int id;
    private double score;
    public Student(int age, String name,int id,double score) {
        super(age, name);
        this.id=id;
        this.score=score;
    }
    public void setId(int id) {this.id = id;}
    public void setScore(double score) {this.score = score;}
    public double getScore() {return score;}
    public int getId() {return id;}
    public  String say(){
        return (super.say()+"学号"+id+"分数"+score);
    }
}
