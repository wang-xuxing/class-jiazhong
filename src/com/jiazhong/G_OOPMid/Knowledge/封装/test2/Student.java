package com.jiazhong.G_forObjectMid.Knowledge.封装.test2;
//2) 编写一个 Student 类，继承 Person 类，增加 id、score 属性/private，以及构造器，定义 say 方法(返回自我介绍的信息)。
public class Student extends Person {
     private  double score;
     private  int id;
    public Student(String name, int age,int id ,double score) {
        super(name, age);
        this.id=id;
        this.score=score;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void say() {
        super.say();
        System.out.println("id:"+id+"分数:"+score);
    }
}
