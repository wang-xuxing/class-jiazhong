package com.jiazhong.G_forObjectMid.Knowledge.封装.test1;
import java.util.Scanner;
//创建程序,在其中定义两个类:Account和AccountTest类体会Java的封装性
//1. Account类要求具有属性:姓名（长度为2位3位或4位)、余额(必须>20).
//密码(必须是六位)，如果不满足，则给出提示信息，并给默认值(程序员自己定)
//2.通过setXxx的方法给Account的属性赋值。3.在AccountTest中测试
public class test1 {
    public static void main(String[] args) {
        Account account=new Account();
        account.show();
    }
}
class Account{
private  String name;
private  double money;
private String password;
    public Account() {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入你的姓名：");
         String name=scanner.next();
        System.out.println("请输入你的密码：");
         String password=scanner.next();
        setName(name);
        setMoney(123);
        setPassword(password);
    }
//    public Account(String name, double money, String password) {
//        this.name = name;
//        this.money = money;
//        this.password = password;
//
//    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        if (name.length()>=2&&name.length()<=4){
            this.name = name;
        }
        else {
            System.out.println("请输入正确姓名！！！");
            this.name="无名人";
        }
    }
    public double getMoney() {
        return money;
    }
    public void setMoney(double money) {
        if (money>=20){
            this.money = money;
        }
        else {
            System.out.println("你真是个穷光蛋！！");
            this.money=0;
        }
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        if (password.length()==6){
            this.password = password;
        }
        else {
            System.out.println("密码格式输入错误！！");
            this.password="******";
        }
    }
    public void show(){
        System.out.println("姓名:"+name+"余额:"+money+"密码:"+password);
    }
}