package com.jiazhong.G_forObjectMid.Knowledge.Eqlues;

public class Demo01 {
    public static void main(String[] args) {
        A a = new A();
        A b = a;
        A c = b;
        System.out.println(a == c);
        System.out.println(b == c);
        B obj = a;
        System.out.println(obj == c);
    }
}

class A extends B {
}

class B {
}
