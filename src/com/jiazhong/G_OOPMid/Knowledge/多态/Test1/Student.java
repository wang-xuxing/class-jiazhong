package com.jiazhong.G_forObjectMid.Knowledge.多态.Test1;

public class Student extends Person{
    double score;

    public Student(String name, int age,double score) {
        super(name, age);
        this.score=score;
    }

    public void say(){
        super.say();
        System.out.println("分数："+score);
    }
}
