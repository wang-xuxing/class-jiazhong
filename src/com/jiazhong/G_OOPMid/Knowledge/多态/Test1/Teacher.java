package com.jiazhong.G_forObjectMid.Knowledge.多态.Test1;

public class Teacher extends Person{
    String work;

    public Teacher(String name, int age, String work) {
        super(name, age);
        this.work = work;
    }

    public void say(){
        super.say();
        System.out.println("职业："+work);
    }
}
