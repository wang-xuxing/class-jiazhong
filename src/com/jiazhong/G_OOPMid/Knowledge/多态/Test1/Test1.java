package com.jiazhong.G_forObjectMid.Knowledge.多态.Test1;
//PloyArray.java 数组的定义类型为父类类型，里面保存的实际元素类型为子类类型
//应用实例:现有一个继承结构如下：要求创建1 个 Person对象、2个 Student 对象和 2 个Teacher 对象,统一放在数组中，并调用每个对象 say 方法.
public class Test1 {
    public static void main(String[] args) {
        Person[] person=new Person[5];
        person[0]=new Person("张三",18);
        person[1]=new Student("李思",19,86.9);
        person[2]=new Student("王武",20,99.6);
        person[3]=new Teacher("老刘",80,"教授");
        person[4]=new Teacher("老舒",18,"讲师");
        for (int i=0;i<person.length;i++){
            person[i].say();
        }
    }
}
