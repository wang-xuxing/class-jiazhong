package com.jiazhong.G_forObjectMid.Knowledge.继承.test1;
//编写Computer类，包含CPU、内存、硬盘等属性，getDetails方法用于返回 Computer的详细信息
//编写PC子类,继承Computer类,添加特有属性【品牌 brand 】
//编写NotePad子类，继承Computer类，添加特有属性【color】
//编写Test类，在main方法中创建PC和 NotePad对象，分别给对象中特有的属性赋值，以及从 Computer类继承的属性赋值，并使用方法并打印输出信息
public class test1 {
    public static void main(String[] args) {
        PC pc=new PC("因特尔.酷睿.i5","12","128G+1T","戴尔");
        System.out.println(pc.getDetails());
        NotePad notePad=new NotePad("因特尔.酷睿.i5","12","128G+1T","紫色");
        System.out.println(notePad.getDetails());
    }
}
