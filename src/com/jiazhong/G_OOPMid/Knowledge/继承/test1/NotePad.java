package com.jiazhong.G_forObjectMid.Knowledge.继承.test1;
//编写NotePad子类，继承Computer类，添加特有属性【color】
public class NotePad extends Computer{
    private  String color;

    public NotePad(String CPU, String neicun, String hardDisc,String color) {
        super(CPU, neicun, hardDisc);
        this.color=color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getDetails(){
        return (super.getDetails()+"，NotePad颜色:"+color);
    }
}
