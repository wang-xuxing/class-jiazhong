package com.jiazhong.G_forObjectMid.Knowledge.继承.test1;
//编写Computer类，包含CPU、内存、硬盘等属性，getDetails方法用于返回 Computer的详细信息
public class Computer {
    private String CPU;
    private String neicun;
    private String hardDisc;

    public Computer(String CPU, String neicun, String hardDisc) {
        this.CPU = CPU;
        this.neicun = neicun;
        this.hardDisc = hardDisc;
    }

    public String getCPU() {
        return CPU;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public String getNeicun() {
        return neicun;
    }

    public void setNeicun(String neicun) {
        this.neicun = neicun;
    }

    public String getHardDisc() {
        return hardDisc;
    }

    public void setHardDisc(String hardDisc) {
        this.hardDisc = hardDisc;
    }
    public String getDetails(){
        return ("CPU:"+CPU+"，内存"+neicun+"，硬盘"+hardDisc);
    }
}
