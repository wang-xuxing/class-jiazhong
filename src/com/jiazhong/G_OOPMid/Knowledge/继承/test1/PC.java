package com.jiazhong.G_forObjectMid.Knowledge.继承.test1;
//编写PC子类,继承Computer类,添加特有属性【品牌 brand 】
public class PC extends Computer{
    private String brand;

    public PC(String CPU, String neicun, String hardDisc,String brand) {
        super(CPU, neicun, hardDisc);
        this.brand=brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getDetails(){
        return (super.getDetails()+"，电脑品牌:"+brand);
    }
}
