package com.jiazhong.E_Array.StrongTest;
//要求：把数组的 元素内容反转。 arr {11,22,33,44,55,66} {66, 55,44,33,22,11}
public class test4 {
    public static void arr1(){
    int[] arr1={11,22,33,44,55,66};
    int temp;
        for (int i=0;i< arr1.length/2;i++){
            temp=arr1[arr1.length-1-i];
            arr1[arr1.length-1-i]=arr1[i];
            arr1[i]=temp;
        }
        System.out.println("反转后arr1为");
        for (int i=0;i< arr1.length;i++){
            System.out.print(arr1[i]+" ");
        }
    }

    public static void arr2(){
        int[] arr1={11,22,33,44,55,66};
        int[] arr2=new int[arr1.length];
        for (int i=0;i< arr1.length;i++) {
            arr2[i]=arr1[arr1.length-1-i];}
               arr1=arr2;
        System.out.println();
        System.out.println("反转后arr1为");
        for (int i=0;i< arr1.length;i++){
            System.out.print(arr1[i]+" ");
        }
    }
    public static void main(String[] args) {
        arr1();
        arr2();
    }
}
