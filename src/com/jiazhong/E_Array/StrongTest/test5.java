package com.jiazhong.E_Array.StrongTest;
import java.util.Scanner;
//要求：实现动态的给数组添加元素效果，实现对数组扩容。
//        1) 原始数组使用静态分配 int[] arr = {1,2,3}
//        2) 增加的元素 4，直接放在数组的最后 arr = {1,2,3,4}
//        3) 用户可以通过如下方法来决定是否继续添加，添加成功，是否继续？y/n
public class test5 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int[] arr={1,2,3,8};
        int[] arr_new=new int[arr.length+1];
        for (int i=0;i< arr.length;i++){
          for (int j=0;j<arr.length;j++){
              arr_new[j]=arr[j];
          }
            System.out.println("请输入想要添加的数字：");
            int arr_z= scanner.nextInt();
                arr_new[arr_new.length-1]=arr_z;

                arr=arr_new;
            for (int j=0;j<arr.length;j++){
                System.out.print(arr[j]+" ");
            }
            System.out.println();
            System.out.println("添加成功，是否继续？y/n");
            char key=scanner.next().charAt(0);
            if (key=='n'){
                System.out.println("程序结束");break;
            }
            else {key='y';
                System.out.println("继续输入");
            }

    }
    }
}
