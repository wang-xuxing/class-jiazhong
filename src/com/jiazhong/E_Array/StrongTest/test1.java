package com.jiazhong.E_Array.StrongTest;
//创建一个 char 类型的 26 个元素的数组，分别 放置'A'-'Z'。使用 for 循环访 问所有元素并打印出来。提示：char 类型 数据运算 'A'+2 -> 'C
public class test1 {
    public static void main(String[] args) {
        char[] zimu = new char[26];
        for (int i = 0; i < zimu.length; i++) {
            zimu[i] = (char) ('A' + i);
            System.out.print(zimu[i] + " ");
        }
        System.out.println();
        System.out.println("===chars 数组===");
        for (int i = 0; i < zimu.length; i++) {//循环 26 次
            System.out.print(zimu[i] + " ");
        }
    }}
