package com.jiazhong.E_Array.StrongTest;
//编写代码 实现数组拷贝(内容复制) 将 int[] arr1 = {10,20,30}; 拷贝到 arr2 数组, 要求数据空间是独立的.
public class test3 {
    public static void main(String[] args) {
        int[] arr1={10,20,30};
        int[] arr2=new int[arr1.length];
        for (int i=0;i< arr1.length;i++){
           arr2[i]= arr1[i];
        }
        for (int i=0;i< arr1.length;i++){
            System.out.print("arr2:"+arr2[i]+" ");
        }
        for (int i=0;i< arr1.length;i++){
            System.out.print("arr1:"+arr1[i]+"");
        }
    }
}
