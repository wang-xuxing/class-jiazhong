package com.jiazhong.E_Array.SuperTest;
//11.在歌唱比赛中，共有10位评委进行打分，在计算歌手得分时，去掉一个最高分，去掉一个最低分，
//        然后剩余的8位评委的分数进行平均，就是该选手的最终得分。输入每个评委的评分，求某选手的得分。

import java.util.Arrays;
import java.util.Scanner;

public class test9 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入八位评委的的评分");
        double sum=0;
        double are=0;
        double score[]=new double[8];
        for (int i=0;i<8;i++){
            score[i]=scanner.nextDouble();
        }
        Arrays.sort(score);
        for (int i=0;i<8;i++){
            score[0]=0;
            score[score.length-1]=0;
            sum=sum+score[i];
        }
        are=sum/8;
        System.out.println(are);
    }
}
