package com.jiazhong.E_Array.SuperTest;
//8.在一个二维数组中存放了三名学生的语文和数学的成绩，分别求语文和数学的总成绩及平均分并输出。
public class test7 {
    public static void main(String[] args) {
        int[][] score={{66,77},{90,88},{60,70}};
        int chinese_arr=score[0][0]+score[1][0]+score[2][0];
        int chinese_ave=chinese_arr/3;
        int english_arr=score[0][1]+score[1][1]+score[2][1];
        int english_ave=english_arr/3;
        System.out.println("语文总成绩"+chinese_arr+"语文平均分"+chinese_ave);
        System.out.println("英语总成绩"+english_arr+"英语平均分"+english_ave);

    }
}
