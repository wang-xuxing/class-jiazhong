package com.jiazhong.E_Array.SuperTest;
//1.有一个整数数组，其中存放着序列1，3, 5，7，9，11， 13， 15，17，19。请将该序列倒序存放并输出。
public class test1 {
    public static void main(String[] args) {
    int[] arr={1,3,5,7,9,11, 13, 15,17,19};
    int temp;
    for (int i=0;i< (arr.length)/2;i++){
        temp=arr[i];
        arr[i]=arr[arr.length-1-i];
        arr[arr.length-1-i]=temp;

    }
    for ( int i=0;i< arr.length;i++){
        System.out.print(arr[i]+" ");
    }
    }
}
