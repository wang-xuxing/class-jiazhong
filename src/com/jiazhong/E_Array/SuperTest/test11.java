package com.jiazhong.E_Array.SuperTest;

import java.util.Random;
import java.util.Scanner;

//取[0-n]的n个随机数存入数组中，要求数据不能重复.
public class test11 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请随机输入一个数:");
        int n=scanner.nextInt();
        int[] arr=new int[n];
        Random r= new Random();
        for(int i=0;i<n;i++)
        {
            int number = r.nextInt(n);
            boolean s;
            do{
                s= false;
                for(int j=0;j<i;j++)
                {
                    if(number == arr[j])
                    {
                        number = r.nextInt(n);
                        s=true;
                        break;
                    }
                }
            }while(s);
            arr[i]=number;
        }
        for(int j=0;j<n;j++)
        {
            System.out.print(arr[j]+" ");
        }
    }
    }


