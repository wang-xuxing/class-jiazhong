package com.jiazhong.E_Array.SuperTest;
import java.util.Scanner;
public class TEXT {
//    public  static void main(String[] args){
//        int[] arr={1,3,5,7,9,11,13 ,15,17,19};
//        int temp=0;
//        for(int i=0; i<arr.length/2;i++){
//            temp=arr[i];
//            arr[i]=arr[arr.length-i-1];
//            arr[arr.length-i-1]=temp;
//        }
//        for(int i=0; i<arr.length;i++){
//            System.out.print(arr[i]+" ");
//        }
//}}
//.	编写一个程序，生成0-9之间的100个整数并且显示每一个数的个数
//public static void main(String[] args) {
//    int[]num=new int[100];
//    int[]a=new int[10];
//    for (int i=0;i<100;i++){
//        num[i]=(int)(Math.random()*10);
//        a[num[i]]++;
//    }
//    System.out.println("0-9之间的100个整数:");
//    for (int i=0;i<100;i++){
//        System.out.print(" "+num[i]);
//    }
//    System.out.println();
//    for (int i=0;i<10;i++){
//        System.out.println(i+"的个数有"+a[i]);
//    }
//
//}
//7. 从键盘输入8个整数存放在一个数组中，然后将奇数和偶数分别存入到两个不同的数组中，并按奇数、偶数交替的顺序输出这两个数组中的所有数据
// （先交替输出，如果奇数个数多，则再输出剩下的奇数，如果偶数个数多，则再输出剩下的偶数）。
//        （提示与要求：
//        （1）定义一个数组存储从键盘输入的8个整数，先判断这8个整数中奇数和偶数的个数，才能定义存储奇数和偶数的数组的长度；
//        （2）把一个大的数组分别存放在奇数和偶数数组中并交替输出的过程定义为方法）
//public static void main(String[] args) {
//    Scanner scanner=new Scanner(System.in);
//    int arr[]=new int[8];
//    System.out.println("请输入八个整数：");
//    for (int i=0;i< arr.length;i++){
//        arr[i]=scanner.nextInt();
//    }
//    int odd_num_count=0;
//    int even_num_count=0;
//    for (int i=0;i< arr.length;i++){
//        if (arr[i]%2==0){
//            even_num_count++;
//        }
//        else {
//            odd_num_count++;
//        }
//    }
//    int[] odd_num=new int[odd_num_count];
//    int[] even_num=new int[even_num_count];
//    int e=0;
//    int o=0;
//    for (int i=0;i<arr.length;i++){
//        if (arr[i]%2==0){
//            even_num[e]=arr[i];
//            e++;
//        }
//        else {
//            odd_num[o]=arr[i];
//            o++;
//            }
//        }
//    if (odd_num_count>even_num_count){
//        for (int i=0;i<even_num_count;i++){
//            System.out.print(odd_num[i]+" "+even_num[i]+" ");
//        }
//        for (int i=even_num_count;i<odd_num_count;i++){
//            System.out.print(odd_num[i]+" ");
//        }
//    }
//    else{
//        for (int i=0;i<odd_num_count;i++){
//            System.out.print(even_num[i]+" "+odd_num[i]+" ");
//        }
//        for (int i=odd_num_count;i<even_num_count;i++) {
//            System.out.print(even_num[i]+" ");
//        }
//
//    }
//}
//去除数组{1,3,1,4,2,3,6,1,5}中的重复项,存入一个新的数组,并从大到小排序.
public static void main(String[] args) {
    int[] arr={1,3,1,4,2,3,6,1,5};
    int num=0;
    int count=0;
    for (int i=0;i< arr.length-1;i++){
        for (int j=0;j< arr.length-1;j++){
              int temp=0;
              if (arr[j]>arr[j+1]){
              temp=arr[j];
              arr[j]=arr[j+1];
              arr[j+1]=temp;}
        }
    }
    for (int i=0;i< arr.length;i++){
        System.out.print(arr[i]+" ");
        num=arr[i];
        boolean flag=true;
        for (int j=0;j<i;j++) {
            if (num == arr[j]) {
                count++;
                flag =false;
            }
        }

        if (!flag){
            arr[i]=-1;}
        else {
            arr[i]=num;
        }
    }
    System.out.println();
    int[] arr2=new int[arr.length-count];
    int j=0;
    for (int i=0;i< arr.length;i++){
        if (arr[i]!=-1){
            arr2[j]=arr[i];
            j++;
        }
    }
    for (int i=0;i< arr2.length;i++){
        System.out.print(arr2[i]+" ");
    }
}
}