package com.jiazhong.E_Array.SuperTest;
//7. 从键盘输入8个整数存放在一个数组中，然后将奇数和偶数分别存入到两个不同的数组中，并按奇数、偶数交替的顺序输出这两个数组中的所有数据
// （先交替输出，如果奇数个数多，则再输出剩下的奇数，如果偶数个数多，则再输出剩下的偶数）。
//        （提示与要求：
//        （1）定义一个数组存储从键盘输入的8个整数，先判断这8个整数中奇数和偶数的个数，才能定义存储奇数和偶数的数组的长度；
//        （2）把一个大的数组分别存放在奇数和偶数数组中并交替输出的过程定义为方法）
import java.util.Scanner;

public class test6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] num = new int[8];
        System.out.println("请输入八个数");
        for (int i = 0; i < num.length; i++) {
            num[i] = scanner.nextInt();
        }
        int[] oddNum;//奇数数组；
        int[] evenNum;//偶数数组；
        int oddNum_count = 0;
        int evenNum_count = 0;
        for (int i = 0; i < num.length; i++) {
            if (num[i] % 2 == 0) {
                evenNum_count++;
            } else {
                oddNum_count++;
            }
        }
        oddNum = new int[oddNum_count];
        evenNum = new int[evenNum_count];
        int o=0;
        int e=0;
        for (int i = 0; i < num.length; i++) {
            if (num[i] % 2 == 0) {
                evenNum[e]=num[i];
                e++;
            }
            else {
                oddNum[o]=num[i];
                o++;
            }
        }
        if (evenNum_count>oddNum_count){
            for (int i=0;i<=oddNum_count;i++){
                System.out.print(oddNum[i]+" "+evenNum[i]+" ");
            }
            for (int i=oddNum.length;i<evenNum.length-1;i++){
                System.out.print(" "+evenNum[i]+" ");
            }
        }
        else {

        }for (int i=0;i<evenNum_count;i++){
            System.out.print(evenNum[i]+" "+oddNum[i]+" ");
        }
        for (int i=evenNum.length;i<=oddNum.length-1;i++){
            System.out.print(" "+oddNum[i]+" ");
        }
    }
    }
