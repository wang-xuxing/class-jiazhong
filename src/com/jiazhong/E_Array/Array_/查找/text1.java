package com.jiazhong.E_Array.Array_.查找;

import java.util.Scanner;
/*
在Java中，我们常用的查找有两种
1.顺序查找
2.二分查找法（二分法，我们在算法中讲）

1) 有一个数列：白眉鹰王、金毛狮王、紫衫龙王、青翼蝠王猜数游戏：
从键盘 中任意输入一个名称，判断数列中是否包含此名称【顺序查找】
要求: 如果找 到了，就提示找到，并给出下标值。
 */
public class text1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] list={"白眉鹰王","金毛狮王","紫衫龙王","青翼蝠王"};
        System.out.print("请输入一个名称：");
        String Find_list=scanner.next();
       int id=-1;
        for (int i=0;i<list.length;i++){
            if (Find_list.equals(list[i])){
                System.out.println("恭喜你找到了："+Find_list+"下标值为：");
                System.out.println(i);
                id=i;
                break;
            }
        }
        if (id==-1){
            System.out.println("不好意思您没找到");}
    }
}
