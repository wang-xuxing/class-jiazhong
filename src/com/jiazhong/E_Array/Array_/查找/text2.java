package com.jiazhong.E_Array.Array_.查找;

import java.util.Scanner;

public class text2 {
    public static void main(String[] args) {
        //定义一个字符串数组
        String[] names = {"白眉鹰王", "金毛狮王", "紫衫龙王", "青翼蝠王"};
        Scanner scanner = new Scanner(System.in);
        String findName="";
        System.out.println("请输入一个字符串里面的名字");
        findName = scanner.next();
        //
        int index = -1;
        for (int i = 0; i < names.length; i++) {
            if (findName.equals(names[i])) {
                System.out.println("恭喜你找到了" + findName);
                System.out.println("下标为" + i);
                index = i;
                break;
            }
        }
        if (index == -1) {
            System.out.println("不好意思没有找到" + findName);
        }
    }

}
