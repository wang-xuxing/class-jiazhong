package com.jiazhong.E_Array.Array_.second;
/*
数组的赋值机制
1.基本数据类型赋值，这个值就是具体的数据，而且不受影响。int n1=2; int n2=n1;
2.数组在默认情况下是引用传递，赋的值是地址，
*/
public class text1 {

        public static void main(String[] args) {
            int n1 = 2;
            int n2 = n1;
            n2 = 80;
            System.out.println(n1);
            System.out.println(n2);

            int[] arr1 = {1, 2, 3};
            int[] arr2 = arr1;//把arr1赋给了arr2
            arr2[1] = 100;
            System.out.println(arr1);
            System.out.println(arr2);
            System.out.println("数组1的遍历");
            for (int i = 0; i < arr1.length; i++) {
                System.out.println(arr1[i]);
            }
            System.out.println("数组2的遍历");
            for (int i = 0; i < arr2.length; i++) {
                System.out.println(arr2[i]);
            }
        }
    }