package com.jiazhong.E_Array.Array_.second;


import java.util.Scanner;

/*
数组的添加-扩容
要求：实现动态的给数组添加元素效果，实现数组扩容的功能
1.原始的数组使用静态分配方式
2.增加的元素4，直接放在数组的最后
3.用户可以通过如下的办法对数组实现操作，是否继续添加，添加成功，是够继续？y/n

思路分析
1.定义一个初始数组arr
2.定义一个新的数组arrNew
3.遍历arr数组，依次将arr的元素拷贝到arrNew数组
4.让4赋给arrNew[arrNew.length-1]=4;,把4赋给arrNew的最后一个元素
5.让arr指向arrNew,原来的数组就会销毁
6.创建一个Scanner
7.不知道用户什么时候退出，用do-while break来控制
 */
public class text4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr={1,2,34};
       do {
           int[] arrNew=new int[arr.length+1];
        for(int i=0;i< arr.length;i++){
            arrNew[i]=arr[i];}
        System.out.println("输入你想输入的数字：");
            int addNum=scanner.nextInt();
            arrNew[arrNew.length -1]=addNum;
            arr=arrNew;
        System.out.println("arr扩容后的元素情况");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");}
        //问用户是否继续
           System.out.println();
        System.out.println("是否继续添加y/n");
        char key = scanner.next().charAt(0);
        //如果输入n，就结束
        if (key == 'n'){break;}
        }
     while (true);
        System.out.println("你退出了添加");
}}

