package com.jiazhong.E_Array.Array_.second;

import java.util.Scanner;

public class text5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //初始化数组
        int arr[] = {1, 2, 3};
        do {
            int[] arrNew = new int[arr.length + 1];
            //遍历arr数组，依次将arr数组中的元素拷贝到srrNew里面
            for (int i = 0; i < arr.length; i++) {
                arrNew[i] = arr[i];
            }
            System.out.println("请输入你要添加的元素");
            int addNum = scanner.nextInt();
            //把addNum赋给arrNew的最后一个元素
            arrNew[arrNew.length - 1] = addNum;
            //让arr指向arrNew
            arr = arrNew;
            //输出arr，看看
            System.out.println("arr扩容后的元素情况");
            for (int i = 0; i < arr.length; i++) {
                System.out.println(arr[i] + " ");
            }
            //问用户是否继续
            System.out.println("是否继续添加y/n");
            char key = scanner.next().charAt(0);
            if (key == 'n') {//如果输入n，就结束
                break;
            }
        } while (true);
        System.out.println("你退出了添加");
    }
}