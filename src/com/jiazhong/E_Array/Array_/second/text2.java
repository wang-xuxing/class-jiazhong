package com.jiazhong.E_Array.Array_.second;
/*
数组的拷贝
将int[] arr1={10,20,30};拷贝到arr2数组，要求数据空间是独立的
 */
public interface text2 {
    public static void main(String[] args) {
       int[] arr1 = {10, 20, 30};
        //创建一个新的数组，arr2，开辟新的数据空间
        int[] arr2 = new int[arr1.length];
        //遍历arr1，把每一个元素拷贝到arr2对应的元素位置
        for (int i = 0; i < arr1.length; i++) {
            arr2[i] = arr1[i];
        }
        //改一下arr2里面的数据，会不会对arr1造成影响
        arr2[0] = 200;
        System.out.println("arr1的元素");
        for (int i = 0; i < arr1.length; i++) {
            System.out.println(arr1[i]);
        }
        System.out.println("arr2的元素");
        for (int i = 0; i < arr2.length; i++) {
            System.out.println(arr2[i]);
        }
   }
}