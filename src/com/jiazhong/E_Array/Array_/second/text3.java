package com.jiazhong.E_Array.Array_.second;
//要求：把数组的元素内容反转。
//        arr {11,22,33,44,55,66} {66, 55,44,33,22,11}
public class text3 {
    public static void main(String[] args) {
            arr1();arr2();
    }
    public static void arr1(){
        int[] arr={11,22,33,44,55,66};
        int temp=0;//定义一个反转中间值
        int len=arr.length;
        for (int i=0; i<len/2;i++){
            temp = arr[len - 1 - i];//保存
            arr[len - 1 - i] = arr[i];
            arr[i] = temp;
        }
        System.out.println("新数组排序为：");
        for (int i=0; i< len;i++){
            System.out.print(arr[i]+"\t");
        }
    }
    public static void arr2(){
        int[] arr={11,22,33,44,55,66};
        int[] arr2=new int[arr.length];
        for (int i = arr.length-1 , j = 0; i >= 0; i--, j++) {
            arr2[j] = arr[i];
        }
        arr=arr2;
        System.out.println("\n"+"新数组排序为");
        for (int i=0;i< arr.length;i++){
            System.out.print(arr[i]+"\t");
        }
    }
}
