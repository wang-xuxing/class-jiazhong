package com.jiazhong.E_Array.Array_.first;

import java.util.Scanner;

/**
 * 数组的使用
 * 使用方式1-动态初始化
 * 数组的定义
 * 数据类型 数组名[]=new 数据类型 [大小]
 * int a[] = new int[5]; 创建了一个数组，名字为a,长度为5，可以放5个int
 * 说明：这是定义数组的一种方法
 * <p>
 * 数组的引用（使用/访问/获取数组的元素）
 * 数组名[下标/索引]比如：我要使用a数组的第三个数a[2],数组的下标是从0开始的
 * <p>
 * <p>
 * 快速入门案例，循环输入五个成绩，保存到double数组，并输出
 */
public class text2 {
    public static void main(String[] args) {
        double num[] = new double[5];
        Scanner sc = new Scanner(System.in);
        int i;
        for (i = 0; i < num.length; i++) {
            System.out.println("请输入第" + (i + 1) + "个成绩");
            num[i] = sc.nextDouble();
            System.out.println("成绩为" + num[i]);
        }

    }
}
