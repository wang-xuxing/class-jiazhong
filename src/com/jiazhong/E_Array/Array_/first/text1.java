package com.jiazhong.E_Array.Array_.first;
//一个养鸡场有 6 只鸡，它们的体重分别是 3kg,5kg,1kg,3.4kg,2kg,50kg 。请 问这六只鸡的总体重是多少?平均体重是多少? 请你编一个程序。 思路：
//        1.六个double变量，求和得到总体重
//数组的介绍：数组可以存放多个同一类型的数据。数组也是一种数据类型，是引用数据类型。即：数(数据)组(一组)就是一组数据
//        数组的快速入门:
public class text1 {
    public static void main(String[] args) {
        double [] height={3,5,1,3.4,2,50};
        double sum_height = 0;
        double are_height=0;
        for (int i=0;i< height.length;i++){
            System.out.println("第"+(i+1)+"只鸡体重为"+height[i]+"kg");
            sum_height=sum_height+height[i];
            are_height=sum_height/height.length;
        }
        System.out.println("鸡的总体重"+sum_height+"kg");
        System.out.println("平均体重"+are_height+"kg");
    }
}
