package com.jiazhong.E_Array.Array_.二维数组;
/*
使用方式1-动态初始化
1.语法：数据类型[][]数组名= new 数据类型[大小][大小]
2.比如 int a [][]=new int [2][3]
3.使用演示
4，二维数组在内存中的存在形式
 */
public class text2 {
        public static void main(String[] args) {
            int arr[][];//声明二维数组
            arr = new int[2][3];//开空间
            arr[1][1] = 8;
            //遍历arr数组
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
        }
    }

