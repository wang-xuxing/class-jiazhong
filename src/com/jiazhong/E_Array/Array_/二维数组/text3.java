package com.jiazhong.E_Array.Array_.二维数组;
/*看一个需求：动态创建下面二维数组，并输出
i = 0: 1
i = 1: 2 2
i = 2: 3 3 3 一个有三个一维数组, 每个一维数组的元素是不一样的
*/
 /*
        使用方式2-动态初始化
        先声明：数据类型 数组名[][];
        在定义，开辟空间，赋值，有默认值，int 0
         */
//创建 二维数组，一个有3个一维数组，但是每个一维数组还没有开数据空间

public class text3 {
    public static void main(String[] args) {
        int arr[][]=new int[3][];
        for (int i=0;i<arr.length;i++){
            //给每一个一维数组开空间
            //如果没有给一维数组new，那么arr[i]就是null
            arr[i]=new int[i+1];
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = i + 1;//赋值
            }
        }
        for (int i=0;i< arr.length;i++){
            for (int j=0;j<arr[i].length;j++){
                System.out.print(arr[i][j]+"\t");
            }System.out.println();
        }

    }
}
