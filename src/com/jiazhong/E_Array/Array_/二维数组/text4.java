package com.jiazhong.E_Array.Array_.二维数组;
/*
使用方式3-静态初始化
定义：类型 数组名[][]={{},{},{}};
使用方式[固定方式访问]
 */
public class text4 {
    public static void main(String[] args) {
        //遍历该二维数组，并且得到和
        int arr[][]={{4,6},{1,4,5,7},{-2}};
        int sum=0;
        for (int i=0;i<arr.length;i++){
            for (int j=0;j<arr[i].length;j++){
                System.out.print(arr[i][j]+"\t");
                sum=sum+arr[i][j];
            }
            System.out.println();
        }
        System.out.println("和为"+sum);
    }

}
