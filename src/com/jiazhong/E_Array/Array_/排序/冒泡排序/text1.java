package com.jiazhong.E_Array.Array_.排序.冒泡排序;
//排序：将多个数据，按照指定的顺序进行排列的过程
//        排序的分类：
//        1.内部排序
//        2.外部排序
//        3.冒泡排序法：
public class text1 {
    public static void main(String[] args) {
        int arr[]={24, 69, 80, 57, 13};
        int temp=0;
        for (int i=0;i<4;i++){
            if (arr[i]>arr[i+1]){
                temp=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=temp;
            }}
            System.out.println("第一次排序后");
            for (int i=0;i< arr.length;i++){
                System.out.print(arr[i]+"\t");}

        for (int i=0;i<3;i++){
            if (arr[i]>arr[i+1]){
                temp=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=temp;
            }
        }
        System.out.println();
        System.out.println("第二次排序后");
        for (int i=0;i< arr.length;i++){
            System.out.print(arr[i]+"\t");
        }

        for (int i=0;i<2;i++){
            if (arr[i]>arr[i+1]){
                temp=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=temp;
            }
        }
        System.out.println();
        System.out.println("第三次排序后");
        for (int i=0;i< arr.length;i++){
            System.out.print(arr[i]+"\t");
        }

        for (int i=0;i<4;i++){
            if (arr[i]>arr[i+1]){
                temp=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=temp;
            }
        }
        System.out.println();
        System.out.println("第四次排序后");
        for (int i=0;i< arr.length;i++){
            System.out.print(arr[i]+"\t");
        }
    }
}

