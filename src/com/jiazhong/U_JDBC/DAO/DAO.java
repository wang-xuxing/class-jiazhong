package com.jiazhong.U_JDBC.DAO;

import java.sql.Connection;
import java.sql.DriverManager;

public class DAO {
    private Connection dbConn = null;

    public Connection getDbConn() throws Exception {
        //1.加载驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        System.out.println("加载驱动成功！");
        //2.连接
        dbConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=true&serverTimezone=UTC", "root", "root");//连接对象
        System.out.println("连接数据库成功！");
        return dbConn;
    }
}
