package com.jiazhong.U_JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectionTest {
    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false","root","root");
        if (conn!=null){
            System.out.println("连接成功!");
        }
        else {
            System.out.println("连接失败");
        }
//        String sql="select *from dept ";
//        Statement statement= dbConn.prepareStatement(sql);
//        ResultSet resultSet=statement.executeQuery(sql);
//        System.out.println(resultSet.getString("DEPTNO"));
    }
}
