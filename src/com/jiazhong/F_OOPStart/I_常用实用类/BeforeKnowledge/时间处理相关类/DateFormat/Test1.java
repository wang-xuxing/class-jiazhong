package com.jiazhong.F_OOPStart.I_常用实用类.BeforeKnowledge.时间处理相关类.DateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
public class Test1 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat format2=new SimpleDateFormat("yyyy-MM-dd");
        String s1=format1.format(new Date());
        System.out.println(s1);
        s1=format2.format(new Date());
        System.out.println(s1);
        Date d1=format1.parse("2002-05-02 12:00:00");
        System.out.println(d1.getTime());
        Date d2=new Date();
        String time1=format2.format(d2);
        System.out.println(time1);
        DateFormat format=new SimpleDateFormat("d");
        System.out.println(format.format(new Date()));
    }
}

