package com.jiazhong.F_OOPStart.I_常用实用类.BeforeKnowledge.String类相关类.String类;
public class Demo01 {
    public static void main(String[] args) {
        String str="aAbcDaaaab";
        String str1="    我要 努力.tx t.java";
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        int str7;
        int str8;
        str2=str.toLowerCase();
        str3=str.toUpperCase();
        System.out.println(str2+" "+str3);
        str4=str.replace("a","b");
        System.out.println(str4);
        str5=str1.substring(2);
        str6=str1.substring(0,2);
        str7=str1.indexOf("我");
        str8=str1.lastIndexOf(".");
        String[] str9=str1.split("\\.");
        System.out.println(str5+" "+str6+" "+str7+" "+str8);
        for (int i=0;i<str9.length;i++){
            System.out.print(str9[i]+" ");
        }
        System.out.println();
        boolean str10=str1.startsWith("我");
        boolean str11=str1.startsWith("sad");
        boolean str12=str1.endsWith("java");
        System.out.println(str10+" "+str11+" "+str12);
        String str13=str1.trim();
        System.out.println(str13);
    }
}
