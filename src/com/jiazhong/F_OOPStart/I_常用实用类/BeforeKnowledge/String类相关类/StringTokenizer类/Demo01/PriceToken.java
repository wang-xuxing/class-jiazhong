package com.jiazhong.F_OOPStart.I_常用实用类.BeforeKnowledge.String类相关类.StringTokenizer类.Demo01;

import java.util.StringTokenizer;

public class PriceToken {
    public void Price(String Price){
        String regex="[^0123456789.]+";
        String newPrice=Price.replaceAll(regex,"#");
        StringTokenizer stringTokenizer=new StringTokenizer(newPrice,"#");
        int count=stringTokenizer.countTokens();
        double sum=0;
        while (stringTokenizer.hasMoreTokens()){
            String item=stringTokenizer.nextToken();
            sum=sum+Double.valueOf(item);

        }

        double ave=sum/count;
        System.out.printf("购物总价格：%7.2f",sum);
        System.out.printf("\n"+"商品数目:%d,平均价格为:%7.2f",count,ave);
    }

}
