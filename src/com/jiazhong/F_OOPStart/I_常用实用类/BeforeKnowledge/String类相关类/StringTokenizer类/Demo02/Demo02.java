package com.jiazhong.F_OOPStart.I_常用实用类.BeforeKnowledge.String类相关类.StringTokenizer类.Demo02;

import java.util.StringTokenizer;
//1.使用StringTokenizer类的实例解析字符串："数学87分，物理76分，英语96分"中的考试成绩，并计算出总成绩以及平均分数。
public class Demo02 {
    public static void main(String[] args) {
        String scoreType="数学87分，物理76分，英语96分";
        Price(scoreType);
    }
    static void Price(String scoreType){
        String newScoreType=scoreType.replaceAll("[^012345679.]+","#");
        StringTokenizer stringtokenizer=new StringTokenizer(newScoreType,"#");
        int count=stringtokenizer.countTokens();
        double sum=0;
        while (stringtokenizer.hasMoreTokens()){
            double score=Double.valueOf(stringtokenizer.nextToken());
            sum=sum+score;
        }
        double ave=sum/count;
        System.out.printf("总成绩：%.2f,平均分数：%.2f",sum,ave);
    }
}
