package com.jiazhong.F_OOPStart.I_常用实用类.BeforeKnowledge.String类相关类.StringBuffer类;

public class Demo01 {
    public static void main(String[] args) {
        long num1=Runtime.getRuntime().freeMemory();
        long num2=System.currentTimeMillis();
        String str="";
        for (int i=0;i<5000;i++ ){
            str=str+i;
        }
        StringBuilder stringBuilder=new StringBuilder("我是大帅哥");
        String str0=String.valueOf(stringBuilder);
        System.out.println(stringBuilder.hashCode()+" "+str0);
        String str1= String.valueOf(stringBuilder.append("123"));
        System.out.println(stringBuilder+" "+str1);
        StringBuffer stringBuffer=new StringBuffer("她是大美女");
        stringBuffer.delete(0,2);
        System.out.println(stringBuffer);
        stringBuffer.deleteCharAt(0);
        System.out.println(stringBuffer);
        stringBuffer.reverse();
        System.out.println(stringBuffer);
        stringBuffer.insert(0,"他是");
        System.out.println(stringBuffer);
        long num11=Runtime.getRuntime().freeMemory();
        long num22=System.currentTimeMillis();
        System.out.println("占用内存"+(num1-num11));
        System.out.println("运行时间"+(num22-num2));
        long Num1=Runtime.getRuntime().freeMemory();
        long Time1=System.currentTimeMillis();
        for (int i=0;i<5000;i++ ){
            stringBuffer.append(i);
        }
        long Num2=Runtime.getRuntime().freeMemory();
        long Time2=System.currentTimeMillis();
        System.out.println("占用内存"+(Num1-Num2));
        System.out.println("占用时间"+(Time2-Time1));
        System.out.println(System.currentTimeMillis());
    }
}
