package com.jiazhong.F_OOPStart.I_常用实用类.BeforeKnowledge.Test;

import java.util.StringTokenizer;

public class Test5 {
    public static void main(String[] args) {
        String str = "My Name Is Wxx";
        GetToken getToken = new GetToken();
        String s1 = getToken.getToken(2, str);
        String s2 = getToken.getToken(4, str);
        System.out.println(s1 + " " +s2);

    }
}

class GetToken {
    String s[];

    public String getToken(int index, String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        int num = stringTokenizer.countTokens();
        s = new String[num + 1];
        int k = 1;
        while (stringTokenizer.hasMoreTokens()) {
            String temp = stringTokenizer.nextToken();
            s[k] = temp;
            k++;
        }
        if (index <= num) {
            return s[index];
        } else {
            return null;
        }
    }
}