package com.jiazhong.F_OOPStart.ClassTest1;

public class Test {
    public static void main(String[] args) {
        CPU cpu=new CPU();
        cpu.setSpeed(2000);
        HardDisk hardDisk=new HardDisk();
        hardDisk.setAmount(200);
        PC pc=new PC();
        pc.setCpu(cpu);
        pc.setHardDisk(hardDisk);
        pc.show();
    }
}
