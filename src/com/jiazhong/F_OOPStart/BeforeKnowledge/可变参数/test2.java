package com.jiazhong.F_OOPStart.BeforeKnowledge.可变参数;
//有三个方法，分别实现返回姓名和两门课成绩（总分），返回姓名和三门课成绩（总分），返回姓名和五门课成绩（总分）。
//        封装成一个可变参数的方法 类名 HspMethod 方法名 showScore
public class test2 {
    public static void main(String[] args) {
        HspMethod hspMethod=new HspMethod();
        System.out.println(hspMethod.showScore("张三",+96.3,89.2));
        System.out.println(hspMethod.showScore("马开",+76.2,96.5,76.3));
        System.out.println(hspMethod.showScore("李思",+99.9,96.6,77.9,77.9,96));
    }
}
class HspMethod{
    String showScore(String name, double ...d){
        double sum=0;
        for (int i=0;i<d.length;i++){
            sum=sum+d[i];
        }
        return ("姓名"+name+"，"+d.length+"门课"+"总分"+sum);
    }
}