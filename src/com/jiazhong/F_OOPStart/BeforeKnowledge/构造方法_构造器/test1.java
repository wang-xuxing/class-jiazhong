package com.jiazhong.F_OOPStart.BeforeKnowledge.构造方法_构造器;
//基本语法
//     [修饰符] 方法名(形参列表){
//    方法体; }
//说明：
//1) 构造器的修饰符可以默认， 也可以是 public protected private
//2) 构造器没有返回值
//3) 方法名 和类名字必须一样
//4) 参数列表 和 成员方法一样的规则
//5) 构造器的调用, 由系统完成
//题:在创建人类的对象时，就直接指 定这个对象的年龄和姓名
public class test1 {
    public static void main(String[] args) {
     Person person=new Person("张三干",18);
        System.out.println(person.age);
        System.out.println(person.name);
    }
}
class Person{
    int age;
    String name;
    public Person(String Name,int Age){
          age=Age;
          name=Name;
    }
}