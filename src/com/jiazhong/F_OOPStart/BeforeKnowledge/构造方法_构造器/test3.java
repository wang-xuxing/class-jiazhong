package com.jiazhong.F_OOPStart.BeforeKnowledge.构造方法_构造器;
//在前面定义的 Person 类中添加两个构造器：
//        第一个无参构造器：利用构造器设置所有人的 age 属性初始值都为 18
//        第二个带 pName 和 pAge 两个参数的构造器：使得每次创建 Person 对象的同 时初始化对象的 age 属性值和 name 属性值。
//        分别使用不同的构造器，创建对 象
public class test3 {
    public static void main(String[] args) {
        Person0 person0=new Person0();
        System.out.println(person0.age+" "+person0.name);
        Person0 person01=new Person0("美丽",18);
        System.out.println(person01.age+" "+person01.name);
    }
}
class Person0{
    String name;
    int age;
    Person0(){
        age=18;
    }
    Person0(String pName,int pAge){
        age=pAge;
        name=pName;
    }

}