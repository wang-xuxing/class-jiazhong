package com.jiazhong.F_OOPStart.BeforeKnowledge.构造方法_构造器;
//一个类可以定义多个不同的构造器，即构造器重载
//        1.比如∶我们可以再给Person类定义一个构造器，用来创建对象的时候，只指定人名，不需要指定年龄
//        2.构造器名和类名要相同
//        3.构造器没有返回值
//        4.构造器是完成对象的初始化，并不是创建对象
//        5.在创建对象时，系统自动的调用该类的构造方法
//        6.如果程序员没有定义构造器，系统会自动给类生成一个默认无参构造器（也叫默认构造器），比如 Dog （）{，使用javap指令 反编译看看
//        7.一旦定义了自己的构造器，默认的构造器就覆盖了，就不能再使用默认的无参构造器，除非显式的定义一下，即∶ Dog（0{ 写（这点很重要）
public class test2 {
    public static void main(String[] args) {
        Person1 person1=new Person1("马开",1);
        System.out.println(person1);
        Person1 person2=new Person1("马开");
        System.out.println(person2);
        Dog dog=new Dog();
    }}
    class Dog { }
        //如果程序员没有定义构造器，系统会自动给类生成一个默认无参构造器(也叫默认构 造器)
        // 使用 javap 指令 反编译看看 /* 默认构造器 Dog() { } */ //一旦定义了自己的构造器,默认的构造器就覆盖了，就不能再使用默认的无参构造器，
        // 除非显式的定义一下,即: Dog(){} 写 (这点很重要) public Dog(String dName) { //... }Dog() { //显式的定义一下 无参构造器 } }
 class Person1 { String name;
     int age;//默认 0
            // 第 1 个构造器
          public Person1(String pName, int pAge) {
           name = pName;
           age = pAge; }
            //第 2 个构造器, 只指定人名，不需要指定年龄
            public Person1(String pName) {
              name = pName; }
}
