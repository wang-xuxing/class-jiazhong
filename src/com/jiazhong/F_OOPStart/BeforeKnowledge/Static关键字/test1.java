package com.jiazhong.F_OOPStart.BeforeKnowledge.Static关键字;

/**
 static修饰的成员变量和方法，从属于类。

 普通变量和方法从属于对象的。
 */
public class test1 {
    int id; // id
    String name; // 账户名
    String pwd; // 密码
    static String company = "北京尚学堂"; // 公司名称
    public test1(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void login() {
        printCompany();
        System.out.println(company);
        System.out.println("登录：" + name);
    }

    public static void printCompany() {
//         login();//调用非静态成员，编译就会报错
        System.out.println(company);
    }

    public static void main(String[] args) {
        test1 u = new test1(101, "高小七");
        test1.printCompany();
        test1.company = "北京阿里爷爷";
        test1.printCompany();
    }
}
