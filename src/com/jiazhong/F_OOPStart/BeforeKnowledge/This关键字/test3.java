package com.jiazhong.F_OOPStart.BeforeKnowledge.This关键字;

public class test3 {
    int a, b, c;

    test3() {
        System.out.println("正要初始化一个Hello对象");
    }
    test3(int a, int b) {
//        test3(); //这样是无法调用构造方法的！
        this(); // 调用无参的构造方法，并且必须位于第一行！
        a = a;// 这里都是指的局部变量而不是成员变量
// 这样就区分了成员变量和局部变量. 这种情况占了this使用情况大多数！
        this.a = a;
        this.b = b;
    }
    test3(int a, int b, int c) {
        this(a, b); // 调用带参的构造方法，并且必须位于第一行！
        this.c = c;
    }

    void sing() {
    }
    void eat() {
        this.sing(); // 调用本类中的sing();
        System.out.println("你妈妈喊你回家吃饭！");
    }

    public static void main(String[] args) {
        test3 hi = new test3(2, 3);
        hi.eat();
    }
}