package com.jiazhong.F_OOPStart.BeforeKnowledge.递归调用;

import java.util.Scanner;

public class test1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        T t=new T();
        //斐波纳吉数
        System.out.println("请输入一个大于1的整数：");
        int n=scanner.nextInt();
        int res=t.feibo(n);
        if (res != -1) { System.out.println("当 n=" + n + " 对应的斐波那契数=" + res); }

        //猴子摘桃
        System.out.println("请输入一个0-10之间的天数：");
        int day=scanner.nextInt();
//        int day=8;
        int peachNum=t.peachNum(day);
        if (peachNum!=-1){
            System.out.println("第"+day+"天剩余"+peachNum+"桃");
        }

    }
}
class T{
    /*请使用递归的方式求出斐波那契数 1,1,2,3,5,8,13...给你一个整数 n，求出它的值是多 思路分析
    1. 当 n = 1 斐波那契数 是 1
    2. 当 n = 2 斐波那契数 是 1
    3. 当 n >= 3 斐波那契数 是前两个数的和
    4. 这里就是一个递归的思路 */
    int feibo(int n) {
        if (n >= 1) {
            if (n == 1 || n == 2) {
                return 1;
            } else {
            return (feibo(n - 1) + feibo(n - 2));
            }
        } else {
            System.out.println("请输入n>1");
            return -1;
        }

    }
    /*猴子吃桃子问题：有一堆桃子，猴子第一天吃了其中的一半，并再多吃了一个！ 以后每天猴子都吃其中的一半，然后再多吃一个。
    当到第 10 天时， 想再吃时（即还没吃），发现只有 1 个桃子了。问题：最初共多少个桃子？
    10=1
    9=(1+1)*2=4
    8=(4+1)*2=10
     */

    public int peachNum(int day){
        if (day==10){
            return 1;}
        else if (day>=0&&day<=9){
            return (peachNum(day+1)+1)*2;
            }
        else{
            System.out.println("请输入0-10之间的天数");
            return -1;
        }
    }
}
