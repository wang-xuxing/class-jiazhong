package com.jiazhong.F_OOPStart.BeforeKnowledge.递归调用;

import java.util.Scanner;

// 汉诺塔传说 汉诺塔：汉诺塔（又称河内塔）问题是源于印度一个古老传说的益智玩具。
//大梵 天创造世界的时候做了三根金刚石柱子，在一根柱子上从下往上按照大小顺序摞 着 64 片圆盘。
//大梵天命令婆罗门把圆盘从下面开始按大小顺序重新摆放在另一 根柱子上。并且规定，在小圆盘上不能放大圆盘，在三根柱子之间一次只能移动 一个圆盘。
//假如每秒钟移动一次，共需多长时间呢？移完这些金片需要 5845.54 亿年以上， 太阳系的预期寿命据说也就是数百亿年。
//真的过了 5845.54 亿年，地球上的一 切生命，连同梵塔、庙宇等，都早已经灰飞烟灭
public class test3 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
       Tower tower=new Tower();
       tower.move(64, 'A','B','C');
    }
}
class Tower{
    void move(int num,char a,char b,char c){
        if (num==1){
            System.out.print(a+"->"+c+" ");
        }
        else {
            move(num-1,a,c,b);
            System.out.print(a+"->"+c+" ");
            move(num-1,b,a,c);
        }
    }
}