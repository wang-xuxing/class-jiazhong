package com.jiazhong.F_OOPStart.BeforeKnowledge.递归调用;

public class test2 {
    public static void main(String[] args) {
        int[][] map=new int[8][7];
        for (int i=0;i< map.length;i++){
            for (int j=0;j< map[i].length;j++){
                map[0][j]=1;
                map[7][j]=1;
                map[i][0]=1;
                map[i][6]=1;
                map[3][1]=1;
                map[3][2]=1;
                map[3][3]=1;
            }
        }
        System.out.println("=====遍历这个地图=====");
        for (int i=0;i< map.length;i++){
            for (int j=0;j< map[i].length;j++) {
                System.out.print(map[i][j]+" ");
            }
            System.out.println();
        }
        FindWay(map,1,1);
        System.out.println("====找路情况如下====");
        for (int i=0;i< map.length;i++){
            for (int j=0;j< map[i].length;j++) {
                System.out.print(map[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static boolean FindWay(int map[][], int i, int j){
        if (map[6][5]==2){
            return true;
        }
        else {
            if (map[i][j]==0){
                map[i][j]=2;
                if (FindWay(map,i+1,j)){
                    return true;
                }
                else if (FindWay(map,i,j-1)){
                    return true;
                }
                else if (FindWay(map,i-1,j)){
                    return true;
                }
                else if (FindWay(map,i,j+1)){
                    return true;
                }
                else {
                    map[i][j]=3;
                    return false;
                }
            }
            else {
                return false;
                }
            }
        }
    public static boolean FindWay1(int map[][], int i, int j){
        if (map[6][5]==2){
            return true;
        }
        else {
            if (map[i][j]==0){
                map[i][j]=2;
                if (FindWay1(map,i-1,j)){
                    return true;
                }
                else if (FindWay1(map,i,j+1)){
                    return true;
                }
                else if (FindWay1(map,i+1,j)){
                    return true;
                }
                else if (FindWay1(map,i,j-1)){
                    return true;
                }
                else {
                    map[i][j]=3;
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }
    }

