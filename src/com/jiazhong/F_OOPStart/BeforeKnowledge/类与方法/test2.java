package com.jiazhong.F_OOPStart.BeforeKnowledge.类与方法;
//看一个需求： 请遍历一个数组 , 输出数组的各个元素值。  解决思路
//        1，传统的方法，就是使用单个 for 循环，将数组输出，大家看看 问题是什么？  解决思路
//        2: 定义一个类 MyTools ,然后写一个成员方法，调用方法实现,看 看效果又如何
public class test2 {
    public static  void main(String[] args) {
        int[][] map1={{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
        int[][] map2={{1,2,3},{43,52,61},{71,82,93},{10,11,12}};
        MyTools tool = new MyTools();
           tool.map(map1);
           tool.map(map2);
    }
}

class MyTools{

    public void map(int[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                System.out.print(map[i][j]+" ");
            }
            System.out.println();
        }

    }
}

