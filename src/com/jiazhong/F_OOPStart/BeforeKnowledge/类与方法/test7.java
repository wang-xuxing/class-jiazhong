package com.jiazhong.F_OOPStart.BeforeKnowledge.类与方法;
//B 类中编写一个方法 test100，可以接收一个数组，在方法中修改该数组，看看 原来的数组是否变化？会变化
//B 类中编写一个方法 test200，可以接收一个 Person(age,sal)对象，在方法中修 改该对象属性，看看原来的对象是否变化？会变化
public class test7 {
    public static void main(String[] args) {
        B b = new B();
        int[] arr = {1, 2, 3, 4, 5};
        b.test100(arr);
        for (int k = 0; k < arr.length;k++){
            System.out.print(arr[k]+" ");

        }
        Person1 person1 = new Person1();
        person1.age = 3;
        person1.sal = "马凯";
        Person1 person2 = b.test200(person1);
        System.out.println();
        System.out.println(person1.sal + person1.age);
        System.out.println(person2.sal + person2.age);
    }
}
class B{
    void test100(int[] arr){
        arr[1]=100;
        for (int i=0;i<arr.length;i++){
//            System.out.print(arr[i]+" ");
        }
    }
    Person1 test200(Person1 person1){
        Person1 person2=new Person1();
        person1.sal="小花";
        person2.sal= "小黄";
        person2.age=17;
return person2;
    }
}
class Person1 {
    int age;
    String sal;

}