package com.jiazhong.F_OOPStart.BeforeKnowledge.类与方法;

import java.util.Scanner;

//张老太养了两只猫猫:一只名字叫小白,今年 3 岁,白色。还有一只叫小花,今年 100 岁,花色。
//        请编写一个程序，当用户输入小猫的名字时，就显示该猫的名字， 年龄，颜色。如果用户输入的小猫名错误，则显示 张老太没有这只猫猫。
public class test1 {
    public static void main(String[] args) {
          Demo2();
    }

    public static void Demo1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入张老太太猫猫的名字：");
        String name = scanner.next();
        if (name.equals("小白")) {
            System.out.println("小白,今年 3 岁,白色");
        } else if (name.equals("小花")) {
            System.out.println("小花,今年 100 岁,花色");
        } else {
            System.out.println("没有这只猫");
        }
    }
    public static void Demo2(){
        String[] cat1 = {"小白", "3", "白色"};
        String[] cat2 = {"小花", "100", "花色"};
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入张老太太猫猫的名字：");
        String name = scanner.next();
        for(int i=0;i< cat1.length;i++){
            if (name.equals("小花")){
                System.out.println(cat2[i]);
            }
            else if (name.equals("小白")){
                System.out.println(cat1[i]);
            }
            else {
                System.out.println("没有这只小猫");break;
            }
        }
    }

    }
