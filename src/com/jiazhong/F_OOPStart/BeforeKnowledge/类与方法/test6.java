package com.jiazhong.F_OOPStart.BeforeKnowledge.类与方法;
//1）编写类 MyTools 类，编写一个方法可以打印二维数组的数据。
//2）编写一个方法 copyPerson，可以复制一个 Person 对象，返回复制的对象。克隆对象， 注意要求得到新对象和原来的对象是两个独立的对象，只是他们的属性相同
public class test6 {
    public static void main(String[] args) {
       MyTool myTool=new MyTool();
       int[][] arr={{1,2},{1,2},{1,2}};
       myTool.printArr2(arr);
       Person person=new Person();
       person.name="马凯";
       person.age=3;
        System.out.println("名字"+person.name+"年龄"+person.age);
        Person person1=myTool.copyPerson(person);
        System.out.println("名字"+person1.name+"年龄"+person1.age);
        System.out.println(person==person1);
    }
}
class MyTool{
    void printArr2(int[][]arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
    Person copyPerson(Person person){
        Person person1=new Person();
        person1.name=person.name;
        person1.age=person.age;

        return person1;
    }
}
class Person {
        String name;
        int age; }



