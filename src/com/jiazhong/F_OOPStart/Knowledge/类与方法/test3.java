package com.jiazhong.F_OOPStart.Knowledge.类与方法;
/**
 * 1.如何创建对象
 * 先声明在创建
 * Cat cat;声明对象
 * cat=new Cat();创建对象
 * 直接创建
 * Cat cat1=new Cat();
 * <p>
 * 如何访问属性
 * 基本语法
 * 对象名.属性名
 * cat.name;
 * cat.age;
 * cat.color;
 * <p>
 * 类和对象的内存分配机制
 * java结构的内存分析
 * 1.栈：一般存放基本数据类型（局部变量）
 * 2.堆：存放对象（Cat cat,数组）
 * 3.方法区：常量池（常量，比如字符串），类加载信息
 *
 *  Java创建对象的流程
 *  Person1 p1 = new Person1();
 *  p1.name = "小明";
 *  p1.age = 10;
 *  1.先加载Person类的信息（属性和方法信息，只会加载一次）
 *  2.在堆中分配空间，进行默认初始化
 *  3.把地址赋给了p1，p1就指向了对象
 *  4.进行指定初始化，比如 p1.age = 10; p1.name = "小明";
 *
 */
public class test3 {
    public static void main(String[] args) {
        Person1 p1 = new Person1();
        p1.age = 10;
        p1.name = "小明";
        Person1 p2 = p1;//把p1赋给了p2,也就是让p2指向p1
        System.out.println(p2.age);
        System.out.println(p1);
        System.out.println(p2);
    }
}

class Person1 {
    //属性
    int age;
    String name;
}