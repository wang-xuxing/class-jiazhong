package com.jiazhong.F_OOPStart.Knowledge.类与方法;
/**
 * 属性/成员变量/字段
 * <p>
 * 基本介绍：
 * 1.从概念或者叫法上看 成员变量=属性=field(字段) 成员变量用来表示属性的，统一叫属性
 * 2.属性:类的一个组成部分，一般时基本数据类型，也可以是引用数据类型(对象，数组)比如我们前面定义这个猫类的 int age 就是属性
 * <p>
 * <p>
 * 注意事项和细节说明
 * 1.属性的定义语法同变量示例：访问修饰符 属性类型 属性名；
 * 顺便来说一下访问修饰符：控制属性的访问范围
 * 有四种访问修饰符 public protected 默认 private 后面会详细的介绍
 * 2.属性的定义类型可以为任意类型，包括基本数据类型和引用类型
 * 3.属性如果不赋值，有默认值，规则和数组一样
 */
public class test2 {
    public static void main(String[] args) {
        //创建一个person类的对象，p1是引用名（对象引用） new person 创建的对象空间，才是真正的对象
        Person p1 = new Person();
        //对象的属性默认值遵循数组的规则
        System.out.println("\n这个人的信息");
        System.out.println("age" + p1.age + "name" + p1.name + "sal" + p1.sal + "pass" + p1.pass);

    }
}

class  Person {
    //四个属性
    int age;
    String name;
    double sal;
    boolean pass;
}
