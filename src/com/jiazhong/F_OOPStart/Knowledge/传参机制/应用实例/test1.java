package com.jiazhong.F_OOPStart.Knowledge.传参机制.应用实例;

//1）编写类 MyTools 类，编写一个方法可以打印二维数组的数据。
//2）编写一个方法 copyPerson，可以复制一个 Person 对象，返回复制的对象。克隆对象， 注意要求得到新对象和原来的对象是两个独立的对象，只是他们的属性相同
public class test1 {
    public static void main(String[] args) {
      int arr[][]={{1,2},{1,12},{1,2}};
      int arr1[][]={{11,2},{11,12},{1,2}};
       Person person1=new Person();
       person1.age=18;
      person1.name="马开";
      MyTools myTools=new MyTools();
      Person person=myTools.copyPerson(person1);
      person.name="沾上干";
      person.age=16;
        System.out.println(person1.age+person1.name);
        System.out.println(person.age+person.name);
        Person person2= myTools.copyPerson(person);
        person2.name="kdj";
        person2.age=16;
        System.out.println(person2.age+person2.name);
        myTools.arr(arr);
        System.out.println("==================");
        myTools.arr(arr1);
    }
}
class Person{
    String name;
    int age;
}
class MyTools{
    void arr(int arr[][]){
        for (int i=0; i<arr.length;i++){
            for (int j = 0; j<arr[i].length; j++){
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }
    }
    Person copyPerson(Person person1){
        Person person=new Person();
        person.name=person1.name;
        person.age= person1.age;
        Person person2=new Person();
        person2.name=person1.name;
        person2.age= person1.age;
        return person;
    }
}