package com.jiazhong.F_OOPStart.Knowledge.传参机制;
//基本数据类型传参机制
public class test2 {
    public static void main(String[] args) {
        int a=10;
        int b=20;
        B b1=new B();
        b1.i(a,b);
        System.out.println(a+" "+b);
    }
}
class B{
    void i(int a,int b){
        System.out.println(a+" "+b);
        int temp=0;
        temp=a;
        a=b;
        b=temp;
        System.out.println(a+" "+b);
    }
}