package com.jiazhong.F_OOPStart.Knowledge.传参机制;
//引用型传参机制
public class test3 {
    public static void main(String[] args) {
    C c =new C();
    c.name="张红";
    c.age=16;
    c.duixiang(c);
        System.out.println("姓名"+c.name+"年龄"+c.age);
    }
}
class C{
    String name="";
    int age;
    void duixiang(C c){
        c.age=13;
        c.age=0;
        c.name="刘德华";
        c.name=null;
//        c=new C();
//        c.name="刘德华";
//        c.age=18;
    }
}
