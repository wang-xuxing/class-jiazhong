package com.jiazhong.F_OOPStart.Knowledge.可变参数.练习题;
//有三个方法，分别实现返回姓名和两门课成绩(总分)，返回姓名和三门课成绩(总分)，返回姓名和五门课成绩(总分)。封装成一个可变参数的方法
public class test1 {
    public static void main(String[] args) {
        way1("张三",96.6,72.3);
        way2("李思",88.9,77.9,66.9);
        way3("王武",99.6,76.7,88,96.6,77.2);
    }
    static void way1(String name,double d1,double d2){
        System.out.println( "姓名"+name+"，2门课"+"总分"+(d1+d2));
    }
    static void way2(String name,double d1,double d2,double d3){
        System.out.println( "姓名"+name+"，3门课"+"总分"+(d1+d2+d3));
    }
    static void way3(String name, double...d){
        double sum=0;
        for (int i=0;i<d.length;i++){
            sum=sum+d[i];
        }
        System.out.println( ("姓名"+name+"，"+d.length+"门课"+"总分"+sum));
    }
}
