package com.jiazhong.F_OOPStart.Knowledge.成员方法;
/**
 * 注意事项和使用细节
 */
public class test3 {
    public static void main(String[] args) {
        AA a=new AA();
        //细节：实参和形参的类型要一致或者兼容，个数，顺序必须一致
        a.f3("tom",45);//ok
        //a.f3(12, "34"); 实际参数和形式参数顺序不对

        int[]  res=a.getSumAndSub(1,4);
        System.out.println("和为"+res[0]);
        System.out.println("和为"+res[1]);
        a.f1();
    }
}
class AA{
    //细节：方法不能嵌套定义
    public void f4(){
//        public void f5(){
//
//        }
    }
    public void f3(String str,int n){

    }
    //一个方法最多只能有一个返回值，如何返回多个结果 返回数组
    public int []getSumAndSub(int n1,int n2){
        int [] resArr=new int[2];
        resArr[0]=n1+n2;
        resArr[1]=n1-n2;
        return resArr;
    }
    //返回类型可以为任意类型，包括基本数据类型或者引用类型（数组，对象）
    //如果这个方法要求有返回类型，则方法中最后的执行语句必须为return值，而且要求返回的值类型必须和return的值类型一致或者兼容
    public double f1(){
        double d1=1.1*3;
        char n='A';
        System.out.println(n);
        return n;
    }

    //如果方法是void，则方法体重可以没有return语句，或者只写return
    //在实际工作中，我们的方法都是为了完成某个功能，所以方法名要有一定的含义，最好是见名思意
    public void f2(){

    }
}