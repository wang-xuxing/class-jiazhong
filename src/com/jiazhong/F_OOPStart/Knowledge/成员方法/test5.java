package com.jiazhong.F_OOPStart.Knowledge.成员方法;
/*
1) 编写类 AA ，有一个方法：判断一个数是奇数 odd 还是偶数, 返回 boolean
2) 根据行、列、字符打印 对应行数和列数的字符，比如：行：4，列：4，字符
*/
public class test5 {
    public static void main(String[] args) {
        AAA aaa=new AAA();
        aaa.isodd(5);
        aaa.arr(4,5,"-");
    }
}
class AAA{
     boolean isodd(int a) {
        if (a%2==0){
            System.out.println("偶数");
            return true;
    }
        else {
            System.out.println("奇数");
            return false;
        }
    }
   void arr(int k,int m,String str){
         for (int i=0;i<k;i++){
             for (int j=0;j<m;j++){
                 System.out.print(str);
             }
             System.out.println();
         }
   }
}