package com.jiazhong.F_OOPStart.Knowledge.成员方法;

public class test6 {
    public static void main(String[] args) {
        //方法的使用
        //1.方法写好之后，如果不去调用，不会输出语句
        //2.先创建对象，然后再调用方法
        Person1 p1 = new Person1();
        p1.cal01();
        //调用cal02方法，并且传了一个值2
        p1.cal02(3);
        //调用getSum方法，给num1赋了一个值7，给num2赋了一个值为8，把方法getSum返回的值，赋给变量returnRes
        int returnRse = p1.getSum(7, 8);
        System.out.println("getsum方法返回的值为" + returnRse);
    }
}

class Person1 {
    //添加cal01方法，可以计算1+1000的结果
    public void cal01() {
        //循环完成
        int res = 0;
        for (int i = 1; i <= 1000; i++) {
            res += i;
        }
        System.out.println("计算结果为" + res);
    }

    //添加cal02方法，该方法可以接受一个数组n，计算从1+n的结果
    //1.（int n）形参列表，表示当前有一个形参n，可以接收用户的输入
    public void cal02(int n) {
        int res = 0;
        for (int i = 1; i <= n; i++) {
            res += i;
        }
        System.out.println("cal02方法的计算结果为" + res);
    }

    //添加一个getSum成员方法，可以计算两个结果的和
    /*
    1.public 表示方法是公开的
    2.int 表示方法执行之后，可以返回一个int 值
    3.getSum 方法名
    4.（int num1,int num2）形参列表，2个形参，可以接收用户传入的两个数
    5.return res;表示把res的值，返回
     */
    public int getSum(int num1, int num2) {
        int res = num1 + num2;
        return res;
    }

}
