package com.jiazhong.F_OOPStart.Knowledge.成员方法;
/**
 *基本介绍：
 * 在某些情况下，我们需要定义 成员方法（简称方法）比如人类除了有一些属性，还有一些行为，需要成员方法才能完成
 */
public class test1 {
    public static void main(String[] args) {
        //方法的使用
        //1.方法写好之后，如果不去调用，不会输出语句
        //2.先创建对象，然后再调用方法
        Person person=new Person();
        person.speak();
    }
}
class Person{
    //方法(成员方法)
    //添加一个speak方法，输出我是一个好人
    //1.public 表示方法是公开的
    //2.void 表示方法没有返回值
    //3.speak();speak是方法名，()形参列表
    //4.{}方法体，可以要写我们要执行的代码
    //5.sout("我是一个好人");表示我们的方法就是输出一句话
    public void speak(){
        System.out.println("我是一个好人");
    }
}