package com.jiazhong.F_OOPStart.Knowledge.成员方法;

public class test4 {
    public static void main(String[] args) {
        /*
        方法的调用细节
        1.同一个类中的方法，直接调用
         */
        A a = new A();
        a.m1();
        a.print1(2);
        a.sayOk();
    }
}

class A {
    // 1.同一个类中的方法，直接调用
    public void print1(int n) {
        System.out.println("print1方法被调用" + n);
    }

    //
    public void sayOk() {//调用print1直接调用
        print1(10);
        System.out.println("继续执行 sayOk方法");
    }

    //跨类中的方法A类调用B类，需要通过对象名调用
    public void m1() {
        //创建一个B对象，然后在调用方法即可
        System.out.println("m1方法被调用");
        B b = new B();
        b.hi();
        System.out.println("m1 继续执行");
    }
}

class B {
    public void hi() {
        System.out.println("B类中的hi方法被执行");
    }
}