package com.jiazhong.F_OOPStart.Knowledge.成员方法;

/**
 * 为什么需要成员方法
 * 看一个需求
 * 遍历一个数组，输出数组的各个元素值
 * 1.传统的方法，就是使用单个for循环，将数组输出
 * 2.定义一个类MyTools，然后 写一个成员方法，调用方法实现，看看效果
 *
 * 成员方法的好处
 * 1.提高了代码的复用性
 * 2.可以将实现的细节封装起来，然后供其他的用户来调用
 *
 * 成员方法的定义
 * 访问修饰符 返回数据类型 方法名 （形参列表）{
 *     方法体
 *     语句
 *     return 返回值；
 * }
 * 1.形参列表：表示成员方法输入cal(int n),getSum (int num1,int num2)
 * 2.返回数据类型：表示成员方法输出，void没有返回值的
 * 3.方法主题：表示为了实现某一功能代码块
 * 4.return语句不是必须的
 */
public class test2 {
    public static void main(String[] args) {
        int[][] arr={{1,2,3,6},{1,2,3,6},{1,2,3,6},{1,2,3,6}};
        Mytools mytools=new Mytools();
        mytools.Mytools(arr);
    }
}
class Mytools{
     void Mytools(int[][] arr){
        for (int i=0;i<arr.length;i++){
            for (int j=0;j<arr[i].length;j++){
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }

    }
}