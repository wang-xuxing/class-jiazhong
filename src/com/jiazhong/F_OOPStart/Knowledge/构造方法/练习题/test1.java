package com.jiazhong.F_OOPStart.Knowledge.构造方法.练习题;
//在前面定义的Person类中添加两个构造器:
//        第一个无参构造器:利用构造器设置所有人的age 属性初始值都为18
//        第二个带 pName和pAge两个参数的构造器:使得每次创建Person对象的同时初始化对象的age属性值和 name属性值。
//        分别使用不同的构造器，创建对象.
public class test1 {
    public static void main(String[] args) {
     Person person=new Person();
        System.out.println(person.name+" "+ person.age);
        Person person1=new Person("王华华",18);
        System.out.println(person1.name+" "+ person1.age);
    }
}
class Person{
    String  name;
    int  age;
     Person(){
         age=18;
     }
     Person(String pName,int pAge){
         name=pName;
         age=pAge;
     }
}