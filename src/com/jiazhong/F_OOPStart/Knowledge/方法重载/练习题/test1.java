package com.jiazhong.F_OOPStart.Knowledge.方法重载.练习题;
//1．编写程序，类Methods中定义三个重载方法并调用。方法名为m。三个方法分
//        别接收一个int参数、两个int参数、一个字符串参数。分别执行平方运算并输出结果，相乘并输出结果，输出字符串信息。
//        在主类的main ()方法中分别用参数区别调用三个方法。OverLoadExercise.java
//2.在Methods类，定义三个重载方法max()，第一个方法，返回两个int值中的最
//        大值，第二个方法，返回两个double值中的最大值，第三个方法，返回三个double值中的最大值,并分别调用三个方法。
public class test1 {
    public static void main(String[] args) {
        Methods methods=new Methods();
        System.out.println(methods.m(10));
        System.out.println(methods.m(20,10));
        System.out.println(methods.m("张三"));
        System.out.println("max"+methods.max(10,20));
        System.out.println("max"+methods.max(20.5,20.35));
        System.out.println("max"+methods.max(20.66,20.77,1086.11));
    }

}
class Methods{
    int max(int i,int j){
        if (i>j){
            return i;
        }
        else {
            return j;
        }
    }
    double max(double d1,double d2){
        if (d1>d2){
            return d1;
        }
        else {
            return d2;
        }
    }
    double max(double d1,double d2,double d3){
        if (d1>d2&&d1>d3){
            return d1;
        }
        else if (d2>d3){
            return d2;
        }
        else {
            return d3;
        }
    }
    int m(int i){
        return i*i;
    };
    int m(int i,int j){
        return i*j;
    };
    String m(String s){
        return s;
    };
}