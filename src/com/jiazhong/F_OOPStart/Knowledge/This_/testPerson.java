package com.jiazhong.F_OOPStart.Knowledge.This_;
//定义Person类，里面有name、age属性，并提供 compareTo 比较方法，用于判断是否和另一个人相等，提供测试类TestPerson
//用于测试，名字和年龄完全一样，就返回true，否则返回false
public class testPerson {
    public static void main(String[] args) {
        Person person=new Person("小王",21);
        System.out.println(person.compareTo("小黄",21));
    }
}
