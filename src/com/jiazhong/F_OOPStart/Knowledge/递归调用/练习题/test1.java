package com.jiazhong.F_OOPStart.Knowledge.递归调用.练习题;
import java.util.Scanner;
//斐波纳吉数
public class test1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入一个整数n：");
        int n= scanner.nextInt();
        if (fbnj(n)!=-1){
        System.out.println("斐波纳吉数为"+fbnj(n));
    }}
    static int fbnj(int n) {
        if (n > 1) {
            if (n <= 2) {
                return 1;
            } else {
                return fbnj(n - 1) + fbnj(n - 2);
            }
        }
            return -1;
    }

}
