package com.jiazhong.F_OOPStart.Knowledge.递归调用.练习题;

import java.util.Scanner;

//猴子吃桃
//猴子吃桃子问题：有一堆桃子，猴子第一天吃了其中的一半，并再多吃了一个！ 以后每天猴子都吃其中的一半，然后再多吃一个。
//        当到第 10 天时， 想再吃时（即还没吃），发现只有 1 个桃子了。问题：？
//      f(x+1)=f(x)/2-1
public class test2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("请输入0-10之间的数字:");
            int n = scanner.nextInt();
            if (n >= 0 && n <= 10) {
                System.out.println("第" + n + "天桃子数量：" + f(n));break;
            } else {
                System.out.println("您的数字输入错误，请输入0-10之间的数字！");continue;

            }
        }
    }
    static int f(int x) {
        if (x >= 0) {
            if (x == 10) {
                return 1;
            } else {
                return (f(x + 1) + 1) * 2;
            }
        }

        return 0;
    }
}