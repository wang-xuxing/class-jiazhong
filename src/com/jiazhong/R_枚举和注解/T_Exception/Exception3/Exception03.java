package com.jiazhong.R_枚举和注解.T_Exception.Exception3;

import java.util.Scanner;

public class Exception03 {
    public static void main(String[] args) throws AgeException{
        Scanner scanner=new Scanner(System.in);
        try{System.out.println("请输入你的年龄：");
            int age=scanner.nextInt();
            if (age<0||age>120){
                throw new AgeException("你的年龄输入错误,年龄在0-120之间");
            }
            else {
                System.out.println("你的年龄："+age);
            }
        }
        catch (AgeException ageException){
            System.out.println(ageException.getMessage());
        }

    }
}
