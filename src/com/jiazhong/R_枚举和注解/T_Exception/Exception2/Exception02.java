package com.jiazhong.R_枚举和注解.T_Exception.Exception2;

import java.util.Scanner;

//如果用户输入的不是一个整数，就提示他反复输入，直到输入一个整数为止
public class Exception02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        String inputStr = "";
        System.out.println("请您输入一个整数：");
        while (true) {
            try {
                inputStr = scanner.next();
                num = Integer.parseInt(inputStr);
            } catch (Exception e) {
                System.out.println("您输入的不是一个整数，请您继续输入:");
                continue;
            }
            System.out.println("您输入的数字是：" + num);
            break;
        }
    }
}

