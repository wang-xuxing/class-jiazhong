package com.jiazhong.R_枚举和注解.枚举;

public class Test03 {
    public static void main(String[] args) {
//        System.out.println(Season.valueOf("春天"));
        System.out.println(Season.SUMMER);
        System.out.println(Season.SUMMER.ordinal());//返回枚举索引
        System.out.println(Season.valueOf("AUTUMN"));//按照枚举名称找枚举定义
        Season[] season=Season.values();//遍历所有枚举定义
        for (Season season1:season){
            System.out.println(season1);
        }
        int[] num={1,2,3,4,5};
        for (int i:num){
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.println(Season.SUMMER.compareTo(Season.LOVE));
        System.out.println(Season.SUMMER.equals("SUMMER"));
        System.out.println(Season.SUMMER.hashCode());
    }
}
enum Season{
    SPRING("春天"),SUMMER("夏天"),AUTUMN("秋天"),WINTER("冬天"),LOVE;
    ;
    String name;
    Season(){}
    Season(String season) {
        this.name=season;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name ;

    }
}