package com.jiazhong.R_枚举和注解.枚举;

public class Test02 {
    public static void main(String[] args) {
     Enum.SAY.say();
        System.out.println(Enum.SAY);
        Enum e=Enum.valueOf("SAY");
        System.out.println(e);
        Enum[] e1=Enum.values();
        for (Enum e2:e1){
            System.out.println(e2);
        }
        int[] iArr=new int[5];
        for (int i:iArr){
            System.out.print(i+" ");
        }
        System.out.println();
        int[] num=new int[5];
        for (int i:num){
            i=(int)(Math.random()*(101-20)+20);
            System.out.print(i+" ");
        }
    }
}
interface face{
    void say();
}
enum Enum implements face{
    SAY("你属猪吗");
   String str;
    Enum(String str) {this.str=str;
    }

    @Override
    public void say() {
        System.out.println("你是猪吗");
    }

    @Override
    public String toString() {
        return str ;
    }
}