package com.jiazhong.R_枚举和注解.枚举;
//1．声明week枚举类，其中包含星期一至星期天的含义，
//MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY，SUNDAY
//2．使用values返回所有枚举数组，并遍历，输出效果

public class Test01 {
    public static void main(String[] args) {
        week[] week1=week.values();
        for (int i=0;i<week1.length;i++){
            System.out.println(week1[i]);
        }
        for (week week:week1){
            System.out.println(week);
        }
        week week2=week.valueOf("MONDAY");
        System.out.println(week2);
        System.out.println(week2.ordinal());//当前枚举次序
        System.out.println(week2.compareTo(week.FRIDAY));
//        System.out.println(week2.clone);
    }
}
enum week{
    MONDAY("星期一"),
    TUESDAY("星期二"),
    WEDNESDAY("星期三"),
    THURSDAY("星期四"),
    FRIDAY("星期五"),
    SATURDAY("星期六"),
    SUNDAY("星期天");
    String name;
    week(String name) {
        this.name=name;
    }

    @Override
    public String toString() {
        return name;
    }
}