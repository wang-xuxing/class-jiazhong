package com.jiazhong.R_枚举和注解.注解;

public class Test01 {
    public static void main(String[] args) {
        AA.love.A1();
        System.out.println(AA.love.name);
        System.out.println(AA.love.love());
        AA.AAA aaa=new AA.AAA();
        BB.BBB bbb= new BB().new BBB();
        AA[] bbb1=AA.values();
        for (AA aa:bbb1){
            System.out.println(aa);
        }
    }
}

interface A {//定义一个A类

    void A1();

    default void A2() {
        System.out.println("我是接口中的实力方法");
    }
}
@SuppressWarnings({"ASD"})
 class BB{
    @Deprecated
    class BBB{


    }
}
enum AA implements A {
    love;

    @Override//重写注解
    public void A1() {
        System.out.println("我是重写的A1方法");
    }

    @Deprecated//表示某个元素（类，变量，方法）已经过时
    String name = "love";

    @Deprecated
    String love() {
        return name;
    }
    @Deprecated
    class BBB{}
    @Deprecated
    static
    class AAA {
    }

}