package com.jiazhong.B_变量及运算符.test;
//3.	定义一个int类型变量a,变量b,都赋值为20.
//4.	定义boolean类型变量bo , 判断++a 是否被3整除,并且a++ 是否被7整除,将结果赋值给bo
//5.	输出a的值,bo的值.
//6.	定义boolean类型变量bo2 , 判断b++ 是否被3整除,并且++b 是否被7整除,将结果赋值给bo2
//7.	输出b的值,bo2的值.

public class test4 {
    public static void main(String[] args) {
        int a=20;
        int b=20;
        boolean bo=(++a%3==0&&a++%7==0);
        System.out.println("a："+a+" "+bo);
        boolean bo2=(b++%3==0&&++b%7==0);
        System.out.println("b："+b+" "+bo2);

    }
}
