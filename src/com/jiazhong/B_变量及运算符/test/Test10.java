package com.jiazhong.B_变量及运算符.test;

public class Test10 {
    public static void main(String[] args) {
//        int i = 1;//i->1
//        i = i++;
//        // 规则使用临时变量: (1) temp=i;(2) i=i+1;(3)i //
//        System.out.println(i); // 1
        int i=5;
        int j=5;
        if (i++==6&&++j==6){
            i=11;
        }
        System.out.println(i+" "+j);
    }
}