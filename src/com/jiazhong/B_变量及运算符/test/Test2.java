package com.jiazhong.B_变量及运算符.test;
//•	编写步骤：
//        1.	定义类 Test3
//        2.	定义 main方法
//        3.	定义char类型变量ch,赋值为'J'
//        4.	使用强制转换的方式,将变量ch转换为小写'j',并输出
//        5.	定义char类型变量ch2,赋值为'a'
//        6.	使用-=的方式,将变量ch2转换为大写'A',并输出
//        7.	定义double类型变量d3,int类型变量i3
//        8.	定义double变量sum3,保存d3与i3的和,输出sum3的值和sum3去除小数部分的值
//        9.	定义double类型变量d4,int类型变量i4
//        10.	定义int变量mul4,保存d4和i4乘积的整数部分,并输出
public class Test2 {
    public static void main(String[] args) {
        char ch = 'J';
        ch = (char)(ch+32);
        System.out.println(ch);
      char  ch2='a';
      ch-=32;
      System.out.println(ch2);
      double d3=12.66;
      int i3=12;
      double sum3=0;
      sum3=d3+i3;
        System.out.println(sum3);
        sum3=(int)(d3+i3);
        System.out.println((int)sum3);
      double d4=12.663;
      int i4=66;
      int mul4=0;
          mul4=(int)(i4*d4);
        System.out.println(mul4);
    }
}
