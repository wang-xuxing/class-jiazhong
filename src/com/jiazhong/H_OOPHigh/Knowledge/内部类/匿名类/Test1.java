package com.jiazhong.H_OOPHigh.Knowledge.内部类.匿名类;
//1.有一个铃声Bell,里面有一个方法
//⒉.有一个手机类cellphone，具有闹钟的功能alarmclock,参数是Bell类型
//3测试手机类的闹钟功能，通过匿名内部类(对象)作为参数，打印:懒猪起床了。
//4.在传入另外一个匿名内部类，打印，兄弟们上课了。
public class Test1 {
    public static void main(String[] args) {
        CellPhone cellPhone=new CellPhone();
        cellPhone.alarmclock(new Bell() {
            @Override
            public void say() {
                System.out.println("懒猪起床了");
            }
        });
        cellPhone.alarmclock(new Bell() {
            @Override
            public void say() {
                System.out.println("兄弟们上课了");
            }
        });
    }
}
 class CellPhone{
     public void alarmclock(Bell bell) {
         bell.say();
     }

}
interface
Bell{
    void say();
}