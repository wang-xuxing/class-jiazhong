package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.习题测试.B;

public interface B {
    abstract void c();

    static int m = 100;

    int f();
}

abstract class IMP implements B {
    @Override
    public int f() {
        return 0;
    }
}