package com.jiazhong.H_forObjectHigh.BeforeKnowledge.接口.习题测试.C;

public interface C {
    int MAX = 100;

    void f();
}

abstract class AS implements C {
    int MIN;
}

class DSS extends AS {
    @Override
    public void f() {
        MIN = 10;
    }
}