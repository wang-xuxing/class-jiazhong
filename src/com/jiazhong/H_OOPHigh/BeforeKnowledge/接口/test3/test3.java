package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test3;

public class test3 {
    public static void main(String[] args) {
        Usb[] usb = new Usb[2];
        usb[0] = new Phone();
        usb[1] = new Camera();
        try {
            for (int i = 0; i < 2; i++) {
                usb[i].work();
                if (usb[i] instanceof Phone) {
                    ((Phone) usb[i]).call();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
}
