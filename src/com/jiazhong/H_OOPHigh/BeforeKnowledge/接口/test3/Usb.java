package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test3;

//演示个案例: 给Usb数组中，存放Phone 和相机对象， Phone类还有一 个特有的方法call
//        () ，请遍历Usb数组，如果是Phone对象，除了调用Usb接口定义的方法外，还需要调用
//        Phone特有方法call.
public interface Usb {
    abstract void work();
}
