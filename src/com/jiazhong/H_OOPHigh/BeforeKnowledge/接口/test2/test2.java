package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test2;

public class test2 {
    public static void main(String[] args) {
        say(new mysqlDB());
        say(new OracleDB());
    }

    static void say(DBInterface dbInterface) {
        dbInterface.connect();
        dbInterface.close();
    }
}
