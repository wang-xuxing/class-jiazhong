package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test2;

//说现在有一个项目经理(段玉),管理三个程序员,功能
// 开发一个软件为了控制和管理软件,项目经理可以定义一些接口，然后由程序员具体实现。
//(1. 项目质量 2.项目进度3.项目奖)参加工作
public interface DBInterface {
    public void connect();//链接方法

    public abstract void close();//关闭方法
}
