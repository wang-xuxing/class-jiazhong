package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test2;

public class OracleDB implements DBInterface {
    @Override
    public void close() {
        System.out.println("关闭Oracle");
    }

    @Override
    public void connect() {
        System.out.println("开启Oracle");
    }
}
