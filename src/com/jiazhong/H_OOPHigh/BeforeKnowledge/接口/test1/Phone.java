package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test1;

public class Phone implements USB {
    @Override
    public void stop() {
        System.out.println("手机开始充电");
    }

    @Override
    public void start() {
        System.out.println("手机充电量已冲满");
    }
}
