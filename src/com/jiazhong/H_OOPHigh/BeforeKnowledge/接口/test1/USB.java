package com.jiazhong.H_OOPHigh.BeforeKnowledge.接口.test1;

public interface USB {
    default void name() {
        System.out.println("我是usb接口");
    }

    static void name1() {
        System.out.println("我是静态方法");
    }

    public void start();

    public void stop();
}

