package com.jiazhong.H_forObjectHigh.BeforeKnowledge.内部类.静态内部类.test2;

public class Test {
    public int a = 1;
    int b=0;
    public class Tes {
        public int a = 3;

        public void out() {
            Test.this.b=0;
            System.out.println("内部类的a值：" + a);
            System.out.println("外部类的a值：" + Test.this.a);
        }
    }

    public Test() {
        new Tes().out();
    }

    public static void main(String[] args) {
        new Test();

    }
}