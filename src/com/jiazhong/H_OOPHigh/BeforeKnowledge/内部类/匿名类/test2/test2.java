package com.jiazhong.H_OOPHigh.BeforeKnowledge.内部类.匿名类.test2;

public class test2 {
    public static void main(String[] args) {
        ShowBank showBank=new ShowBank();
        showBank.showMess(new Bank() {
            @Override
            public void outPut() {
                money+=100;
                System.out.println("中国银行资金"+money);
            }
        });
        showBank.showMess(new Bank(500) {
            @Override
            public void outPut() {
                money+=100;
                System.out.println("建设银行资金"+money);
            }
        });
    }
}
