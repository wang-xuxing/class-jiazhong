package com.jiazhong.H_OOPHigh.BeforeKnowledge.内部类.Interface;
//多态数组InterfacePolyArr.java
//演示一个案例:给Usb数组中，存放 Phone和相机对象，
//Phone类还有一个特有的方法call0，请遍历Usb数组，如果是Phone对象，除了调用Usb接口定义的方法外，还需要调用Phone特有方法call.
public interface Usb1 {
    void  work();
}
