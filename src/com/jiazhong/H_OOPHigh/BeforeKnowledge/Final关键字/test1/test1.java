package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Final关键字.test1;

import java.util.Scanner;

//请编写一个程序，能够计算圆形的面积。要求圆周率为3.14.赋值的位置3个方式都写一下.
/*

 */
public class test1 {
   public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入圆的半径：");
        double radius=scanner.nextDouble();
        circle circle1=new circle(radius);
        System.out.println("圆的面积为："+circle1.getArea());
    }
}
