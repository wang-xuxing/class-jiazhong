package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Final关键字.test1;

public class circle{
    public double radius;
    public final double PI=3.14;

    public circle(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return (radius*radius*PI);
    }
}
