package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test3;
//最佳实践 需求：
// 1. 有多个类，完成不同的任务 job
// 2. 要求能够得到各自完成任务的时间
// 3. 编程实现
//设计一个抽象类（Template），完成如下功能 1. 编写方法 calculateTime(),可以计算某段代码的耗时时间
//2. 编写抽象方法 job
//3. 编写一个子类 Sub，继承抽象类 Template，并实现 job 方法
//4. 编写一个测试类 TestTemplate。看看是否好用
public class test3 {
    public static void main(String[] args) {
        say(new job1Time());
        say(new job2Time());
    }
    static void say(Template template){
        template.calculateTime();
    }
}
