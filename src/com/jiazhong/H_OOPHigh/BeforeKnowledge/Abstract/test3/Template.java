package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test3;

public abstract class Template {
    public abstract void job();
    void calculateTime(){
        long start= System.currentTimeMillis();
        job();
        long end=System.currentTimeMillis();
        System.out.println("任务执行时间为："+(end-start));
    }
}
