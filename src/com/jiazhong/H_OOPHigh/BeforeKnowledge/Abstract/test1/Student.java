package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test1;

 public  abstract class Student {
    private String name;

    public Student(String name) {
        this.name=name;
    }

    abstract public void study();
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public String say(){
        return ("学生姓名"+name);
    }
}
