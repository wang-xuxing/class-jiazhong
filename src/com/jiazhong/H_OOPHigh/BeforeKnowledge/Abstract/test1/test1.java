package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test1;

public class test1 {
    public static void main(String[] args) {
        say(new mathStudent("张开"));
        say(new phyStudent("李勇"));
    }
    static void say(Student student){
        System.out.println(student.say());
        student.study();
    }
}
