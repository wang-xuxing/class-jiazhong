package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test2;
//对于Manager类来说，他既是员工，还具有奖金(bonus)的属性。
public class Manager extends Employee {
    private double bones;
    public void setBones(double bones) {
        this.bones = bones;
    }
    public double getBones() {
        return bones;
    }

    public Manager(double bones) {
        this.bones = bones;
    }

    public Manager(String name, int id, double salary) {
        super(name, id, salary);
    }
    @Override
    void work() {
        System.out.println("经理"+getName()+"工作中...");
    }

//    @Override
//    public String say() {
//        return super.say();
//    }
}
