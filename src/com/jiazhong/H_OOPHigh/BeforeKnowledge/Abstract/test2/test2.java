package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test2;
/*编写一个Employee类,声明为抽象类，包含如下三个属性: name, id, salary。
提供必要的构造器和抽象方法: work()。 对于Manager类来说，他既是员工，还具有奖金(bonus)的属性。
请使用继承的思想，设计CommonEmployee类和 Manager类，要求类中提供必要的方法进行属性访问，实现work(),
提示"经理/普通员工名字工作中..
 oop+抽象类
 */
public class test2 {
    public static void main(String[] args) {
      say(new CommonEmployee("张勇",102,3000));
      say(new Manager("刘健",003,10000));
      Manager manager=new Manager(8000);
        System.out.println("奖金："+manager.getBones()+"元");
    }
    static void say(Employee employee){
        employee.work();
        System.out.println(employee.say());
    }
}
