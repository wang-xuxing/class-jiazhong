package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test2;

public class CommonEmployee extends Employee{
    public CommonEmployee(String name, int id, double salary) {
        super(name, id, salary);
    }
    @Override
    void work() {
        System.out.println("员工"+getName()+"正在工作中...");
    }

//    @Override
//    public String say() {
//        return super.say();
//    }
}
