package com.jiazhong.H_forObjectHigh.BeforeKnowledge.Abstract.test2;
//编写一个Employee类,声明为抽象类，包含如下三个属性: name, id, salary。
public abstract class Employee {
    private String name;
    private int id;
    private double salary;
    abstract void work();
    public Employee(String name,int id,double salary) {
        this.id=id;this.name=name;this.salary=salary;
    }

    public Employee() {
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
    public double getSalary() {
        return salary;
    }
    public String say(){
        return ("姓名："+name+"，工号："+id+"，薪水："+salary+"元");
    }
}
