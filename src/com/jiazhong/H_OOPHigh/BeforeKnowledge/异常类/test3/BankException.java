package com.jiazhong.H_OOPHigh.BeforeKnowledge.异常类.test3;

public class BankException extends  Exception {
    String message;
    public BankException(int n,int m) {
        message="入账资金"+n+"是负数或支出"+m+"是正数，不符合系统要求";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
