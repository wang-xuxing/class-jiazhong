package com.jiazhong.H_OOPHigh.BeforeKnowledge.异常类.test3;

public class Bank {
    private int money;
    public void income(int in,int out) throws BankException {
        if (in <= 0 || out >= 0 || in + out <= 0) {
            throw new BankException(in, out);
        }
        int netIncome = in + out;
        System.out.println("本次计算出的纯收入是：" + netIncome);
        money = money + netIncome;
    }

    public int getMoney() {
        return money;
    }
}

