package com.jiazhong.H_OOPHigh.BeforeKnowledge.异常类.test3;

public class test3 {
    public static void main(String[] args) {
        Bank bank=new Bank();
        try {
            bank.income(200,-100);
            bank.income(300,-100);
            bank.income(400,-100);
            System.out.println("银行的目前有"+bank.getMoney()+"元");
            bank.income(999999, -888);
            bank.income(200,100);
            bank.income(500 ,-100);//不在执行
        } catch (BankException e) {
            System.out.println("银行收益过程中出现如下问题：");
            System.out.println(e.getMessage());
        }
        System.out.println("银行目前有"+bank.getMoney()+"元");
    }

}
