package com.jiazhong.H_OOPHigh.BeforeKnowledge.异常类.test5;

import java.util.Scanner;

public class User {
    double num=0;
    double sum= 0;
    double ave= 0;
    public void Num() throws ScoreException{
        Scanner scanner=new Scanner(System.in);
        int count=0;
        System.out.println("键盘依次输入若干个数字（每个数字都需要按回车确认,输出非数字停止输出）:");
        while(scanner.hasNextDouble()){
            num=scanner.nextDouble();
            if (num>100||num<0)
                throw new ScoreException("错误信息："+num+"这是个非法成绩数据!"+"");
            count ++;
            sum=sum+num;
            ave=sum/count;
        }
    }
}
