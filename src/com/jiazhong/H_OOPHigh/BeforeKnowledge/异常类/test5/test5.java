package com.jiazhong.H_OOPHigh.BeforeKnowledge.异常类.test5;

//键盘依次输入若干个数字（每个数字都需要按回车确认），程序将计算这些数和以及平均值，
//当用户输入的数字大于>100或者小于0时程序立刻终止执行，并提示这是个非法成绩数据。
public class test5 {
    public static void main(String[] args) {
        User user=new User();
        try {
           user.Num();
        }
        catch (ScoreException e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("这些数和:"+user.sum+"这些数平均值:"+user.ave);
        }
        }
}
