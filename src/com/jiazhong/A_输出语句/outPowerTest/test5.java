package com.jiazhong.A_输出语句.outPowerTest;
//•	编写步骤：
//        1.	定义类 Test7
//        2.	定义 main方法
//        3.	定义两个整数变量a，b并赋值
//        4.	控制台输出变量a，b互换前的值
//        5.	定义一个第三方变量temp，不赋值
//        6.	利用第三方变量temp使a，b的值互换
//        7.	控制台输出变量a，b互换后的值

public class test5 {
    public static void main(String[] args) {
        int a=10;
        int b=20;
        int temp=0;
        System.out.println("互换前"+"\n"+a+"\n"+b);
        temp=a;
        a=b;
        b=temp;
        System.out.println("互换后"+"\n"+a+"\n"+b);
    }
}
