package com.jiazhong.A_输出语句.outPowerTest;
//1.	定义类 Test4
//        2.	定义 main方法
//        3.	定义2个 byte类型变量,分别赋byte类型范围内最大值和最小值,并输出在控制台.
//        4.	定义2个 short类型变量,分别赋short类型范围内的值,并输出在控制台.
//        5.	定义2个 int类型变量,分别赋int类型范围内的值,并输出在控制台.
//        6.	定义2个 long类型变量,分别赋超过int类型范围的值,并输出在控制台.

public class test4 {
    public static void main(String[] args) {
        byte b1=127;
        byte b2=-128;
        System.out.println("byte类型最大值为"+b1+"\n"+"byte类型最大值为"+b2);
        short s1=32000;
        short s2=-31602;
        System.out.println("short类型最大值为"+s1+"\n"+"short类型最大值为"+s2);
        int  i1=669999999;
        int  i2=-9697465;
        System.out.println("int类型最大值为"+i1+"\n"+"int类型最大值为"+i2);
        long l1=545645789;
        long l2=-456789789;
        System.out.println("long类型最大值为"+l1+"\n"+"long类型最大值为"+l2);
    }
}
