package com.jiazhong.A_输出语句.ballGame;

import java.awt.*;
import javax.swing.*;
public class ballGame extends JFrame{
    Image ball = Toolkit.getDefaultToolkit().getImage("D:/加中实训/输出语句/images/小球.png");
    Image desk = Toolkit.getDefaultToolkit().getImage("D:/加中实训/输出语句/images/球桌.png");
    double x=100;//小球横坐标
    double y=100;//小球纵坐标
    boolean right=true;
    //画窗口里的画
    public void paint(Graphics g){
        System.out.println("窗口被画了1次！");
        g.drawImage(desk, 0, 0, null);
        g.drawImage(ball, (int)x, (int)y, null);
        if(right){
            x = x +10;
        }else{
            x = x - 10;
        }
        if(x>856-40-30){    //856是窗口宽度，40是桌子边框的宽度，30是小球的直径
            right = false;
        }
        if(x<40){        //40是桌子边框的宽度
            right = true;
        }
    }
    public static void main(String[] args) {
        System.out.println("i'm your father");
        ballGame ballGame = new ballGame();
        ballGame.langhFrame();
    }
    void langhFrame(){
        setSize(865,500);//窗口大小
        setLocation(50,50);//位置
        setVisible(true);

        //重画
        while(true){
            repaint();
            try {
                Thread.sleep(40);//40ms
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}
