package com.jiazhong.A_输出语句.ballGame;

import java.awt.*;
import javax.swing.*;
public class ballGame2 extends JFrame{
    Image ball = Toolkit.getDefaultToolkit().getImage("D:/加中实训/输出语句/images/小球.png");
    Image desk = Toolkit.getDefaultToolkit().getImage("D:/加中实训/输出语句/images/球桌.png");
    double x=100;//小球横坐标
    double y=100;//小球纵坐标
    double degree=3.14/3;//此处为60度
    // 画窗口里的画
    public void paint(Graphics g){
        System.out.println("窗口被画了1次！");
        g.drawImage(desk, 0, 0, null);
        g.drawImage(ball, (int)x, (int)y, null);
        x=x+10*Math.cos(degree);
        y=y+10*Math.sin(degree);
        //碰到上下边界
        if(y>500-40-30||y<40+40){//500是窗口高度；40是桌子边框，30是球直径；最后一个40是标题栏的高度
            degree = -degree;
        }
        //碰到左右边界
        if(x<40||x>856-40-30){
            degree = 3.14 - degree;
        }

    }
     void langhFrame(){
        setSize(865,500);//窗口大小
        setLocation(50,50);//位置
        setVisible(true);
        //重画
        while(true){
            repaint();
            try {
                Thread.sleep(40);//40ms
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
    public static void main(String[] args) {
        System.out.println("i'm your father");
        ballGame2 ballGame2 = new ballGame2();
        ballGame2.langhFrame();
    }
}
