package com.jiazhong.C_分支语句.test2;
import java.util.Scanner;
//1) 使用 switch 把小写类型的 char 型转为大写(键盘输入)。只转换 a, b, c, d, e. 其它的输出 "other"。
public class test8 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        boolean  sc=true;
        for (int i=0;i<5;i++)
            {System.out.print("请输入一个字母：");
            char zimu = scanner.next().charAt(0);
            switch (zimu) {
                case 'a':
                    System.out.println('A');
                    break;
                case 'b':
                    System.out.println('B');
                    break;
                case 'c':
                    System.out.println('C');
                    break;
                case 'd':
                    System.out.println('D');
                    break;
                case 'e':
                    System.out.println('E');
                    break;
                default:
                    System.out.println("other");
                    break;
            }
            }
        }
    }


