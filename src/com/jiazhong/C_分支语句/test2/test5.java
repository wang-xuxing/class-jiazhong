package com.jiazhong.C_分支语句.test2;

import java.util.Scanner;

//输入保国同志的芝麻信用分： 如果：
//        1) 信用分为 100 分时，输出 信用极好；
//        2) 信用分为(80，99]时，输出 信用优秀；
//        3) 信用分为[60,80]时，输出 信用一般；
//        4) 其它情况 ，输出 信用 不及格
//        5) 请从键盘输入保国的芝麻信用分，并加以判断
public class test5 {
    public static void main(String[] args) {
        Scanner  scanner=new Scanner(System.in);
        System.out.print("输入保国同志的芝麻信用分：");
        for (int i=0;i<3;i++) {
            int scorer = scanner.nextInt();
            if (scorer >= 0 && scorer <= 100) {
                if (scorer == 100) {
                    System.out.println("信用极好");break;
                } else if (scorer > 80) {
                    System.out.println("信用优秀");break;
                } else if (scorer >= 60) {
                    System.out.println("信用一般");break;
                } else {
                    System.out.println("信用 不及格");break;
                }
            } else {
                System.out.println("数字在0--100之间，请重新输出：");
            }
        }
    }
}
