package com.jiazhong.C_分支语句.test2;

import java.util.Scanner;

//判断一个年份是否是闰年，闰年的条件是符合下面二者之一：(1)年份能被 4 整除，但不能被 100 整除；(2)能被 400 整除
public class test4 {
    public static void main(String[] args) {
        Scanner  scanner=new Scanner(System.in);
        System.out.println("请输入年份：");
        int year= scanner.nextInt();
        if (year%4==0&&year%100!=0||year%400==0){
            System.out.println("今年是闰年");
        }
        else {
            System.out.println("今年不是闰年");
        }
    }
}
