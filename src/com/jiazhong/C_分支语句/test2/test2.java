package com.jiazhong.C_分支语句.test2;

import java.util.Scanner;

//编写一个程序,可以输入人的年龄,如果该同志的年龄大于 18 岁, 则输出 "你年龄大于 18,要对自己的行为负责,送入监狱
// .否则 ,你年龄不大这次放过你了
public class test2 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入您的年龄：");
        int age= scanner.nextInt();
        if (age>18){
            System.out.println("你年龄大于 18,要对自己的行为负责,送入监狱");
        }
        else {
            System.out.println("你年龄不大这次放过你了");
        }
    }
}
