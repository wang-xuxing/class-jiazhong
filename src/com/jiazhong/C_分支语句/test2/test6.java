package com.jiazhong.C_分支语句.test2;

import java.util.Scanner;

//参加歌手比赛，如果初赛成绩大于 8.0 进入决赛，否则提示淘汰。并且根据性别提示进入男子组或 女子组。,
//        输入成绩和性别，进行判断和输出信息。
//        提示: double score; char gender;
//        接收字符: char gender = scanner.next().charAt(0)
public class test6 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入参赛总人数：");
        int class_num=scanner.nextInt();
        for (int i=0;i<class_num;i++){
        System.out.print("请输入成绩：");
        double score=scanner.nextDouble();
        if (score>8.0){
            System.out.println("进入决赛----->");
            System.out.println("请输入性别");
            char sex=scanner.next().charAt(0);
            if (sex=='女'){
                System.out.println("进入女子组--->");continue;
            }
            else if (sex=='男'){
                System.out.println("进入男子组--->");
            }
            else {System.out.println("性别输入有误请重新输入");
            }
        }
        else {
            System.out.println("你被淘汰了！");
        }
    }
        System.out.println("输入完毕...");
    }
}
