package com.jiazhong.C_分支语句.test2;

import java.util.Scanner;

//2) 根据用于指定月份，打印该月份所属的季节。
//        3,4,5 春季 6,7,8 夏季 9,10,11 秋季 12, 1, 2 冬季
public class test9 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        for (int i=0;i<12;i++){
        System.out.println("请输入一个月份：");
        int month=scanner.nextInt();
        switch (month){
            case 3:
            case 4:
            case 5:
                System.out.println("这个月是春季");break;
            case 6:
            case 7:
            case 8:
                System.out.println("这个月是夏季");break;
            case 9:
            case 10:
            case 11:
                System.out.println("这个月是秋季");break;
            case 12:
            case 1:
            case 2:
                System.out.println("这个月是冬季");break;
            default:
                System.out.println("请输入正确的月份！");
        }
    }
    }
}
