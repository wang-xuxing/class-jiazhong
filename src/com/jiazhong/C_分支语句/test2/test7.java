package com.jiazhong.C_分支语句.test2;
import java.util.Scanner;
//请编写一个程序，该程序可以接收一个字符，比如:a,b,c,d,e,f,g a 表示星期一，b 表示星期二 …
//        根据用户的输入显示相应的信息.要求使用 switch 语句完成 思路分析
//        1. 接收一个字符 , 创建 Scanner 对象
//        2. 使用 switch 来完成匹配,并输出对应信息 代码
public class test7 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入一个字符a~g：");
        char a_g=scanner.next().charAt(0);
        switch (a_g){
            case 'a':
                System.out.println("星期一");break;
            case 'b':
                System.out.println("星期二");break;
            case 'c':
                System.out.println("星期三");break;
            case 'd':
                System.out.println("星期四");break;
            case 'e':
                System.out.println("星期五");break;
            case 'f':
                System.out.println("星期六");break;
            case 'g':
                System.out.println("星期天");break;
            default:
                System.out.println("输入错误，请输入一个字符a~g...");break;
        }
    }
}
