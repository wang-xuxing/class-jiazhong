package com.jiazhong.C_分支语句.test1;

import java.util.Scanner;

//1.	编写一个Java程序：求一个数的绝对值。
public class test1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入一个值：");
        double i=scanner.nextDouble();
        if (i<0){
            i=-i;
        }
        System.out.println("|"+i+"|");
    }
}
