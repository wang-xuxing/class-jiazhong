package com.jiazhong.C_分支语句.test1;

public class test7 {
    public static void main(String[] args){
        for(int i=100;i<1000;i++) {
            int x = i / 100;
            int y = i / 10 % 10;
            int z = i % 10;
            if (x * x * x + y * y * y + z * z * z == i) {
                System.out.println(x + "," + y + "," + z + "|" + "三位水仙花数是：" + i);
            }
        }}
}
