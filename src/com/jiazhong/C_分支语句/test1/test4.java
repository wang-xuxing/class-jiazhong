package com.jiazhong.C_分支语句.test1;

import java.util.Scanner;

//4.	编写一个Java程序：判断一个整数是否为偶数。
public class test4 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入一个整数：");
        int i=scanner.nextInt();
        if (i%2==0){
            System.out.println("这个数为偶数");
        }
        else {
            System.out.println("这个数为奇数");
        }
    }
}
