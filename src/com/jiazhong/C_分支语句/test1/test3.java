package com.jiazhong.C_分支语句.test1;

import java.util.Scanner;

//3.	编写一个Java程序：求三个数中的最大值。
public class test3 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请分别输入三个值：");
        double d1=scanner.nextDouble();
        double d2=scanner.nextDouble();
        double d3=scanner.nextDouble();
        double max=0;
//      for (int i=0;i<3;i++){
//          double d1=scanner.nextDouble();
//          if (max<d1){
//              max=d1;
//          }
//      }
        if (d1>d2){
            max=d1;
        }
        else {
            max=d2;
        }
        if (d3<max){
            System.out.println("最大值为："+max);
        }
        else {
            max=d3;
            System.out.println("最大值为："+max);
        }

    }
}
