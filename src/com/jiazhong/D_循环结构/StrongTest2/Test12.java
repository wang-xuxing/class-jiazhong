package com.jiazhong.D_循环结构.StrongTest2;

public class Test12 {
    public static void main(String[] args) {
        int num = 5;
        for (int i = 1; i <= num; i++) {//行的循环
            for (int j = num; j >= i; j--) {//空格列的循环
                System.out.print(" ");
            }
            for (int j = 1; j <= num - 1; j = num) {//左边*列的循环
                System.out.print("*");
            }
            for (int j = 1; j < i * 2 - 2; j++) {//中间空格的循环
                if (i==num){
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            for (int j = 1; j <= num; j = j + num) {//右边*列的循环
                if (i == 1) {
                    System.out.print(" ");
                } else if (i > 1) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }

    }
}


