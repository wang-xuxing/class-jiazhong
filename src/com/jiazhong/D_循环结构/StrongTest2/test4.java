package com.jiazhong.D_循环结构.StrongTest2;

import java.util.Scanner;
//从键盘接收一个正整数，以该正整数作为行数，输出如下三角形：
//           *
//          ***
//         *****
//        *******
public class test4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("请随机输出一个数字：");
        int num=sc.nextInt();
        for (int i=0;i<num;i++){
            for (int j=0;j<num-i;j++){
                System.out.print(" ");
            }
            for (int m=0;m<2*i+1;m++){
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
