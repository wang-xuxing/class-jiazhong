package com.jiazhong.D_循环结构.StrongTest2;
//5.	小芳的妈妈每天给她2.5元钱，她都会存起来，但是，每当这一天是存钱的天数是5或5的倍数的话。她都会花去6元钱，请问，经过多少天，小芳才可以存到100元钱。
public class test5 {
    public static void main(String[] args) {
//        int count = 0;
//        double num;
//        for (num = 0; num <= 100; num = num + 2.5) {
//            if (count % 5 == 0) {
//                num = num - 6;
//            }
//            count++;
//        }
//        System.out.println(num);
//
//        System.out.println(count);
//    }
//}
    //每天要存储的钱是2.5元
    double dayMoney = 2.5;
    //存钱的初始化值是0
    double daySum = 0;
    //从第一天开始存储
    int dayCount = 1;
    //最终存储不小于100就不存储了
    int result = 100;
//因为不知道是多少天，所以我用死循环，
                while(true) {
                        //累加钱
                        daySum += dayMoney;
                        //一旦超过100元我就退出循环。
                        if(daySum >= result) {
                        System.out.println("共花了"+dayCount+"天存储了100元");
                        break;
                        }
                        if(dayCount%5 == 0) {
                        //花去6元钱
                        daySum -= 6;
                        System.out.println("第"+dayCount+"天花了6元钱");
                        }
                        //天数变化
                        dayCount++;
                        }
            }
            }
