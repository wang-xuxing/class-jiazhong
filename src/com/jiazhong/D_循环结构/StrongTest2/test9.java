package com.jiazhong.D_循环结构.StrongTest2;
//        2.给出一组字符串如：a2b1c,请你编程输出里面的字母：abc。（参考资料：字母0的ascll码是48，字母9的ascll码是57）
public class test9 {
    public static void main(String[] args) {
                String str = "a2b&1c";
                //将字符串转换为字符数组
                char[] charArr = str.toCharArray();
                String newStr = "";
                //charArr.length获得数组的长度
                for (int i = 0; i < charArr.length; i++) {
                    char c = charArr[i];
                    if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z') {
                        newStr = newStr + c;
                    }
                }
                System.out.println(newStr);
            }
        }