package com.jiazhong.D_循环结构.StrongTest2;
//1. 模拟登陆：
//        提示：
//        登陆最多只能三次，如果第三次还没有成功直接退出程序；（与固定的用户名和密码判断是否相等）


import java.util.Scanner;

public class test8 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        String username="我真帅";
        String password="123456";
        int count=3;
        for (int i=0;i<3;i++){
            System.out.print("请输入用户名：");
            String user=sc.next();
            System.out.print("请输入密码：");
            String pawd=sc.next();
            if (user.equals(username)&&pawd.equals(password)){
                System.out.println("登陆成功");break;
            }
            else {count--;
                System.out.println("您还有"+count+"次机会");
            }
        }

    }
}
