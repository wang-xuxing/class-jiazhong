package com.jiazhong.D_循环结构.ssx;
import java.util.Scanner;
//实现登录验证，有 3 次机会，如果用户名为"丁真" ,密码"666"提示登录成功，
//否则提示还有几次机会，请使用 for+break 完成
public class text3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = "";
        String passwd = "";
        int chance = 3; //登录一次 ，就减少一次
        for (int i = 1; i <= 3; i++) {//3 次登录机会
            System.out.println("请输入名字");
            name = scanner.next();
            System.out.println("请输入密码");
            passwd = scanner.next();
//比较输入的名字和密码是否正确
//补充说明字符串 的内容 比较 使用的 方法 equals
            if ("丁真".equals(name) && "666".equals(passwd)) {
                System.out.println("恭喜你，登录成功~");
                break;
            }
//登录的机会就减少一次
            chance--;
            System.out.println("你还有" + chance + "次登录机会");
        }
    }}
