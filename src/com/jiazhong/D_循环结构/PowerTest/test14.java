package com.jiazhong.D_循环结构.PowerTest;
//5.	小芳的妈妈每天给她2.5元钱，她都会存起来，但是，每当这一天是存钱的天数是5或5的倍数的话。她都会花去6元钱，请问，经过多少天，小芳才可以存到100元钱。
public class test14 {
    public static void main(String[] args) {
        int count=0;
        double num=0;
        int  money=100;
        while (true){count++;
            num=num+2.5;
            if (count%5==0){
                num=num-6;
            }
            if (num>=money){
                System.out.println("攒了"+count+"天，攒了"+num+"元");break;
            }
        }
    }
}
