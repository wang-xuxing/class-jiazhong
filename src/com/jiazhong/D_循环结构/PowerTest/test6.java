package com.jiazhong.D_循环结构.PowerTest;

import java.util.Scanner;

//10、	在屏幕上打印出n行的金字塔图案，如，若n=5,则图案如下：
//         *
//        ***
//       *****
//      *******
//     *********
public class test6 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入打印几行金字塔：");
        int n= scanner.nextInt();
        for (int i=0;i<n;i++){
            for(int j=0;j<n-i-1;j++){
                System.out.print(" ");
            }
            for (int k=0;k<i*2+1;k++){
                System.out.print("*");
            }System.out.println();
        }
    }
}
