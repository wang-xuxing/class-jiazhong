package com.jiazhong.D_循环结构.PowerTest;
//13、	将一个数组中的元素倒排过来，不能新开一个数组的临时存储空间，只能在原数组上改
public class test9 {
    public static void main(String[] args) {
        int[] shuzu={1,2,3,4,5,6,8,9};
        for (int i=0;i<shuzu.length/2;i++){
            int temp=shuzu[i];
            shuzu[i]=shuzu[shuzu.length-i-1];
            shuzu[shuzu.length-1-i]=temp;
        }
        for (int i=0;i< shuzu.length;i++){
            System.out.print(shuzu[i]+" ");
        }
    }
}
