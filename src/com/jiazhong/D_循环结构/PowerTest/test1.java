package com.jiazhong.D_循环结构.PowerTest;
import java.util.Scanner;
//5、	实现一个课程名称和课程代号的转换器：
//        输入下表中的课程代号，输出课程的名称。用户可以循环进行输入，如果输入n就退出系统。（使用while循环实现）
public class test1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String num="" ;
        while (!num.equals("n")){
            System.out.println("请输入课程代号（1~4之间的数字）:");
            num=scanner.next();
            switch (num){
                case "1":
                    System.out.println("使用Java语言理解程序逻辑");break;
                case "2":
                    System.out.println("使用HTML语言开发商业站点");break;
                case "3":
                    System.out.println("使用SQL Server管理和查询数据");break;
                case "4":
                    System.out.println("使用C#开发数据库应用程序");break;
                default:
                    System.out.println("无这门课程！");break;
            }System.out.println("输入n退出系统:");
            num=scanner.next();
        }
        System.out.println("退出系统");

    }
}
