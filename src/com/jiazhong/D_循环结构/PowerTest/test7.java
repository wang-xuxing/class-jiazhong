package com.jiazhong.D_循环结构.PowerTest;
//11、	打印出100~999之间的所有“水仙花数”。所谓“水仙花数”，是指一个3位数，其各位数字立方和等于该数本身。例如：153是一个“水仙花数”，因为153=13+53+33。
public class test7 {
    public static void main(String[] args) {
        for (int i=100;i<1000;i++){
            int i_g=i%10;
            int i_s=i/10%10;
            int i_b=i/100;
            if (i==(i_b*i_b*i_b)+(i_s*i_s*i_s)+(i_g*i_g*i_g)){
                System.out.println(i);
            }
        }
    }
}
