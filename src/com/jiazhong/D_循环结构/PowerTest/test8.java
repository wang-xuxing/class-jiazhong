package com.jiazhong.D_循环结构.PowerTest;
//	幸运猜猜猜：游戏随机给出一个0~99（包括0和99）的数字，然后让你猜是什么数字。你可以随便猜一个数字，游戏会提示太大还是太小，从而缩小结果范围。
//	经过几次猜测与提示后，最终推出答案。在游戏过程中，记录你最终猜对时所需要的次数，游戏结束后公布结果。
//        积分对照表
//        次数	结果
//        1	你太有才了！
//        2~6	这么快就猜出来了，很聪明么！
//        大于7	猜了半天才猜出来，小同志，尚需努力啊！
//        猜测次数最多20次。
//        提示:
//        (1)int number = (int)(Math.random()*100)
//        (2)	使用for循环结构，其中循环计数器counter同时也记录你猜测的次数
//        (3)	计算积分可以使用switch结构

import java.util.Scanner;

public class test8 {
    public static void main(String[] args) {
        int number = (int) (Math.random() * 100);//产生0~99之间的随机数字:
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        boolean flag = false;
        int number_cai;
        while (count < 20) {
            count++;
            System.out.println("请输入数字：(你有20次机会)");
            number_cai = scanner.nextInt();
            if (number < number_cai) {
                System.out.println("太大了！小点！");
            } else if (number > number_cai) {
                System.out.println("太小了！大点！");
            } else {
                flag = true;
                System.out.println("恭喜你，猜测正确");
                break;
            }
        }
           if (flag){
        switch (count) {
            case 1:
                System.out.println("你太有才了");break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                System.out.println("这么快就猜出来了，很聪明么！");break;
            default:
                System.out.println("你真笨，这么久都猜不出来！");break;

        }
           }
           else {
               System.out.println("真笨，这么久都猜不出来！");
           }
    }
}

