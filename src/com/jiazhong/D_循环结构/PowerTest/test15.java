package com.jiazhong.D_循环结构.PowerTest;

import java.util.Scanner;

//6.	循环录入5个人的年龄，并计算平均年龄，如果录入的数据出现负数或者大于130的数，立即停止输出报错（无需打印平均年龄）
public class test15 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请依次输入5个人的年龄:");
       for (int i=0;i<5;i++){
           int age=scanner.nextInt();
           if (age>130||age<0){
               System.out.println("停止输出报错");
               break;
           }
       }
    }
}
