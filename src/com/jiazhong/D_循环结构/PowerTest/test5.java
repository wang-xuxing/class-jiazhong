package com.jiazhong.D_循环结构.PowerTest;
import java.util.Scanner;
//9、	输入一个正整数，将该数的各位左右反转输出，即输入123，输出321。（使用while循环实现）
public class test5 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int num=0;
        String new_num="";
        System.out.print("输入一个正整数:");
        num=scanner.nextInt();
        while (num/10!=0){
            int num_g=num%10;
            new_num=new_num+num_g;
            num=num/10;}
        new_num=new_num+num;
        System.out.println("该数反转输出为："+new_num);
        }
    }


