package com.jiazhong.D_循环结构.PowerTest;
//7、	求整数1~100的累加值，但要求跳过所有个位为3的数。（使用for循环实现
public class test3 {
    public static void main(String[] args) {
        int sum=0;
    for(int i=1;i<=100;i++){
        if (i%10!=3){
            sum+=i;
        }
    }
        System.out.println("1~100的累加值，但要求跳过所有个位为3的数"+sum);
}}
