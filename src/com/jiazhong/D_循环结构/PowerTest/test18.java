package com.jiazhong.D_循环结构.PowerTest;

import java.util.Scanner;

//2.给出一组字符串如：a2b1c,请你编程输出里面的字母：abc。（参考资料：字母0的ascll码是48，字母9的ascll码是57）
public class test18 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入一个字符串：");
        String str=scanner.next();
        char[] charArr = str.toCharArray();
        String str1="";
        for (int i=0;i<charArr.length;i++){
            if (charArr[i]<'z'&&charArr[i]>'a'||charArr[i]<'Z'&&charArr[i]>'A'){
                str1=str1+charArr[i];
            }
        }
        System.out.println("去数字后字符串为："+str1);

    }
}
