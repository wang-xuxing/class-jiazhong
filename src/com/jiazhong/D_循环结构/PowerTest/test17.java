package com.jiazhong.D_循环结构.PowerTest;
//模拟登陆：
//        提示：
//        登陆最多只能三次，如果第三次还没有成功直接退出程序；（与固定的用户名和密码判断是否相等）

import java.util.Scanner;

public class test17 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String username="我要好好学习";
        String password="123456";
        for(int i=1;i<=3;i++){
            System.out.print("请输入用户名：");
            String username_1=scanner.next();
            System.out.print("请输入密码：");
            String password_1=scanner.next();
            if (username.equals(username_1)&&password.equals(password_1)){
                System.out.println("登陆成功！");break;
            }
            else {
                System.out.println("登陆失败请重新登陆，你还有"+(3-i)+"次机会");
            }
        }
    }
}
