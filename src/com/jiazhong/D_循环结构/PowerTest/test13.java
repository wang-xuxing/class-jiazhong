package com.jiazhong.D_循环结构.PowerTest;
//4.从键盘接收一个正整数，以该正整数作为行数，输出如下三角形：
//           *
//          ***
//         *****
//        *******

import java.util.Scanner;

public class test13 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入一个正整数：");
        int num=scanner.nextInt();
        for (int i=0;i<num;i++){
            for (int j=0;j<num-i-1;j++){
                System.out.print(" ");
            }
            for (int k=0;k<i*2+1;k++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
