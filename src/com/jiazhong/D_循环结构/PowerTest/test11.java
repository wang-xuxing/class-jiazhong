package com.jiazhong.D_循环结构.PowerTest;
//2、计算下列式子的值：
//        （1）1+3+……+99
//        （2）1+2+4+8+……+128+256

public class test11 {
    public static void main(String[] args) {
        int sum_1=0;
        int sum_2=0;
        for(int i=1;i<100;i=i+2){
          sum_1=sum_1+i;
        }
        for (int j=1;j<=256;j=j*2)
        {
            sum_2=sum_2+j;
        }
        System.out.println("1+2+4+8+……+128+256="+sum_2);
        System.out.println("1+3+……+99="+sum_1);
    }
}
