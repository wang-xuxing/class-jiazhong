package com.jiazhong.D_循环结构.StrongTest;
import java.util.Scanner;
//8、	输入一个正整数N，判断该数是不是质数，如果是质数输出“N是一个质数”，否则输出“N不是质数”。提示：质数的含义：除了1和它本身不能被任何数整除。（使用for循环实现）
public class text8 {
    public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.println("请输入一个正整数");
                int sum = sc.nextInt();
                //定义一个临时变量，用于表示num是否为质数，假设所有数都是质数
                boolean flag = true;//true表示质数，false不是质数
                for (int i = 2; i < sum; i++) {
                    //判断sum不是质数
                    if (sum % i == 0) {
                        flag = false;//不是质数
                        break;
                    }
                }
                if (flag) {
                    System.out.println(sum + "是质数");
                } else {
                    System.out.println(sum + "不是质数");
                }
            }
        }


