package com.jiazhong.D_循环结构.StrongTest;

//9、输入一个正整数，将该数的各位左右反转输出，即输入123，输出321。（使用while循环实现）
public class text9 {
    public static void main(String[] args) {
        int num=97868;
        String new_num ="";
        while (num/10!=0){
            int ge=num%10;//个位数
            new_num=new_num+ge;
            num=num/10;//去掉最后一位
             }
        //将num的最后一位加入到newNum中
        new_num = new_num + num;
        System.out.println(new_num);
    }
}