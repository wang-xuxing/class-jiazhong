package com.jiazhong.D_循环结构.StrongTest;
//12、	幸运猜猜猜：游戏随机给出一个0~99（包括0和99）的数字，然后让你猜是什么数字。
// 你可以随便猜一个数字，游戏会提示太大还是太小，从而缩小结果范围。经过几次猜测与提示后，最终推出答案。在游戏过程中，记录你最终猜对时所需要的次数，游戏结束后公布结果。
//        积分对照表
//        次数	结果
//        1	你太有才了！
//        2~6	这么快就猜出来了，很聪明么！
//        大于7	猜了半天才猜出来，小同志，尚需努力啊！
//        猜测次数最多20次。
//        提示:
//        (1)	产生0~99之间的随机数字:
//        int number = (int)(Math.random()*100)
//        (2)	使用for循环结构，其中循环计数器counter同时也记录你猜测的次数
//        (3)	计算积分可以使用switch结构
import java.util.Scanner;
public class text12 {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int count=0;
        int number = (int)(Math.random()*100);
        Boolean flag=false;   //默认值为false猜测错误
        for (int i=0;i<20;i++){
            count++;
        System.out.println("请输入你猜的数字：");
        int n = sc.nextInt();
        if (number>n){
            System.out.println("你输出的数字太小了");
        }
        else if (number<n){
            System.out.println("你输出的数字太大了");
        }
        else {flag= true;
            System.out.println("恭喜你输入正确");break;
        }
        }
        if (flag){
            switch (count){
                case 1:
                    System.out.println("你太有才了一次就对"); break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    System.out.println("挺聪明呀！这么快就猜出来了");break;
                default:
                    System.out.println("猜半天才猜出来，得努力了啊");break;
            }
        }
        else {
            System.out.println("很抱歉你没猜对");
        }
    }
}


