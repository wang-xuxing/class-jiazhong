package com.jiazhong.D_循环结构.StrongTest;
import java.util.Scanner;
//5、	实现一个课程名称和课程代号的转换器：输入下表中的课程代号，输出课程的名称。用户可以循环进行输入，如果输入n就退出系统。
public class text5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入课程代号（1~4之间的数字）：");
        String class_id =sc.next();
        while (!class_id.equals("n")){
            switch (class_id) {
                case "1":
                    System.out.println("课程名为：使用Java语言理解程序逻辑");break;
                case "2":
                    System.out.println("使用HTML语言开发商业站点");break;
                case "3":
                    System.out.println("使用SQL Server管理和查询数据");break;
                case "4":
                    System.out.println("使用C#开发数据库应用程序");break;
                default:
                    System.out.println("输的啥嘛" );break;
            }
            System.out.println("请输入课程代码");
            class_id=sc.next();
        }
        System.out.println("程序结束");
        }
}


