package com.jiazhong.D_循环结构.StrongTest;
//11、	打印出100~999之间的所有“水仙花数”。所谓“水仙花数”，是指一个3位数，其各位数字立方和等于该数本身。例如：153是一个“水仙花数”，因为153=13+53+33。
public class text11 {
    public static void main(String[] args) {
        for (int i=100;i<1000;i++){
            int ge=i%10;
            int shi=i/10%10;
            int bai=i/100;
            if (i ==(Math.pow(ge, 3)) + (Math.pow(shi, 3)) + (Math.pow(bai, 3))) {
                System.out.println("水仙花数有："+i);
            }
        }
    }
}