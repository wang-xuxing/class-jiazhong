package com.jiazhong.D_循环结构.StrongTest;
//6、	本金10000元存入银行，年利率是千分之三。每过1年，将本金和利息相加作为新的本金。计算5年后，获得的本息和是多少？
public class text6 {
    public static void main(String[] args) {
            double money = 10000; // 本金
            for (int year = 1; year <= 5; year++) {
                money *= 1 + 0.003;
            }
            System.out.println("五年后本金是" +  money);
        }
    }

