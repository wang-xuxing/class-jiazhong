package com.jiazhong.D_循环结构.do_while;
// 统计 1---200 之间能被 5 整除但不能被 3 整除的个数
public class text4 {
    public static void main(String[] args) {
        int i = 1;
        int j = 200;
        int sum = 0;
        do {
            if (i % 5 == 0 && i % 3 != 0) {
                System.out.println(i);
                sum++;
            }
            i++;
        } while (i <= j);
        System.out.println("他的个数为" + sum);
    }
}