package com.jiazhong.D_循环结构.for_.嵌套循环;

public class text3 {
    public static void main(String[] args) {
        int num = 5;//定义列数
        for (int i = 1; i <= num; i++) {//行的循环
            //列的循环
            for (int j = num; j >= i; j--) {//空格
                System.out.print(" ");
            }
            for (int j = 1; j <= i * 2 - 1; j++) {
                if (i==1||i==num||j==1||j==i*2-1){
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }

            System.out.println();
        }
    }
}
