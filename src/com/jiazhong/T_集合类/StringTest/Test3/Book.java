package com.jiazhong.T_集合类.StringTest.Test3;

//3.1开发一个书类Book（具体实现自拟）
public class Book implements Comparable<Book> {
    private String ID;//书籍编号
    private String BookName;//书名
    private Double price;//书价格

    public Book() {
    }

    public Book(String ID, String bookName, Double price) {
        this.ID = ID;
        BookName = bookName;
        this.price = price;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String bookName) {
        BookName = bookName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "ID='" + ID + '\'' +
                ", BookName='" + BookName + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(Book book) {
        if (Integer.parseInt(this.ID)>Integer.parseInt(book.ID)){
            return 1;
        }
        if (this.ID==book.ID){
            return (this.BookName.compareTo(book.BookName));
        }
        return -1;
    }
}
