package com.jiazhong.T_集合类.StringTest.Test3;
//3.2将100个书对象放入LinkedList集合，并输出在控制台；
import java.util.LinkedList;

public class Linked_List {
    public static void main(String[] args) {
        LinkedList<Book> linkedList=new LinkedList<>();
        linkedList.add(new Book("001","书名1",996.6));
        linkedList.add(new Book("002","书名2",996.6));
        linkedList.add(new Book("003","书名3",996.6));
        linkedList.add(new Book("004","书名4",996.6));
        linkedList.add(new Book("005","书名5",996.6));
        linkedList.add(new Book("006","书名6",996.6));
        linkedList.add(new Book("007","书名7",996.6));
        linkedList.add(new Book("008","书名8",996.6));
        linkedList.add(new Book("009","书名9",996.6));
        linkedList.add(new Book("010","书名10",996.6));
        for (Book book: linkedList) {
            System.out.println(book);
        }

    }
}
