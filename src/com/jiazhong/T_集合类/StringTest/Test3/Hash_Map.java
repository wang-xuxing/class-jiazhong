package com.jiazhong.T_集合类.StringTest.Test3;

import java.util.HashMap;
import java.util.Map;

public class Hash_Map {
    public static void main(String[] args) {
            Map<String,Book> hashMap=new HashMap<>();
        for (int i=0;i<100;i++){
            Book book=new Book("bookNum"+i,"book"+i, 10.0+i);
            hashMap.put(book.getID(),book);
        }
        for (Map.Entry<String,Book> map:hashMap.entrySet()){
            System.out.println(map);
        }
    }
}
