package com.jiazhong.T_集合类.StringTest.Test04;

public class Stack {
    private int size;
    private int index = -1;//栈顶下标
    private Integer[] nums = null;

    public Stack(Integer size) {
        nums = new Integer[size];
    }

    public void push(int element) {
        index++;
        nums[index] = element;
        size++;
    }

    public int pop() {
        if (size == 0) {
            throw new RuntimeException("空栈无法取栈顶元素");
        }
        int num = nums[index];
        nums[index] = null;
        index--;
        size--;
        return num;
    }

    /**
     * 读取栈顶元素
     */
    public int peek() {
        if (size == 0) {
            throw new RuntimeException("空栈，无法读取栈顶元素");
        }
        int num = nums[index];
        return num;
    }


    public int size() {
        return this.size;
    }

}
