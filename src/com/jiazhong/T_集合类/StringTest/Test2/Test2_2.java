package com.jiazhong.T_集合类.StringTest.Test2;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

//2.2实例化一个Hashtable集合，放入十个字符串，并遍历输出到控制台。
public class Test2_2 {
    public static void main(String[] args) {
        Hashtable<String,String> hashtable=new Hashtable<>();
        for (int i=0;i<10;i++){
            hashtable.put("key"+i,"value"+i);
        }
        for (Map.Entry<String,String> map:hashtable.entrySet()){
            System.out.println(map.getKey()+"<---->"+map.getValue());
        }
//        System.out.println("---------------------------");
//        Enumeration<String> enumeration=hashtable.elements();
//        System.out.println(enumeration);
//        while (enumeration.hasMoreElements());{
//            String s=enumeration.nextElement();
//            System.out.println("1");
//        }
    }
}
