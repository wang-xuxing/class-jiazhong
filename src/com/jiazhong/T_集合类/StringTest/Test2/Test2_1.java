package com.jiazhong.T_集合类.StringTest.Test2;

import java.util.HashMap;
import java.util.Map;

//2.1实例化一个HashMap集合，放入十个整数，并遍历输出到控制台。
public class Test2_1 {
    public static void main(String[] args) {
        Map<Integer,Integer> hasMap=new HashMap<>();
        for (int i=0;i<10;i++){
            hasMap.put(1+i,1+i);
        }
        for (Map.Entry<Integer,Integer> entry: hasMap.entrySet()){
            System.out.println(entry.getKey()+"<-------------->"+entry.getValue());
        }
    }
}
