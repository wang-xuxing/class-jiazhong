package com.jiazhong.T_集合类.StringTest.Test1;

import java.util.HashSet;
import java.util.Iterator;

//实例化一个HashSet集合，放入十个字符串，使用迭代器遍历该集合输出到控制台
public class Test1_2 {
    public static void main(String[] args) {
        HashSet<String> hashSet=new HashSet<>();
        hashSet.add("一是一");
        hashSet.add("二是二");
        hashSet.add("三是三");
        hashSet.add("四是四");
        hashSet.add("五是五");
        hashSet.add("六是六");
        hashSet.add("七是七");
        hashSet.add("八昰八");
        hashSet.add("九是九");
        hashSet.add("是是是");
        Iterator<String> iterator= hashSet.iterator();
        while (iterator.hasNext()){
            String str=iterator.next();
            System.out.println(str);
        }
    }
}
