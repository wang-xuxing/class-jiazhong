package com.jiazhong.T_集合类.StringTest.Test1;

import java.util.ArrayList;

//1.3实例化一个ArrayList集合，放入十个字符，使用get方法遍历该集合输出到控制台；
public class Test1_3 {
    public static void main(String[] args) {
        ArrayList<Character> arrayList=new ArrayList<>();
        arrayList.add('一');
        arrayList.add('二');
        arrayList.add('三');
        arrayList.add('四');
        arrayList.add('五');
        arrayList.add('六');
        arrayList.add('七');
        arrayList.add('八');
        arrayList.add('九');
        arrayList.add('⑩');
        for (int i=0;i< arrayList.size();i++){
            System.out.println(arrayList.get(i));
        }
    }
}
