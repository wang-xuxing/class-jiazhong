package com.jiazhong.T_集合类.StringTest.Test1;

import java.util.Iterator;
import java.util.TreeSet;

//1.4实例化一个TreeSet集合，放入十个字符串，使用迭代器遍历该集合输出到控制台；
public class Test1_4 {
    public static void main(String[] args) {
        TreeSet<String> treeSet=new TreeSet<>();
        treeSet.add("我是一");
        treeSet.add("我是二");
        treeSet.add("我是三");
        treeSet.add("我是四");
        treeSet.add("我是五");
        treeSet.add("我是六");
        treeSet.add("我是七");
        treeSet.add("我是八");
        treeSet.add("我是九");
        treeSet.add("我是十");

        Iterator<String> iterator= treeSet.iterator();
        while (iterator.hasNext()){
            String str=iterator.next();
            System.out.println(str);
        }


    }
}
