package com.jiazhong.T_集合类.StringTest.Test1;
import java.util.Iterator;
import java.util.LinkedList;
//1.1实例化一个LinkedList集合，放入十个浮点数，使用迭代器遍历该集合输出到控制台；
public class Test1_1{
    public static void main(String[] args) {
        LinkedList<Double> linkedList=new LinkedList<>();
          linkedList.add(12.01);
          linkedList.add(15.15);
          linkedList.add(45.5);
          linkedList.add(78.6);
          linkedList.add(79.9);
          linkedList.add(89.9);
          linkedList.add(78.9);
          linkedList.add(78.92);
          linkedList.add(89.44);
          linkedList.add(99.87);
          Iterator<Double> iterable=  linkedList.iterator();
          while (iterable.hasNext()){
              Double aDouble=iterable.next();
              System.out.println(aDouble);
          }
    }
}
