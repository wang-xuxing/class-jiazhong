package com.jiazhong.T_集合类.StringTest2.Test1;

import java.util.HashMap;
import java.util.Map;

/**
（2）统计每个字符出现的次数，使用map存储，字符为键，次数为值。
（3）遍历map，打印统计信息
 */
public class Test02 {
    public static void main(String[] args) {

        String str="adhflkalkfdhasdkhflsa";
        HashMap<Character,Integer> hashMap=new HashMap<>();
        char[] chars=str.toCharArray();
        for (char cs:chars){
            int count = 1;
            //检测字符cs在map集合中是否存在
            if(hashMap.containsKey(cs)){//检测key是否在map集合中，如果存在返回true
                count = hashMap.get(cs)+1;
            }
            hashMap.put(cs,count);
        }

        for (Map.Entry<Character,Integer> entry:hashMap.entrySet()){
            System.out.println(entry.getKey()+"<------->"+entry.getValue());
        }
    }
}
