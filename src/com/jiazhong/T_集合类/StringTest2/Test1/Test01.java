package com.jiazhong.T_集合类.StringTest2.Test1;

import java.util.HashSet;

/**
（1）统计去掉重复后的字符
 */
public class Test01 {
    public static void main(String[] args) {
        String str="adhflkalkfdhasdkhflsa";
        HashSet<Character> hashSet=new HashSet<>();
        for (int i=0;i< str.length();i++){
            hashSet.add(str.charAt(i));
        }
        for (char c:hashSet){
            System.out.println(c);
        }
       }
}
