package com.jiazhong.T_集合类.StringTest2.Test3;

public class Account {
    private long id;
    private double balance;
    private String password;
    private static long count=1;

    public Account(double balance, String password) {
        id=count++;
        this.balance = balance;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                ", password='" + password + '\'' +
                '}';
    }
}
