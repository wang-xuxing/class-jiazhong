package com.jiazhong.T_集合类.StringTest2.Test3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test03 {
    public static void main(String[] args) {
        List<Account> list = new ArrayList<>();
        list.add(new Account(10.00, "1234"));
        list.add(new Account(15.00, "5678"));
        list.add(new Account(0, "1010"));

        Map<Long,Account> map=new HashMap<>();
        for (Account account:list){
            map.put(account.getId(),account);
        }
        for (Map.Entry<Long,Account> entry:map.entrySet()){
            System.out.println(entry.getKey()+"<----->"+entry.getValue());
        }

    }
}
