package com.jiazhong.T_集合类.StringTest2.Test2;

import java.util.*;

public class Test02 {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1930, "乌拉圭");
        map.put(1934, "意大利");
        map.put(1938, "意大利");
        map.put(1950, "乌拉圭");
        map.put(1954, "西德");
        map.put(1958, "巴西");
        map.put(1962, "巴西");
        map.put(1966, "英格兰");
        map.put(1970, "巴西");
        map.put(1974, "西德");
        map.put(1978, "阿根廷");
        map.put(1982, "意大利");
        map.put(1986, "阿根廷");
        map.put(1990, "西德");
        map.put(1994, "巴西");
        map.put(1998, "法国");
        map.put(2002, "巴西");
        map.put(2006, "意大利");
        map.put(2010, "西班牙");
        map.put(2014, "德国");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个字符串表示年份:");
        Integer year = scanner.nextInt();
        if (map.containsKey(year)) {
            System.out.println("该年的世界杯冠军:" + map.get(year));
        } else {
            System.out.println("没有举办世界杯!");
        }
        System.out.println("请读入一支球队的名字：");
        String teamName = scanner.next();
        if (map.containsValue(teamName)) {
            List<Integer> list = new ArrayList<>();
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                if (entry.getValue().equals(teamName)) {
                    list.add(entry.getKey());
                }
            }
                System.out.println(teamName+"获得冠军的年份:");
                for(Integer num : list){
                    System.out.print(num+" ");
                }

        } else {
            System.out.println("没有获得过世界杯");
        }
    }
}

