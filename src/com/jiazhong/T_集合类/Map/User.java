package com.jiazhong.T_集合类.Map;

public class User implements Comparable<User>{
    private  Integer ID;
    private  String name;
    private  Integer age;
    private static Integer count=1;
    public User() {
    }
    public User(String name, Integer age) {
        ID=count++;
        this.name = name;
        this.age = age;
    }
    public Integer getID() {
        return ID;
    }
    public String getName() {
        return name;
    }
    public Integer getAge() {
        return age;
    }
    @Override
    public String toString() {
        return "User{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    @Override
    public int compareTo(User user) {
        return -(this.ID-user.ID);
}
}
