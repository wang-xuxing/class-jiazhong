package com.jiazhong.T_集合类.Map;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Tree_Map {
    public static void main(String[] args) {
        List<User> list= new ArrayList<>();
        list.add(new User("mike",18));
        list.add(new User("janny",22));
        list.add(new User("danny",21));
        Map<Integer,User> treeMap=new TreeMap<>();
        for (User user:list){
            treeMap.put(user.getID(),user);
        }
//        for (Map.Entry<Integer,User> entry:treeMap.entrySet()){
//            System.out.println(entry.getKey()+"<----->"+entry.getValue());
//        }
        for (Integer key:treeMap.keySet()){
            System.out.println(key+"<------>"+treeMap.get(key));
        }
    }
}
