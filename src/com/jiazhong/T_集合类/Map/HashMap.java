package com.jiazhong.T_集合类.Map;

import java.util.Iterator;
import java.util.Map;

public class HashMap {
    public static void main(String[] args) {
        Map<String,String> hashMap =new java.util.HashMap<>();
        hashMap.put("10","100");
        hashMap.put("20","200");
        hashMap.put("30","300");
        hashMap.put("40","400");
        hashMap.put("50","500");
        hashMap.put("60","600");
        hashMap.put("70","700");
        hashMap.put("80","800");
        hashMap.put("90","900");
        hashMap.put("100","1000");
//        使用Entry
       for ( Map.Entry<String,String> entry:hashMap.entrySet()){
           System.out.println(entry.getKey()+"<-------->"+entry.getValue());
       }

//        使用keySey
        //获取hashmap中所有元素
        System.out.println(hashMap.keySet());
        for (String map :hashMap.keySet()){
            System.out.println(map+"<--------->"+hashMap.get(map));
        }
    }
}
