package com.jiazhong.T_集合类.Collection.Demo;

import java.util.ArrayList;
import java.util.Iterator;

//产生1-10之间的随机数([1,10]闭区间)，将不重复的10个随机数放到容器中。
public class ListDemo {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList=new ArrayList<>();
        while (true){
            //生成随机数
            int nums=(int) (Math.random()*10+1);
//            如果列表不存在nums，向arrayList加入nums
            if (!arrayList.contains(nums)){
                arrayList.add(nums);
                }
            if (arrayList.size()==10){
                break;
            }

        }
        System.out.println("随机生成数为：");
        Iterator<Integer> iterator= arrayList.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next()+" ");
        }
        System.out.println("\n-------------检查生成了几个数");
        System.out.println(arrayList.size());
    }
}
