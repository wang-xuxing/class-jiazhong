package com.jiazhong.T_集合类.Collection.Demo;

import java.util.HashSet;

public class SetDemo {
    public static void main(String[] args) {
        HashSet<Integer> hashSet=new HashSet<>();
        while (true){
            int nums=(int)(Math.random()*10+1);
            hashSet.add(nums);
            if (hashSet.size()==10){
                break;
            }
        }
        System.out.println("随机生成不重复十个整数");
        for (Integer integer:hashSet){
            System.out.print(integer+"  ");
        }
    }
}
