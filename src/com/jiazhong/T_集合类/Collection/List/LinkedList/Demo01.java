package com.jiazhong.T_集合类.Collection.List.LinkedList;

import java.util.Iterator;
import java.util.LinkedList;

public class Demo01 {
    public static void main(String[] args) {
        LinkedList<String> linkedList=new LinkedList<>();
        linkedList.add("a");
        linkedList.add("b");
        System.out.println(linkedList.get(1));
        linkedList.push("c");
        System.out.println(linkedList.get(0));
        linkedList.add("c");
        Iterator<String> iterator=linkedList.iterator();
        System.out.println("-----------------");
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
