package com.jiazhong.T_集合类.Collection.List.ArrayList;

import java.util.ArrayList;
import java.util.Iterator;

public class Demo03 {
    public static void main(String[] args) {
        ArrayList<String> arrayList=new ArrayList<>();
        arrayList.add("1");
        arrayList.add("2");
        ArrayList<String> arrayList1=new ArrayList<>();
        arrayList1.add("我是一");
        arrayList1.add("我是二");
        arrayList.addAll(arrayList1);
        Iterator<String> iterator= arrayList.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("----------集合交集操作---------");
        ArrayList<String> arrayList2=new ArrayList<>();
        arrayList2.add("1");
        arrayList2.add("我是二");
        arrayList2.add("我是一");
        ArrayList<String> arrayList3=new ArrayList<>();
        arrayList3.add("我是一");
        arrayList3.add("我是二");
        boolean flag= arrayList2.retainAll(arrayList3);//判断是否交集成功
        System.out.println(flag);
        Iterator<String> iterator1=arrayList2.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }
        System.out.println("---------------容器中差集操作");
        ArrayList<String> arrayList4=new ArrayList<>();
        arrayList4.add("1");
        arrayList4.add("我是二");
        arrayList4.add("我是一");
        ArrayList<String> arrayList5=new ArrayList<>();
        arrayList5.add("我是一");
        arrayList5.add("我是二");
        boolean flag1=arrayList4.removeAll(arrayList5);
        for (String str:arrayList4){
            System.out.println(str);
        }
    }

}
