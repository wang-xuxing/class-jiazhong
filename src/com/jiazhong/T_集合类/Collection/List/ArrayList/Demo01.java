package com.jiazhong.T_集合类.Collection.List.ArrayList;

import java.util.ArrayList;
import java.util.Iterator;


public class Demo01 {
    public static void main(String[] args) {
        ArrayList<Object> arrayList=new ArrayList<>();
        arrayList.add('a');
        arrayList.add('b');
        arrayList.add('c');
        arrayList.add("dad");
        arrayList.add('e');
        arrayList.add("dad");
        arrayList.add("dad");
        arrayList.add(1);
        arrayList.add(123.15);
        arrayList.add(true);
        arrayList.add(2,"ian 2");
        arrayList.remove(1);
        arrayList.remove("dad");
        System.out.println(arrayList.get(2));
        for (Object o:arrayList){
            System.out.print(o+" ");
        }
        System.out.println();
        System.out.println("-------------");
        Iterator iterator= arrayList.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next()+" ");
        }
        System.out.println("-------替换元素--------");
        arrayList.set(2,"我被替换");
        System.out.println(arrayList.get(2));
        Iterator iterator1=arrayList.iterator();
        while (iterator1.hasNext()){
            System.out.print(" "+iterator1.next());
        }
        System.out.println("\n---");
        System.out.println(arrayList.contains("我被替换")+"\n"+arrayList.isEmpty());
    }
}
