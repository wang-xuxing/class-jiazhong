package com.jiazhong.T_集合类.Collection.List.ArrayList;

import java.util.ArrayList;
import java.util.Iterator;

public class Demo02 {
    public static void main(String[] args) {
        ArrayList arrayList=new ArrayList();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        Iterator iterator =arrayList.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("------删除为类型整数的元素-----------");
        arrayList.remove(new Integer(1));
        System.out.println(arrayList.get(0)+"\nArrayList长度："+arrayList.size());
        arrayList.set(0,1);
        //插入元素内存移位,插入效率低
        arrayList.add(0,2);
        System.out.println(arrayList.get(0));
        System.out.println(arrayList.get(1));
    }

}
