package com.jiazhong.T_集合类.Collection.List.Vector.Stark;

import java.util.Stack;

public class Demo01 {
    public static void main(String[] args) {
        //实例化栈容器
        Stack<String> stack=new Stack<>();
        //入栈
        stack.push("one");
        stack.push("two");
        stack.push("three");
        //查看栈顶元素
        System.out.println(stack.peek());
        //返回栈元素所在容器位置
        System.out.println(stack.search("one"));
        //获取栈容器元素
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
       //检查栈容器是否为空
        System.out.println(stack.empty());
          stack.peek();
    }

}
