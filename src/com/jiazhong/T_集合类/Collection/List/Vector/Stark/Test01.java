package com.jiazhong.T_集合类.Collection.List.Vector.Stark;

import java.util.Stack;

public class Test01 {
    public static void main(String[] args) {
        String str="... {.....[....(....)...]....}..(....)..[...]..." ;
        Stack<String> stack=new Stack<>();
        //假设修正法
        boolean flag=true;//假设是匹配的
        //拆分字符串获取字符
        for (int i=0;i<str.length();i++){
            char chars=str.charAt(i);
            if (chars == '['){
                stack.push("]");
            }
            if (chars =='{'){
                stack.push("}");
            }
            if (chars =='('){
                stack.push(")");
            }
            //判断符号是否匹配
            if (chars =='}'||chars==']'||chars==')'){
                if (stack.empty()){
                    flag=false;
                    break;
                }
                String str1= stack.pop();
                if (str1.charAt(0)!=chars){
                    flag=false;
                    break;
                }
            }
        }
        if (!stack.empty()){
            flag=false;
        }
        System.out.println(flag);
    }
}
