package com.jiazhong.T_集合类.Collection.常用类;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(5);
        list.add(3);
        for (int is:
             list) {
            System.out.println(is);
        }
        System.out.println("---------------------");
        System.out.println(Collections.binarySearch(list,1));
        System.out.println("--------------------");
        Collections.addAll(list,2,3,5,6,9,7,4);
        for (int iss:list){
            System.out.println(iss);
        }
    }
}
