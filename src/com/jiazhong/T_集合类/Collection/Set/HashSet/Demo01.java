package com.jiazhong.T_集合类.Collection.Set.HashSet;

import java.util.HashSet;
import java.util.Iterator;

public class Demo01 {
    public static void main(String[] args) {
        HashSet<String> hashSet=new HashSet<>();
        hashSet.add("a");
        hashSet.add("b");
        hashSet.add("c");
        hashSet.add("b");
        Iterator iterator=hashSet.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("-----------");
        for (String str:hashSet){
            System.out.println(str);
        }
        hashSet.remove("c");
        System.out.println("----00");
        for (String str:hashSet){
            System.out.println(str);
        }
        System.out.println("----------HashSet存储自定义对象-----------");
        HashSet<User> users=new HashSet<>();
        users.add(new User("1","alan"));
        users.add(new User("1","alan"));
        Iterator<User> iterator1= users.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }
        System.out.println("--------");
        User user=new User("2","mike");
        System.out.println(user);
        System.out.println("----------");
        System.out.println(user);
    }
}
