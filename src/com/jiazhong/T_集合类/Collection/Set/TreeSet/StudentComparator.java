package com.jiazhong.T_集合类.Collection.Set.TreeSet;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student student, Student student1) {
        if (student.getId()>student1.getId()){
            return 1;
        }
        if (student.getId()==student1.getId()){
            return student.getName().compareTo(student1.getName());
        }
        return -1;
    }
}
