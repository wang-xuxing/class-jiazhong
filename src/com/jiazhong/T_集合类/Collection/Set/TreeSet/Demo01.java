package com.jiazhong.T_集合类.Collection.Set.TreeSet;
import java.util.Iterator;
import java.util.TreeSet;

public class Demo01 {
    public static void main(String[] args) {
        TreeSet<String> treeSet=new TreeSet<>();
        //添加元素
        treeSet.add("a");
        treeSet.add("c");
        treeSet.add("d");
        treeSet.add("a");
        treeSet.add("b");
        //获取元素
        Iterator<String> iterator=treeSet.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");
        TreeSet<User> treeSet1=new TreeSet<>();
        treeSet1.add(new User("1","mike"));
        treeSet1.add(new User("1","alen"));
        treeSet1.add(new User("5","like"));
        Iterator<User> iterator1=treeSet1.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }

        System.out.println("-----------------------------------");
        TreeSet<Student> treeSet2=new TreeSet<>(new StudentComparator());
        treeSet2.add(new Student(1,"mike"));
        treeSet2.add(new Student(1,"alen"));
        treeSet2.add(new Student(5,"like"));
        Iterator<Student> iterator2=treeSet2.iterator();
        while (iterator2.hasNext()){
            System.out.println(iterator2.next());
        }
    }
}
