package com.jiazhong.T_集合类.Collection.Set.TreeSet;

import java.util.Objects;

public class User implements Comparable<User>{
    private String id;
    private transient String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
//定义比较规则
//正数 大 负数 小 0：相等
//    按照id定义
    @Override
    public int compareTo(User user) {
        if (Integer.parseInt(this.id)>Integer.parseInt(user.id)){
            return 1;
        }
        if (Integer.parseInt(this.id)==Integer.parseInt(user.id)){
            return this.name.compareTo(user.name);
        }
        return -1;
    }
}
