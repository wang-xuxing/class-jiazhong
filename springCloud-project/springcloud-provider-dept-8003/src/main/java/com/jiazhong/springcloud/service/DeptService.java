package com.jiazhong.springcloud.service;

import com.jiazhong.springcloud.bean.Dept;

import java.util.List;

public interface DeptService {

    void addDept(Dept dept);

    Dept getDeptById(int deptno);
    List<Dept> getAllDept();
}
