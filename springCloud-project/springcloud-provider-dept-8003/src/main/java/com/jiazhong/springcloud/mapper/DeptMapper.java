package com.jiazhong.springcloud.mapper;

import com.jiazhong.springcloud.bean.Dept;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DeptMapper {

    @Insert("insert into db03.dept values(default,#{dname},DATABASE())")
    void addDept(Dept dept);

    @Select("select * from db03.dept where deptno=#{deptno}")
    Dept getDeptById(int deptno);

    @Select("select * from db03.dept")
    List<Dept> getAllDept();
}
