package com.jiazhong.springcloud.Conntroller;


import com.jiazhong.springcloud.bean.Dept;
import com.jiazhong.springcloud.service.DeptService;
import com.jiazhong.springcloud.utils.Result;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

//提供Restful服务
@RestController
@RequestMapping("/dept")
public class DeptController {
    @Resource
    private DeptService deptService;
    @Resource
    private DiscoveryClient client;
    @PostMapping("/")
    public Result addDept(@RequestBody Dept dept){
        try {
            deptService.addDept(dept);
            return Result.success("数据添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(500,"数据添加失败!!!");
        }
    }
    @GetMapping("/{deptno}")
    public Dept getDeptById(@PathVariable("deptno") int deptno){
        return deptService.getDeptById(deptno);
    }
    @GetMapping("/")
    List<Dept> getAllDept(){
        return deptService.getAllDept();
    }


    @GetMapping("/discovery")
    public Object discovery(){
        //得到微服务清单
        List<String> services = client.getServices();
        System.out.println("discovery=>services:"+services);
        //得到具体微服务信息
        List<ServiceInstance> instances = client.getInstances("SPRINGCLOUD-PROVIDER-DEPT");
        for (ServiceInstance instance : instances) {
            System.out.println(instance.getHost()+"\n"+instance.getInstanceId()+"\n"+instance.getUri());
        }
        return this.client;
    }

}
