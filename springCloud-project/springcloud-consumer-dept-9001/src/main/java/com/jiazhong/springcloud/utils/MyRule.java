package com.jiazhong.springcloud.utils;


import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义策略，里面可以自定义自己的策略
 */
@Configuration
public class MyRule {


    //随机策略
    public IRule myRule() {
        return new RandomRule();
    }
}
