package com.jiazhong.springcloud.controller;




import com.jiazhong.springcloud.bean.Dept;
import com.jiazhong.springcloud.utils.Result;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/consumer/dept")
public class DeptConsumerController {
    /**
     * 消费者不能有service层
     * RestTemplate:Rest模版供我们直接调用，注册到spring中
     * (url,实体:Map,Class<T> responseType)
     */
    @Resource
    private RestTemplate restTemplate;
    //ribbon:我们这里的地址，应该是一个变量，通过服务名来访问
    //private static  final String REST_URL_PREFIX ="http://localhost:8001";
    private static  final String REST_URL_PREFIX ="http://SPRINGCLOUD-PROVIDER-DEPT";
    @GetMapping("/{id}")
    public Dept get(@PathVariable int id){
        return restTemplate.getForObject(REST_URL_PREFIX+"/dept/"+id, Dept.class);
    }
    @PostMapping("/")
    public Result add(@RequestBody Dept dept){
        try {
            restTemplate.postForObject(REST_URL_PREFIX+"/dept/",dept,Result.class);
            return Result.success("数据添加成功");
        } catch (RestClientException e) {
            e.printStackTrace();
            return Result.fail(500,"数据添加失败!!!");
        }
    }
    @GetMapping("/")
    public List<Dept> getAll(){
        return restTemplate.getForObject(REST_URL_PREFIX+"/dept/",List.class);
    }
}
