package com.jiazhong.springcloud;

import com.jiazhong.springcloud.utils.MyRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableEurekaClient
//@RibbonClient(name = "SPRINGCLOUD-PROVIDER-DEPT",configuration = MyRule.class)
public class DeptConsumer_9001 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_9001.class);
    }
    @Bean
    @LoadBalanced //配置负载均衡
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}