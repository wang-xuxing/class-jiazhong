package com.jiazhong.springcloud.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)//链式写法
public class Dept implements Serializable {
    private Long deptno;
    private String dname;
    // 这些数据存放在哪个数据库的字段
    // 微服务，一个服务对应一个数据库，同一个信息可能存在不同数据库
    private String dbsource;


    public Dept(String dname) {
        this.dname = dname;

    }
}
