package com.jiazhong.springcloud.service;


import com.jiazhong.springcloud.bean.Dept;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;

/**
 * value:服务名
 */
@FeignClient(value = "SPRINGCLOUD-PROVIDER-DEPT",path = "/consumer/dept")//写了该注解后可被服务直接调用
@Component
public interface DeptClientService {
    @PostMapping("/")
    void addDept(@RequestBody Dept dept);
    @GetMapping("{id}")
    Dept getDeptById(@PathVariable("id") int deptno);
    @GetMapping ("/")
    List<Dept> getAllDept();
}
