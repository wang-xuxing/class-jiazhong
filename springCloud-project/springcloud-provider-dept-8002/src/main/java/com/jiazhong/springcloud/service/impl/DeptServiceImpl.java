package com.jiazhong.springcloud.service.impl;

import com.jiazhong.springcloud.bean.Dept;
import com.jiazhong.springcloud.mapper.DeptMapper;
import com.jiazhong.springcloud.service.DeptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {
    @Resource
    private DeptMapper deptMapper;

    @Override
    public void addDept(Dept dept) {
         deptMapper.addDept(dept);

    }

    @Override
    public Dept getDeptById(int deptno) {
        return deptMapper.getDeptById(deptno);
    }

    @Override
    public List<Dept> getAllDept() {
        return deptMapper.getAllDept();
    }
}
