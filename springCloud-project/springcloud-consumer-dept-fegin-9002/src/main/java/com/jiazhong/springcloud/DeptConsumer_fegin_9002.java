package com.jiazhong.springcloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;


import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.jiazhong.springcloud")
public class DeptConsumer_fegin_9002 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_fegin_9002.class);
    }
    @Bean
    @LoadBalanced //配置负载均衡
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
