package com.jiazhong.springcloud.controller;


import com.jiazhong.springcloud.bean.Dept;
import com.jiazhong.springcloud.service.DeptClientService;
import com.jiazhong.springcloud.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/consumer/dept")
public class DeptConsumerController {
    /**
     * 消费者不能有service层
     * RestTemplate:Rest模版供我们直接调用，注册到spring中
     * (url,实体:Map,Class<T> responseType)
     */


    @Resource
    private DeptClientService deptClientService;

    @GetMapping("/{id}")
    public Dept get(@PathVariable int id) {
        return deptClientService.getDeptById(id);
    }

    @PostMapping("/")
    public Result add(@RequestBody Dept dept) {
        try {
            deptClientService.addDept(dept);
            return Result.success("数据添加成功");
        } catch (RestClientException e) {
            e.printStackTrace();
            return Result.fail(500, "数据添加失败!!!");
        }
    }

    @GetMapping("/")
    public List<Dept> getAll() {
        return deptClientService.getAllDept();
    }
}
