package com.jiazhong.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@MapperScan("com.jiazhong.springcloud.mapper")
@EnableEurekaClient  //开启后，在服务启动后自动注入到Eureka
@EnableDiscoveryClient
public class Dept_provider_8001 {
    public static void main(String[] args) {
        SpringApplication.run(Dept_provider_8001.class,args);
    }

}
