package com.jiazhong.curator;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CuratorTest {
    private CuratorFramework client;

    @Before
    public void testConnect() {

        /*
          重试策略
          参数1:间隔多长时间重试
          参数2:重试多少次
         */
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(3000, 10);
        /**
         * 第一种方式
         * 参数1 connectString:       连接字符串:zk server地址和端口 127.0.0.1:2181,127.0.0.1:2182
         * 参数2 sessionTimeoutMs:    会话超时时间: 单位ms
         * 参数3 connectionTimeoutMs: 连接超时时间: 单位ms
         * 参数4 retryPolicy:         重试策略
         */
//        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", 60 * 1000, 15 * 1000, retryPolicy);

//        client.start();
        //第二种方式
        client = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2181")
                .sessionTimeoutMs(60 * 1000)
                .connectionTimeoutMs(15 * 1000)
                .retryPolicy(retryPolicy)
                .namespace("hello") //设置操作根目录
                .build();
        client.start();

    }


//--------------------------创建操作----------------------------------------------------

    /**
     * 创建节点:create 临时  持久  顺序   数据
     * 1. 基本创建
     * 2. 设置节点，带有数据
     * 3. 设置节点类型
     * 4. 创建多级节点
     */
    @Test
    public void testCreat4() throws Exception {
        //设置节点，带有数据
        //creatingParentsIfNeeded，如果父节点不存在创建父节点
        client.create().creatingParentsIfNeeded().forPath("/app4/p1", "hello zookeeper".getBytes());
    }

    @Test
    public void testCreat3() throws Exception {
        //设置节点，带有数据
        client.create().withMode(CreateMode.EPHEMERAL).forPath("/app3", "hello zookeeper".getBytes());

        TimeUnit.MILLISECONDS.sleep(1000 * 60);
    }

    @Test
    public void testCreat2() throws Exception {
        //设置节点，带有数据
        client.create().forPath("/app2", "hello zookeeper".getBytes());
    }

    @Test
    public void testCreat() throws Exception {
        //基本创建
        //创建节点，若节点没有数据，则默认节点数据为ip地址
        client.create().forPath("/app1");
    }

//----------------------------------查询节点----------------------------------------------------

    /**
     * 1.查询节点数据 get
     * 2.查询子节点  ls
     * 3.查询节点状态信息 ls -s
     */
    @Test
    public void testGet3() throws Exception {
        //创建stat
        Stat stat = new Stat();
        System.out.println(stat);
        System.out.println("------查询之后-----");
        client.getData().storingStatIn(stat).forPath("/app4");
        System.out.println(stat);
    }

    @Test
    public void testGet2() throws Exception {
        List<String> list = client.getChildren().forPath("/");
        System.out.println(list);
    }

    @Test
    public void testGet() throws Exception {
        byte[] bytes = client.getData().forPath("/app1");
        System.out.println(new String(bytes));
    }


    //----------------------------------修改节点----------------------------------------------------
    @Test
    public void testSet2() throws Exception {
        Stat stat = new Stat();
        client.getData().storingStatIn(stat).forPath("/app1");
        int version =stat.getVersion();//查询当前节点状态的版本
        System.out.println(version);
        client.setData().withVersion(version).forPath("/app1", "hhahaha".getBytes());//修改一次vsrsion++
    }

    @Test
    public void testSet() throws Exception {
        client.setData().forPath("/app1", "setapp1date".getBytes());
    }


    //----------------------------------删除节点----------------------------------------------------

    /**
     * 1. 删除单个节点:delete
     * 2. 删除带有子节点的节点:deleteAll
     * 3. 必须成功的删除
     * 4. 回调
     */
    @Test
    public void testDelete4() throws Exception {
        client.delete().inBackground(new BackgroundCallback() {
            @Override
            public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
                System.out.println("删除成功....");
                System.out.println(curatorEvent);
            }
        }).forPath("/app1");
    }
    @Test
    public void testDelete3() throws Exception {
        client.delete().guaranteed().forPath("/app2");
    }
    @Test
    public void testDelete2() throws Exception {
        client.delete().deletingChildrenIfNeeded().forPath("/app4");
    }
    @Test
    public void testDelete() throws Exception {
        client.delete().forPath("/app1");
    }

    @After
    public void close() {
        if (client != null) {
            client.close();
        }
    }
}
