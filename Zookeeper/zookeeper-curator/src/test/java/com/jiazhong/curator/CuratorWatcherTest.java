package com.jiazhong.curator;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CuratorWatcherTest {
    private CuratorFramework client;




    @Before
    public void testConnect() {

        /*
          重试策略
          参数1:间隔多长时间重试
          参数2:重试多少次
         */
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(3000, 10);
        /**
         * 第一种方式
         * 参数1 connectString:       连接字符串:zk server地址和端口 127.0.0.1:2181,127.0.0.1:2182
         * 参数2 sessionTimeoutMs:    会话超时时间: 单位ms
         * 参数3 connectionTimeoutMs: 连接超时时间: 单位ms
         * 参数4 retryPolicy:         重试策略
         */
//        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", 60 * 1000, 15 * 1000, retryPolicy);

//        client.start();
        //第二种方式
        client = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2181")
                .sessionTimeoutMs(60 * 1000)
                .connectionTimeoutMs(15 * 1000)
                .retryPolicy(retryPolicy)
                .namespace("hello") //设置操作根目录
                .build();
        client.start();

    }
    @Test
    public void testCuratorCache() throws Exception {
        //1. 创建NodeCatch对象
        CuratorCache treeCache =CuratorCache.build(client,"/app2");
        //2. 注册监听
        treeCache.listenable().addListener(new CuratorCacheListener() {
            @Override
            public void event(Type type, ChildData oldData, ChildData data) {
                if (type.equals(Type.NODE_CHANGED)){//节点发生改变
                    String path = oldData.getPath();
                    System.out.println("节点"+path+"改变");
                    System.out.println("----------老数据-----------");
                    byte[] oldDataStr = oldData.getData();
                    System.out.println(new String(oldDataStr));
                    System.out.println("----------新数据-----------");
                    byte[] dataStr = data.getData();
                    System.out.println(new String(dataStr));
                }
            }
        });
        //3. 开启监听，若果设置为true，开启监听
        treeCache.start();
        //此处延时只是让，会话不会快速关闭，以便测试监听功能
        while (true){
        }
    }

    @Test
    public void testTreeCache() throws Exception {
        //1. 创建NodeCatch对象
        TreeCache treeCache =new TreeCache(client,"/app2");
        //2. 注册监听
        treeCache.getListenable().addListener(new TreeCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, TreeCacheEvent treeCacheEvent) throws Exception {
                System.out.println("节点变化");
                System.out.println(treeCacheEvent);
            }
        });
        //3. 开启监听，若果设置为true，开启监听
        treeCache.start();
        //此处延时只是让，会话不会快速关闭，以便测试监听功能
        while (true){
        }
    }



    @Test
    public void testPathChildrenCache() throws Exception {
        //1. 创建NodeCatch对象
        PathChildrenCache childrenCache =new PathChildrenCache(client,"/app2",true);
        //2. 注册监听
        childrenCache.getListenable().addListener(new PathChildrenCacheListener() {
            //类似于删除节点的回调
            @Override
            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                System.out.println("子节点变化了....");
                System.out.println(pathChildrenCacheEvent);
                //监听变更，拿到变更后的数据
                // 1.获取类型
                PathChildrenCacheEvent.Type type = pathChildrenCacheEvent.getType();
                //2. 判断类型是否是update
                if (type.equals(PathChildrenCacheEvent.Type.CHILD_UPDATED)){
                    System.out.println("数据改变");
                    byte[] data = pathChildrenCacheEvent.getData().getData();
                    System.out.println(new String(data));
                }
            }
        });
        //3. 开启监听，若果设置为true，开启监听
        childrenCache.start(true);
        //此处延时只是让，会话不会快速关闭，以便测试监听功能
        while (true){
        }
    }
    /**
     * 给单一节点注册监听器
     */
    @Test
    public void testNodeCatch() throws Exception {
        //1. 创建NodeCatch对象
        NodeCache nodeCache =new NodeCache(client,"/app1");
        //2. 注册监听
        nodeCache.getListenable().addListener(new NodeCacheListener() {
            @Override
            public void nodeChanged() throws Exception {
                System.out.println("节点改变了...");
                //获取修改后当前节点的数据
                byte[] data = nodeCache.getCurrentData().getData();
                System.out.println(new String(data));
            }
        });
        //3. 开启监听，若果设置为true，开启监听
        nodeCache.start(true);
        //此处延时只是让，会话不会快速关闭，以便测试监听功能
        while (true){
        }
    }


    @After
    public void close() {
        if (client != null) {
            client.close();
        }
    }
}
