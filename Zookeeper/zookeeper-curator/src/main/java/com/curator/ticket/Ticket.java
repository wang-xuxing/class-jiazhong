package com.curator.ticket;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessLock;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.util.concurrent.TimeUnit;

public class Ticket implements Runnable{
    private Integer tickets =20;//票数
    private CuratorFramework client;
    private InterProcessMutex lock;

    public Ticket() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(3000, 10);
        client = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2181")
                .sessionTimeoutMs(60 * 1000)
                .connectionTimeoutMs(15 * 1000)
                .retryPolicy(retryPolicy)
                .build();
        client.start();
        lock =new InterProcessMutex(client,"/lock");
    }

    @Override
    public void run() {
        while (true){
            //获取锁
            try {
                lock.acquire(3000, TimeUnit.MILLISECONDS);
                if (tickets>0){
                    System.out.println(Thread.currentThread()+":"+tickets);
                    TimeUnit.MILLISECONDS.sleep(100);
                    tickets--;
                }
                if (tickets==0){
                    System.out.println("票售空了...");
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                //释放锁
                try {
                    lock.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
