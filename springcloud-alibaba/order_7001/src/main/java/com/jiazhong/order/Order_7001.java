package com.jiazhong.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class Order_7001 {
    public static void main(String[] args) {
        SpringApplication.run(Order_7001.class);
    }

    @Bean
    @LoadBalanced//必须配置负载均衡
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
