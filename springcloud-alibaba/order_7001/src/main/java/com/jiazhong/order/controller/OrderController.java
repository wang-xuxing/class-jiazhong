package com.jiazhong.order.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private RestTemplate restTemplate;

    private static final String STOCK_URL ="http://stock-service";

    @RequestMapping("/add")
    public String add(){
        String message = restTemplate.getForObject(STOCK_URL + "/stock/del", String.class);
        System.out.println("下单成功!!!");
        return "下单成功!!!"+message;
    }

    @RequestMapping("/test1")
    public String test1(){
        return "1";
    }
    @RequestMapping("/test2")
    public String test2(){
        return "2";
    }


    @RequestMapping("/get")
    public String get(){
        System.out.println("查询订单!!!");
        return "查询订单!!!";
    }

    @RequestMapping("/flow")
//    @SentinelResource(value = "flow" , blockHandler = "flowBlockHandler")
    public String flow(){
        return "正常访问";
    }
    @RequestMapping("/flowThread")
//    @SentinelResource(value = "flowThread" , blockHandler = "flowBlockHandler")
    public String flowThread(){
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "正常访问";
    }


    @RequestMapping("/myError")
    public String myError(){
        int a=1/0;
        return "error";
    }

    /**
     * 热点规则:必须使用@SentinelResource
     * @param id
     * @return
     */
    @RequestMapping("/get/{id}")
    public String getById(@PathVariable("id") Integer id){
        System.out.println("查询商品"+id);
        return "查询商品"+id;
    }

}
