package com.jiazhong.sentinel.controller;


import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.EntryType;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.Tracer;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.Rule;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.jiazhong.sentinel.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
public class HelloController {
    private static final String RESOURCE_NAME = "hello";
    private static final String USER_RESOURCE_NAME = "user";
    private static final String DEGRADE_RESOURCE_NAME = "degrade";

    //进行Sentinel流控
    @RequestMapping("/hello")
    public String hello() {
        Entry entry = null;
        try {
            //把资源交给sentinel进行监控
            entry = SphU.entry(RESOURCE_NAME);
            //被保护业务逻辑
            String str = "hello word";
            log.info("---------" + str + "---------");
            return str;
        } catch (BlockException e) {
            log.info("block");
            return "被流控了!";
        } catch (Exception e) {
            //若需要配置降级规则，需要通过这种方式记录业务异常
            Tracer.traceEntry(e, entry);
        } finally {
            if (entry != null) {
                entry.exit();

            }
        }
        return null;
    }

    @RequestMapping("/user")
    @SentinelResource(value = USER_RESOURCE_NAME, fallback = "fallbackHandlerForGetUser", blockHandler = "blockHandlerForGetUser")
    public User getUser(String id) {
        throw new RuntimeException("抛出异常");
//        return new User("huozhexiao");
    }

    @RequestMapping("/degrade")
    @SentinelResource(value = DEGRADE_RESOURCE_NAME, entryType = EntryType.IN, blockHandler = "blockHandlerForFb")
    public User degrade(String id) {
        throw new RuntimeException("抛出异常");
//        return new User("huozhexiao");
    }

    /**
     * 注意:
     * 1. 一定要是public
     * 2. 返回值要与资源方法保持一致，包含源方法的参数
     * 3.  可以在参数后添加BlockException，可以区分是什么方法的处理方法
     *
     * @param id
     * @param e
     * @return
     */
    public User blockHandlerForFb(String id, BlockException e) {
        e.printStackTrace();
        return new User("降级处理!!!");
    }

    public User blockHandlerForGetUser(String id, BlockException e) {
        e.printStackTrace();
        return new User("被流控了!!");
    }

    public User fallbackHandlerForGetUser(String id, Throwable e) {
        e.printStackTrace();
        return new User("异常处理");
    }

    /**
     * 定义流控规则
     */
    @PostConstruct   //被该注解修饰后，该该创建后，会自动调用被改注解修饰的方法
    private static void initFlowRules() {
        //流控规则集合
        List<FlowRule> rules = new ArrayList<>();
        //流控
        FlowRule rule = new FlowRule();
        //设置被保护的资源
        rule.setResource(RESOURCE_NAME);
        //设置流控规则:QPS
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // 设置受保护阈值为1
        rule.setCount(1);//
        //添加规则
        rules.add(rule);


        /**
         *为USER_RESOURCE_NAME设置流控规则
         */
        FlowRule rule2 = new FlowRule();
        //设置被保护的资源
        rule.setResource(USER_RESOURCE_NAME);
        //设置流控规则:QPS
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // 设置受保护阈值为1
        rule.setCount(1);//
        //添加规则
        rules.add(rule2);

        //加载配置的规则
        FlowRuleManager.loadRules(rules);
    }

    /**
     * 定义降级规则
     */
    @PostConstruct
    public void initDegradeRule() {
        //降级规则集合
        List<DegradeRule> degradeRules = new ArrayList<>();

        DegradeRule degradeRule = new DegradeRule();
        degradeRule.setResource(DEGRADE_RESOURCE_NAME);
        //设置规则侧率:异常数
        degradeRule.setGrade(RuleConstant.DEGRADE_GRADE_EXCEPTION_COUNT);
        //阈值
        //异常数
        degradeRule.setCount(2);
        //最小请求数
        degradeRule.setMinRequestAmount(2);
        //统计时长:1min
        degradeRule.setStatIntervalMs(60 * 1000);
        /*熔断持续时长:出现熔断后，10s内调用我们的降级方法
        10s后会进入半开状态:恢复接口请求，如果第一次请求就异常，再次熔断，不会根据设置条件进行判定
         */
        degradeRule.setTimeWindow(10);

        degradeRules.add(degradeRule);
        DegradeRuleManager.loadRules(degradeRules);
    }
}
