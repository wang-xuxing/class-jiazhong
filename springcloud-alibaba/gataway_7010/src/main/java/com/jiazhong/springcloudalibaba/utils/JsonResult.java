package com.jiazhong.springcloudalibaba.utils;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonResult<T> implements Serializable {
    private Boolean success;
    private String code;
    private T data;
    private String message;
}
