//package com.jiazhong.springcloudalibaba.filter;
//
//import com.alibaba.fastjson.JSONArray;
//import com.jiazhong.springcloudalibaba.utils.JsonResult;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.core.io.buffer.DataBuffer;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.server.reactive.ServerHttpResponse;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.nio.charset.StandardCharsets;
//import java.util.List;
//
//@Component
//@Slf4j
//public class TokenGlobalFilter implements GlobalFilter, Ordered {
//    //鉴权
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        log.info("开始鉴权...");
//        List<String> tokens = exchange.getRequest().getHeaders().get("token");
//        if (tokens != null || tokens.size() == 0) {
//            return error(exchange);
//        }
//        //获取token
//        String token = tokens.get(0);
//        log.info("token:{}",token);
//        //token判断
//        return chain.filter(exchange);
//    }
//
//    private Mono<Void> error(ServerWebExchange exchange) {
//        // 浏览器没有携带token
//        // 获取响应对象
//        ServerHttpResponse response = exchange.getResponse();
//        // 设置响应状态码
//        response.setStatusCode(HttpStatus.resolve(403));
//        // 设置返回格式
//        response.getHeaders().set("ContentType", "application/json;charset=utf-8");
//        // 设置返回的内容
//        JsonResult result = new JsonResult(true,"200",null,"用户未登陆...");
//        String str = JSONArray.toJSONString(result);
//        DataBuffer data = response.bufferFactory().wrap(str.getBytes(StandardCharsets.UTF_8));
//        log.info("⽤户未携带token,{}", str);
//        return response.writeWith(Mono.just(data));
//    }
//
//
//
//    //优先级
//    @Override
//    public int getOrder() {
//        return 1;
//    }
//}
