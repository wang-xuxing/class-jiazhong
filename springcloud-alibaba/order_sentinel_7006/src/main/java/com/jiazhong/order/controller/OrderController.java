package com.jiazhong.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.jiazhong.order.Service.OrderService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private OrderService orderService;
    @RequestMapping("/test1")
    public String test1(){
        return orderService.getUser()+"1";
    }
    @RequestMapping("/test2")
    public String test2(){
        return orderService.getUser()+"2";
    }

    @RequestMapping("/add")
    public String add(){
        System.out.println("下单成功!!!");
        return "下单成功!!!";
    }
    @RequestMapping("/get")
    public String get(){
        System.out.println("查询订单!!!");
        return "查询订单!!!";
    }

    @RequestMapping("/flow")
//    @SentinelResource(value = "flow" , blockHandler = "flowBlockHandler")
    public String flow(){
        return "正常访问";
    }
    @RequestMapping("/flowThread")
//    @SentinelResource(value = "flowThread" , blockHandler = "flowBlockHandler")
    public String flowThread(){
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "正常访问";
    }
    public String flowBlockHandler(BlockException e){
        return "流控";
    }

    @RequestMapping("/myError")
    public String myError(){
        int a=1/0;
        return "error";
    }

    /**
     * 热点规则:必须使用@SentinelResource
     * @param id
     * @return
     */
    @RequestMapping("/get/{id}")
    @SentinelResource(value = "getById",blockHandler = "HotBlockHandler")
    public String getById(@PathVariable("id") Integer id){
        System.out.println("查询商品"+id);
        return "查询商品"+id;
    }
    public String HotBlockHandler(@PathVariable("id") Integer id,BlockException e){
        return "热点异常处理";
    }
}
