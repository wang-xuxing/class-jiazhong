package com.jiazhong.order.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    private Integer code;
    private String msg;
    private String data;
    private Boolean success;


    public static Result error(Integer code , String msg){
        return new Result(code,msg,null,false);
    }
}
