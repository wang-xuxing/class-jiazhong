package com.jiazhong.order.interceptor.fegin;

import org.slf4j.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.LoggerFactory;

public class MyFeginInterceptor implements RequestInterceptor {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 可以做一些拦截
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("xxx", "xxxx");
        requestTemplate.query("id", "111");
        requestTemplate.uri("/9");

        logger.info("一些日志信息");
    }
}
