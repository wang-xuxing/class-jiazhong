package com.jiazhong.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
//@EnableDiscoveryClient
@EnableFeignClients//启动fegin
public class Order_openfegin_7004 {
    public static void main(String[] args) {
        SpringApplication.run(Order_openfegin_7004.class);
    }

}
