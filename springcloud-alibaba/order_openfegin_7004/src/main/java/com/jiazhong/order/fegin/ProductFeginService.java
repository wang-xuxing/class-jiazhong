package com.jiazhong.order.fegin;

import com.jiazhong.order.conf.FeginConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
@FeignClient(name = "product-service",path = "/product",configuration = FeginConfig.class)
public interface ProductFeginService {

    @RequestMapping("/{id}")
    public String get(@PathVariable("id")/*不指定id会报错*/ Integer id);
}
