package com.jiazhong.order.conf;

import com.jiazhong.order.interceptor.fegin.MyFeginInterceptor;
import feign.Contract;
import feign.Logger;
import feign.Request;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 全局配置: 当使用@Configuration，会将配置作用于服务提供方
 * 局部配置: 如果只想针对某一个(几个)服务进行配置就不要加@Configuration
 */
//@Configuration
public class FeginConfig {

    @Bean
    Logger.Level feginLoggerLevel() {
        return Logger.Level.FULL;
    }

    /**
     * 修改契约配置，支持原生注解
     */
    /*
    @Bean
    Contract feginContract() {
        return new Contract.Default();
    }

    */


    /**
     * 配置超时时间
     */
    /*
    @Bean
    Request.Options options(){
        return new Request.Options(5000,10000);
    }
    */

    /**
     * 自定义拦截器
     */
    /*
    @Bean
    public MyFeginInterceptor myFeginInterceptor(){
        return new MyFeginInterceptor();
    }
     */
}
