package com.jiazhong.order.fegin;



import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 参数1:指定调用服务名
 * 参数2:指定调用路径,路径为@RequestMapping指定路径，若没有@RequestMapping则不需指定路径
 */
@FeignClient(name = "stock-service", path = "/stock ")
public interface StockFeginService {
    //声明需要调用的方法
    @RequestMapping("/del")
    String del();
}
