package com.jiazhong.springcloudalibaba.stock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.jiazhong.springcloudalibaba.stock.mapper")
public class AlibabaSeataStockApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlibabaSeataStockApplication.class,args);
    }


}
