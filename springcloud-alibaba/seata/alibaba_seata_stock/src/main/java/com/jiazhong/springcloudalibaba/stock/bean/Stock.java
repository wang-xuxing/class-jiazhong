package com.jiazhong.springcloudalibaba.stock.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock implements Serializable {

    private Integer id;
    private Integer product_id;
    private Integer count;

}
