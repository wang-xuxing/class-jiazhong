package com.jiazhong.springcloudalibaba.stock.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface StockMapper {
    @Update("update seata_stock.stock_tbl set count=count-1 where product_id=#{productId}")
    void reduct(Integer productId);
}
