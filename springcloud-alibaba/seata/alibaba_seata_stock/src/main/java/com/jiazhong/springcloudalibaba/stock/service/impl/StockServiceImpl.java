package com.jiazhong.springcloudalibaba.stock.service.impl;


import com.jiazhong.springcloudalibaba.stock.mapper.StockMapper;
import com.jiazhong.springcloudalibaba.stock.service.StockService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StockServiceImpl implements StockService {

    @Resource
    private StockMapper stockMapper;

    @Override
    public void reduct(Integer productId) {
        System.out.println("更新商品:"+ productId);
        stockMapper.reduct(productId);
    }
}
