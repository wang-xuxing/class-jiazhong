package com.jiazhong.springcloudalibaba.stock.controller;


import com.jiazhong.springcloudalibaba.stock.service.StockService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/stock")
public class StockController {

    @Resource
    private StockService stockService;
    @RequestMapping("/reduct")
    public String reduct(Integer productId) {
        stockService.reduct(productId);

        return "库存扣减成功!!!";
    }

}
