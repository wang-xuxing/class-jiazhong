package com.jiazhong.springcloudalibaba.order.mapper;

import com.jiazhong.springcloudalibaba.order.bean.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper {

    @Insert("insert into seata_order.order_tbl values (default,#{product_id},#{total_amount},#{status})")
    void add(Order order);
}
