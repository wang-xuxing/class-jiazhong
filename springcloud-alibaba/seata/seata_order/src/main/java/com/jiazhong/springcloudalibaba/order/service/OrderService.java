package com.jiazhong.springcloudalibaba.order.service;

import com.jiazhong.springcloudalibaba.order.bean.Order;

public interface OrderService {
    Order create(Order order);
}
