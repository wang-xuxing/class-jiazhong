package com.jiazhong.springcloudalibaba.order.service.Impl;

import com.jiazhong.springcloudalibaba.order.bean.Order;
import com.jiazhong.springcloudalibaba.order.mapper.OrderMapper;
import com.jiazhong.springcloudalibaba.order.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Service
public class OrderServiceImpl implements OrderService {


    @Resource
    private OrderMapper orderMapper;
    @Resource
    private RestTemplate restTemplate;
    @Override
    @Transactional
    public Order create(Order order) {
        orderMapper.add(order);

        //扣减库存能否成功
        MultiValueMap<String,Object> paramMap =new LinkedMultiValueMap<>();
        paramMap.add("productId",order.getProduct_id());
        String msg = restTemplate.postForObject("http://localhost:7101/stock/reduct",paramMap,String.class);
        int a =1/0;
        return order;

    }
}
