package com.jiazhong.springcloudalibaba.order.controller;


import com.jiazhong.springcloudalibaba.order.bean.Order;
import com.jiazhong.springcloudalibaba.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private OrderService orderService;

    @RequestMapping("/add")
    public String add(){
        Order order =new Order();
        order.setProduct_id(9);
        order.setStatus(0);
        order.setTotal_amount(100);
        orderService.create(order);
        return "下单成功";
    }
}
