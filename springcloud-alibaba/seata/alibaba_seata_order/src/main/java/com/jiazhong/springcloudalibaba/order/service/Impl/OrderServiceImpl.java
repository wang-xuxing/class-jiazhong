package com.jiazhong.springcloudalibaba.order.service.Impl;

import com.jiazhong.springcloudalibaba.order.bean.Order;
import com.jiazhong.springcloudalibaba.order.fegin.StockFegin;
import com.jiazhong.springcloudalibaba.order.mapper.OrderMapper;
import com.jiazhong.springcloudalibaba.order.service.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.skywalking.apm.toolkit.trace.Tag;
import org.apache.skywalking.apm.toolkit.trace.Tags;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Service
public class OrderServiceImpl implements OrderService {


    @Resource
    private OrderMapper orderMapper;
    @Resource
    private StockFegin stockFegin;

    @Override
    @GlobalTransactional
    @Trace
    @Tags({@Tag(key = "param",value = "arg[0]"),
            @Tag(key = "order",value = "returnObj")})
    public Order create(Order order) {
        orderMapper.add(order);

        //扣减库存能否成功
        stockFegin.reduct(order.getProduct_id());

        return order;

    }
}
