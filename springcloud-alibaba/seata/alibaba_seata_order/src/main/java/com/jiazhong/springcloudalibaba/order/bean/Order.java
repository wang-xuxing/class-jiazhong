package com.jiazhong.springcloudalibaba.order.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {

    private Integer id;
    private Integer product_id;
    private Integer total_amount;
    private Integer status;
}
