package com.jiazhong.springcloudalibaba.order.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "alibaba-seata-stock" ,path = "/stock")
public interface StockFegin {


    @RequestMapping("/reduct")
    public String reduct(@RequestParam(value = "productId") Integer productId);

}
