package com.jiazhong.springcloudalibaba.order;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.jiazhong.springcloudalibaba.order.mapper")
@EnableFeignClients
public class AlibabaSeataOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlibabaSeataOrderApplication.class,args);
    }


}
