package com.jiazhong.order.controller;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.loadbalancer.core.RoundRobinLoadBalancer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private RestTemplate restTemplate;

    private static final String STOCK_URL ="http://stock-service";

    @RequestMapping("/add")
    public String add(){
        String message = restTemplate.getForObject(STOCK_URL + "/stock/del", String.class);
        System.out.println("下单成功!!!");
        return "下单成功!!!"+message;
    }
}
