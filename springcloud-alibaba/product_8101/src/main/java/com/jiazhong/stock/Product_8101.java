package com.jiazhong.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Product_8101 {
    public static void main(String[] args) {
        SpringApplication.run(Product_8101.class);
    }

}
