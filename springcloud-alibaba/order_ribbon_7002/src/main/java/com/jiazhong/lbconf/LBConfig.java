package com.jiazhong.lbconf;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LBConfig {

     @Bean
     //方法名必须为iRule
    public IRule iRule(){
         return new RandomRule();
     }
}
