package com.jiazhong.lbconf.rule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MyRule extends AbstractLoadBalancerRule {


    @Override
    public Server choose(Object o) {
        ILoadBalancer loadBalancer = this.getLoadBalancer();
        //获得当前请求服务的实例
        List<Server> reachableServers = loadBalancer.getReachableServers();
        //获得随机数
        int random = ThreadLocalRandom.current().nextInt(reachableServers.size());
        Server server = reachableServers.get(random);
//        if (server.isAlive()){
//            return null;
//        }
        return server;
    }
    //初始化一些配置
    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }
}
