package com.jiazhong.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.jiazhong.lbconf.LBConfig;

@SpringBootApplication
//@EnableDiscoveryClient
//@RibbonClients(
//        value = {
//                /**
//                 * 参数1:访问服务名
//                 * 参数2:配置类
//                 */
//                @RibbonClient(value = "stock-service",configuration = LBConfig.class )
//        }
//)
public class Order_ribbon_7002 {
    public static void main(String[] args) {
        SpringApplication.run(Order_ribbon_7002.class);
    }

    @Bean
    @LoadBalanced//必须配置负载均衡
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}

