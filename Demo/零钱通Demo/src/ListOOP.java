import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
class say{
    public static void main(String[] args) {
        ListOOP listOOP=new ListOOP();
        listOOP.map();
    }
}
public class ListOOP {
    boolean flag = true;
    String key = "";
    String details = "-------------零钱通明细------------";
    double money;
    double balance = 0;//余额
    Date date = null;//格式化时间
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    String IncomeDetails;
    String OutcomeDetails;
    Scanner scanner = new Scanner(System.in);
    public  void map() {
        System.out.println("------------零钱通菜单---------");
        System.out.println("         1.零钱通收益明细");
        System.out.println("         2.收 益   入 账");
        System.out.println("         3.  消    费");
        System.out.println("         4.  退    出");
        while (flag) {
            System.out.println("请选择1-4:");
            key = scanner.next();
            switch (key) {
                case "1":
                    List();
                    break;
                case "2":
                    InMoney();
                    break;
                case "3":
                    OutMoney();
                    break;
                case "4":
                    exit();
                    break;
                    default:
                    System.out.println("您输入的有误请重新选择！");
                    break;
            }
        }
        System.out.println("--------您退出了零钱通系统--------");

    }
    void List(){
        System.out.println(details);
    }
    void InMoney(){
        System.out.print("2.收益入账金额");
        System.out.println();
        date = new Date();
        while (flag) {
            System.out.println("请输入入账金额：");
            money = scanner.nextDouble();
            if (money >= 0) {
                System.out.print("请输入入账明细：");
                IncomeDetails = scanner.next();
                balance = balance - money;
                details+=("\n"+"收益入账：+" + money + "元 " + dateFormat.format(date) + " " + "余额：" + balance + "元 "+"入账明细：" + IncomeDetails);
                break;
            } else {
                System.out.println("您输入的格式错误！");
                break;
            }
        }
    }
    void OutMoney(){
        System.out.println("3.   消   费  ");
        date = new Date();
        boolean flag1=true;
        while (flag1) {
            System.out.println("请输入消费金额：");
            money = scanner.nextDouble();
            if (money >= 0) {
                System.out.print("请输入消费明细：");
                OutcomeDetails = scanner.next();
                balance = balance + money;
                details+=("\n"+"收益入账：-" + money + "元 " + dateFormat.format(date) + " " + "余额：" + "" + balance + "元 "+"消费明细：" + OutcomeDetails);
                break;
            }
            else {
                System.out.println("您输入的格式错误！");
                break;
            }
        }
    }
    void exit(){
        System.out.println("4.   退   出  ");
        String choice = "";
        while (true) {
            System.out.println("你确定你要退出吗？y/n");
            choice = scanner.next();
            if ("y".equals(choice) || "n".equals(choice)) {
                break;
            }
        }
        if (choice.equals("y")) {
            flag = false;
        }
    }
}
