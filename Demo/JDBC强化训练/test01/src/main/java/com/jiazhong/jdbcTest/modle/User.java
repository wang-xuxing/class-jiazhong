package com.jiazhong.jdbcTest.modle;


public class User {
    private Integer user_id;
    private String user_nike_name;
    private String usr_register_date;
    private Integer user_score;
    private String usr_sex;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_nike_name() {
        return user_nike_name;
    }

    public void setUser_nike_name(String user_nike_name) {
        this.user_nike_name = user_nike_name;
    }

    public String getUsr_register_date() {
        return usr_register_date;
    }

    public void setUsr_register_date(String usr_register_date) {
        this.usr_register_date = usr_register_date;
    }

    public Integer getUser_score() {
        return user_score;
    }

    public void setUser_score(Integer user_score) {
        this.user_score = user_score;
    }

    public String getUsr_sex() {
        return usr_sex;
    }

    public void setUsr_sex(String usr_sex) {
        this.usr_sex = usr_sex;
    }

    @Override
    public String toString() {
        return "User{" +
                "user_id=" + user_id +
                ", user_nike_name='" + user_nike_name + '\'' +
                ", usr_register_date=" + usr_register_date +
                ", user_score=" + user_score +
                ", usr_sex=" + usr_sex +
                '}'+"\n";
    }
}
