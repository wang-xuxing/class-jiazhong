package com.jiazhong.jdbcTest.dao;

import com.jiazhong.jdbcTest.modle.User;

import java.util.List;

public interface UserDao {
    //增加操作
    public void addUser(User user);
    //删除操作
    public void detectUser(int... user_id);
    //修改操作
    public void upDateUser(int score,int userid);
    //查询指定月份的用户
    public List<User> QueryUserByMonth(int date_Month);
    //根据ID查询指定用户信息
    public User QueryUserById(int user_id);
    //查询积分大于某个值的用户信息
    public List<User> QueryUserByScore(int score);
     //查询昵称中包含"x"的所有用户信息
    public List<User> QueryUserByName(String name);
}
