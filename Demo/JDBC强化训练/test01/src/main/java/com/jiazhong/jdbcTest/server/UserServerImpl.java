package com.jiazhong.jdbcTest.server;

import com.jiazhong.jdbcTest.dao.DBUtil;
import com.jiazhong.jdbcTest.dao.UserDao;
import com.jiazhong.jdbcTest.dao.impl.UserDaoImpl;
import com.jiazhong.jdbcTest.modle.User;


import java.util.HashSet;
import java.util.List;

public class UserServerImpl implements UserServer {
    UserDao userDao=new UserDaoImpl();
    /**修改指定用户的积分
     *
     * @param score
     * @param userid
     */
    @Override
    public void upDateUser(int score, int userid) {
        userDao.upDateUser(score,userid);
    }

    /**根据月份查询指定日期
     *
     * @param date_Month
     * @return
     */
    @Override
    public List<User> QueryUserByMonth(int date_Month) {
        return userDao.QueryUserByMonth(date_Month);
    }

    /**根据 ID 查询指定用户的信息*
     *
     * @param user_id
     * @return
     */
    @Override
    public User QueryUserById(int user_id) {
        return userDao.QueryUserById(user_id);
    }

    /**查出积分大于某个值的用户信息
     *
     * @param score
     * @return
     */

    @Override
    public List<User> QueryUserByScore(int score) {
        return userDao.QueryUserByScore(score);
    }

    /** 查询昵称中包含“x”的所有用户信息
     *
     * @param name
     * @return
     */
    @Override
    public List<User> QueryUserByName(String name) {
        return userDao.QueryUserByName(name);
    }



    /**
     * 根据用户 ID 删除用户(支持批量删除)
     */
    public  void setSelect(int... UserId){
             userDao.detectUser(UserId);
    }


    /**
     * 使用循环和随机数技巧，增加 1000 个数据，要求积分在 0-200，注册时间均分布在 2019
     * 年各个月份，从 26 个字母中随机取 3 个小写字母作为昵称，昵称不能一样，ID 自增
     */
    public void setAdd() {
        HashSet<String> set = new HashSet<>();
        User user = new User();
        while (set.size()<1000){
            String name = "";
            for (int i = 0; i < 3; i++) {
                name=(name + (char) (Math.random() * 26 + 'a')) ;
            }
            set.add(name);
        }
        for (String str : set) {
            UserDao userDao = new UserDaoImpl();
            int score=(int)(Math.random() * 200);
            user.setUser_score(score);


            int Month = (int) (Math.random() * 11 + 1);
            int Day = 0;
            if (Month % 2 != 0) {
                Day = (int) (Math.random() * 30 + 1);
            } else if (Month == 2) {
                Day = (int) (Math.random() * 27 + 1);
            } else {
                Day = (int) (Math.random() * 29 + 1);
            }
            user.setUsr_register_date("2019-" + Month + "-" + Day + " "
                    + (int) (Math.random() * 24) + ":" + (int) (Math.random() * 59 + 1) + ":" + (int) (Math.random() * 59 + 1));

            user.setUser_nike_name(str);

            if (user.getUser_score() % 2 == 0) {
                user.setUsr_sex("男");
            } else {
                user.setUsr_sex("女");
            }
           userDao.addUser(user);
        }

    }


}
