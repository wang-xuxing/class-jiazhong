package com.jiazhong.jdbcTest.dao.impl;

import com.jiazhong.jdbcTest.dao.DBUtil;
import com.jiazhong.jdbcTest.dao.UserDao;
import com.jiazhong.jdbcTest.modle.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends DBUtil implements UserDao {
    /**
     * 使用循环和随机数技巧，增加 1000 个数据，要求积分在 0-200，注册时间均分布在 2019
     * 年各个月份，从 26 个字母中随机取 3 个小写字母作为昵称，昵称不能一样，ID 自增
     */

    @Override
    public void addUser(User user) {
//        非批量添加
        /**
         String sql="insert into tbl_user values(default,?,?,?,?)";
         executeUpdate(sql,user.getUser_nike_name(),user.getUsr_register_date(),user.getUser_score(),user.getUsr_sex());
         */
        //使用SQL语句一次添加多条数据库
        StringBuffer bufSql;
        try {
            //获得连接
            super.getConn();
            bufSql = new StringBuffer("insert into tbl_user values");

            bufSql.append("(").append("default").append(",")
                    .append("'" + user.getUser_nike_name() + "'").append(",'")
                    .append(user.getUsr_register_date()).append("',")
                    .append(user.getUser_score()).append(",")
                    .append("'" + user.getUsr_sex() + "'").append(")").append(",");
            bufSql.deleteCharAt(bufSql.length() - 1);

            ps = conn.prepareStatement(bufSql.toString());
            int num = ps.executeUpdate();
            System.out.println("影响行数:" + num);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            super.closeAll();
        }

        //使用jdbc的批量操纵进行批量添加
        /**
         try {
         getConn();
         String sql = "insert into tbl_user values(default,?,?,?,?)";
         ps = conn.prepareStatement(sql);
         ps.setString(1, user.getUser_nike_name());
         ps.setString(2, user.getUsr_register_date());
         ps.setInt(3, user.getUser_score());
         ps.setString(4, user.getUsr_sex());
         ps.addBatch();//将预处理对象添加到批处理中
         ps.executeUpdate();
         } catch (SQLException e) {
         e.printStackTrace();
         }finally {
         closeAll();
         }
         */
    }

    /**
     * 根据用户 ID 删除用户(支持批量删除)
     *
     * @param user_id
     */
    @Override
    public void detectUser(int... user_id) {
        try {
            StringBuffer sql = new StringBuffer("delete from tbl_user where user_id in (");
            for (int userId : user_id) {
                sql.append("?").append(",");
            }
            sql.deleteCharAt(sql.length() - 1);//删除最后一个逗号
            sql.append(")");
            getConn();
            ps = conn.prepareStatement(sql.toString());
            if (user_id != null && user_id.length != 0) {
                for (int i = 0; i < user_id.length; i++) {
                    ps.setInt(i + 1, user_id[i]);
                }
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
    }

    /**
     * 修改指定用户的积分
     *
     * @param userid
     */
    @Override
    public void upDateUser(int score, int userid) {
        String sql = "update tbl_user set user_score=? where user_id=?";
        executeUpdate(sql, score, userid);
    }

    /**
     * 根据月份查询指定日期
     *
     * @param date_Month
     * @return
     */
    @Override
    public List<User> QueryUserByMonth(int date_Month) {
        try {
            String sql = "select * from tbl_user where MONTH(user_register_date)=?";
            super.getConn();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, date_Month);
            rs = ps.executeQuery();
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt(1));
                user.setUser_nike_name(rs.getString(2));
                user.setUsr_register_date(rs.getString(3));
                user.setUser_score(rs.getInt(4));
                user.setUsr_sex(rs.getString(5));
                users.add(user);
            }
            return users;
        } catch (SQLException s) {
            s.printStackTrace();
        } finally {
            super.closeAll();
        }
        return null;

    }

    /**
     * 根据 ID 查询指定用户的信息*
     *
     * @param user_id
     * @return
     */
    @Override
    public User QueryUserById(int user_id) {
        try {
            getConn();
            String sql = "select * from tbl_user where user_id=? ";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, user_id);
            rs = ps.executeQuery();
            User user = new User();
            if (rs.next()) {
                user.setUser_id(rs.getInt(1));
                user.setUser_nike_name(rs.getString(2));
                user.setUsr_register_date(rs.getString(3));
                user.setUser_score(rs.getInt(4));
                user.setUsr_sex(rs.getString(5));
            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查出积分大于某个值的用户信息
     *
     * @param score
     * @return
     */

    @Override
    public List<User> QueryUserByScore(int score) {
        try {
            getConn();
            String sql = "select * from tbl_user where user_score=? ";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, score);
            rs = ps.executeQuery();
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt(1));
                user.setUser_nike_name(rs.getString(2));
                user.setUsr_register_date(rs.getString(3));
                user.setUser_score(rs.getInt(4));
                user.setUsr_sex(rs.getString(5));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查询昵称中包含“x”的所有用户信息
     *
     * @param name
     * @return
     */
    @Override
    public List<User> QueryUserByName(String name) {
        try {
            getConn();
            String sql = "select * from tbl_user where user_nike_name like  concat('%',?,'%')";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt(1));
                user.setUser_nike_name(rs.getString(2));
                user.setUsr_register_date(rs.getString(3));
                user.setUser_score(rs.getInt(4));
                user.setUsr_sex(rs.getString(5));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
