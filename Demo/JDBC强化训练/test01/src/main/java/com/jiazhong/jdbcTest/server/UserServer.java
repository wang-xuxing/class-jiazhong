package com.jiazhong.jdbcTest.server;

import com.jiazhong.jdbcTest.modle.User;

import java.util.List;

public interface UserServer {
    public  void setAdd();
    public  void setSelect(int... userId);
    public void upDateUser(int score,int userid);
    public List<User> QueryUserByMonth(int date_Month);
    //根据ID查询指定用户信息
    public User QueryUserById(int user_id);
    //查询积分大于某个值的用户信息
    public List<User> QueryUserByScore(int score);
    //查询昵称中包含"x"的所有用户信息
    public List<User> QueryUserByName(String name);


}
