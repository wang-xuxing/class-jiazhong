package Class.Jiazhong.Test04.modle;


import java.util.Date;

public class Emp {
    private int emp_id;
    private String emp_name;
    private int emp_sex;
    private Date emp_birthday;
    private float emp_salary;

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_birthday(Date emp_birthday) {
        this.emp_birthday = emp_birthday;
    }

    public Date getEmp_birthday() {
        return emp_birthday;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public int getEmp_sex() {
        return emp_sex;
    }

    public void setEmp_sex(int emp_sex) {
        this.emp_sex = emp_sex;
    }

    public float getEmp_salary() {
        return emp_salary;
    }

    public void setEmp_salary(float emp_salary) {
        this.emp_salary = emp_salary;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "emp_id=" + emp_id +
                ", emp_name='" + emp_name + '\'' +
                ", emp_sex='" + emp_sex + '\'' +
                ", emp_birthday='" + emp_birthday + '\'' +
                ", emp_salary=" + emp_salary +
                '}';
    }
}
