package Class.Jiazhong.Test04.dao;

import java.sql.*;

/*
sql操作类封装
 */
public abstract class  DBUtils {
    protected Connection conn = null;
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;

    public Connection getConn()  {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/test0?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false","root","root");}
        catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return conn;
    }

    public void closeAll() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            ps = null;
            conn = null;
        }
    }

    //封装增删改操作
    public int executeUpdate(String sql, Object... element) {
        try {
            //连接数据库
            getConn();
//        预处理sql语句
            ps = conn.prepareStatement(sql);
//            对？进行设置
//            判断是否存在问号占位符
            if (element != null && element.length != 0) {
                for (int i = 0; i < element.length; i++) {
                    ps.setObject(i + 1, element[i]);
                }
            }
//返回受影响行数
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeAll();
        return 0;
    }

}
