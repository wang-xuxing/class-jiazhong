package Class.Jiazhong.Test04;

import Class.Jiazhong.Test04.dao.empDao;
import Class.Jiazhong.Test04.dao.imp.empDaoImp;
import Class.Jiazhong.Test04.modle.Emp;
import java.util.Date;

public class Test {
    public static void main(String[] args) {
        empDao empDao=new empDaoImp();
        addEmp(empDao);
        UpdateEmpBySalary(empDao);
        QueryEmpByAll(empDao);
        QueryEmpBySalary(empDao);

    }

    /**添加员工信息
     *
     * @param empDao
     */
    private static void addEmp(empDao empDao) {
        Emp emp=new Emp();
        emp.setEmp_name("张薪");
        emp.setEmp_sex(1);
        emp.setEmp_birthday(new Date());
        emp.setEmp_salary(1000.5f);
        empDao.addEmp(emp);
    }

    /**  修改员工姓名为“张薪”的员工工资，在原有工资基础上加 1000 元
     * 
     * @param empDao
     */
    private static void UpdateEmpBySalary(empDao empDao) {
        empDao.updateEmp(1000,"张薪");
    }

    /** 查询所有员工信息，显示结果为:员工编号,姓名，性别(男| 女),年龄，工资
     * 
     * @param empDao
     */
    private static void QueryEmpByAll(empDao empDao) {
        System.out.println(empDao.AllQueryEmp());
    }

    /**获得员工工资最高的 3 个员工的员工信息，显示结果为:员工 编号,姓名，性别(男|女),年龄，工资
     *
     * @param empDao
     */
    private static void  QueryEmpBySalary(empDao empDao) {
        System.out.println(empDao.QueryEmpByThirdSalary());
    }
}