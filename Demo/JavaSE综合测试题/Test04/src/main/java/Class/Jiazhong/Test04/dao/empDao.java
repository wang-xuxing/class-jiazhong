package Class.Jiazhong.Test04.dao;

import Class.Jiazhong.Test04.modle.Emp;
import Class.Jiazhong.Test04.modle.EmpView;

import java.util.List;

public interface empDao {
    void addEmp(Emp emp);//添加员工信息
    void updateEmp(float salary,String name);//修改员工信息
    List<EmpView> AllQueryEmp();//查询所有员工信息
    List<EmpView> QueryEmpByThirdSalary();//获得员工工资最高的 3 个员工的员工信息
}
