package Class.Jiazhong.Test04.dao.imp;

import Class.Jiazhong.Test04.dao.DBUtils;
import Class.Jiazhong.Test04.dao.empDao;
import Class.Jiazhong.Test04.modle.Emp;
import Class.Jiazhong.Test04.modle.EmpView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class empDaoImp extends DBUtils implements empDao {
    //添加员工信息
    @Override
    public void addEmp(Emp emp) {
        String sql = "insert into tbl_employee values(default,?,?,?,?)";
        super.executeUpdate(sql, emp.getEmp_name(), emp.getEmp_sex(), emp.getEmp_birthday(), emp.getEmp_salary());
    }

    @Override
    public void updateEmp(float salary, String name) {
        String sql = "update tbl_employee set emp_salary=emp_salary+? where emp_name=?";
        executeUpdate(sql, salary, name);
    }

    //    查询所有员工信息
    @Override
    public List<EmpView> AllQueryEmp() {
        try {
            String sql = "select emp_id,emp_name,CASE WHEN emp_sex=1 THEN '男' WHEN emp_sex=0 THEN '女' end as 'emp_sex' ,TIMESTAMPDIFF(YEAR,emp_birthday,now()) as 'emp_age' , emp_salary from tbl_employee";
            super.getConn();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            List<EmpView> emps = new ArrayList<>();
            while (rs.next()) {
                EmpView emp = new EmpView();
                emp.setEmp_id(rs.getInt(1));
                emp.setEmp_name(rs.getString(2));
                emp.setEmp_sex(rs.getString(3));
                emp.setEmp_age(rs.getInt(4));
                emp.setEmp_salary(rs.getFloat(5));
                emps.add(emp);
            }
            return emps;
        } catch (SQLException s) {
            s.printStackTrace();
        } finally {
            super.closeAll();
        }
        return null;

    }

    //    获得员工工资最高的 3 个员工的员工信息
    @Override
    public List<EmpView> QueryEmpByThirdSalary() {
        try {
            String sql = "select emp_id,emp_name,CASE WHEN emp_sex=1 THEN '男' WHEN emp_sex=0 THEN '女' end as 'emp_sex' ,TIMESTAMPDIFF(YEAR,emp_birthday,now()) as 'emp_age' , emp_salary from tbl_employee order by emp_salary desc limit 0,3";
            super.getConn();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            List<EmpView> emps = new ArrayList<>();
            while (rs.next()) {
                EmpView emp = new EmpView();
                emp.setEmp_id(rs.getInt(1));
                emp.setEmp_name(rs.getString(2));
                emp.setEmp_sex(rs.getString(3));
                emp.setEmp_age(rs.getInt(4));
                emp.setEmp_salary(rs.getFloat(5));
                emps.add(emp);
            }
            return emps;
        } catch (SQLException s) {
            s.printStackTrace();
        } finally {
            super.closeAll();
        }
        return null;

    }
}
