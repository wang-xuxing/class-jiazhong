package com.HuoZhExIA0.飞机大战;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/**
 * 游戏窗口
 */
public class MyGameFrame extends Frame {
    //初始化窗口
    public void launchFrame(){
         this.setTitle("我的第一个小项目练习-飞机大战");
         setVisible(true);
         setSize(800,800);
         setLocation(480,200);    //窗口打开的位置
        //增加关闭窗口动作
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    @Override
    public void paint(Graphics graphics) { //graphics就是画笔
        graphics.drawLine(200,200,400,400);
        //画矩形
        graphics.drawRect(200,200,400,300);
        //画圆形
        graphics.drawOval(200,200,300,300);
    }

    public static void main(String[] args) {
        MyGameFrame myGameFrame=new MyGameFrame();
        myGameFrame.launchFrame();
    }
}
