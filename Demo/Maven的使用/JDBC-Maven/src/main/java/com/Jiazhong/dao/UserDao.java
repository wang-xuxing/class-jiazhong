package com.Jiazhong.dao;

import com.Jiazhong.modle.User;

import java.util.List;

public interface UserDao {
    //添加用户
    public void addUser(User user);
    //更新用户
    public void updateUser(User user);
    //根据id删除用户
    public void detectUser(int userId);
    //查询表中所有数据
    public List<User> selectUser();
    //根据id来查询用户
    public User QueryUserById(int userId);
    //根据用户名和密码来查询用户(用户登录)
    public User QueryUserByUserNamePassword(String username,String password);
}
