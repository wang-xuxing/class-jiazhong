package com.Jiazhong.dao.impl;

import com.Jiazhong.dao.DBUtil;
import com.Jiazhong.dao.UserDao;
import com.Jiazhong.modle.User;

import java.util.ArrayList;
import java.util.List;

/**
 * 次类操作对于数据库admin
 */
public class UserDaoImpl extends DBUtil implements UserDao {
    //添加用户
    @Override
    public void addUser(User user) {
        String sql="insert into admin values(default,?,?)";
        executeUpdate(sql, user.getUserName(),user.getPassWord());
    }
    //修改数据
    @Override
    public void updateUser(User user) {
       String sql="update admin set UserName=?,PassWord=? where UserId=?";
       executeUpdate(sql,user.getUserName(),user.getPassWord(),user.getUserId());
    }
    //根据id删除用户
    @Override
    public void detectUser(int userId) {
        String sql="detect from admin where userId=?";
        executeUpdate(sql,userId);
    }
    //查询所有用户
    @Override
    public List<User> selectUser() {
        try {
            //连接数据库
            getConn();
            String sql="select * from admin";
            ps=conn.prepareStatement(sql);
            rs=ps.executeQuery();
            List<User> users=new ArrayList<>();
            while (rs.next()){//从结果集获取数据
               int UserId= rs.getInt(1);
               String UserName=rs.getString(2);
               String Password=rs.getString(3);
               User user1=new User();
               user1.setUserId(UserId);
               user1.setUserName(UserName);
               user1.setPassWord(Password);
               //将User对象添加到User集合中
                users.add(user1);
            }
            return users;
        }catch (Exception e){
            e.getMessage();
        }finally {
            closeAll();
        }

        return null;
    }

    @Override
    public User QueryUserById(int userId) {
        try {
            //连接数据库
            getConn();
            String sql="select * from admin where usrId=?";
            ps=conn.prepareStatement(sql);
            ps.setInt(1,userId);
            rs=ps.executeQuery();
            while (rs.next()){//从结果集获取数据
                User user=new User();
                user.setUserId(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setPassWord(rs.getString(3));
                return user;
            }
        }catch (Exception e){
            e.getMessage();
        }finally {
            closeAll();
        }

        return null;
    }

    @Override
    public User QueryUserByUserNamePassword(String username, String password) {
        try {
            //连接数据库
            getConn();
            String sql="select * from admin where username=? and password=?";
            ps=conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            rs=ps.executeQuery();
            while (rs.next()){//从结果集获取数据
                User user=new User();
                user.setUserId(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setPassWord(rs.getString(3));
                return user;
            }
        }catch (Exception e){
            e.getMessage();
        }finally {
            closeAll();
        }

        return null;
    }
}
