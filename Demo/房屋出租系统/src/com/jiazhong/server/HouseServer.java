package com.jiazhong.server;

import com.jiazhong.domain.House;

public class HouseServer {
    private House[] houses;
    private int houseNums = 0;//记录当前 有多少个房屋信息
    private int idCounter = 0;//记录当前的id增长到那一个值

    public HouseServer(int size) {
        this.houses = new House[size];

    }

    public House[] list() {
        return houses;
    }

    public boolean add(House newHouse) {
        if (houseNums == houses.length) {
            System.out.println("数组已满，不能再添加了");
            return false;
        }
        houses[houseNums] = newHouse;//把新的对象加入到数组的最后
        houseNums++;//新增加了一个房屋
        //我们需要设置一个id自增长的机制
        idCounter++;
        newHouse.setId(idCounter);
        return true;
    }

    public boolean del(int delId) {
        //应当找到删除的房屋信息，一定搞清楚下标和房屋的编号不是一回事
        int index = -1;
        for (int i = 0; i < houseNums; i++) {
            if (delId == houses[i].getId()) {//删除的房屋对应的id，在数组下标为i的元素

                index = i;//就是用index记录i
            }
        }
        if (index == -1) {//说明delId在数组中不存在
            return false;
        }
        //如果找到，就删除
        for (int i = index; i < houseNums - 1; i++) {
            houses[i] = houses[i + 1];
        }
        houses[houseNums - 1] = null;//把当前存在的房屋信息的最后一个设置为空
        houseNums--;//少一个
        return true;
    }

    public House findById(int findId) {
        for (int i = 0; i < houses.length; i++) {
            if (findId == houses[i].getId()) {
                return houses[i];
            }
        }
        return null;
    }

}
