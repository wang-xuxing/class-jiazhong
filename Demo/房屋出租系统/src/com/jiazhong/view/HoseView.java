package com.jiazhong.view;

import com.jiazhong.domain.House;
import com.jiazhong.server.HouseServer;
import com.jiazhong.util.Utility;

import java.util.Scanner;

public class HoseView {
    Scanner scanner = new Scanner(System.in);
    boolean flag = true;
    HouseServer houseServer = new HouseServer(10);

    public void mainMenu() {
        System.out.println("--------------------房屋出租系统--------------------");
        System.out.println("                1 新 增 房 源");
        System.out.println("                2 查 找 房 屋");
        System.out.println("                3 删 除 房 屋");
        System.out.println("                4 修 改 房 屋 信 息");
        System.out.println("                5 房 屋 列 表");
        System.out.println("                6 退      出");
        while (flag) {
            System.out.println("请选择（1-6）：");
            String num = scanner.next();
            switch (num) {
                case "1":
                    System.out.println("---------------------添加房屋---------------------");
                    addHouse();
                    break;
                case "2":
                    System.out.println("---------------------查找房屋---------------------");
                    System.out.println("请输入要查找的id");
                    findHouse();
                    break;
                case "3":
                    System.out.println("---------------------删除房屋---------------------");
                    System.out.println("请输入待删除房屋的编号（-1退出）");
                    delHouse();
                    break;
                case "4":
                    System.out.println("---------------------修改房屋---------------------");
                    System.out.println("请选择待修改的房屋的编号：（-1表示退出）");
                    updateHouse();
                    System.out.println("---------------------修改完成---------------------");
                    break;
                case "5":
                    System.out.println("---------------------房屋列表---------------------");
                    System.out.println("编号  房主      电话           地址            月租   状态(已出租/未出租)");
                    listHouse();
                    System.out.println("--------------------房屋列表完成-------------------");
                    break;
                case "6":
                    exit();
                    System.out.println("您退出了程序~~~~~~");
                    break;
            }
        }
    }

    void listHouse() {
        House[] houses = houseServer.list();
        //得到所有房屋的信息
        for (int i = 0; i < houses.length; i++) {//这里的问题
            if (houses[i] == null) {
                break;
            }
            System.out.println(houses[i] + " ");
        }
    }

    void addHouse() {
        System.out.println("姓名：");
        String name = Utility.readString(8);
        System.out.println("电话：");
        String phone = Utility.readString(12);
        System.out.println("地址：");
        String address = Utility.readString(16);
        System.out.println("月租：");
        String rent = Utility.readString(7);
        System.out.println("状态：");
        String condition = Utility.readString(3);
        //创建一个新的House对象,注意id是系统分配的，用户不能输入
    if (houseServer.add(new House(0, name, phone, address, rent, condition))) {
            System.out.println("---------------------添加完成---------------------");
        } else {
            System.out.println("---------------------添加失败---------------------");
        }
    }

    void delHouse() {
        int delId = Utility.readInt();
        if (delId == -1) {
            System.out.println("--------------------放弃删除房屋信息--------------------");
            return;
        }
        char choice = Utility.readConfirmSelection();//注意该方法本身就有循环判断的逻辑，必须输入y或者n，否则是出不来的
        if (choice == 'Y') {//真的删除
            if (houseServer.del(delId)) {
                System.out.println("--------------------删除房屋成功--------------------");
            } else {
                System.out.println("--------------------编号不存在删除失败--------------------");
            }
        } else {
            System.out.println("--------------------放弃删除房屋信息--------------------");
        }
    }

    void findHouse() {
        int findId = Utility.readInt();
        //调用方法
        House house = houseServer.findById(findId);
        if (house != null) {
            System.out.println(house);
        } else {
            System.out.println("没有查找到相应的信息，id不存在");
        }
    }

    void updateHouse() {
        int updateId = Utility.readInt();
        if (updateId == -1) {
            System.out.println("--------------------你放弃了修改房屋信息--------------------");
            return;
        }
        //根据输入得到updateId,查找对象
        House house = houseServer.findById(updateId);
        if (house == null) {
            System.out.println("--------------------修改的房屋信息编号不存在--------------------");
            return;
        }
        System.out.println("姓名(" + house.getName() + "):");
        String name = Utility.readString(8, "");//如果用户直接回车，表示不修改该信息默认“ ”
        if (!"".equals(name)) {//修改
            house.setName(name);
        }
        System.out.println("电话(" + house.getTelephone() + "):");
        String phone = Utility.readString(12, "");
        if (!"".equals(phone)) {
            house.setTelephone(phone);
        }
        System.out.println("地址(" + house.getAddress() + "):");
        String address = Utility.readString(18, "");
        if (!"".equals(address)) {
            house.setAddress(address);
        }
        System.out.println("租金(" + house.getRent() + "):");
        String rent = Utility.readString(-1);
        if (rent.equals(-1)) {
            house.setRent(rent);
        }
        System.out.println("状态(" + house.getCondition() + "):");
        String state = Utility.readString(3, "");
        if (!"".equals(state)) {
            house.setCondition(state);
        }
    }

    public void exit() {
        //这里我们使用方法提供的方法，完成确认
        char c = Utility.readConfirmSelection();
        if (c == 'Y') {
            flag = false;
        }
    }

}
