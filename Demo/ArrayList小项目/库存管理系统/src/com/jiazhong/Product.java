package com.jiazhong;
//定义商品类，属性：商品名(proName)、商品尺寸(proSize)、商品价格(proPrice)、库存量(proNum)
public class Product {
    private Integer proNO;//编号
    private String proName;//商品名
    private String proSize;//商品尺寸
    private Double proPrice;//商品价格
    private Integer proNum;//库存量

    public Product() {
    }

    public Integer getProNO() {
        return proNO;
    }

    public void setProNO(Integer proNO) {
        this.proNO = proNO;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProSize() {
        return proSize;
    }

    public void setProSize(String proSize) {
        this.proSize = proSize;
    }

    public Double getProPrice() {
        return proPrice;
    }

    public void setProPrice(Double proPrice) {
        this.proPrice = proPrice;
    }

    public Integer getProNum() {
        return proNum;
    }

    public void setProNum(Integer proNum) {
        this.proNum = proNum;
    }

    @Override
    public String toString() {
        return "Product{" +
                "proNO=" + proNO +
                ", proName='" + proName + '\'' +
                ", proSize='" + proSize + '\'' +
                ", proPrice=" + proPrice +
                ", proNum=" + proNum +
                '}';
    }
}
