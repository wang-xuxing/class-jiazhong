package com.jiazhong;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品操作类，该类用于操作库存商品
 * 操作类中包含一系列的操作方法:
 *  1.向库存中添加商品
 *  2.查看库存信息
 *  3.修改库存量
 */
public class ProductOperate {
    //创建一个用于储存产品的集合
    private List<Product> list=new ArrayList<>();
    //向库存中添加商品
    public void addProduct(Product product)throws NullPointerException{
        if (product==null){
            throw  new NullPointerException("商品对象为空！");
        }
        list.add(product);
    }

    //查看库存信息
    public void queryProducts(){
        System.out.println("商品编号\t商品名称\t单价\t尺寸\t库存量");
        for (Product product:list){
            System.out.println(product.getProNO()+"\t"+product.getProName()+"\t"
                    + product.getProPrice()+"\t"+product.getProSize()+"\t"+product.getProNum());
        }
    }
    /**
     * 根据商品编号更新商品的库存量
     * 增加库存参数为正整数，减少库存参数为负整数
     */
     public void updateProductNum(int productNO,int productNum) throws Exception {
         if (productNum ==0){
             throw new Exception("库存更新量不能为零");
         }
         //根据商品编号获得商品对象
         for(Product product:list ){
             if (product.getProNO()==productNO){//找到商品
                 if (productNum < 0 ){//检测库存量，为负数表示错误
                     if (product.getProNum()+productNum<0){
                         throw new Exception("库存不足！");
                     }
                 }
                 //大于0表示增加，小于0表示减少
                 product.setProNum(product.getProNum()+productNum );
                 return;//返回
             }
         }
         throw new Exception("您要修改的商品不存在！");
     }
}
