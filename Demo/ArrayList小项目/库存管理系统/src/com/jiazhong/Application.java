package com.jiazhong;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        ProductOperate productOperate=new ProductOperate();
        for ( int i=0;i<10;i++) {
            Product product = new Product();
            product.setProNO(1000+i);
            product.setProName("手机"+i);
            product.setProPrice(1000.0*(i+1));
            product.setProNum(1+i);
            product.setProSize("6.8");
            try {
                productOperate.addProduct(product);
                System.out.println("添加商品成功");
            } catch (NullPointerException e) {
                System.out.println("商品添加失败");
            }
        }
        productOperate.queryProducts();
        try{
            System.out.println("修改库存数量:");
            Scanner input=new Scanner(System.in);
            System.out.println("要修改的商品编号:");
            int no = input.nextInt();
            System.out.print("要更新的库存量(减少请输入负数):");
            int num = input.nextInt();
            productOperate.updateProductNum(no,num);
            System.out.println("库存更新成功!");
            //查看库存
            productOperate.queryProducts();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
