package com.jiazhong.nginx;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nginx")
public class NginxController {
    @Value("${server.port}")
    private Integer port;
    @RequestMapping("/demo")
    public String demo(){
        return "nginx_demo--------->:"+port;
    }
}
