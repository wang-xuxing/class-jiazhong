<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2023/1/7
  Time: 17:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Tabler - Premium and Open Source dashboard template with responsive and high quality UI.</title>
    <!-- CSS files -->
    <link href="./dist/css/tabler.min.css?1668287865" rel="stylesheet"/>
    <link href="./dist/css/tabler-flags.min.css?1668287865" rel="stylesheet"/>
    <link href="./dist/css/tabler-payments.min.css?1668287865" rel="stylesheet"/>
    <link href="./dist/css/tabler-vendors.min.css?1668287865" rel="stylesheet"/>
    <link href="./dist/css/demo.min.css?1668287865" rel="stylesheet"/>
    <style>
        @import url('https://rsms.me/inter/inter.css');

        :root {
            --tblr-font-sans-serif: Inter, -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
        }
    </style>
</head>

<body>
<script src="./dist/js/demo-theme.min.js?1668287865"></script>


<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">用户信息</h3>
        </div>
        <div class="card-body border-bottom py-3">
            <div class="d-flex">
                <div class="text-muted">
                    Show
                    <div class="mx-2 d-inline-block">
                        <input type="text" class="form-control form-control-sm" value="8" size="3"
                               aria-label="Invoices count">
                    </div>
                    entries
                </div>
                <div class="ms-auto text-muted">
                    Search:
                    <div class="ms-2 d-inline-block">
                        <input type="text" class="form-control form-control-sm" aria-label="Search invoice">
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table card-table table-vcenter text-nowrap datatable">
                <thead>
                <tr>
                    <th class="w-1"><input class="form-check-input m-0 align-middle" type="checkbox"
                                           aria-label="Select all invoices"></th>
                    <th class="w-1">编号</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>日期</th>
                    <th>年龄</th>
                    <th>地址</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="user" items="${PAGE_INFO.list}">
                    <tr>
                        <td><input class="form-check-input m-0 align-middle" type="checkbox"
                                   aria-label="Select invoice"></td>
                        <td><span class="text-muted">${user.userId}</span></td>
                        <td>${user.username}</td>
                        <td>${user.sex == 1 ? "男" : "女"}</td>
                        <td>${user.date}</td>
                        <td>${user.age}</td>
                        <td>${user.address}</td>
                        <td class="text-end">user
                            <span class="dropdown">
                              <button class="btn dropdown-toggle align-text-top" data-bs-boundary="viewport"
                                      data-bs-toggle="dropdown">Actions</button>
                              <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="#">
                                  Action
                                </a>
                                <a class="dropdown-item" href="#">
                                  Another action
                                </a>
                              </div>
                            </span>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
        <div class="card-footer d-flex align-items-center">
            <p class="m-0 text-muted">Showing
                <span>${PAGE_INFO.startRow}</span> to
                <span>${PAGE_INFO.endRow}</span> of
                <span>${PAGE_INFO.total}</span> entries</p>
            <ul class="pagination m-0 ms-auto">
                <li class="page-item">
                    <a class="page-link" href="user?method=selectUser&page=${PAGE_INFO.prePage}" tabindex="-1" aria-disabled="true">
                        <!-- Download SVG icon from http://tabler-icons.io/i/chevron-left -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                             stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                            <polyline points="15 6 9 12 15 18"/>
                        </svg>
                        prev
                    </a>
                </li>
                <c:forEach var="p" step="1" end="${PAGE_INFO.pages}" begin="1">
                    <c:choose>
                        <c:when test="${PAGE_INFO.pageNum==p}">
                            <li class="page-item active"><a class="page-link" href="user?method=selectUser&page=${p}">${p}</a></li>
                        </c:when>
                        <c:otherwise>
                            <li class="page-item"><a class="page-link" href="user?method=selectUser&page=${p}">${p}</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <li class="page-item">
                    <a class="page-link" href="user?method=selectUser&page=${PAGE_INFO.nextPage}">
                        next <!-- Download SVG icon from http://tabler-icons.io/i/chevron-right -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                             stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                            <polyline points="9 6 15 12 9 18"/>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Libs JS -->
<!-- Tabler Core -->
<script src="./dist/js/tabler.min.js?1668287865" defer></script>
<script src="./dist/js/demo.min.js?1668287865" defer></script>
</body>
</html>
