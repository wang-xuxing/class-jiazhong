package com.jiazhong.mybatis02.service;

import com.github.pagehelper.PageInfo;
import com.jiazhong.mybatis02.bean.User;


public interface UserService {
    public PageInfo<User> selectUser(int page);
}
