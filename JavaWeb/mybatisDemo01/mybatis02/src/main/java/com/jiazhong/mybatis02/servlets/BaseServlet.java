package com.jiazhong.mybatis02.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("UTF-8");
            String methodName = request.getParameter("method");
            if (methodName.trim().equals(" ") || methodName==null){
                throw new Exception("请求失败,请携带method参数");
            }
            //每个方法的参数名为HttpServletRequest或HttpServletResponse
            Method method=this.getClass().getDeclaredMethod(methodName,HttpServletRequest.class,HttpServletResponse.class);
            //调用该方法
            method.invoke(this,request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

