package com.jiazhong.mybatis02.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiazhong.mybatis02.bean.User;
import com.jiazhong.mybatis02.mapper.UserMapper;
import com.jiazhong.mybatis02.service.UserService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class UserServiceImpl implements UserService {
    UserMapper userMapper;

    public UserServiceImpl() {
        try {
            InputStream in = Resources.getResourceAsStream("config.xml");
            SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
            SqlSessionFactory ssf = ssfb.build(in);
            SqlSession session = ssf.openSession();
            userMapper = session.getMapper(UserMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PageInfo<User> selectUser(int page) {
        PageHelper.startPage(page,5);
        List<User> users = userMapper.selectUserByPageHelper();

        return new PageInfo<>(users);
    }

}
