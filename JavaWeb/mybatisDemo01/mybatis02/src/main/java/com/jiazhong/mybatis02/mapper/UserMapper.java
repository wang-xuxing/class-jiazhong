package com.jiazhong.mybatis02.mapper;

import com.jiazhong.mybatis02.bean.MyPage;
import com.jiazhong.mybatis02.bean.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    //根据id查询数据
    User selectUserByUserid(int id);
    //根据id和username查询数据
    List<User> selectUserByUseridAndUserName01(MyPage myPage);
    List<User> selectUserByUseridAndUserName02(Map map);
    List<User> selectUserByUseridAndUserName03(@Param("userId") Integer userId,@Param("username") String username);
    //分页查询
    List<User> selectUser(@Param("start") Integer start,@Param("size") Integer size);
    //PageHelper
    List<User> selectUserByPageHelper();

}
