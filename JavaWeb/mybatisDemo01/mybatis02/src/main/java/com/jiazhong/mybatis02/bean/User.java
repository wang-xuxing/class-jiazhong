package com.jiazhong.mybatis02.bean;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class User implements Serializable {
    private Integer userId;
    private String username;
    private Integer sex;
    private Date date;
    private Integer age;
    private String address;
}
