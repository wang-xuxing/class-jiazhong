package com.jiazhong.mybatis02.servlets;


import com.github.pagehelper.PageInfo;
import com.jiazhong.mybatis02.bean.User;
import com.jiazhong.mybatis02.service.UserService;
import com.jiazhong.mybatis02.service.impl.UserServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user")
public class UserServlet extends BaseServlet {
    private UserService userService = new UserServiceImpl();

    public void selectUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //获取前端用户第几页数据
        String p = request.getParameter("page");
        int page;
        if (p == null) {
            page = 1;
        } else {
            page = Integer.parseInt(p);
        }

        //指定页码数据
        PageInfo<User> pageInfo = userService.selectUser(page);
        //存放到session中
        request.getSession().setAttribute("PAGE_INFO", pageInfo);
        response.sendRedirect("userList.jsp");
    }
}
