import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiazhong.mybatis02.bean.MyPage;
import com.jiazhong.mybatis02.bean.User;
import com.jiazhong.mybatis02.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class test {
    private static SqlSession session;
    UserMapper userMapper;

    public test() {
        try {
            InputStream in = Resources.getResourceAsStream("config.xml");
            SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
            SqlSessionFactory ssf = ssfb.build(in);
            session = ssf.openSession();
            userMapper = session.getMapper(UserMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //根据id查询一条数据
    @Test
    public void selectUserByUserid() {

        User user = userMapper.selectUserByUserid(8);
        System.out.println(user);
        session.close();

    }

    @Test
    public void selectUserByUseridAndUsername() {
//        Map<String, Object> map=new HashMap<>();
//        map.put("userId",17);
//        map.put("username","活着笑");
        List<User> users = userMapper.selectUserByUseridAndUserName03(17, "活着笑");
        for (User user : users) {
            System.out.println(user);
        }
        session.close();
    }

    @Test
    public void selectUser() {
        /**
         * 参数1:pageNum 多少页
         * 参数2:pageSize 每页显示多少条
         */
        PageHelper.startPage(3, 2);
        List<User> users = userMapper.selectUserByPageHelper();
        PageInfo<User> pageInfo = new PageInfo<>();
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println(pageInfo);
        session.close();
    }
}
