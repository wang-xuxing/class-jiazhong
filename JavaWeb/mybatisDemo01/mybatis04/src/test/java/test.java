import com.jiazhong.bean.Emp;
import com.jiazhong.mapper.EmpMapper;
import com.jiazhong.service.EmpService;
import com.jiazhong.service.impl.EmpServiceImpl;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class test {
    private SqlSession session;
    private EmpMapper empMapper;
    {
        try {
            InputStream in = Resources.getResourceAsStream("config.xml");
            SqlSessionFactoryBuilder sqlSessionFactoryBuilder=new SqlSessionFactoryBuilder();
            SqlSessionFactory ssf =sqlSessionFactoryBuilder.build(in);
            session = ssf.openSession();
            empMapper = session.getMapper(EmpMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void selectEmpByEname(){
        Emp emp =new Emp();
        emp.setENAME("a");
//        emp.setJOB("SALESMAN");
        List<Emp> o = empMapper.selectEmpByEname(emp);
//        for (Emp emp:o) {
//            System.out.println(emp);
//        }
        o.forEach(System.out::println);
    }

    @Test
    public void updateEmpByEMPNO(){
        Emp emp =new Emp();
        emp.setENAME("TOKEN");
        emp.setEMPNO(7521);
        int i=empMapper.updateEmpBy(emp);
        if (i!=0){
            System.out.println(i+"行响应");
        }
        session.commit();
        session.close();
    }

    @Test
    public void selectEmpByJOB(){
        List<String> list = Arrays.asList("CLERK","MANAGER");
        List<Emp> emps = empMapper.selectEmpByJob(list);
        emps.forEach(System.out::println);
    }

//    @Test
//    public void select(){
//        Emp emp=new Emp();
//        EmpService empService=new EmpServiceImpl();
//        List<Emp> emps = empService.selectEmpByEmp(emp);
//        emps.forEach(System.out::println);
//    }
}
