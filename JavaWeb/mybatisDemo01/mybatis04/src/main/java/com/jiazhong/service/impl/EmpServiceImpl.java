package com.jiazhong.service.impl;

import com.jiazhong.bean.Emp;
import com.jiazhong.bean.EmpSelect;
import com.jiazhong.mapper.EmpMapper;
import com.jiazhong.service.EmpService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class EmpServiceImpl implements EmpService {

    private EmpMapper empMapper;
    private SqlSession session;
    {
        try {
            InputStream in = Resources.getResourceAsStream("config.xml");
            SqlSessionFactoryBuilder sqlSessionFactoryBuilder=new SqlSessionFactoryBuilder();
            SqlSessionFactory ssf =sqlSessionFactoryBuilder.build(in);
            session = ssf.openSession();
            empMapper = session.getMapper(EmpMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<Emp> selectEmpByEmp(EmpSelect emp) {
        List<Emp> emps = empMapper.selectEmpByOther(emp);
//        session.close();
        return emps;
    }

    @Override
    public List<String> selectAllJob() {
        return empMapper.selectAllJob();
    }
}
