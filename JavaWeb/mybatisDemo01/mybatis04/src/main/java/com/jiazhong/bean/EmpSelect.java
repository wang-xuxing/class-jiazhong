package com.jiazhong.bean;

import lombok.Data;

import java.sql.Date;

@Data
public class EmpSelect {
    private String ENAME;
    private Double salStart;
    private Double salEnd;
    private Date dateStart;
    private Date dateEnd;
    private String[] jobs;
    private String orderType;
}
