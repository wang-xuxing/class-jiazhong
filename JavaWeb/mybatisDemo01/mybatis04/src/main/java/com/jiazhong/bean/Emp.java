package com.jiazhong.bean;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class Emp implements Serializable {
    private Integer EMPNO;
    private String ENAME;
    private String JOB;
    private Integer MGR;
    private Date   HIREDATE;
    private Double SAL;
    private Double COMM;
    private Integer DEPTNO;
    private Dept dept;
}