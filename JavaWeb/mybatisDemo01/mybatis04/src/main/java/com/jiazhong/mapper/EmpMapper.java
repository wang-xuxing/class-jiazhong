package com.jiazhong.mapper;

import com.jiazhong.bean.Emp;
import com.jiazhong.bean.EmpSelect;


import java.util.List;

public interface EmpMapper {
    //查询
    List<Emp> selectEmpByEname(Emp emp);
    //修改
    int updateEmpBy(Emp emp);

    List<Emp> selectEmpByJob(List<String> jobs);

    //级联检索
    List<Emp> selectEmpByOther(EmpSelect empSelect);

    //查询所有工作
    List<String> selectAllJob();
}
