package com.jiazhong.service;

import com.jiazhong.bean.Emp;
import com.jiazhong.bean.EmpSelect;

import java.util.List;

public interface EmpService {

    List<Emp> selectEmpByEmp(EmpSelect empSelect);

    List<String> selectAllJob();
}
