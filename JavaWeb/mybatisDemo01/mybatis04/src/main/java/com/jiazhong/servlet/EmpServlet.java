package com.jiazhong.servlet;

import com.jiazhong.bean.Emp;
import com.jiazhong.bean.EmpSelect;
import com.jiazhong.service.EmpService;
import com.jiazhong.service.impl.EmpServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet("/emp")
public class EmpServlet extends BaseServlet {
    private EmpService empService = new EmpServiceImpl();

    protected void select(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //开始级联查询
        String ename = request.getParameter("ename");
        String salStart = request.getParameter("salStart");
        String salEnd = request.getParameter("salEnd");
        String dateStart = request.getParameter("dateStart");
        String dateEnd = request.getParameter("dateEnd");
        String[] jobs = request.getParameterValues("jobs");
        String orderType = request.getParameter("orderType");
        EmpSelect empSelect = new EmpSelect();
        empSelect.setENAME(ename);
        empSelect.setDateEnd("".equals(dateEnd) || dateEnd == null ? null : Date.valueOf(dateEnd));
        empSelect.setDateStart("".equals(dateStart) || dateStart == null ? null : Date.valueOf(dateStart));
        empSelect.setSalStart("".equals(salStart) || salStart == null ? null : Double.parseDouble(salStart));
        empSelect.setSalEnd("".equals(salEnd) || salEnd == null ? null : Double.parseDouble(salEnd));
        empSelect.setJobs(jobs);
        empSelect.setOrderType(orderType);
        List<Emp> emps = empService.selectEmpByEmp(empSelect);
        request.getSession().setAttribute("emps", emps);
        response.sendRedirect("empList.jsp");
    }


    protected void selectJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> jobs = empService.selectAllJob();
        request.getSession().setAttribute("jobs", jobs);
        response.sendRedirect("empList.jsp");
    }
}
