<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2023/1/10
  Time: 22:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<style>
    table {
        border: 1px solid grey;
        margin: auto;
        width: 80%;
        border-collapse: collapse;
        text-align: center;
    }

    th {
        background-color: gainsboro;
        border: 1px solid grey;
    }

    td {
        background-color: lightgoldenrodyellow;
        border: 1px solid grey;
    }

    button {
        margin-top: 10px;
        margin-left: 4px;
        width: 100px;
        height: 30px;
        border: 1px solid aquamarine;
        border-radius: 15px;
    }

    button:hover {
        background-color: palegoldenrod;
    }

    input {
        margin-top: 10px;
        border: 1px solid aquamarine;
        border-radius: 5px;
    }

</style>
<body>
<table>
    <form action="emp?method=select" method="post">
        <tr>
            <th>姓名:</th>
            <td colspan="6">
                <input type="text" name="ename" value=""/>
            </td>
        </tr>
        <tr>
            <th>工资:</th>
            <td colspan="6">
                <input type="number" name="salStart" value=""/>
                -
                <input type="number" name="salEnd" value=""/>
            </td>
        </tr>
        <tr>

            <th>入职时间:</th>
            <td colspan="6">
                <input type="date" name="dateStart" value=""/>
                -
                <input type="date" name="dateEnd" value=""/>
            </td>
        </tr>
        <tr>
            <th>职位:</th>
            <td colspan="6">
                <c:forEach var="job" items="${jobs}">
                    <span style="font-size:10px"><input type="checkbox"  name="jobs" value="${job}"/>${job}</span>
                </c:forEach>
                <button type="button" onclick="chooseAll(true)">全选</button>
                <button type="button" onclick="chooseAll(false)">全不选</button>
            </td>
        </tr>
        <tr>
            <th></th>
            <td colspan="6">
                <button type="submit">搜索一下</button>
            </td>
        </tr>
    </form>
</table>
<br/>
<table>
    <tr>
        <th>工号</th>
        <th>姓名</th>
        <th>职位</th>
        <th>上司ID</th>
        <th>入职时期</th>
        <th><a href="javascript:order()" id="salOrder">工资</a></th>
        <th>绩效</th>
        <th>部门编号</th>
        <th>部门名称</th>
        <th>部门地址</th>
    </tr>
    <c:forEach var="emp" items="${emps}" varStatus="j">
        <tr>
            <td>${emp.EMPNO}</td>
            <td>${emp.ENAME}</td>
            <td>${emp.JOB}</td>
            <td>${emp.MGR}</td>
            <td>${emp.HIREDATE}</td>
            <td>${emp.SAL}</td>
            <td>${emp.COMM}</td>
            <td>${emp.DEPTNO}</td>
            <td>${emp.dept.DNAME}</td>
            <td>${emp.dept.LOC}</td>
        </tr>
    </c:forEach>
</table>
<script>
    let  count =0;
    function chooseAll(bool) {
        let jobsObj = document.getElementsByName("jobs");
        for (let i = 0; i < jobsObj.length; i++) {
            jobsObj[i].checked = bool;
        }
    }

    function order() {
     let c=window.sessionStorage.getItem('c');
     if (c ===undefined){
         count=0;
     }else {
         count=c;
     }
     count++;
     window.sessionStorage.setItem('c',count);
     if (c%2===0){
         location.href="emp?method=select&orderType=desc"
     }else {
         location.href="emp?method=select&orderType=asc"
     }

    }
</script>
</body>
</html>
