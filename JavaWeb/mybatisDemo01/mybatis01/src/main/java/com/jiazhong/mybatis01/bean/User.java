package com.jiazhong.mybatis01.bean;
import lombok.Data;

import java.sql.Date;

@Data
public class User {
    private Integer userId;
    private String username;
    private Integer sex;
    private Date date;
    private Integer age;
    private String address;
}
