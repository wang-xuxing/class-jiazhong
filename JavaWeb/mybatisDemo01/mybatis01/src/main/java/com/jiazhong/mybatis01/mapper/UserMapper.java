package com.jiazhong.mybatis01.mapper;

import com.jiazhong.mybatis01.bean.User;

import java.util.List;

public interface UserMapper {
    //查询User表中所有数据
    List<User> selectAllUser();
    //给User表增加一条数据
    void  addUser(User user);
    //根据用户id删除User表一条数据
    void  deleteUserByUserid(int userid);
    //根据用户id修改表中一条数据
    void  updateUserByUserid(User user);

}
