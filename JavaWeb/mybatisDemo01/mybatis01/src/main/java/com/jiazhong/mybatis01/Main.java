package com.jiazhong.mybatis01;

import com.jiazhong.mybatis01.bean.User;
import com.jiazhong.mybatis01.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Main {
    /**
     * 查询user表中所有数据
     */
    static void testSelectAllUser() throws IOException {
        //1.加载config.xml配置文件
        InputStream in = Resources.getResourceAsStream("config.xml");
        //2.建立工厂
        SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
        //3.造手机
        SqlSessionFactory ssf = ssfb.build(in);
        //4.建立会话(java-mysql)
        SqlSession session = ssf.openSession();
        //5.获得接口的代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        //6.执行方法
        List<User> users = userMapper.selectAllUser();
        for (User user : users) {
            System.out.println(user);
        }
        //7.关闭会话
        session.close();
    }
    //插入测试
    static void testInsertUser() throws IOException {
        InputStream in = Resources.getResourceAsStream("config.xml");
        SqlSessionFactoryBuilder ssfb =new SqlSessionFactoryBuilder();
        SqlSessionFactory ssf = ssfb.build(in);
        SqlSession session = ssf.openSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user=new User();
        user.setUsername("张三");
        user.setSex(1);
        user.setAge(18);
        user.setAddress("陕西省宝鸡市");
        userMapper.addUser(user);
        session.commit();
        session.close();
    }

    //根据id删除数据测试
    static void testDeleteUserByUserid() throws IOException {
        InputStream in = Resources.getResourceAsStream("config.xml");
        SqlSessionFactoryBuilder ssfb =new SqlSessionFactoryBuilder();
        SqlSessionFactory ssf = ssfb.build(in);
        SqlSession session = ssf.openSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        userMapper.deleteUserByUserid(18);
        session.commit();
        session.close();
    }

    //根据id修改数据测试
    static void testUpdateUserByUserid() throws IOException {
        InputStream in = Resources.getResourceAsStream("config.xml");
        SqlSessionFactoryBuilder ssfb =new SqlSessionFactoryBuilder();
        SqlSessionFactory ssf = ssfb.build(in);
        SqlSession session = ssf.openSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user=new User();
        user.setUsername("小李子");
        user.setSex(0);
        user.setUserId(8);
        userMapper.updateUserByUserid(user);
        session.commit();
        session.close();
    }
    public static void main(String[] args) throws IOException {
        testUpdateUserByUserid();
    }
}