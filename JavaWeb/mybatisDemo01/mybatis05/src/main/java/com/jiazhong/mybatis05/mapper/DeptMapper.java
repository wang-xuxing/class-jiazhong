package com.jiazhong.mybatis05.mapper;

import com.jiazhong.mybatis05.bean.Dept;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface DeptMapper {
    @Select("select * from dept where DEPTNO=#{DEPTNO}")
    Dept selectDeptBy(int id);

    @Select("select * from dept where DEPTNO=#{DEPTNO}")
    @Results(id = "map03" ,value = {
            @Result(column = "DEPTNO",property = "emps" ,javaType = List.class,
            many = @Many(select = "com.jiazhong.mybatis05.mapper.EmpMapper.selectEmpByDeptno"))
    })
    Dept selectDeptByNO(int id);

    @Select("select * from dept where DEPTNO=#{DEPTNO}")
    @ResultMap("map03")
    Dept selectDeptByNO01(int id);


}
