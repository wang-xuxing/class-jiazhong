package com.jiazhong.mybatis05;

import com.jiazhong.mybatis05.bean.Dept;
import com.jiazhong.mybatis05.bean.Emp;
import com.jiazhong.mybatis05.mapper.DeptMapper;
import com.jiazhong.mybatis05.mapper.EmpMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.List;

public class Main {
    public static EmpMapper empMapper;
    public static SqlSession session;
    public static DeptMapper deptMapper;
    static {
        try {
            InputStream in = Resources.getResourceAsStream("config.xml");
            SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
            SqlSessionFactory ssf = ssfb.build(in);
            session = ssf.openSession(true);
            empMapper = session.getMapper(EmpMapper.class);
//            deptMapper= session.getMapper(DeptMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void selectDeptByNo(){
        Dept dept = deptMapper.selectDeptBy(20);
        System.out.println(dept);
    }
    static void selectEmp(){
        Emp emp=new Emp();
        emp.setENAME("a");
        List<Emp> emps = empMapper.selectEmp(emp);
        emps.forEach(System.out::println);
        session.close();
    }
    static void insertEmpById(){
        Emp emp=new Emp();
        emp.setEMPNO(8066);
        emp.setENAME("HUOZHE");
        emp.setJOB("CLERK");
        emp.setMGR(7798);
        emp.setHIREDATE(Date.valueOf("2002-01-03"));
        emp.setSAL(1000.0);
        emp.setDEPTNO(30);
        empMapper.insertEmpById(emp);
        session.close();
    }

    static void updateEmpById(){
        Emp emp=new Emp();
        emp.setEMPNO(7798);
        emp.setENAME("SMITH");
        emp.setHIREDATE(Date.valueOf("1981-1-1"));
        emp.setSAL(550.0);
        empMapper.updateEmpById(emp);
        session.close();
    }

      static void selectEmpAndDeptByEmpno(){
          Emp emp1 = empMapper.selectEmpAndDeptByEmpno(7788);
          Emp emp2 = empMapper.selectEmpAndDeptByEmpno(7788);
          System.out.println(emp1);
          System.out.println(emp2);
    }

    static void selectEmpByEmpno(){
        Dept dept = deptMapper.selectDeptByNO01(20);
        System.out.println(dept);
        dept.getEmps().forEach(System.out::println);
    }
    public static void main(String[] args) {
//        insertEmpById();
//        updateEmpById();
        selectEmpAndDeptByEmpno();
//        selectDeptByNo();
    }
}