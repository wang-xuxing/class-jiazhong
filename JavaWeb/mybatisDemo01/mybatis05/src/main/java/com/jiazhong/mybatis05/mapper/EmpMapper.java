package com.jiazhong.mybatis05.mapper;

import com.jiazhong.mybatis05.bean.Dept;
import com.jiazhong.mybatis05.bean.Emp;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface EmpMapper {
    //查询emp表所有员工
    @Select("<script>select * from Emp <where> <if test=\"ENAME!=null\"> and ENAME like concat('%',#{ENAME},'%') </if> </where> </script>")
    List<Emp> selectEmp(Emp emp);
    //插入一条数据
    @Insert("insert into emp value (#{EMPNO},#{ENAME},#{JOB},#{MGR} ,#{HIREDATE} ,#{SAL} ,#{COMM} ,#{DEPTNO})")
    void insertEmpById(Emp emp);
    //根据员工编号修改信息
    @Update("update emp set ENAME=#{ENAME},JOB=#{JOB},MGR=#{MGR} ,HIREDATE=#{HIREDATE} ,SAL=#{SAL} ,COMM=#{COMM} ,DEPTNO=#{DEPTNO} where EMPNO=#{EMPNO}")
    void updateEmpById(Emp emp);
    //删除用户
    @Delete("delete from emp where EMPNO=#{EMPNO}")
    void DeleteEmpById(int id);



    // 获取员工信息,并通过@one获取到部门信息
    @Select("select * from emp where EMPNO=#{EMPNO}")
    @Results(id = "map" , value = {
            @Result(column = "EMPNO" ,property = "EMPNO" , id = true),
            @Result(column = "DEPTNO" ,property = "dept",javaType = Dept.class
            ,one=@One(select = "com.jiazhong.mybatis05.mapper.DeptMapper.selectDeptBy")
            )
    })
    Emp selectEmpAndDeptByEmpno(int id);

    //通过部门编号查询员工
    @Select("select * from emp where DEPTNO=#{DEPTNO}")
    List<Emp> selectEmpByDeptno();
}
