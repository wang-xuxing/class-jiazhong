package com.jiazhong.mybatis05.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Dept implements Serializable {
    private Integer DEPTNO;
    private String DNAME;
    private String LOC;
    private List<Emp> emps;
}
