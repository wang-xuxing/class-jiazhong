package com.jiazhong.mybatis.bean.oneToMany;

import lombok.Data;

import java.io.Serializable;

import java.util.Set;

@Data
public class Dept01 implements Serializable {
    private Integer DEPTNO;
    private String DNAME;
    private String LOC;
    /**
     * 一对多，多个员工对应一个部门
     */
    private Set<Emp01> emps;
}
