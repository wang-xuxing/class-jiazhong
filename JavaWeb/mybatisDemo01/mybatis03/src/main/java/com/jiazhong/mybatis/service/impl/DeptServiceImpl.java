package com.jiazhong.mybatis.service.impl;

import com.jiazhong.mybatis.bean.oneToMany.Dept01;
import com.jiazhong.mybatis.bean.oneToMany.Emp01;
import com.jiazhong.mybatis.mapper.DeptMapper;
import com.jiazhong.mybatis.mapper.EmpMapper;
import com.jiazhong.mybatis.service.DeptService;

import java.util.Set;

public class DeptServiceImpl implements DeptService {
    private EmpMapper empMapper;
    private DeptMapper deptMapper;

    public DeptServiceImpl() {
    }

    public DeptServiceImpl(EmpMapper empMapper, DeptMapper deptMapper) {
        this.empMapper = empMapper;
        this.deptMapper = deptMapper;
    }

    public DeptServiceImpl(EmpMapper empMapper) {
        this.empMapper = empMapper;
    }
    @Override
    public Dept01 selectEmpNO02(int id) {
        Dept01 dept = deptMapper.selectDeptByDeptno02(id);
        Set<Emp01> emp01s = empMapper.selectEmpno02(dept.getDEPTNO());
        dept.setEmps(emp01s);
        return dept;
    }
}
