package com.jiazhong.mybatis.mapper;

import com.jiazhong.mybatis.bean.manyToOne.Emp;
import com.jiazhong.mybatis.bean.oneToMany.Dept01;
import com.jiazhong.mybatis.bean.oneToMany.Emp01;


import java.util.List;
import java.util.Set;

public interface EmpMapper {

    /**
     * 多对一关系
     *  处理方式1:拼接
     */
    //根据id查询员工信息
    Emp selectEmpno(int id);

    /**
     * 多对一关系
     * 处理方式2:关联
     */
    Emp selectEmpno01(int id);


    /**
     * 一对多关系
     * 处理方式1:拼接
     */
    Set<Emp01> selectEmpno02(int id);


}
