package com.jiazhong.mybatis.bean.oneToMany;


import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class Emp01 implements Serializable {
    private Integer EMPNO;
    private String ENAME;
    private String JOB;
    private Integer MGR;
    private Date   HIREDATE;
    private Double SAL;
    private Double COMM;
    private Integer DEPTNO;

}
