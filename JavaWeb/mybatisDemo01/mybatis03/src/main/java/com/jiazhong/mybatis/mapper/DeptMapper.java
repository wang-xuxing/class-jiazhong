package com.jiazhong.mybatis.mapper;


import com.jiazhong.mybatis.bean.manyToOne.Dept;
import com.jiazhong.mybatis.bean.oneToMany.Dept01;

import java.util.List;


public interface DeptMapper {
    //根据部门编号查询
    Dept selectDeptByDeptno(int id);

    /**
     * 一对多关系
     * 处理方式1:拼接
     */

    Dept01 selectDeptByDeptno02(int id);

    /**
     * 一对多关系
     * 处理方式2:集合
     */

    Dept01 selectDeptByDeptno03(int id);

}
