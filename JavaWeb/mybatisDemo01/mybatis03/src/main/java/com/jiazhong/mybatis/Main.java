package com.jiazhong.mybatis;

import com.jiazhong.mybatis.bean.User;
import com.jiazhong.mybatis.bean.manyToOne.Emp;
import com.jiazhong.mybatis.bean.oneToMany.Dept01;
import com.jiazhong.mybatis.bean.oneToMany.Emp01;
import com.jiazhong.mybatis.mapper.DeptMapper;
import com.jiazhong.mybatis.mapper.EmpMapper;
import com.jiazhong.mybatis.mapper.UserMapper;
import com.jiazhong.mybatis.service.DeptService;
import com.jiazhong.mybatis.service.EmpService;
import com.jiazhong.mybatis.service.impl.DeptServiceImpl;
import com.jiazhong.mybatis.service.impl.EmpServiceImpl;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

public class Main {
    public static UserMapper userMapper;
    public static DeptMapper deptMapper;
    public static EmpMapper empMapper;

    static {
        try {
            InputStream in = Resources.getResourceAsStream("config.xml");
            SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
            SqlSessionFactory ssf = ssfb.build(in);
            SqlSession session = ssf.openSession();
            userMapper = session.getMapper(UserMapper.class);
            deptMapper = session.getMapper(DeptMapper.class);
            empMapper = session.getMapper(EmpMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //通过员工id查询员工信息及其对应部门
    static void selectEmpByEmpno() {
        Emp emp = empMapper.selectEmpno01(7788);
        System.out.println(emp);
    }

    static void selectUserByUserId() {
        User user = userMapper.selectUserByUserid02(8);
        System.out.println(user);
    }

    //通过员工id部门及其所有员工
    static void selectDeptByDeptno() {
        DeptService deptService = new DeptServiceImpl(empMapper, deptMapper);
        Dept01 dept01 = deptService.selectEmpNO02(20);
        System.out.println("部门信息:"+dept01);
        System.out.println("部门员工有:");
        Set<Emp01> emps = dept01.getEmps();
        for (Emp01 emp: emps) {
            System.out.println(emp);
        }
    }
    static void selectDeptByDeptno01(){
        Dept01 dept = deptMapper.selectDeptByDeptno03(20);
        System.out.println("部门信息:"+dept);
        System.out.println("部门员工有");
        Set<Emp01> emps = dept.getEmps();
        for (Emp01 emp: emps) {
            System.out.println(emp);
        }
    }
    public static void main(String[] args) {
        selectDeptByDeptno01();
    }
}