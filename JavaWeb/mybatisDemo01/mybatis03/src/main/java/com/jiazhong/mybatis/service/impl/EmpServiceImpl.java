package com.jiazhong.mybatis.service.impl;

import com.jiazhong.mybatis.bean.manyToOne.Dept;
import com.jiazhong.mybatis.bean.manyToOne.Emp;
import com.jiazhong.mybatis.bean.oneToMany.Dept01;
import com.jiazhong.mybatis.bean.oneToMany.Emp01;
import com.jiazhong.mybatis.mapper.DeptMapper;
import com.jiazhong.mybatis.mapper.EmpMapper;
import com.jiazhong.mybatis.service.EmpService;

import java.util.List;
import java.util.Set;

public class EmpServiceImpl implements EmpService {

    private EmpMapper empMapper;
    private DeptMapper deptMapper;

    public EmpServiceImpl() {
    }

    public EmpServiceImpl(EmpMapper empMapper, DeptMapper deptMapper) {
        this.empMapper = empMapper;
        this.deptMapper = deptMapper;
    }

    public EmpServiceImpl(EmpMapper empMapper) {
        this.empMapper = empMapper;
    }

    @Override
    public Emp selectEmpNO(int id) {
        //员工信息
        Emp emp = empMapper.selectEmpno(id);
        //部门信息
        Dept dept = deptMapper.selectDeptByDeptno(id);
        //组装
        emp.setDept(dept);
        return emp;
    }

    @Override
    public Emp selectEmpNO01(int id) {
        Emp emp = empMapper.selectEmpno01(id);
        return emp;
    }


}
