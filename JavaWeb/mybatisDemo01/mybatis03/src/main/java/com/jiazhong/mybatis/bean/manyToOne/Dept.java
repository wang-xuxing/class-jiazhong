package com.jiazhong.mybatis.bean.manyToOne;

import lombok.Data;

import java.io.Serializable;


@Data
public class Dept implements Serializable {
    private Integer DEPTNO;
    private String DNAME;
    private String LOC;


}
