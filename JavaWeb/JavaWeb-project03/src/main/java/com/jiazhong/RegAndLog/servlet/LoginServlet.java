package com.jiazhong.RegAndLog.servlet;

import com.jiazhong.RegAndLog.DAO.UserDao;
import com.jiazhong.RegAndLog.DAO.impl.UserDaoImpl;
import com.jiazhong.RegAndLog.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username=req.getParameter("username");
        String password=req.getParameter("password");


        UserDao userDao=new UserDaoImpl();
        User user=userDao.login(username,password);

        if (user!=null){
            resp.sendRedirect("LoginSuccess.html");
        }else{
              //向请求对象设置属性
            req.setAttribute("errMSG","登陆失败请检查用户名和密码");
            req.getRequestDispatcher("index.jsp").forward(req,resp);
        }
    }
}
