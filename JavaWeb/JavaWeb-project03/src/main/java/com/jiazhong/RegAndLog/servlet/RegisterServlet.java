package com.jiazhong.RegAndLog.servlet;

import com.jiazhong.RegAndLog.DAO.UserDao;
import com.jiazhong.RegAndLog.DAO.impl.UserDaoImpl;
import com.jiazhong.RegAndLog.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register.do")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            //设置编码集
            req.setCharacterEncoding("UTF-8");

            //得到请求内容
            String UserName=req.getParameter("username");
            String Password=req.getParameter("password");


            //给实体赋值
            User user=new User();
            user.setUserName(UserName);
            user.setPassWord(Password);

            //调用底层方法
            UserDao userDao=new UserDaoImpl();
            userDao.register(user);
            //获得响应
            resp.sendRedirect("RegisterSuccess.html");
        }catch (Exception e){
            e.printStackTrace();
            resp.sendRedirect("Register.jsp");
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
