package com.jiazhong.RegAndLog.DAO;

import com.jiazhong.RegAndLog.model.User;

public interface UserDao {
    /**
     * 注册功能
     * @param user
     */
    public void register(User user) throws Exception;

    /**
     * 登录功能接口
     * @param UserName
     * @param PassWord
     * @return
     */
    public User login(String UserName,String PassWord);
}
