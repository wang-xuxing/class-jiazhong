package com.jiazhong.RegAndLog.DAO.impl;

import com.jiazhong.RegAndLog.DAO.DBUntil;
import com.jiazhong.RegAndLog.DAO.UserDao;
import com.jiazhong.RegAndLog.model.User;

import java.sql.SQLException;

public class UserDaoImpl  extends DBUntil implements UserDao {
    @Override
    public void register(User user) throws Exception {
        String sql="insert into test.admin value(default,?,?)";
        executeUpdate(sql,user.getUserName(),user.getPassWord());
    }

    @Override
    public User login(String UserName, String PassWord) {
        getConn();
        String sql="select * from test.admin where UserName=? AND PassWord=? ";
        try {
            ps=conn.prepareStatement(sql);
            ps.setString(1,UserName);
            ps.setString(2,PassWord);
            rs=ps.executeQuery();
            while (rs.next()){
              User user=new User();
                user.setUserId(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setPassWord(rs.getString(3));
                return user;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        CloseAll();
        return null;
    }
}
