package com.jiazhong.RegAndLog.DAO;

import java.sql.*;


public class DBUntil {
    protected Connection conn;
    protected PreparedStatement ps;
    protected ResultSet rs;

    /**
     * 封装驱动
     *
     * @return
     */
    public Connection getConn() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?" +
                            "serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false",
                    "root", "root");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 封装释放资源
     */
    public void CloseAll() {
        try {
            if (conn != null) {
                conn.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            conn = null;
            ps = null;
            rs = null;
        }
    }

    /**
     * 封装增删改操作
     */
    public int executeUpdate(String sql, Object... element) throws Exception {
        getConn();
        try {
            ps = conn.prepareStatement(sql);
            if (element != null && element.length != 0) {
                for (int i = 0; i < element.length; i++) {
                    ps.setObject(i + 1, element[i]);
                }
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("------");
            throw new Exception();
        }finally {
            CloseAll();
        }

    }

}
