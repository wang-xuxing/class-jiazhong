<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/11
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户登陆</title>
</head>
<style>
    body{
        background-color: aquamarine;
    }
    table{
        margin-top: 200px;
        height: 150px;
        width: 250px;
        text-align: center;
        border: 1px solid black;
        background-color: yellow;
    }
</style>
<body>
<h2 align="center">用户登陆</h2>
<%
    String errMSG=(String) request.getAttribute("errMSG");
    if (errMSG!=null){
%>
<p align="center" style="color: red"><%=errMSG%></p>
<%
    }
%>
<p></p>
<form method="post" action="login.do">
    <table align="center">
        <tr>
            <td>用户名：</td>
            <td><input type="text" name="username"></td>
        </tr>

        <tr>
            <td>密码：</td>
            <td><input type="password" name="password"></td>
        </tr>

        <tr>
            <td colspan="2">
                <a href="Register.jsp"><font size="1">注册</font></a>
                <button type="submit">登陆</button>
                <button type="reset">重置</button>
            </td>

        </tr>
    </table>
</form>
</body>
</html>
