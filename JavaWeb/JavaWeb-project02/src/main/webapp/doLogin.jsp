<%@ page import="com.jiazhong.registeAndLogin.dao.UserDao" %>
<%@ page import="com.jiazhong.registeAndLogin.dao.impl.UserDaoImpl" %>
<%@ page import="com.jiazhong.registeAndLogin.model.User" %><%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/9/21
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    /**
     * 1.接收用户数据
     * 2.调用底层类中的处理方法实现功能
     * 3.根据处理结果实现页面管理
     */
    //设置编码集
//    request.setCharacterEncoding("UFT-8");

    /**
       1.接受用户数据
     */
    String username=request.getParameter("username");
    String password=request.getParameter("password");

    /**
      2.调用底层类中的处理方法实现功能
     */
    UserDao userDao=new UserDaoImpl();
    User user=userDao.login(username,password);

    /**
     *3.根据处理结果实现页面管理
     */
    if (user!=null){
        //登陆成功
        response.sendRedirect("LogSucc.jsp");
    }
    else {
        //登陆失败
//        response.sendRedirect("login.jsp");         //重定向跳转
        //向请求对象设置属性(错误消息)
        request.setAttribute("errMSG","登陆失败，请检查用户名或密码");
        //使用请求转发实现跳转页面,将请求对象转发到指定位置
        request.getRequestDispatcher("login.jsp").forward(request,response);
    }

%>