<%@ page import="com.jiazhong.registeAndLogin.dao.UserDao" %>
<%@ page import="com.jiazhong.registeAndLogin.dao.impl.UserDaoImpl" %>
<%@ page import="com.jiazhong.registeAndLogin.model.User" %><%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/9/21
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    /**
     * 该页面作用是进行注册功能的处理，不做任何页面展示只是做一个处理页面
     * 该处理页所要做的事情有以下几步
     * 1.获得用户提交的数据
     * 2.调用底层类中的处理方法实现具体功能、
     * 3.根据处理结果进行页面跳转
     */
//设置request对象编码集
    request.setCharacterEncoding("UTF-8");
    /**
     * 1.或得用户提交的数据
     * 从请求对象(request)中获取用户提交的数据
     * 通过页面提交的表单的参数的参数名获得对应的值
     * 从客户端获得的所有数据都是字符串
     */
    String username=request.getParameter("username");
    String password=request.getParameter("password");

    /**
     * 2.调用底层类中的处理方法实现具体功能、
     */
    User user=new User();
    user.setUserName(username);
    user.setPassword(password);
    UserDao userDao=new UserDaoImpl();
    userDao.register(user);
    userDao.login(username,password);
    /**
     * 3.根据处理结果进行页面跳转
     * 通过响应对象跳转到注册成功页面(重定向)
     */

    response.sendRedirect("regSucc.jsp");

%>