package com.jiazhong.registeAndLogin.dao.impl;

import com.jiazhong.registeAndLogin.dao.DBUntils;
import com.jiazhong.registeAndLogin.dao.UserDao;
import com.jiazhong.registeAndLogin.model.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class UserDaoImpl extends DBUntils implements UserDao {
    public void register(User user) {
        String sql="insert into admin value(default,?,?)";
        executeUpdate(sql,user.getUserName(),user.getPassword());
    }

    public User login(String userName,String password) {
        try {
            String sql="select * from test.admin where UserName=? and PassWord=?";
            getConn();
            ps= conn.prepareStatement(sql);
            ps.setString(1,userName);
            ps.setString(2,password);
            rs= ps.executeQuery();
            while (rs.next()){
                User user=new User();
                user.setUserId(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setPassword(rs.getString(3));
                return user;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}
