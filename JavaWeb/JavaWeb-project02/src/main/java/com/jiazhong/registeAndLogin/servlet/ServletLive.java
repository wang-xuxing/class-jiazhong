package com.jiazhong.registeAndLogin.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/live.do")
public class ServletLive extends HttpServlet {
    public ServletLive() {
        System.out.println("执行Servlet构造方法");
    }

    /**
     * Servlet初始化方法
     * 该方法主要在Servlet创建后自动执行
     * 用于初始化Servlet所用的相关资源
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
//        super.init(config);
        System.out.println("init方法");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doGet(req, resp);
        System.out.println("use get");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doPut(req, resp);
        System.out.println("use post");
    }

    /**
     * Servlet销毁方法
     * 服务器关闭自动调用该方法
     * 清理服务器所用的资源
     */
    @Override
    public void destroy() {
//        super.destroy();
        System.out.println("destroy");
    }
}
