package com.jiazhong.registeAndLogin.dao;

import com.jiazhong.registeAndLogin.model.User;

import java.util.List;

public interface UserDao {
    /**
     *添加用户
     * @param user
     */
    public void register(User user);

    /**
     *登陆
     * @param userName
     * @param passWord
     * @return
     *
     */
    public User login(String userName,String passWord);

}
