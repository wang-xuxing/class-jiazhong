package com.jiazhong.registeAndLogin.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/test02.do")
public class requestServletTest extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name=     req.getParameter("name");
        System.out.println(name);

//        resp.sendRedirect("Test01.jsp");
        req.getRequestDispatcher("Test01.jsp").forward(req,resp );
    }

}
