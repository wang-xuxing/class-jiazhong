package com.jiazhong.servlets;

import com.jiazhong.dao.GoodsDao;
import com.jiazhong.dao.impl.GoodsDaoImpl;
import com.jiazhong.model.Goods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/update.do")
public class updateGoodsServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("UTF-8");
            Integer goodsId = Integer.parseInt(request.getParameter("goodsId"));
            String goodsName = request.getParameter("goodsName");
            Double price = Double.parseDouble(request.getParameter("price"));
            Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("startDate"));
            Goods goods=new Goods();
            goods.setGoodsId(goodsId);
            goods.setGoodsName(goodsName);
            goods.setPrice(price);
            goods.setStartDate(startDate);
            GoodsDao goodsDao=new GoodsDaoImpl();
            goodsDao.updateGoodsById(goods);

            request.getRequestDispatcher("query.do").forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("errMSG1","修改失败，商品编号不可重复哟!!!");
            request.getRequestDispatcher("updateGoods.jsp").forward(request,response);
        }
    }
}
