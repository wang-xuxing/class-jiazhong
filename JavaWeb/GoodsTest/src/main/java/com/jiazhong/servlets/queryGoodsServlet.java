package com.jiazhong.servlets;

import com.jiazhong.dao.GoodsDao;
import com.jiazhong.dao.impl.GoodsDaoImpl;
import com.jiazhong.model.Goods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/query.do")
public class queryGoodsServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        GoodsDao goodsDao = new GoodsDaoImpl();
        List<Goods> goods = goodsDao.queryGoods();
        request.setAttribute("goods", goods);

        request.getRequestDispatcher("queryGoods.jsp").forward(request, response);


    }
}
