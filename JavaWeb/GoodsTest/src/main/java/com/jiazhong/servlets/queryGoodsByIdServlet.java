package com.jiazhong.servlets;

import com.jiazhong.dao.GoodsDao;
import com.jiazhong.dao.impl.GoodsDaoImpl;
import com.jiazhong.model.Goods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/queryById.do")
public class queryGoodsByIdServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Integer goodsId = Integer.parseInt(request.getParameter("goodsId"));

        GoodsDao goodsDao = new GoodsDaoImpl();
        Goods good = goodsDao.queryGoodsById(goodsId);
        request.setAttribute("good", good);
        request.getRequestDispatcher("updateGoods.jsp").forward(request, response);

    }
}
