package com.jiazhong.servlets;

import com.jiazhong.dao.GoodsDao;
import com.jiazhong.dao.impl.GoodsDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteById.do")
public class deleteGoodsByIdServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        Integer goodsId=Integer.parseInt(request.getParameter("goodsId"));

        GoodsDao goodsDao=new GoodsDaoImpl();
        try {
            goodsDao.deleteGoodsById(goodsId);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        request.getRequestDispatcher("query.do").forward(request,response);
    }
}
