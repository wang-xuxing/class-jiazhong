package com.jiazhong.dao;

import com.jiazhong.model.Goods;

import java.util.List;

public interface GoodsDao {

    /**
     * 查询商品列表
     * @return
     */
    public List<Goods> queryGoods();

    /**
     * 根据商品编号查询商品信息
     * @param goodsId
     * @return
     */
    public Goods queryGoodsById(Integer goodsId);

    /**
     * 根据商品编号修改商品信息
     */
    public void updateGoodsById(Goods goods) throws Exception;

    /**
     * 根据
     * @param GoodId
     * @throws Exception
     */
    public void deleteGoodsById(Integer GoodId)throws  Exception;
}
