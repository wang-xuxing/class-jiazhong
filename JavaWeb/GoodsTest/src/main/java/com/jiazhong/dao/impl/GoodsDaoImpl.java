package com.jiazhong.dao.impl;

import com.jiazhong.dao.DBUtils;
import com.jiazhong.dao.GoodsDao;
import com.jiazhong.model.Goods;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodsDaoImpl extends DBUtils implements GoodsDao {

    @Override
    public List<Goods> queryGoods() {
        getConn();
        try {
            String sql = "select * from goods";
            ps = conn.prepareStatement(sql);
            rs=ps.executeQuery();
            List<Goods> goods = new ArrayList<>();
            while (rs.next()) {
                Goods good=new Goods();
                good.setGoodsId(rs.getInt(1));
                good.setGoodsName(rs.getString(2));
                good.setPrice(rs.getDouble(3));
                good.setStartDate(rs.getDate(4));
                goods.add(good);
            }
            return goods;
        } catch (SQLException e) {
            e.printStackTrace();
             }closeAll();
        return null;
    }

    @Override
    public Goods queryGoodsById(Integer goodsId) {
        getConn();
        try {
            String sql="select * from goods where goodsId=?";
            ps=conn.prepareStatement(sql);
            ps.setInt(1,goodsId);
            rs=ps.executeQuery();
            Goods good=new Goods();
            while (rs.next()){
                good.setGoodsId(rs.getInt(1));
                good.setGoodsName(rs.getString(2));
                good.setPrice(rs.getDouble(3));
                good.setStartDate(rs.getDate(4));
            }
            return good;
        } catch (SQLException e) {
            e.printStackTrace();
        }closeAll();
         return null;
    }


    @Override
    public void updateGoodsById(Goods goods) throws Exception {
        String sql="update goods set goodsName=?,price=?,startTime=? where goodsId=?";

        executeUpdate(sql,goods.getGoodsName(),goods.getPrice(),goods.getStartDate(),goods.getGoodsId());
    }

    @Override
    public void deleteGoodsById(Integer GoodId) throws Exception {
        String sql="delete from goods where goodsId=?";
        executeUpdate(sql,GoodId);
    }
}
