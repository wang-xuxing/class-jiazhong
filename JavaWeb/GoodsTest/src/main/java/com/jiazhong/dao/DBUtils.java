package com.jiazhong.dao;

import java.sql.*;

public class DBUtils {
    protected Connection conn;
    protected PreparedStatement ps;
    protected ResultSet rs;

    /**
     * 封装数据库驱动
     *
     * @return
     */
    public Connection getConn() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false",
                    "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 释放资源
     */
    public void closeAll() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            ps = null;
            conn = null;
        }
    }

    /**
     * 增删改操作
     *
     * @param sql
     * @param element
     * @return
     */
    public int executeUpdate(String sql, Object... element) throws Exception {
        getConn();
        try {
            ps = conn.prepareStatement(sql);
            if (element != null && element.length != 0) {
                for (int i = 0; i < element.length; i++) {
                    ps.setObject(i + 1, element[i]);
                }
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception();
        } finally {
            closeAll();
        }
    }
}

