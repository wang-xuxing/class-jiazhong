<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/21
  Time: 19:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>修改商品</title>
</head>
<style>
    table{
        background-color: navajowhite;
        width: 400px;
        height: 300px;
        border: 1px black solid;
        border-collapse: collapse;
        text-align: center;
    }
    tr,td{
        text-align: center;
        border: 1px black solid;
    }
</style>
<body>
<h2 align="center">修改商品信息</h2>

<form method="post" action="update.do">
    <c:if test="${requestScope.errMSG1!=null}">
        <h2 align="center" style="color: red">${requestScope.errMSG1}</h2>
    </c:if>
    <table align="center">
        <tr>
        <td>商品编号</td>
        <td><input name="goodsId" type="text" value="${requestScope.good.goodsId}"></td>
        </tr>
        <tr>
            <td>商品名称</td>
            <td><input name="goodsName" type="text" value="${requestScope.good.goodsName}"></td>
        </tr>
        <tr>
            <td>商品价格</td>
            <td><input name="price" type="text" value="${requestScope.good.price}"></td>
        </tr>
        <tr>
            <td>上架时间</td>
            <td><input name="startDate" type="date" value="${requestScope.good.startDate}"></td>
        </tr>

        <tr>
            <td colspan="2">
                <button type="submit">修改</button>
                <button type="reset">重置</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
