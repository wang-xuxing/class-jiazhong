<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/21
  Time: 17:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>首页</title>
</head>
<style>
    table {
        margin-top: 50px;
        width: 900px;
        height: auto;
        border: 1px black solid;
        border-collapse: collapse;
    }

    th, td {
        border: black 1px solid;
        height: 30px;
        line-height: 30px;
        text-align: center;
    }

    th {
        background-color: blanchedalmond;
    }
</style>
<body>
<h2 align="center">商品列表</h2>
<table align="center">
    <thead>
    <tr>
        <th>商品编号</th>
        <th>商品名称</th>
        <th>商品单价</th>
        <th>上架日期</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="goods" items="${requestScope.goods}">
        <tr>
            <td>${goods.goodsId}</td>
            <td>${goods.goodsName}</td>
            <td>${goods.price}</td>
            <td>${goods.startDate}</td>
            <td>
                <a href="queryById.do?goodsId=${goods.goodsId}">修改</a>
                <a href="deleteById.do?goodsId=${goods.goodsId}">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>

</table>

</body>
</html>
