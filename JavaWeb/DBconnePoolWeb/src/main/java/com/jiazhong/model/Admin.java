package com.jiazhong.model;

import lombok.Data;

@Data
public class Admin {
    private Integer UserId;
    private String UserName;
    private String PassWord;
}
