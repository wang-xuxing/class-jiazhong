package com.jiazhong.dao.Impl;

import com.jiazhong.dao.DBUntils;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends DBUntils implements UserDao {
    @Override
    public void addUser(User user) throws Exception {
        String sql = "insert into user value (default,?,?,?,?,?)";
        executeUpdate(sql, user.getUsername(), user.getSex(), user.getDate(), user.getAge(), user.getAddress());
    }

    @Override
    public List<User> QueryUser() {
        String sql="select * from user";
        return executeQuery(User.class,sql);
    }


    @Override
    public void deleteUser(int userid) throws Exception {
        String sql = "delete from user where userId=?";
        executeUpdate(sql, userid);
    }

    @Override
    public void updateUser(User user) throws Exception {
        String sql = "update user set username=? ,sex=? ,date=?,age=?,address=? where userid=?";
        executeUpdate(sql,user.getUsername(),user.getSex(),user.getDate(),user.getAge(),user.getAddress(),user.getUserId());

    }

    @Override
    public User queryById(int userId) {
        String sql="select * from user where userId=?";
        return (User) executeQueryOne(User.class,sql,userId);
    }

}
