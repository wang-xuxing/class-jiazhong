package com.jiazhong.dao;

import com.jiazhong.model.User;

import java.util.List;

public interface UserDao {
    /**
     * 添加用户
     * @param user
     */
    public void addUser(User user) throws Exception;

    /**
     * 查询用户
     * @return
     */
    public List<User> QueryUser();


    /**
     * 删除用户
     */
    public void deleteUser(int userId) throws Exception;


    /**
     * 修改用户
     *
     */

    public void updateUser(User user) throws Exception;

    /**
     * 根据ID查用户信息
     * 
     */
    public User queryById(int userId);
}
