<%@ page import="com.jiazhong.model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="com.jiazhong.model.Admin" %>

<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/16
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户列表</title>
</head>
<style>
    body {
        background-color: blanchedalmond;
    }

    table {
        width: 1080px;
        border: 1px solid black;
        border-collapse: collapse;
    }

    th, td {
        border: 1px solid black;
        height: 30px;
        line-height: 30px;
        text-align: center;
    }

    th {
        background-color: lavender;
        font-weight: bold;
    }
</style>
<body>
<h2 align="center">用户列表 </h2>
<%--<%--%>
<%--    String errMSG1 = (String) request.getAttribute("errMSG1");--%>
<%--    /**--%>
<%--     * 从session中获取信息--%>
<%--     */--%>
<%--    if (errMSG1 != null) {--%>
<%--%>--%>
<c:if test="${requestScope.errMSG1!=null}">
    <h2 style="color: red" align="center">${requestScope.errMSG1}
    </h2>
</c:if>
<%--<%--%>
<%--    }--%>
<%--<%--%>
<%--    Admin admin = (Admin) session.getAttribute("admin");--%>
<%--    if (admin == null) {--%>
<%--%>--%>
<c:choose>
    <c:when test="${sessionScope.admin==null}">
        <h2 style="color: red" align="center">你还未登陆...!!!
        </h2>
    </c:when>
<%--<%--%>
<%--    }--%>
<%--    else--%>
<%--    {--%>
<%--%>--%>
    <c:otherwise>
          <h4 style="color: darkslateblue" align="center">欢迎您:${sessionScope.admin.userName}
    </c:otherwise>
</c:choose>
<%--    (--%>
<%--    )%>--%>
<%--</h4>--%>
<%--<%--%>
<%--    }--%>
<%--%>--%>

<table align="center">
    <thead>
    <tr>
        <th>用户编号</th>
        <th>用户名</th>
        <th>性别</th>
        <th>出生日期</th>
        <th>年龄</th>
        <th>地址</th>
        <th>功能</th>
    </tr>
    </thead>
    <tbody>
    <%--    <%--%>
    <%--        //从request对象中读取Users属性的值--%>
    <%--        List<User> users = (List<User>) request.getAttribute("users");--%>

    <%--        //遍历集合--%>
    <%--        if (users != null) {--%>
    <%--    // for (User user : users) {--%>
    <%--    %>--%>
    <c:if test="${requestScope.users!=null}">
        <c:forEach var="user" items="${requestScope.users}">
            <tr>
                <td>${user.userId}
                </td>
                <td>${user.username}
                </td>
                <td>${user.sex == 1 ? "男" : "女"}
                </td>
                <td>${user.date}
                </td>
                <td>${user.age}
                </td>
                <td>${user.address}
                </td>
                <td>
                    <a href="user.do?method=deleteUser&userId=${user.userId}">删除</a>
                    <a href="user.do?method=queryById&userId=${user.userId}">修改</a>
                </td>
            </tr>
        </c:forEach>
    </c:if>
    <%--    <%--%>
    <%--           }--%>
    <%--        }--%>
    <%--    %>--%>
    </tbody>
</table>
<h4 align="center">
    <a href="addUser.jsp">添加用户</a>&nbsp;&nbsp;&nbsp;
    <a href="admin.do?method=exit">退出登陆</a>
</h4>
</body>
</html>
