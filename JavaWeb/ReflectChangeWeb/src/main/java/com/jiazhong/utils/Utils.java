package com.jiazhong.utils;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.locale.converters.DateLocaleConverter;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class Utils {

    /**
     * 使用反射编写一个赋值器，通过赋值器将值赋给对象的相关属性中
     *  	定义一个方法，参数为Class和Map集合
     *  	参数说明:
     *      1.Class 实体类的字节码对象
     *      2.Map集合的key为实体的属性名，value为属性的值
     *   	返回值:设置属性的对象
     */
    /**
     * 总体来说就是给调用属性的set方法，并给该属性赋值
     * 1.获得属性名和属性值，拼接该属性对应的set方法
     * 2.获得属性的field对象及方法的method对象
     * 3.调用该方法返回该类的对象
     *
     * @param tClass
     * @param map
     * @return
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     */
    public static <T> T Setter(Class<T> tClass,
                               Map<String, Object> map) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
//        使用反射实体类创建对象
        T t = tClass.getDeclaredConstructor().newInstance();
        //遍历集合
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            //获得属性名
            String fieldName = entry.getKey();
            //拼接set方法
            String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            //创建属性对象
            Field field = tClass.getDeclaredField(fieldName);
            //创建方法对象
            Method method = tClass.getDeclaredMethod(methodName, field.getType());
            //调用该方法
            method.invoke(t, entry.getValue());
        }
        return t;
    }


    /**
     * 定义一个请求参数自动封装的工具类，类中定义一个静态方法，该方法用于将请求的参数字符封装为一个JavaBean对象并返回；
     * 参数说明:
     * 1.Class要封装的类的字节码对象
     * 2.HttpServletRequest 请求对象
     * 返回值：封装好的对象
     */


    public static <T> T encapsulationRequestToBean(Class<T> tClass, HttpServletRequest request) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        T javaBean = tClass.getDeclaredConstructor().newInstance();
        Map<String, String[]> map = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : map.entrySet()) {
            //获取请求参数名(参数名=属性)
            String fieldName = entry.getKey();
            //获得参数值，默认为第一个参数
            String value = entry.getValue()[0];
            if (fieldName.equals("method")){
                continue;
            }
            //拼接获得方法名
            String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            //获得属性对象
            Field field = tClass.getDeclaredField(fieldName);
            //获得方法对象
            Method method = tClass.getDeclaredMethod(methodName, field.getType());
            //使用Converter自身带有的类型转换器注册对Date进行处理

            /**
             * 方案1：判断属性类型如果是util.Date则进行手动转换
             */
//            if (field.getType() == Date.class) {
//                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(entry.getValue()[0]);
//                 method.invoke(t, date);
//            } else {
//                method.invoke(t, ConvertUtils.convert(entry.getValue(), field.getType()));
//            }
            /**
             * 方案2：使用自己的转换器，向Convert中注册
             */
//            ConvertUtils.register(new Converter() {
//                @Override
//                public Object convert(Class aClass, Object o) {
//                    if (entry.getValue()[0] == null) {
//                        return null;
//                    }
//                    if (!(entry.getValue()[0] instanceof String)) {
//                        throw new ConversionException("只支持字符类转换");
//                    }
//                       String str=(String) entry.getValue()[0];
//                    try {
//                        return new SimpleDateFormat("yyyy-MM-dd").parse(str);
//                    } catch (ParseException e) {
//                        throw new RuntimeException(e);
//                    }
//                }
//            }, Date.class);

            /**
             * 方案3：向Convert中注册一个类型转换，该类型转换器BeanUtils中提供的转换器
             *
             *   向ConvertUtils中注册一个转换器，DteLocaleConverter
             *   参数1：类型转换器对象
             *   参数2：转换类型，当需要转换时使用该类型当前注册的类型转换器进行转换
             */
            ConvertUtils.register(new DateLocaleConverter(Locale.getDefault(), "yyyy-MM-dd"), Date.class);
            //调用方法
            method.invoke(javaBean, ConvertUtils.convert(value, field.getType()));
        }
        return javaBean;
    }
}
