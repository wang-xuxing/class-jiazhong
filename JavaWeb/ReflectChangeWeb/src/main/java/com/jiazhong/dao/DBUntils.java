package com.jiazhong.dao;


import com.jiazhong.model.User;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * jdbc工具类
 */
public class DBUntils<T> {
    protected Connection conn = null;
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;

    /**
     * 封装驱动
     *
     * @return
     */
    public Connection getConn() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?" +
                            "serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false",
                    "root", "root");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 释放资源
     */
    public void closeAll() {
        try {
            if (conn != null) {
                conn.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn = null;
            ps = null;
            rs = null;

        }
    }

    /**
     * 封装增删改操作
     *
     * @param sql
     * @param element
     * @return
     */
    public int executeUpdate(String sql, Object... element) throws Exception {
        getConn();
        try {
            ps = conn.prepareStatement(sql);
            if (element != null && element.length != 0) {
                for (int i = 0; i < element.length; i++) {
                    ps.setObject(i + 1, element[i]);
                }
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("------");
            throw new Exception();
        } finally {
            closeAll();
        }

    }

    /**
     * 封装查询多行数据功能
     *
     * @param clas
     * @param sql
     * @param params
     * @return
     */
    public List<T> executeQuery(Class<T> clas, String sql, Object... params) {
        try {
            getConn();
            ps = conn.prepareStatement(sql);
            if (params != null && params.length != 0) {
                for (int i = 0; i < params.length; i++) {
                    ps.setObject(i + 1, params[i]);
                }
            }
            rs = ps.executeQuery();
            List<T> list = new ArrayList<>();
            //获取元数据
            ResultSetMetaData metaData = ps.getMetaData();
            //获取查询列数
            int countNum = metaData.getColumnCount();
            while (rs.next()) {
                //创建对象
                T t = clas.getDeclaredConstructor().newInstance();
                for (int i = 0; i < countNum; i++) {

                    //列名与属性名一致
                    String fieldName = metaData.getColumnName(i + 1);
                    //拼接得到set方法
                    String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    //获得属性对象
                    Field field = clas.getDeclaredField(fieldName);
                    //获得对应方法对象
                    Method method = clas.getDeclaredMethod(methodName, field.getType());
                    //获得一列数据,并将该数据转换为指定的类型
                    Object value = rs.getObject(i + 1, field.getType());
                    method.invoke(t, value);
                }
                list.add(t);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    /**
     * 封装查询单行数据功能
     *
     * @param clas
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    public <T> T executeQueryOne(Class<T> clas, String sql, Object... params) {
        try {
            getConn();
            ps = conn.prepareStatement(sql);
            if (params != null && params.length != 0) {
                for (int i = 0; i < params.length; i++) {
                    ps.setObject(i + 1, params[i]);
                }
            }
            rs = ps.executeQuery();
            //获取元数据
            ResultSetMetaData metaData=ps.getMetaData();
            //获取列数
            int countNum = metaData.getColumnCount();
            if (rs.next()){
//                创建类对象
                T t=clas.getDeclaredConstructor().newInstance();
                for (int i=0;i<countNum;i++){
                    //获取列名=属性名
                    String fieldName= metaData.getColumnName(i+1);
                    //获取方法名
                    String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    //获得属性对象
                    Field field = clas.getDeclaredField(fieldName);
                    //获得对应方法对象
                    Method method = clas.getDeclaredMethod(methodName, field.getType());
                    //获取列值
                    Object value =rs.getObject(i+1,field.getType());
                    method.invoke(t,value);
                }
                return t;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }


    /**
     * 通用查询-统计查询
     * @param sql       SQL语句
     * @param params    SQL语句中？号占位符的值
     * @return          存有查询结果的Bean对象
     */
    public Long executeQueryCount(String sql, Object... params){
        try {
            //连接数据库
            this.getConn();
            //创建预处理对象
            ps = conn.prepareStatement(sql);
            //判断SQL语句中是否包含?号占位符(params有值表示有？号占位符，有一个值表示有一个？，有多个值表示有多个？)
            if(params!=null && params.length!=0){
                //循环设置？号占位符的
                for (int i = 0; i < params.length; i++) {
                    ps.setObject(i+1,params[i]);
                }
            }
            //执行查询语句
            rs = ps.executeQuery();
            if(rs.next()){
                return Long.valueOf(rs.getInt(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }  finally {
            closeAll();
        }
        return 0L;
    }

}
