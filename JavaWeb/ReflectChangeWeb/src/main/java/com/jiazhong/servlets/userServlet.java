package com.jiazhong.servlets;


import com.jiazhong.dao.Impl.UserDaoImpl;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;
import com.jiazhong.utils.Utils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user.do")
public class userServlet extends BaseServlet {
    protected void queryUserList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDao userDao = new UserDaoImpl();
        List<User> users = userDao.QueryUser();
        request.setAttribute("users", users);
        request.getRequestDispatcher("UserList.jsp").forward(request, response);
    }

    protected void queryById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userId = Integer.parseInt(request.getParameter("userId"));

        UserDao userDao = new UserDaoImpl();
        User user = userDao.queryById(userId);
        request.setAttribute("user", user);
        request.getRequestDispatcher("updateUser.jsp").forward(request, response);
    }

    protected void deleteUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        int userId = Integer.parseInt(request.getParameter("userId"));

        UserDao userDao = new UserDaoImpl();
        try {
            userDao.deleteUser(userId);
            response.sendRedirect("user.do?method=queryUserList");
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("errMSG1", "删除失败!");
            request.getRequestDispatcher("user.do?method=queryUserList").forward(request, response);
        }
    }

    protected void updateUserList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            UserDao userDao = new UserDaoImpl();
            User user = Utils.encapsulationRequestToBean(User.class, request);
            userDao.updateUser(user);
            response.sendRedirect("user.do?method=queryUserList");
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("errMSG2", "修改失败！");
            request.getRequestDispatcher("updateUser.jsp").forward(request, response);
        }
    }

    protected void addUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            User user = Utils.encapsulationRequestToBean(User.class, request);

            UserDao userDao = new UserDaoImpl();
            userDao.addUser(user);

            response.sendRedirect("user.do?method=queryUserList");
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("errMSG", "添加失败!");
            request.getRequestDispatcher("addUser.jsp").forward(request, response);
        }
    }
}
