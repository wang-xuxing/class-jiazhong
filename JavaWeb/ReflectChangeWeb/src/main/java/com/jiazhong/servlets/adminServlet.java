package com.jiazhong.servlets;


import com.jiazhong.dao.AdminDao;
import com.jiazhong.dao.Impl.AdminDaoImpl;
import com.jiazhong.model.Admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/admin.do")
public class adminServlet extends BaseServlet {
    /**
     * 登录方法
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //请求对象
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        //处理数据
        AdminDao adminDao = new AdminDaoImpl();
        Admin admin = adminDao.login(username, password);

        //得到响应
        if (admin != null) {
            request.getSession().setAttribute("admin", admin);
            response.sendRedirect("user.do?method=queryUserList");
        } else {
            request.setAttribute("errMSG0", "登陆失败,请检查用户名和密码!");
            request.getRequestDispatcher("userLogin.jsp").forward(request, response);
        }

    }

    protected void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.invalidate();

        response.sendRedirect("index.jsp");
    }
}
