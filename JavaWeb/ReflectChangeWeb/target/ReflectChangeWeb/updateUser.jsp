<%@ page import="com.jiazhong.model.User" %><%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/17
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>修改用户</title>
</head>
<style>
    body {
        background-color: burlywood;
    }

    table {
        width: 300px;
        border: 1px solid black;
        border-collapse: collapse;
    }

    td {
        border: 1px solid black;
        height: 40px;
        line-height: 30px;
        text-align: center;
    }


</style>
<body>
<h2 align="center">修改用户</h2>
<%--<%--%>
<%--    User user = (User) request.getAttribute("user");--%>
<%--    String errMSG2=(String) request.getAttribute("errMSG2");--%>
<%--    if (errMSG2!=null){--%>
<%--%><h2 style="color: red"><%=errMSG2%></h2>--%>
<%--<%--%>
<%--    }--%>
<%--%>--%>

<form action="user.do?method=updateUserList" method="post">
    <c:if test="${requestScope.errMSG2!=null}">
        <h2 align="center" style="color: red">&nbsp;&nbsp;&nbsp;${requestScope.errMSG2} </h2>
        <h3 align="center" style="color:darkslateblue"><a href="user.do?method=queryUserList">&nbsp;返回用户列表</a> </h3>
    </c:if>
    <table align="center">
        <input type="hidden" name="userId" value="${requestScope.user.userId}">

        <tr>
            <td>用户名:</td>
            <td><input type="text" name="username" value="${requestScope.user.username}"></td>
        </tr>
        <tr>
            <td>性别:</td>
            <td>
                <%--                <%--%>
                <%--                    if (user.getSex() == 1) {--%>
                <%--                %>--%>
                <%--                <input type="radio" name="sex" value="1" checked >男--%>
                <%--                <input type="radio" name="sex" value="0">女--%>
                <%--                <%--%>
                <%--                }else {--%>
                <%--                %>--%>

                <%--                <input type="radio" name="sex" value="1">男--%>
                <%--                <input type="radio" name="sex" value="0"checked>女--%>

                <%--                <%--%>
                <%--                    }--%>
                <%--                %>--%>
                <c:choose>
                    <c:when test="${requestScope.user.getSex()==1}">
                        <input type="radio" name="sex" value="1" checked>男
                        <input type="radio" name="sex" value="0">女
                    </c:when>
                    <c:otherwise>
                        <input type="radio" name="sex" value="1">男
                        <input type="radio" name="sex" value="0" checked>女
                    </c:otherwise>
                </c:choose>

            </td>
        </tr>
        <tr>
            <td>出生日期:</td>
            <td><input type="date" name="date" value="${requestScope.user.date}"></td>
        </tr>
        <tr>
            <td>年龄:</td>
            <td><input type="number" name="age" value="${requestScope.user.age}"></td>
        </tr>
        <tr>
            <td>用户地址:</td>
            <td><input type="text" name="address" value="${requestScope.user.address}"></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="submit">修改</button>
                <button type="reset">重置</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
