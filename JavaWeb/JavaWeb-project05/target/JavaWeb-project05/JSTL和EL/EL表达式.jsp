<%@ page import="com.jiazhong.model.User" %>
<%@ page import="com.jiazhong.model.Student" %><%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/19
  Time: 17:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>EL标签用于替换JSP表达式代码</h2>

           ${"真的删了"}
<c:set value="1" var="sex" scope="request"/>${sex==1?"男":"女"}
<br><hr>
<%
    Integer i=Integer.valueOf((String) request.getAttribute("sex"));
%>
<%=i+10%>
${sex+11}
<hr>

      <c:set var="name" value="sessionName" scope="session"/>
      <c:set var="name" value="application" scope="application"/>
      <c:set var="name" value="requestName" scope="request"/>
${sessionScope.name}------->session作用域取值<br>
${requestScope.name}-------->request作用域取值<br/>
${applicationScope.name}------->application取值<br/>

<%
    Student student=new Student("小臭臭","05");
    request.setAttribute("student",student);
%>
${ requestScope.student}
</body>
</html>
