<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/19
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%--
    taglib:引入JSTL标签库
    prefix:设置标签的前缀，核心标签一般为c
    url:指定标签库位置
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>out标签的使用
    <br>
    out标签用于向页面输出内容
</h3>
<%
    request.setAttribute("name", "<h2 style='color:red'>我是大晒个</h2>");
%>
<c:out value="${name}" default="没有数据" escapeXml="false"/>
</br>
<hr>
<h3>set标签的使用
    <br>
    set标签用于向某个作用域设置属性
</h3>
<c:set value="20" var="age" scope="request"></c:set>
<c:out value="${age}" default="没有数据" escapeXml="false"/>
</br>
<hr>
<h3>remove标签的使用
    <br>
    remove标签用于删除作用域设置属性
</h3>
<c:remove var="age" scope="request"></c:remove>
<c:out value="${age}" default="没有数据" escapeXml="false"/>
<br>
<hr>
<c:if test="${1==1}">你很聪明马</c:if>
<br>
<hr>
<c:choose>
    <c:when test="1=1">1</c:when>
    <c:when test="2=2">2</c:when>
    <c:otherwise>其他</c:otherwise>
</c:choose>
<br>
<hr>
<%
    List<String> strList = new ArrayList<>();
    strList.add("1");
    strList.add("2");
    strList.add("3");
    request.setAttribute("strList", strList);
%>
<c:set var="str" value="${strList}" scope="request"></c:set>
<c:forEach var="str1" items="${str}" varStatus="status">
    ${str1}--->${status.index}---->${status.count}<br>
</c:forEach>

<c:forEach begin="1" end="9" step="1" varStatus="status">
    ${status.index}-------->你好《=<br>
</c:forEach>
<br>
<hr>
<%
    request.setAttribute("nowTime", new Date());
%>
<fmt:formatDate value="${nowTime}" pattern="yyyy年MM月dd日 HH:mm:ss"/>
<br>
<fmt:formatNumber value="1234567895.646" pattern="###,###,###.##"></fmt:formatNumber>
</body>
</html>
