package com.jiazhong.dao.Impl;

import com.jiazhong.dao.DBUntils;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends DBUntils implements UserDao {
    @Override
    public void addUser(User user) throws Exception {
        String sql = "insert into user value (default,?,?,?,?,?)";
        executeUpdate(sql, user.getUsername(), user.getSex(), user.getDate(), user.getAge(), user.getAddress());
    }

    @Override
    public List<User> QueryUser() {
        try {
            getConn();
            String sql = "select * from user";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            List<User> user = new ArrayList<User>();
            while (rs.next()) {
                User user1 = new User();
                user1.setUserId(rs.getInt(1));
                user1.setUsername(rs.getString(2));
                user1.setSex(rs.getInt(3));
                user1.setDate(rs.getDate(4));
                user1.setAge(rs.getInt(5));
                user1.setAddress(rs.getString(6));
                user.add(user1);

            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    @Override
    public void deleteUser(int userid) throws Exception {
        String sql = "delete from user where userId=?";
        executeUpdate(sql, userid);
    }

    @Override
    public void updateUser(User user) throws Exception {
        String sql = "update user set uesrname=? ,sex=? ,date=?,age=?,address=? where userid=?";
        executeUpdate(sql,user.getUsername(),user.getSex(),user.getDate(),user.getAge(),user.getAddress(),user.getUserId());

    }

    @Override
    public User queryById(int userId) {
        try {
            getConn();
            String sql = "select * from user where userId=?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            User user1 = new User();
            while (rs.next()) {

                user1.setUserId(rs.getInt(1));
                user1.setUsername(rs.getString(2));
                user1.setSex(rs.getInt(3));
                user1.setDate(rs.getDate(4));
                user1.setAge(rs.getInt(5));
                user1.setAddress(rs.getString(6));

            }
            return user1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

}
