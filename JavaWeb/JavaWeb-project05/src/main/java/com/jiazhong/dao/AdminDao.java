package com.jiazhong.dao;

import com.jiazhong.model.Admin;

public interface AdminDao {
    /**
     * 管理员登陆接口
     * @param username
     * @param password
     * @return
     */
    public Admin login(String username, String password);
}
