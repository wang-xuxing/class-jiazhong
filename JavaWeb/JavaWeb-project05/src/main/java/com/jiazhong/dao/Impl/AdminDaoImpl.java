package com.jiazhong.dao.Impl;

import com.jiazhong.dao.DBUntils;
import com.jiazhong.dao.AdminDao;
import com.jiazhong.model.Admin;

import java.sql.SQLException;

public class AdminDaoImpl extends DBUntils implements AdminDao {
    public Admin login(String username, String password) {
        getConn();
        String sql="select * from test.admin where UserName=? and PassWord=?";
        try {
            ps=conn.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            rs= ps.executeQuery();
            while (rs.next()){
                Admin admin=new Admin();
                admin.setUserId(rs.getInt(1));
                admin.setUserName(rs.getString(2));
                admin.setPassWord(rs.getString(3));
                return admin;
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
        closeAll();
        return null;
    }
}
