package com.jiazhong.servlets;


import com.jiazhong.dao.Impl.UserDaoImpl;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/queryById.do")
public class queryByIdServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        int userId=Integer.parseInt(request.getParameter("userId"));

        UserDao userDao = new UserDaoImpl();
        User user=userDao.queryById(userId);
        request.setAttribute("user",user);
        request.getRequestDispatcher("updateUser.jsp").forward(request,response);
    }
}
