package com.jiazhong.servlets;

import com.jiazhong.dao.AdminDao;
import com.jiazhong.dao.Impl.AdminDaoImpl;
import com.jiazhong.model.Admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login.do")
public class loginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            request.setCharacterEncoding("UTF-8");

            //请求对象
            String username=request.getParameter("username");
            String password=request.getParameter("password");

            //处理数据
            AdminDao adminDao=new AdminDaoImpl();
            Admin admin=adminDao.login(username,password);

            //得到响应
            if (admin!=null){
                request.getSession().setAttribute("admin",admin);
                response.sendRedirect("userList.do");
            }
            else {
                request.setAttribute("errMSG0","登陆失败,请检查用户名和密码!");
                request.getRequestDispatcher("userLogin.jsp").forward(request,response);
            }

    }
}
