package com.jiazhong.servlets;

import com.jiazhong.dao.Impl.UserDaoImpl;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/addUser.do")
public class addServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        try {
            //发送请求
            String username =request.getParameter("username");
            Integer sex = Integer.valueOf(request.getParameter("sex"));
            Date date=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
            Integer age=Integer.valueOf(request.getParameter("age"));
            String address=request.getParameter("address");

            User user=new User();
            user.setUsername(username);
            user.setSex(sex);
            user.setDate(date);
            user.setAge(age);
            user.setAddress(address);

            UserDao userDao=new UserDaoImpl();
            userDao.addUser(user);

            response.sendRedirect("userList.do");
        }catch (Exception e){
            e.printStackTrace();
            request.setAttribute("errMSG","添加失败!");
            request.getRequestDispatcher("addUser.jsp").forward(request,response);
        }



    }
}
