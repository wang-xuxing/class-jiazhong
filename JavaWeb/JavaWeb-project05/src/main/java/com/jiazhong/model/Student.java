package com.jiazhong.model;

public class Student {
    private String name;
    private String stuId;

    public Student(String name, String stuId) {
        this.name = name;
        this.stuId = stuId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", stuId='" + stuId + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }
}
