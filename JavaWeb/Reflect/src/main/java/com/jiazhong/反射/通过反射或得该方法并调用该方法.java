package com.jiazhong.反射;

import com.jiazhong.model.User;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class 通过反射或得该方法并调用该方法 {

    public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Class<User> userClass= User.class;

        //获得方法对象，根据方法名和参数列表
        Method setUserId=userClass.getDeclaredMethod("setUserId", Integer.class);
        //调用该方法
        /**
         * 反射中调用方法
         * 方法对象名.invoke(类对象名,方法参数列表)
         */
        //创建类对象
        User user=userClass.newInstance();
        setUserId.invoke(user,2);

        Method getUserId=userClass.getDeclaredMethod("getUserId");
        Object userId=getUserId.invoke(user);
        System.out.println(userId);

    }
}
