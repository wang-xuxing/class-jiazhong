package com.jiazhong.反射;

import com.jiazhong.model.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

public class 通过反射操作类的构造器 {
    public static void main(String[] args) {
        Class<User> userClass = User.class;
        //获得类中构造方法
        Constructor[] constructors = userClass.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            //修饰符
            int modifiers = constructor.getModifiers();
            String modifier = Modifier.toString(modifiers);
            //参数列表
            Class[] aClass = constructor.getParameterTypes();
            System.out.print("参数列表");
            for (Class object : aClass) {
                System.out.print(object+"/");
            }
            //异常
            System.out.println();
            System.out.print("异常列表:");
            Class[] exception = constructor.getExceptionTypes();
            for (Class c:exception){
                System.out.print(c+" ");
            }
            //方法名
            String name = constructor.getName();
//
        }

    }
}
