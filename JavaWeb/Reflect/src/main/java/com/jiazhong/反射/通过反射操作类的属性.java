package com.jiazhong.反射;

import com.jiazhong.model.User;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * 1. 获得类的Class对象
 * 2.通过该类的Class对象获得该类的属性
 *    对象名.getFields():获得的属性是访问权限为public的属性（包括static属性）
 *
 *    对象名.getDeclaredFields();可或得该类所有方法(含static)
 * 3.获得类的属性名，修饰符，属性类型
 */
public class 通过反射操作类的属性 {
    public static void main(String[] args) {
        Class userClass= User.class;
//          通过得类的Class对象获得该类的属性
//        Field[] userFields   = userClass.getFields();
        Field[] userFields= userClass.getDeclaredFields();
        for (Field field :userFields){
            System.out.println(field);
            //获得属性名
            String fieldName =field.getName();
            System.out.println(fieldName);

            //获得属性类型
            Class type=field.getType();
            System.out.println(type);

            //获得属性修饰符
            int modifierNum =field.getModifiers();
            //由于通过getModifiers()获得的是整数形式，我们需要Modifier类这个整数进行解码
            String modifier=Modifier.toString(modifierNum);
            System.out.println(modifier);
            System.out.println("*******************");
        }


    }
}
