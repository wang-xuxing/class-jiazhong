package com.jiazhong.反射;

import com.jiazhong.model.User;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * 通过反射只能获得方法的声明部分
 * 不能获得方法的方法体
 */
public class 通过反射操作类中的方法 {
    public static void main(String[] args) {
        //获得类的Class对象
        Class<User> userClass= User.class;
        //获得该类方法
        Method[] userMethods = userClass.getDeclaredMethods();
        for (Method method:userMethods){
            //或得方法的修饰符
            int methodModifiersNum     =method.getModifiers();
            String methodModifiers = Modifier.toString(methodModifiersNum);
            //或得方法的名
            String methodName =method.getName();
            //方法的返回类型
            Class type=method.getReturnType();
            //获得参数列表
            Class[] parType=method.getParameterTypes();

            Class[] exceptions=method.getExceptionTypes();
            for (Class param : parType){
                System.out.println("参数列表"+param);
            }
//            System.out.println();
        }

    }
}
