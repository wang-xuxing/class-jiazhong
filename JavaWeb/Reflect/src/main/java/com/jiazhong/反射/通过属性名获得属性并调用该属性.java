package com.jiazhong.反射;

import com.jiazhong.model.User;
import java.lang.reflect.Field;

/**
 * 1.或得该类Class对象
 * 2.或得属性名
 * 3.调用该属性
 *
 */
public class 通过属性名获得属性并调用该属性 {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, InstantiationException {
        Class userClass= User.class;

        Field username=userClass.getDeclaredField("username");

        //反射调用该属性的方法
        //1.创建该类对象
        Object obj= userClass.newInstance();
        /**
         *   2.设置属性值
         *属性名.set(对象名,"属性值")
         */
       username.set(obj,"大王");
        /**
         * 3.调用属性
         */
        Object username1=username.get(obj);
        System.out.println(username1);

    }
}
