package com.jiazhong.反射;


import com.jiazhong.model.Admin;

/**
 * 反射是在运行时使用，我们需先获得类的Class对象
 * 三种方法：
 * 1.通过类的完整路径
 * 2.通过类名获取
 * 3.通过对象名获得对应Class 对象
 */
public class 反射的基本使用 {
    public static void main(String[] args) throws ClassNotFoundException {
        //      1.通过类的完整路径获得类的Class对象
        Class class1=Class.forName("com/jiazhong/model/Admin.java");
        // 2.通过类名获取
        Class class2= Admin.class;
        //3.通过对象名获得对应Class对象
        Admin admin=new Admin();
        Class class3 =admin.getClass();

        System.out.println(class1);
    }
}
