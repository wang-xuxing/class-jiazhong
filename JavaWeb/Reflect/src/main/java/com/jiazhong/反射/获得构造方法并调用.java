package com.jiazhong.反射;

import com.jiazhong.model.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class 获得构造方法并调用 {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Class<User> userClass=User.class;
        //使用默认构造方法创建对象
       //  Object o =userClass.newInstance();
      //获得无参构造方法
        Constructor constructor=userClass.getConstructor();
        //使用构造方法创建类的对象
        Object o =constructor.newInstance();

        //获得有参数的构造方法
        Constructor constructor1=userClass.getConstructor(Integer.class,String.class,Integer.class);
        Object o1=constructor1.newInstance(1,"张三",1);

        System.out.println(o1);
    }
}
