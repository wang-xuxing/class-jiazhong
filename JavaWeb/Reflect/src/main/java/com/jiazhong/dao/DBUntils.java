package com.jiazhong.dao;


import java.sql.*;

/**
 * jdbc工具类
 */
public class DBUntils {
    protected Connection conn=null;
    protected PreparedStatement ps=null;
    protected ResultSet rs=null;

    /**
     * 封装驱动
     * @return
     */
    public Connection getConn(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/test?" +
                            "serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&useSSL=false",
                    "root", "root");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 释放资源
     */
    public void closeAll(){
        try {
            if (conn!=null){
                conn.close();
            }
            if (ps!=null){
                ps.close();
            }
            if (rs!=null){
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            conn=null;
            ps=null;
            rs=null;

        }
    }

    /**
     * 封装增删改操作
     * @param sql
     * @param element
     * @return
     */
    public int executeUpdate(String sql,Object... element) throws Exception {
        getConn();
        try {
            ps = conn.prepareStatement(sql);
            if (element != null && element.length != 0) {
                for (int i = 0; i < element.length; i++) {
                    ps.setObject(i + 1, element[i]);
                }
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("------");
            throw new Exception();
        }finally {
            closeAll();
        }

    }

}
