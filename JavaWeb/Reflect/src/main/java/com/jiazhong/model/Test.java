package com.jiazhong.model;

import java.lang.reflect.Field;

public class Test {
    public static void main(String[] args) {
        Class userClass = User.class;
        Field[] userFields = userClass.getFields();
        for (Field field :
                userFields) {
            System.out.println(field);
        }
    }
}
