package com.jiazhong.model;

import lombok.Data;

import java.util.Date;
@Data
public class User {
    private Integer userId;
    public String username;
    private Integer sex;
    protected Date date;
    public Integer age;
    public String address;

    public User() {
    }

    public User(Integer userId, String username, Integer sex) throws Exception{
        this.userId = userId;
        this.username = username;
        this.sex = sex;

    }
}
