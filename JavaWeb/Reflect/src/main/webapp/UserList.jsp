<%@ page import="com.jiazhong.model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="com.jiazhong.model.Admin" %>

<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/16
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户列表</title>
</head>
<style>
    body {
        background-color: blanchedalmond;
    }

    table {
        width: 1080px;
        border: 1px solid black;
        border-collapse: collapse;
    }

    th, td {
        border: 1px solid black;
        height: 30px;
        line-height: 30px;
        text-align: center;
    }

    th {
        background-color: lavender;
        font-weight: bold;
    }
</style>
<body>
<h2 align="center">用户列表 </h2>
<%
    String errMSG1 = (String) request.getAttribute("errMSG1");
    /**
     * 从session中获取信息
     */
    Admin admin = (Admin) session.getAttribute("admin");
    if (errMSG1 != null) {
%>
<h2 style="color: red" align="center"><%=errMSG1%>
</h2>
<%
    }
    if (admin == null) {
%>
<h2 style="color: red" align="center">你还未登陆...!!!
</h2>
<%
} else {
%>
<h4 style="color: darkslateblue" align="center">欢迎您:<%=admin.getUserName()%>
</h4>
<%
    }
%>

<table align="center">
    <thead>
    <tr>
        <th>用户编号</th>
        <th>用户名</th>
        <th>性别</th>
        <th>出生日期</th>
        <th>年龄</th>
        <th>地址</th>
        <th>功能</th>
    </tr>
    </thead>
    <tbody>
    <%
        //从request对象中读取Users属性的值
        List<User> users = (List<User>) request.getAttribute("userList");

        //遍历集合
        if (users != null) {
            for (User user : users) {
    %>
    <tr>
        <td><%=user.getUserId()%>
        </td>
        <td><%=user.getUsername()%>
        </td>
        <td><%=user.getSex() == 1 ? "男" : "女"%>
        </td>
        <td><%=user.getDate()%>
        </td>
        <td><%=user.getAge()%>
        </td>
        <td><%=user.getAddress()%>
        </td>
        <td>
            <a href="delete.do?userId=<%=user.getUserId()%>">删除</a>
            <a href="queryById.do?userId=<%=user.getUserId()%>">修改</a>
        </td>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>
<h4 align="center">
    <a href="addUser.jsp">添加用户</a>&nbsp;&nbsp;&nbsp;
    <a href="exit.do">退出登陆</a>
</h4>
</body>
</html>
