package com.jiazhong.Thread.线程调度;

public class priorityDemo {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }, "t1");
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }, "t2");
//        t1.setPriority(10);
//        t2.setPriority(5);
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        System.out.println("t1线程优先级"+t1.getPriority());
        System.out.println("t2线程优先级"+t2.getPriority());
        t1.start();
        t2.start();
    }
}

