package com.jiazhong.Thread.implementRunnable;

/**
 * 当一个类实现了Runable接口该类就是一个线程任务类
 * 线程任务类只定义线程要执行的任务
 * 线程任务需要将线程任务提交给线程对象执行
 */
public class Demo01 implements Runnable {

    @Override
    public void run() {
        for (int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName()+"1:"+i);
        }
    }
}
