package com.jiazhong.Thread.Test.Test07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 七、开发一个应用程序，该程序的功能是对若干的整型数组计算总和，
 * 为了提高计算效率要求使用多线程技术开发。（提示:每一个数组由一个线程求和，最后再将这些和数求总和。）(提高题)
 */
public class Demo {

     static FutureTask<Integer>  futureTask1=new FutureTask<>(new Callable<Integer>() {
         @Override
         public Integer call() throws Exception {
             int[] nums={12,21,5,34,5,46,66,63,53,66,16,46,4,64,454,45,54,5,45,45,45,45,45,2,12,4,84,8,11,9,2,29,15,9,49,49,49,23};
             int sum=0;
             for (Integer integer: nums) {
                 sum=sum+integer;
             }
         return sum;
         }
     });
    static FutureTask<Integer>  futureTask2=new FutureTask<>(new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            int[] nums = {12, 21, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5,
                    46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 45,
                    4, 45, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 54, 5,
                    45, 45, 45, 45, 45, 2, 12, 4, 84, 8, 11, 9, 2, 29, 15, 9, 49, 49, 49, 23};
            int sum=0;
            for (Integer integer: nums) {
                sum=sum+integer;
            }
            return sum;
        }
    });

    static FutureTask<Integer>  futureTask3=new FutureTask<>(new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            int[] nums = {12, 21, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66,
                    16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 4,
                    6, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 54, 5, 45, 45, 45, 45, 45, 2, 12, 4, 84, 8, 11, 9, 2, 29, 15, 9, 49, 49, 49, 23};
            int sum=0;
            for (Integer integer: nums) {
                sum=sum+integer;
            }
            return sum;
        }
    });

    static FutureTask<Integer>  futureTask4=new FutureTask<>(new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            int[] nums = {12, 21, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45,
                    5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 54, 5, 45, 45, 45,
                    45, 45, 2, 12, 4, 84, 8, 11, 9, 2, 29, 15, 9, 49, 49, 49, 23};
            int sum=0;
            for (Integer integer: nums) {
                sum=sum+integer;
            }
            return sum;
        }
    });

    static FutureTask<Integer>  futureTask5=new FutureTask<>(new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            int[] nums = {12, 21, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66,
                    16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63, 53, 66, 16, 46, 4, 64, 454, 45, 5, 34, 5, 46, 66, 63
                    , 53, 66, 16, 46, 4, 64, 454, 45, 54, 5, 45, 45, 45, 45, 45, 2, 12, 4, 84, 8, 11, 9, 2, 29, 15, 9, 49, 49, 49, 23};
            int sum=0;
            for (Integer integer: nums) {
                sum=sum+integer;
            }
            return sum;
        }
    });

    static List<FutureTask<Integer>> futureTasks= Arrays.asList(futureTask1,futureTask2,futureTask3,futureTask4,futureTask5);
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ArrayList<Integer> arrayList=new ArrayList<>();
        for (FutureTask<Integer> futureTask: futureTasks) {
            new Thread(futureTask).start();
            arrayList.add(futureTask.get());
        }

        long sum=0;

        long start =System.currentTimeMillis();
        for (Integer integer: arrayList) {
            sum=sum+integer;
        }
        long end=System.currentTimeMillis();
        System.out.println("耗时:"+(end-start)+"总和:"+sum);
    }

}
