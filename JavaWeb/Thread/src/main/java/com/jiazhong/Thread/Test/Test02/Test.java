package com.jiazhong.Thread.Test.Test02;

public class Test extends Test02 implements Runnable{


    public Test(Thread thread) {
        super(thread);
    }

    @Override
    public void run() {
        try {
            print200_300();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
