package com.jiazhong.Thread.线程同步.死锁;

public class Test {
    public static void main(String[] args) {
        Demo demo=new Demo();

        new Thread(new Runnable() {
            @Override
            public void run() {
                demo.f1();
            }
        },"f1").start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                demo.f2();
            }
        },"f2").start();
    }
}
