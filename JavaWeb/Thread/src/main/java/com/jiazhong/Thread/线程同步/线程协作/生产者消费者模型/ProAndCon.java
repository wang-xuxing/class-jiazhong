package com.jiazhong.Thread.线程同步.线程协作.生产者消费者模型;

public class ProAndCon {
    public static void main(String[] args) {
        GuiZi guiZi=new GuiZi(10);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i< 100;i++){
                    MianBao mianBao=new MianBao(i+1);
                    guiZi.add(mianBao);
                }
            }
        },"生产者1").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i< 100;i++){
                    MianBao mianBao=new MianBao(i+1);
                    guiZi.add(mianBao);
                }
            }
        },"生产者2").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i< 100;i++){
                    guiZi.sub();
                }
            }
        },"消费者1").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i< 100;i++){
                    guiZi.sub();
                }
            }
        },"消费者2").start();
    }
}

