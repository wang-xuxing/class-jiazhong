package com.jiazhong.Thread.extendThread;

/**
 * 当一个类继承了Thread类，该类就成了一个线程类
 * 该类必须重写Thread中的run()方法
 */
public class Demo01 extends  Thread{
    /**
     * run方法不允许手动调用
     * 当开启一个线程后，该方法会自动执行
     */
    @Override
    public void run() {
        for (int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName()+"0:"+i);
        }
    }
}
