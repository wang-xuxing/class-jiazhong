package com.jiazhong.Thread.ThreadTools;


import java.util.concurrent.*;

public class 自定义ThreadPool {
    public static void main(String[] args) {
        /**
         * 使用自定义线程池对象
         * 参数1:初始化线程数(核心线程数,不会被销毁)
         * 参数2:最大线程数
         * 参数3:空闲时间，当一个非核心线程允许空闲的最大时间，如果空闲时间大于该时间则自动销毁
         * 参数4:空闲时间时间单位
         * 参数5:等待队列(阻塞队列)
         * 参数6:设置拒绝策略，当线程池无法接受新提交线程任务时，应该采取的措施
         * 拒绝策略(4种):
         *      new ThreadPoolExecutor.AbortPolicy()异常策略,当有新线程任务提交后会报拒绝异常(默认)
         *      new ThreadPoolExecutor.CallerRunsPolicy()谁提交谁执行策略，线程池已满，该策略将线程给提交者执行
         *      new ThreadPoolExecutor.DiscardPolicy()丢弃策略，当新任务提交后，线程池无法满足，直接将新提交任务丢弃
         *      new ThreadPoolExecutor.DiscardOldestPolicy()丢弃策略，丢弃等待时间最久的任务
         */
        //创建一个等待队列对象
        ArrayBlockingQueue queue = new ArrayBlockingQueue<>(2);
        ExecutorService executorService = new ThreadPoolExecutor(3,
                5,
                1000L,
                TimeUnit.MILLISECONDS,
                queue
                , new ThreadPoolExecutor.DiscardOldestPolicy());
        for (int i = 1; i <= 8; i++) {
            final int temp = i;
            executorService.submit(() -> {
                for (int i1 = 0; i1 < 10; i1++) {
                    System.out.println(Thread.currentThread().getName() + ":" + temp);
                }
                return null;
            });
        }
        executorService.shutdown();
    }
}
