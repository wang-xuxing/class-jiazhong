package com.jiazhong.Thread.线程同步.死锁;

public class Demo {
    public static Object LOCK1 = new Object();
    public static Object LOCK2 = new Object();

    public void f1(){
        System.out.println(Thread.currentThread().getName()+"等待获取锁1");
        synchronized (LOCK1){
            System.out.println(Thread.currentThread().getName()+"获得锁1,等待获取锁2");
        synchronized (LOCK2){
            System.out.println(Thread.currentThread().getName()+"获得锁2");
        }
            System.out.println(Thread.currentThread().getName()+"释放锁2");
        }
        System.out.println(Thread.currentThread().getName()+"释放锁1");
    }

    public  void f2(){
        System.out.println(Thread.currentThread().getName()+"等待获取锁2");
        synchronized (LOCK2){
            System.out.println(Thread.currentThread().getName()+"获得锁2,等待获取锁1");
            synchronized (LOCK1){
                System.out.println(Thread.currentThread().getName()+"获得锁1");
            }
            System.out.println(Thread.currentThread().getName()+"释放锁1");
        }
        System.out.println(Thread.currentThread().getName()+"释放锁2");
    }


}
