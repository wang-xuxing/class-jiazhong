package com.jiazhong.Thread.ThreadTools;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadDemo01 {
    public static void main(String[] args) {
        /**
         * 创建线程对象并返回一个ExecutorService对象
         * ExecutorService用于管理线程池，并向线程池提交任务
         * ExecutorService常用方法:
         *     ① shutdown():关闭线程池，平滑关闭，该方法执行后不再接受新线程任务，将已提交的线程任务结束后关闭线程池
         *     ② shutdownNow():立即关闭线程池，该方法执行后不再接受新线程任务，如果当前已提交线程任务未执行完，也立即关闭
         *     ③ submit():向线程池中提交线程任务,
         *     当一个线程任务被提交到线程池中，线程池会找到一个空闲线程执行该任务
         *     如果不存在空闲线程则等待
         */
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 5; i++) {
        //提交线程任务
        executorService.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                for (int i = 0; i < 100; i++) {
                    System.out.println(Thread.currentThread().getName()+":"+i);
                }
                return null;
            }
        });
        }
        executorService.shutdown();
    }
}
