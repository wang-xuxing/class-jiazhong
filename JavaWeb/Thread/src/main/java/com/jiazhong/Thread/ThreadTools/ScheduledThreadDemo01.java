package com.jiazhong.Thread.ThreadTools;

import java.util.concurrent.*;

public class ScheduledThreadDemo01 {
    public static void main(String[] args) {
        /**
         * 创建线程对象并返回一个ExecutorService对象
         * ExecutorService用于管理线程池，并向线程池提交任务
         * ExecutorService常用方法:
         *     ① shutdown():关闭线程池，平滑关闭，该方法执行后不再接受新线程任务，将已提交的线程任务结束后关闭线程池
         *     ② shutdownNow():立即关闭线程池，该方法执行后不再接受新线程任务，如果当前已提交线程任务未执行完，也立即关闭
         *     ③ submit():向线程池中提交线程任务,
         *     当一个线程任务被提交到线程池中，线程池会找到一个空闲线程执行该任务
         *     如果不存在空闲线程则等待
         */
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        //提交线程任务
        /**
         * 执行周期性任务线程
         * 参数1:线程任务(Runnable任务)不能执行Callable任务
         * 参数2:延迟时间，负数表示不延迟
         * 参数3:间隔时间量
         * 参数4:时间单位，该时间单位用于设置延迟时间和间隔时间的单位
         */
            scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    System.out.println("执行周期性任务...");
                }
            },0,1000, TimeUnit.MILLISECONDS);
    }
}
