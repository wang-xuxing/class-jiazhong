package com.jiazhong.Thread.implementRunnable;

/**
 * 一般使用
 */
public class Test01 {
    public static void main(String[] args) {
        //创建一个线程类对象并启动线程
        new Thread(() -> {
            for (int i=0;i<10;i++){
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        },"T0").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<10;i++){
                    System.out.println(Thread.currentThread().getName()+":"+i);
                }
            }
        },"T1").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<10;i++){
                    System.out.println(Thread.currentThread().getName()+":"+i);
                }
            }
        },"T2").start();

        for (int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName()+"1:"+i);
        }
    }
}
