package com.jiazhong.Thread.extendThread;

public class Test {
    public static void main(String[] args) {
        Demo01 demo01=new Demo01();
        //run方法为线程方法，该方法不允许手动调用
        //如果手动调用run()方法就成了线程内部调用一个方法，手动调用该方法就会被自动归入到调用线程
        demo01.run();
        //开启线程，开启后自动执行run()方法
//        demo01.start();
        for (int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName()+"1:"+i);
        }
    }
}
