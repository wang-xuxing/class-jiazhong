package com.jiazhong.Thread;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileFilter;

public class manyThreadView extends JFrame {
    private JButton button;
    private JLabel label;
      manyThreadView(){
          setBounds(200,150,800,500);
          setVisible(true);
          setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
          init();
      }
      private void init(){
          button= new JButton("扫描文件夹");
          button.setBounds(20,20,120,30 );
          //为按钮添加单机事件
          button.addActionListener(new AbstractAction() {
              @Override
              public void actionPerformed(ActionEvent e) {
//                  JOptionPane.showMessageDialog(manyThreadView.this,"多线程演示");
                  new Thread(new Runnable() {
                      @Override
                      public void run() {
                          ScannerFileFound(new File("E:/桌面/我爱学习"));
                      }
                  }).start();
                                }
          });
          getContentPane().add(button);
          label=new JLabel("文本");
          label.setBorder(new LineBorder(Color.BLUE));
          label.setBounds(140,20,520,30);
          getContentPane().add(label);
      }
    public static void main(String[] args) {

          new manyThreadView();
    }

    public  void ScannerFileFound(File file) {
        if (file == null) {//文件是否有效
            return;
        }
        if (file.isFile()) {
            System.out.println("-" + file.getAbsolutePath());
              label.setText("-" + file.getAbsolutePath());
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
//                    if (file.isDirectory()) {//如果是目录直接返回
//                        return true;
//                    }
//                    if (file.isFile()) {//如果是文件则判断是否为word文件
//                        if (file.getName().endsWith(".doc") || file.getName().endsWith(".docx")) {
                            return true;
//                        }
//                    }
//                    return false;
                }
            });
            if (files != null && files.length != 0) {//判断files是否有效
                for (File file1 : files) {//遍历子目录及其子文件
                    //递归调用此方法
                    ScannerFileFound(file1);
                }
            }
        }
    }
}



