package com.jiazhong.Thread.线程调度;

public class yieldDemo {
    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
                if (i>900){
                    Thread.yield();//线程让步
                }
            }
        }, "t1").start();
        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }, "t2").start();
    }
}

