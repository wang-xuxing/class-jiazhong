package com.jiazhong.Thread.Test.Test02;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * 开发一个功能类（该类未继承Thread类），该类有一个公有实例方法，功能为从201打印到300；
 * 再实现一个线程，该线程的父类是功能类，该线程的功能通过调用父类的功能方法实现。
 */
@NoArgsConstructor
public class Test02 {
    private  Thread thread;

    public Test02(Thread thread) {
        this.thread = thread;
    }

    public void print200_300() throws InterruptedException {
        for (int i = 201; i <= 300; i++) {
            if (i == 260){
                thread.join();
            }
                System.out.println(Thread.currentThread().getName() + ":" + i);
//            Thread.yield();
            //基于并发操作的休眠
            TimeUnit.MILLISECONDS.sleep(100);
        }
    }
}
