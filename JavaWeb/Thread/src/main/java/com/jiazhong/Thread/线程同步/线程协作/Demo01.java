package com.jiazhong.Thread.线程同步.线程协作;

public class Demo01 {

    private int num;

    public void add() {
        synchronized (this) {
            if (num == 50) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            num = num + 1;
            System.out.println(Thread.currentThread().getName() + ":" + num);
            notify();
        }
    }

    public synchronized void sub() {
        synchronized (this) {
            if (num == 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            num = num - 1;
            System.out.println(Thread.currentThread().getName() + ":" + num);
            notify();
        }
    }

    public static void main(String[] args) {
        Demo01 demo01 = new Demo01();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    demo01.add();
                }
            }
        }, "t1").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    demo01.sub();
                }
            }
        }, "t2").start();
    }
}



