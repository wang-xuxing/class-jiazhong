package com.jiazhong.Thread.线程同步.线程协作.生产者消费者模型;

/**
 * 面包柜
 */
public class GuiZi {
    private MianBao[] mianBaos;//面包容器
    private int size;//柜子中面包数量

    /**
     * 构造方法，初始面包数组
     * @param initSize
     */
    public GuiZi(int initSize) {
        //创建容器并设置大小
        mianBaos = new MianBao[initSize];
    }

    /**
     * 生产面包
     * @param mianBao
     */
    public synchronized void add(MianBao mianBao) {

        /**
         * 1.生产者线程继续生成，由于面包柜已满,此时size的值为10，面包师傅1线程进入等待状态
         * 2.如果此时还未消费，生产者2线程继续生成，由于面包柜已满,此时size的值为10，面包师傅2线程进入等待状态
         * 现在处于等待面包师傅线程有2个
         * 3.消费者线程消费一个商品，会唤醒所有处于等待的线程，此时生产者1和2两个线程同时被唤醒
         * 4.生产者1继续生产,生产后，size的值为10
         * 5.生产者1继续生产,由于唤醒后执行的位置是等待的位置，所以不会在检测面包柜是否已满，而此时面包柜满了，所以会报越界异常
         */
        while (size == mianBaos.length) {
            System.out.println("柜子已满....");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mianBaos[size] = mianBao;//添加一个面包对象
        size++;
        System.out.println(Thread.currentThread().getName() + ":生产第" + size + "个面包");
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        notifyAll();
    }

    /**
     * 消费面包
     *
     */
    public synchronized void sub() {
        while (size == 0) {
            System.out.println("柜子已空....");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + ":消费第" + size + "个面包");
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        size--;
        MianBao mianBao = mianBaos[size];
        notifyAll();
    }
}
