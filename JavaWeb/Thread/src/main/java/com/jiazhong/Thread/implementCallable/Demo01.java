package com.jiazhong.Thread.implementCallable;

import java.util.concurrent.Callable;

/**
 * 线程任务类
 */
public class Demo01 implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        int sum=0;
        for (int i=0;i<100;i++){
            sum+=i;
            System.out.println(Thread.currentThread().getName()+":"+i);
            Thread.sleep(1000);
        }
        return sum;
    }
}
