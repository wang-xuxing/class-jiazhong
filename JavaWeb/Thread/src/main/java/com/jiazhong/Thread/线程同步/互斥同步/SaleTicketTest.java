package com.jiazhong.Thread.线程同步.互斥同步;

public class SaleTicketTest {
    public static void main(String[] args) {
        SaleTicket saleTicket = new SaleTicket(10);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    saleTicket.sale();
                }
            }
        }, "1号窗口").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    saleTicket.sale();
                }
            }
        }, "2号窗口").start();
    }
}
