package com.jiazhong.Thread.线程同步.互斥同步;

import java.util.Random;

public class Demo01 {
    private static int num = 0;
    private Random random = new Random();
    //创建一个唯一锁对象
    private static final Object LOCK = new Object();

    /**
     * 为num+1
     * synchronized 同步关键字
     * 同步实现两种方式:
     * 1.同步代码块
     *       synchronized (锁对象) {同步代码块}
     *       PS：锁对象可以为任意对象，但是必须唯一，当本类的不同对象在多个线程中使用不能是this，因为this对像不唯一
     */
    public void add() {
        synchronized (this) {
            int result = num + 1;
            //进入阻塞状态()
            try {
                Thread.sleep(random.nextInt(5) + 1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            num = result;
            System.out.println(Thread.currentThread().getName() + ":" + num);
        }
    }
}
