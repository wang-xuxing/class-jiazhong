package com.jiazhong.Thread.implementCallable;



import java.util.concurrent.FutureTask;

public class Demo01Test  {
    public static void main(String[] args) throws InterruptedException {
        //创建demo01对象
        Demo01 demo01=new Demo01();
        //创建FutureTask对象，并将callableTask对象传入
        FutureTask  futureTask=new FutureTask<>(demo01);
        //执行线程任务
        Thread t1=new Thread(futureTask);
        t1.start();//启动现场
        /**
         * 循环输出主线程内容，并在适合位置取消线程任务
         */
        for (int i=0;i<100;i++){
            if (i==20){
                futureTask.cancel(true);//取消线程任务
                break;
            }
            System.out.println(Thread.currentThread().getName()+":"+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (futureTask.isCancelled()){//判断线程是否取消
            System.out.println("任务已经取消...");
        }
        while (!futureTask.isDone()){
            System.out.println("任务执行完成...");
            try {
                //获得线程任务执行结果，未完成进入堵塞状态
                System.out.println("结果为:"+futureTask.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("任务结束...");
            Thread.sleep(100);
        }
    }
}
