package com.jiazhong.Thread.线程同步.互斥同步;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SaleTicket {
    private Integer ticket;
    private static final Object LOCK = new Object();

    public SaleTicket(Integer ticket) {
        this.ticket = ticket;
    }

    public void sale() {
        synchronized (LOCK) {
            if (ticket <= 0) {
                System.out.println("票已售完....");
                return;
            }
            System.out.println(Thread.currentThread().getName() + ":售出第" + ticket + "张票");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ticket--;
        }
    }
}
