package com.jiazhong.Thread.线程调度;

public class joinDemo {
    public static void main(String[] args) {
        Thread t1=new Thread(() -> {
            for (int i=0;i<100;i++){
                System.out.println(Thread.currentThread().getName()+":"+i);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        },"t1");
        Thread t2=new Thread(() -> {
            for (int i=0;i<100;i++){
                System.out.println(Thread.currentThread().getName()+":"+i);
                if (i==50){//当i=50将t2连接到t1后
                    try {
                        t1.join();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        },"t2");
        t1.start();
        t2.start();
    }
}
