package com.jiazhong.Thread.线程同步.互斥同步;

public class Demo01Test {
    public static void main(String[] args) {
        Demo01 demo01=new Demo01();
        Demo01 demo02=new Demo01();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<100;i++){
                    demo01.add();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<100;i++){
                    demo02.add();
                }
            }
        }).start();
    }
}
