package com.jiazhong.Thread.Test.Test01;

import lombok.NoArgsConstructor;

/**
 * 开发一个线程类，该类的构造方法参数是线程名称，该线程的功能是从1打印到100。
 */
@NoArgsConstructor
public class Test01 extends Thread {
    public Test01(String ThreadName) {
//        this.setName(ThreadName);
        super(ThreadName);
    }

    @Override
    public void run() {
        for (int i=0;i<100;i++){
            System.out.println(Thread.currentThread().getName()+":"+i);
            try {
                Thread.sleep(100);
//                Thread.yield();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}
