package com.sm.service.impl;

import com.sm.mapper.UserMapper;
import com.sm.model.User;
import com.sm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public void addUser(User user) {
        userMapper.addUser(user);
    }

    @Override
    public void  deleteUserByUserId(int userId) {
         userMapper.deleteUserByUserId(userId);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public User selectUserById(int userId) {
        User user = userMapper.selectUserById(userId);
        return user;
    }


}
