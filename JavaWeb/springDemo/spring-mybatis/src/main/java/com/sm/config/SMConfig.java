package com.sm.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import javax.sql.DataSource;
import java.io.BufferedOutputStream;

@Configuration
@PropertySource("classpath:mybatis.properties")//引入外部文件
@ComponentScan("com.sm.service")
@MapperScan("com.sm.mapper")//映射器扫描器
@EnableTransactionManagement //开启事务管理器，开启后就可以使用spring提供的声明式事务
public class SMConfig {
    @Value("${jdbc.driverClassName}")
    private  String driverClassName;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;
    @Bean
    public DataSource dataSource(){
          //创建dataSource对象
        DruidDataSource dataSource=new DruidDataSource();
        //添加数据库连接属性
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        //数据库连接池属性
        dataSource.setMaxActive(50);
        dataSource.setInitialSize(20);
        dataSource.setMaxWait(1000);
        dataSource.setMinIdle(10);
        return dataSource;
    }

    /**
     * 配置sqlSessionFactory对象
     * @param dataSource
     * @return
     * @throws Exception
     */
    @Bean
    @Autowired
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource, ApplicationContext applicationContext) throws Exception {
        //创建sqlSessionBean对象
        SqlSessionFactoryBean sqlSessionFactoryBean=new SqlSessionFactoryBean();
        //设置数据源
        sqlSessionFactoryBean.setDataSource(dataSource);
        //设置日志
        org.apache.ibatis.session.Configuration configuration=new org.apache.ibatis.session.Configuration();
        configuration.setLogImpl(StdOutImpl.class);
        sqlSessionFactoryBean.setConfiguration(configuration);
        //配置映射文件
        //classpath:当前应用的根目录，编译后的classes目录
        Resource resource = applicationContext.getResource("classpath:mapper/userMapper.xml");
        sqlSessionFactoryBean.setMapperLocations(resource);

        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 配置事务管理器
     */
    @Bean
    @Autowired
    public DataSourceTransactionManager txManager(DataSource dataSource){
        //基于事务平台管理器
        //PlatformTransactionManager  platformTransactionManager
        //创建事务管理器对象
        DataSourceTransactionManager txManager =new DataSourceTransactionManager();
        txManager.setDataSource(dataSource);
        return txManager;
    }


}
