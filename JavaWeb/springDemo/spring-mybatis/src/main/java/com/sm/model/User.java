package com.sm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
   private Integer userId;
   private String username;
   private Integer sex;
   private Date date;
   private Integer age;
   private String address;
}
