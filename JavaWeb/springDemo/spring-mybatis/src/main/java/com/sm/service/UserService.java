package com.sm.service;


import com.sm.model.User;



public interface UserService {

    public void addUser(User user);

    public void  deleteUserByUserId(int userId);
    public void updateUser(User user);

    User  selectUserById(int userId);

}
