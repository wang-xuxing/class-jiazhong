package com.sm.mapper;

import com.sm.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;


public interface UserMapper {
    @Insert("insert into test.user values(default,#{username},#{sex},#{date},#{age},#{address})")
    public void addUser(User user);

    @Delete("delete from test.user where userId=#{userId}")
    public void deleteUserByUserId(int userId);


    public void updateUser(User user);

    User  selectUserById(int userId);
}
