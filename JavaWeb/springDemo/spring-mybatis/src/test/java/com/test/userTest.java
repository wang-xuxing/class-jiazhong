package com.test;

import com.sm.config.SMConfig;
import com.sm.model.User;
import com.sm.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import java.util.Date;


public class userTest {

    @Test
    public void addTest(){
        ApplicationContext  applicationContext=new AnnotationConfigApplicationContext(SMConfig.class);
        UserService userService = (UserService) applicationContext.getBean("UserService");
        User user=new User();
        user.setUsername("小张");
        user.setDate(new Date());
        user.setSex(1);
        user.setAge(0);
        user.setAddress("陕西省西安市");
        userService.addUser(user);
    }
    @Test
    public void deleteUserTest(){
        ApplicationContext applicationContext=new AnnotationConfigApplicationContext(SMConfig.class);
        UserService userService = (UserService) applicationContext.getBean("UserService");
        userService.deleteUserByUserId(134);
    }
    @Test
    public void updateUserTest(){
        ApplicationContext applicationContext=new AnnotationConfigApplicationContext(SMConfig.class);
        UserService userService = (UserService) applicationContext.getBean("UserService");
        User user=new User();
        user.setUserId(132);
        user.setUsername("小张");
        user.setDate(new Date());
        user.setSex(1);
        user.setAge(0);
        user.setAddress("陕西省西安市");
        userService.updateUser(user);
    }

    @Test
    public void selectUserById(){
        ApplicationContext applicationContext=new AnnotationConfigApplicationContext(SMConfig.class);
        UserService userService = (UserService) applicationContext.getBean("UserService");
        userService.selectUserById(2);
    }
}
