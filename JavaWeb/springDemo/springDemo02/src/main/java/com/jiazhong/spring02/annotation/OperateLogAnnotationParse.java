package com.jiazhong.spring02.annotation;

import com.jiazhong.spring02.service.UserService;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 解析注解
 */
public class OperateLogAnnotationParse {
    public static void main(String[] args) {
        Class<UserService> userServiceClass = UserService.class;
        Method[] methods = userServiceClass.getDeclaredMethods();
        for (Method method : methods) {
            OperateLogAnnotation operateLogAnnotation = method.getDeclaredAnnotation(OperateLogAnnotation.class);
            String value = operateLogAnnotation.value();
            System.out.println("操作内容:"+value);
            String time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new Date());
            System.out.println("操作时间:"+time);

        }
    }
}
