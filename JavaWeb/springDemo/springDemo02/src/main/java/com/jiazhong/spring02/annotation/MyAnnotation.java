package com.jiazhong.spring02.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解
 */
@Target({ElementType.TYPE,ElementType.METHOD,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    /**
     * 为注解定义属性
     * 当属性名为value时，在使用该属性时可用直接写属性值，不需要加属性名
     * 注解中属性的权限可使用public和默认的，但不可以使用protected或private
     * @return
     */
    public String value() default "";//声明一个属性并设置默认值
    int num() default 0;
}