package com.jiazhong.spring02.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Data
@Component("user01")
public class User {
   private Integer userId;
   private String username;
   private Integer sex;
   private Date date;
   private Integer age;
   private String address;

}
