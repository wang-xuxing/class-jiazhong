package com.jiazhong.spring02.service.impl;

import com.jiazhong.spring02.mapper.UserMapper;
import com.jiazhong.spring02.model.User;
import com.jiazhong.spring02.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.util.List;


@Service("UserS")
public class UserServiceImpl implements UserService {
    @Autowired
    @Qualifier("userMapperImpl")
    private UserMapper userMapper;

    public UserServiceImpl() {
        System.out.println("UserServiceImpl对象被创建");
    }

    @Override
    public void addUser(User user) {
        System.out.println("UserServiceImpl-->执行addUser");
        userMapper.addUser(user);
    }

    @Override
    public void deleteUser(int userId) {
        System.out.println("UserServiceImpl-->执行deleteUser");
        userMapper.deleteUser(userId);
    }

    @Override
    public void updateUser(User user) {
        System.out.println("UserServiceImpl-->执行updateUser");
        userMapper.updateUser(user);
    }

    @Override
    public List<User> selectUsers() {
        System.out.println("UserServiceImpl-->执行selectUser");
        List<User> users = userMapper.selectUsers();
        return users;
    }
}
