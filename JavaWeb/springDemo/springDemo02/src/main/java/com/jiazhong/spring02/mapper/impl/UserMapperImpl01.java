package com.jiazhong.spring02.mapper.impl;

import com.jiazhong.spring02.mapper.UserMapper;
import com.jiazhong.spring02.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserMapperImpl01 implements UserMapper {

    public UserMapperImpl01() {
        System.out.println("UserMapperImpl01构造方法被执行...");
    }

    @Override
    public void addUser(User user) {

    }

    @Override
    public void deleteUser(int userId) {

    }

    @Override
    public void updateUser(User user) {

    }

    @Override
    public List<User> selectUsers() {
        return null;
    }
}
