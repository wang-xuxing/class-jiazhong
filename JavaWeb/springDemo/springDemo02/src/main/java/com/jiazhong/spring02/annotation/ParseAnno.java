package com.jiazhong.spring02.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class ParseAnno {
    public static void main(String[] args) throws NoSuchMethodException {
        Class<AnnotationDemo> annotationDemoClass = AnnotationDemo.class;
        Method method = annotationDemoClass.getDeclaredMethod("say");
        //获得方法上面的注解
        MyAnnotation myAnnotation = method.getDeclaredAnnotation(MyAnnotation.class);
        String value = myAnnotation.value();
        int num = myAnnotation.num();
        System.out.println(value+"\n"+num);
    }
}
