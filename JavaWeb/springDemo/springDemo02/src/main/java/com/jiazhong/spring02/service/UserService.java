package com.jiazhong.spring02.service;

import com.jiazhong.spring02.annotation.OperateLogAnnotation;
import com.jiazhong.spring02.model.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService {
    @OperateLogAnnotation("添加操作")
    public void addUser(User user);
    @OperateLogAnnotation("删除操作")
    public void deleteUser(int userId);
    @OperateLogAnnotation("修改操作")
    public void updateUser(User user);
    @OperateLogAnnotation("查找操作")
    public List<User> selectUsers();
}
