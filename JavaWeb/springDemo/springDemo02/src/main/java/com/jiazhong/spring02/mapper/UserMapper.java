package com.jiazhong.spring02.mapper;

import com.jiazhong.spring02.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface UserMapper {
    public void addUser(User user);

    public void deleteUser(int userId);

    public void updateUser(User user);

    public List<User> selectUsers();
}
