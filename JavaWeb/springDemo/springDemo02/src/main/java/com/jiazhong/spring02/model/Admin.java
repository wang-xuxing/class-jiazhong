package com.jiazhong.spring02.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
@Data
@Component
public class Admin {

    @Resource(name = "user01")
    private User user01;
    @Resource(name = "user")
    private User user;


}
