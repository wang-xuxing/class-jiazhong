package com.jiazhong.spring02.config;


import com.jiazhong.spring02.mapper.UserMapper;
import com.jiazhong.spring02.mapper.impl.UserMapperImpl;
//import com.jiazhong.spring02.mapper.impl.UserMapperImpl01;
import com.jiazhong.spring02.service.UserService;
import com.jiazhong.spring02.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * 当一个类使用了@Configuration注解后该类就是一个配置类
 * 配置类可以对spring进行相关配置
 * 该类等同于spring的xml配置文件
 */
@Configuration
@ComponentScan({"com.jiazhong.spring02.mapper","com.jiazhong.spring02.service","com.jiazhong.spring02.model"})
public class AppConfig {

}
