package com.jiazhong.spring01.service.impl;

import com.jiazhong.spring01.mapper.UserMapper;
import com.jiazhong.spring01.mapper.impl.UserMapperImpl;
import com.jiazhong.spring01.model.User;
import com.jiazhong.spring01.service.UserService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {
    private UserMapper userMapper;
    private String str;
    private Integer num;
    private String[] strs;
    private List<String> list;
    private HashMap<String, Object> map;

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserMapper userMapper, String str, Integer num, String[] strs,
                           List<String> list, HashMap<String, Object> map) {
        System.out.println("UserServiceImpl对象被创建");
        this.userMapper = userMapper;
        this.str = str;
        this.num = num;
        this.strs = strs;
        this.list = list;
        this.map = map;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public void setMap(HashMap<String, Object> map) {
        this.map = map;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public void setStrs(String[] strs) {
        this.strs = strs;
    }

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public void addUser(User user) {
        System.out.println("执行addUser");
        System.out.println("str的值:" + str);
        System.out.println("num的值:" + num);
        System.out.println("strs数组中的值:");
        for (String s : strs) {
            System.out.println(s);
        }
        System.out.println("list集合中的值:");
        list.forEach(System.out::println);
        System.out.println("map中的值");

        for (Map.Entry<String,Object> entry: map.entrySet()){
            System.out.println(entry.getKey()+"--->"+entry.getValue());
        }
        userMapper.addUser(user);
    }

    @Override
    public void deleteUser(int userId) {
        System.out.println("执行deleteUser");
        userMapper.deleteUser(1);
    }

    @Override
    public void updateUser(User user) {
        System.out.println("执行updateUser");
    }

    @Override
    public List<User> selectUsers() {
        System.out.println("执行selectUsers");
        return null;
    }


}
