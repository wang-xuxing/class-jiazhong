package com.jiazhong.spring01.mapper;

import com.jiazhong.spring01.model.User;

import java.util.List;

public interface UserMapper {
    public void addUser(User user);

    public void deleteUser(int userId);

    public void updateUser(User user);

    public List<User> selectUsers();
}
