package com.jiazhong.spring01.mapper.impl;

import com.jiazhong.spring01.mapper.UserMapper;
import com.jiazhong.spring01.model.User;
import com.jiazhong.spring01.service.UserService;
import lombok.Data;

import java.util.List;

public class UserMapperImpl implements UserMapper {
    public UserMapperImpl() {
        System.out.println("UserMapperImpl对象被创建...");
    }

    @Override
    public void addUser(User user) {
        System.out.println("执行userMapper-addUser()");
    }

    @Override
    public void deleteUser(int userId) {

    }

    @Override
    public void updateUser(User user) {

    }

    @Override
    public List<User> selectUsers() {
        return null;
    }
}
