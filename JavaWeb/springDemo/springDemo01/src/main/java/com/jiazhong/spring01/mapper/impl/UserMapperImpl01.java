package com.jiazhong.spring01.mapper.impl;

import com.jiazhong.spring01.mapper.UserMapper;
import com.jiazhong.spring01.model.User;

import java.util.List;

public class UserMapperImpl01 implements UserMapper {

    public UserMapperImpl01() {
        System.out.println("UserMapperImpl01构造方法被执行...");
    }

    @Override
    public void addUser(User user) {

    }

    @Override
    public void deleteUser(int userId) {

    }

    @Override
    public void updateUser(User user) {

    }

    @Override
    public List<User> selectUsers() {
        return null;
    }
}
