package com.jiazhong.test;

import com.jiazhong.spring01.model.User;
import com.jiazhong.spring01.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.TimeUnit;

public class UserTest {
    @Test
    public void addUser(){
        //初始化spring容器对象，此时spring容器会解析application.xml中的内容
        ApplicationContext context=new ClassPathXmlApplicationContext("application.xml");
        //从容器中获取对象
        UserService userService=(UserService) context.getBean("userService");
        userService.addUser(new User());
    }
    @Test
    public void ObjTest() throws InterruptedException {
        ApplicationContext context=new ClassPathXmlApplicationContext("application.xml");
        TimeUnit.MILLISECONDS.sleep(1000);


        context.getBean("userMapper01");
        context.getBean("userMapper01");
    }
}
