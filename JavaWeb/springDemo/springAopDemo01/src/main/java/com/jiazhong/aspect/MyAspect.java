package com.jiazhong.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * 切面类
 * 切面类定义两方面内容:
 * (1)定义切入点，用于定义要拦截的方法(连接点)
 * (2)定义通知，拦截到方法后要做的事情
 */
@Aspect//切面注解当一个类使用该注解，该类就成为了一个切面类
//@Component
@EnableAspectJAutoProxy //开启切面自动代理模式
public class MyAspect {
    /**
     * 定义切入点
     * 切入点的声明类似于方法，但不能有方法体内容和参数
     * myPointCut():切入点的名字
     * 在切入点声明上加入连接点定义
     * 连接点使用切入点表达式定义(拦截到方法的定义)
     * spring连接点只能是方法
     */
//    @Pointcut("execution(public void com.jiazhong.service.impl.UserServiceImpl.addUser())")
    @Pointcut("execution(public void com.jiazhong.service..*.*(..))")
    public void myPointCut() {
    }

    /**
     * 定义相关通知:拦截到指定方法后要做的事情
     * 通知是指具体功能的方法
     *
     * @Before(切入点):通过切入点名字指定当前通知的连接点，当执行哪些方法方法时执行哪些通知
     **/
//    @Before("myPointCut() && args(args)")//前置通知注解，当一个方法使用该注解后则该方法称为
//    public void beforeAdvice(Object args) {
//        System.out.println("前置通知被执行"+args);
//    }
//
//    /**
//     *  后置通知
//     */
//    @AfterReturning("myPointCut()")
//    public void afterRunningAdvice(){
//        System.out.println("后置通知被执行");
//    }
//    /**
//     * 异常通知，目标方法异常时才执行
//     */
//    @AfterThrowing("myPointCut()")
//    public void exceptionAdvice(){
//        System.out.println("异常通知被执行");
//    }
//
//    /**
//     * 最终通知
//     */
//    @After("myPointCut()")
//    public void afterAdvice(){
//        System.out.println("最终通知被执行...");
//    }


    /**
     * 环绕通知
     * 可通过编程方法替换，前置，后置，异常，最终通知
     *   目标方法必须在环绕通知内部手动调用
     * 在环绕通知中为了实现更加灵活的通知处理需要传入参数:ProceedingJoinPoint
     * ProceedingJoinPoint:处理连接点参数，改参数内部封装了目标方法的签名
     * 使用该参数后可以在通知内部获得内部方法并调用目标方法
     * 方法签名:方法的声明
     */
    @Around("myPointCut()")
    public void aroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        try {
            System.out.println("环绕通知-->前置");
            proceedingJoinPoint.proceed();//调用目标方法
            /* Signature signature = proceedingJoinPoint.getSignature();//获得目标方法签名
            System.out.println(signature);*/
            System.out.println("环绕通知-->后置");
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println("环绕通知-->异常通知");
        } finally {
            System.out.println("环绕通知-->最终通知");
        }
    }
}
