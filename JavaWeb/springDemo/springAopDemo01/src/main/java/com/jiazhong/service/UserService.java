package com.jiazhong.service;

import com.jiazhong.annotation.OperateAnnotation;
import com.jiazhong.model.User;

import java.util.List;

public interface UserService {
    @OperateAnnotation("添加用户操作")
    public void addUser();
    @OperateAnnotation("删除用户操作")
    public void deleteUser(int userId);
    @OperateAnnotation("修改用户操作")
    public void updateUser(User user);
    @OperateAnnotation("查询用户操作")
    public List<User> selectUsers();
}
