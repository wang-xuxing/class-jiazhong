package com.jiazhong.mapper.impl;

import com.jiazhong.annotation.OperateAnnotation;
import com.jiazhong.mapper.UserMapper;
import com.jiazhong.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserMapperImpl implements UserMapper {
    public UserMapperImpl() {
        System.out.println("UserMapperImpl对象被创建...");
    }

    @Override

    public void addUser() {
        System.out.println("执行userMapper-addUser()");
    }

    @Override

    public void deleteUser(int userId) {
        System.out.println("执行userMapper-deleteUser()");
    }

    @Override

    public void updateUser(User user) {
        System.out.println("执行userMapper-updateUser()");
    }

    @Override

    public List<User> selectUsers() {
        System.out.println("执行userMapper-selectUsers()");
        return null;
    }
}
