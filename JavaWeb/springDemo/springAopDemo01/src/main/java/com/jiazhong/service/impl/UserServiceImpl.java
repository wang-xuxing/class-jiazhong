package com.jiazhong.service.impl;

import com.jiazhong.mapper.UserMapper;
import com.jiazhong.model.User;
import com.jiazhong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

@Service("UserService")
public class UserServiceImpl implements UserService {
    @Autowired
    @Qualifier("userMapperImpl")
    private UserMapper userMapper;

    public UserServiceImpl() {
    }
    public UserServiceImpl(UserMapper userMapper) {
        System.out.println("UserServiceImpl对象被创建");
        this.userMapper = userMapper;

    }

    @Override
    public void addUser() {
        System.out.println("执行addUser");
        userMapper.addUser();
    }

    @Override
    public void deleteUser(int userId) {
        System.out.println("执行deleteUser");
        userMapper.deleteUser(userId);
//        throw new RuntimeException();
    }

    @Override
    public void updateUser(User user) {
        System.out.println("执行updateUser");
    }

    @Override
    public List<User> selectUsers() {
        userMapper.selectUsers();
        System.out.println("执行selectUsers");
        return null;
    }


}
