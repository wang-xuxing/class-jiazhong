package com.jiazhong.mapper;

import com.jiazhong.annotation.OperateAnnotation;
import com.jiazhong.model.User;

import java.util.List;
public interface UserMapper {

    public void addUser();

    public void deleteUser(int userId);

    public void updateUser(User user);

    public List<User> selectUsers();
}
