package com.jiazhong.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.jiazhong.service")
@ComponentScan("com.jiazhong.mapper")
@ComponentScan("com.jiazhong.aspect")
public class AppConfig {
}
