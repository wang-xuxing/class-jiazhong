package com.jiazhong.aspect;

import com.jiazhong.annotation.OperateAnnotation;
import com.jiazhong.mapper.impl.UserMapperImpl;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 操作日志的切面
 */
@Component
@Aspect
@EnableAspectJAutoProxy
public class LogAspect {

    /**
     * 定义切入点
     */
    @Pointcut("execution(* com.jiazhong.service..*.*(..))")
    public void logPointCut(){}


    /**
     * 定义环绕通知
     *
     */
    @Around("logPointCut()")
    public void aroundAdvice(ProceedingJoinPoint proceedingJoinPoint){
        try {
            proceedingJoinPoint.proceed();//调用目标方法
            Signature signature = proceedingJoinPoint.getSignature();//获得方法签名
            MethodSignature methodSignature =(MethodSignature)signature;//将方法签名转化为MethodSignature对象
            Method method = methodSignature.getMethod();
            OperateAnnotation operateAnnotation = method.getDeclaredAnnotation(OperateAnnotation.class);
            if (operateAnnotation!=null){
                String value = operateAnnotation.value();
                String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new Date());
                System.out.println("操作时间:"+time+"\n操作内容:"+value);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
