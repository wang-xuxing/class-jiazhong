<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/16
  Time: 19:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>添加用户</title>
</head>
<style>
    table {
        width: 500px;
        border: 1px solid black;
        border-collapse: collapse;
    }

    td {
        border: 1px solid black;
        height: 35px;
        line-height: 35px;
    }

    td:first-child {
        text-align: right;
        padding-right: 5px;
    }

    td:last-child {
        padding-left: 5px;
    }
</style>
<body>
<h2 align="center">添加用户</h2>
<%--<%--%>
<%--    String errMSG=(String) request.getAttribute("errMSG");--%>
<%--    if (errMSG!=null){--%>
<%--%>--%>
<%--<h2 align="center" style="color: red"><%=errMSG%></h2>--%>
<%--<%--%>
<%--    }--%>
<%--%>--%>

<c:if test="${requestScope.errMSG!=null}">
    <h2 align="center" style="color: red">${requestScope.errMSG}</h2>
</c:if>
<form action="/User/user.do?method=addUser" method="post">
    <table align="center">
        <tr>
            <td>用户名:</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>性别:</td>
            <td>
                <input type="radio" name="sex" value="1">男
                <input type="radio" name="sex" value="0">女
            </td>
        </tr>
        <tr>
            <td>出生日期:</td>
            <td><input type="date" name="date"></td>
        </tr>
        <tr>
            <td>年龄:</td>
            <td><input type="number" name="age"></td>
        </tr>
        <tr>
            <td>用户地址:</td>
            <td><input type="text" name="address"></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="submit">提交</button>
                <button type="reset">重置</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
