package com.jiazhong.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 编码过滤器
 */
@WebFilter("/*")
public class EncodingFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {
            //将ServletRequest和ServletResponse强转为HttpRequest和HttpResponse
        HttpServletRequest request=(HttpServletRequest)servletRequest;
        HttpServletResponse response=(HttpServletResponse)servletResponse;
        //设置编码集
        request.setCharacterEncoding("UTF-8");
        //放行之前为请求期代码，放行后为相应期代码
        chain.doFilter(request,response);
        response.setContentType("text/html;charset=utf-8");
    }
}
