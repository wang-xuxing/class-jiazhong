package com.jiazhong.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/Demo/*","/User/*"})
public class SimpleQuanXianFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {
        //将ServletRequest和ServletResponse强转为HttpRequest和HttpResponse
        HttpServletRequest request=(HttpServletRequest)servletRequest;
        HttpServletResponse response=(HttpServletResponse)servletResponse;
        /**
         * 从session中获取属性名为admin的属性
         * 如果属性存在表示登录成功，放行
         * 如果不存在表示登录失败，继续登录
         */
        HttpSession session =request.getSession();
        Object obj=session.getAttribute("admin");
        if (obj!=null){//登陆成功
            chain.doFilter(request,response);
            return;
        }
        response.sendRedirect("/index.jsp");

    }
}
