package com.jiazhong.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

//@WebFilter("/admin.do/*")
public class demoFilter implements Filter {
    /**
     * 该方法用于实现过滤逻辑
     * 过滤器过滤功能在此方法中执行
     *
     * @param servletRequest  请求对象
     * @param servletResponse 响应对象
     * @param filterChain     过滤请对象
     * @throws IOException
     * @throws ServletException
     */

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filter被执行！！！");
        //将ServletRequest和ServletResponse强转为HttpRequest和HttpResponse
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Random random = new Random();
        if (random.nextInt(10) % 2 == 0) {
            filterChain.doFilter(servletRequest, servletResponse);//放行继续访问该位置
        } else {
            response.sendRedirect("/jsp.index");
        }
    }
}
