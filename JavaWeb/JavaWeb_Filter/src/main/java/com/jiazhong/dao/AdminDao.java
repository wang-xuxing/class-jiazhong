package com.jiazhong.dao;

import com.jiazhong.model.Admin;

public interface AdminDao {
    /**
     * 管理员登陆接口
     * @param username
     * @param password
     * @return
     */
    public Admin login(String username, String password);

    /**
     * 根据用户名判断用户名是否存在
     * @param username
     * @return
     */
    public Admin loadLoginByUsername(String username);
}
