package com.jiazhong.dao.Impl;

import com.jiazhong.dao.DBUntils;
import com.jiazhong.dao.AdminDao;
import com.jiazhong.model.Admin;

import java.sql.SQLException;

public class AdminDaoImpl extends DBUntils implements AdminDao {
    @Override
    public Admin login(String username, String password) {
        String sql ="select * from admin where username=? and password=?";
        return (Admin) executeQuery(Admin.class,sql,username,password);

    }

    @Override
    public Admin loadLoginByUsername(String username) {
        String sql ="select * from admin where username=?";
        return (Admin) executeQueryOne(Admin.class,sql,username);
    }



}
