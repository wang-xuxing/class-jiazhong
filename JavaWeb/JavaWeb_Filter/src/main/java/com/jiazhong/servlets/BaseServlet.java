package com.jiazhong.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * 基础Servlet类,继承HttpServlert
 * 该类只用于接受客户端请求的method然后根据method将请求转发到对应的Servlet方法进行处理
 * 约定:method的参数值,必须为某个"Servlet"方法名
 */
public class BaseServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获得客户端请求,只取参数为method的请求
        try {
//            request.setCharacterEncoding("UTF-8");
            String methodName = request.getParameter("method");
            if (methodName == null || methodName.trim().equals(" ")) {
                throw new Exception("请求失败,请携带method参数");
            }
            //每个方法的参数名为HttpServletRequest或HttpServletResponse
            Method method = this.getClass().getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
            //调用method方法
            method.invoke(this,request,response);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}
