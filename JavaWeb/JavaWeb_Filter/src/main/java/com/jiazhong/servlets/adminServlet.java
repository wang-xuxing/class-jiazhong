package com.jiazhong.servlets;


import com.jiazhong.dao.AdminDao;
import com.jiazhong.dao.Impl.AdminDaoImpl;
import com.jiazhong.exception.BadPassWordException;
import com.jiazhong.exception.NotFoundUsernameException;
import com.jiazhong.model.Admin;
import com.jiazhong.service.AdminService;
import com.jiazhong.service.impl.AdminServiceImpl;
import com.jiazhong.utils.Utils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;

@WebServlet("/admin.do")
public class adminServlet extends BaseServlet {
    /**
     * 登录方法
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //请求对象
        Admin admin = null;
        try {
            admin = Utils.encapsulationRequestToBean(Admin.class, request);
            //处理数据
            AdminService adminService = new AdminServiceImpl();
            adminService.login(admin.getUsername(), admin.getPassword());
            request.getSession().setAttribute("admin", admin);
            response.sendRedirect("/User/user.do?method=queryUserList");
        } catch (NotFoundUsernameException e) {
            response.sendRedirect("/userLogin.jsp?errMSG0="+ URLEncoder.encode(e.getMessage(),"UTF-8"));
        }catch (BadPassWordException e){
            response.sendRedirect("/userLogin.jsp?errMSG0="+URLEncoder.encode(e.getMessage(),"UTF-8"));
        }
        catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    protected void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.invalidate();

        response.sendRedirect("index.jsp");
    }
}
