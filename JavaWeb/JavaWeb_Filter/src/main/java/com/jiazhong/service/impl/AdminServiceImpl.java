package com.jiazhong.service.impl;

import com.jiazhong.dao.AdminDao;
import com.jiazhong.dao.Impl.AdminDaoImpl;
import com.jiazhong.exception.BadPassWordException;
import com.jiazhong.exception.NotFoundUsernameException;
import com.jiazhong.model.Admin;
import com.jiazhong.service.AdminService;

public class AdminServiceImpl implements AdminService {
    @Override
    public Admin login(String username, String password) throws NotFoundUsernameException,BadPassWordException{
        AdminDao adminDao=new AdminDaoImpl();
        Admin admin=adminDao.loadLoginByUsername(username);
        if (admin==null){
           throw new NotFoundUsernameException();
        }
        if (!admin.getPassword().equals(password)){//如果密码错误
            throw new BadPassWordException();
        }
        //设置user对象中的密码为null
        admin.setPassword(null);
        return admin;
    }
}
