package com.jiazhong.service;

import com.jiazhong.model.Admin;

public interface AdminService {
    public Admin login(String username,String password);
}
