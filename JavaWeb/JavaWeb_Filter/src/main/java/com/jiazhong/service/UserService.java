package com.jiazhong.service;

import com.jiazhong.model.User;

import java.util.List;

public interface UserService {
    /**
     * 查询所有用户
     * @return
     */
    public List<User> queryUser();

    /**
     * 根据用户id删除用户
     * @param userId
     */
    public void delUserById(int userId);

    /**
     * 根据用户名查询用户数据
     * @param userId
     * @return
     */
    public User queryUserById(int userId);

    /**
     * 修改用户数据
     */
    public void  updateUser();
}
