package com.jiazhong.exception;

public class NotFoundUsernameException extends RuntimeException{
    public NotFoundUsernameException() {
       super("用户不存在");
    }

    public NotFoundUsernameException(String message) {
        super(message);
    }
}
