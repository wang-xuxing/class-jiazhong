package com.jiazhong.exception;

public class BadPassWordException extends RuntimeException{

    public BadPassWordException() {
        super("密码错误");
    }

    public BadPassWordException(String message) {
        super(message);
    }
}
