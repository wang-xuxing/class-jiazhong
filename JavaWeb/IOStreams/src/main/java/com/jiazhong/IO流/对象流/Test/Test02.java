package com.jiazhong.IO流.对象流.Test;

import com.jiazhong.IO流.对象流.Test.model.cat;

import java.io.*;

/**
 * 2.编写Java程序，读取上题创建的文件中存储的猫，并把猫信息打印至控制台。
 */
public class Test02 {
    public static void main(String[] args){
        try {
            ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream("D:/catBean.java"));
            while (true){
                cat cat=(cat)objectInputStream.readObject();
                System.out.println(cat);
                if (cat==null){
                    break;
                }
            }
            objectInputStream.close();
        } catch (IOException e) {
            System.out.println("打印完毕");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
