package com.jiazhong.IO流.字符流;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * 字符输入流
 */
public class Demo02 {
    public static void main(String[] args) throws IOException {
        Reader reader=new FileReader("d:/hello java.txt");
        char[] buf=new char[1024];
        int len=-1;
        while ((len=reader.read(buf))!=-1){
            String str=new String(buf,0,len);
            System.out.println(str);
        }
        reader.close();

    }
}
