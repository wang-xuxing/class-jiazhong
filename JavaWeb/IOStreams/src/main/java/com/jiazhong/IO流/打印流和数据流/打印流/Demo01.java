package com.jiazhong.IO流.打印流和数据流.打印流;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class Demo01 {
    public static void main(String[] args) throws IOException {
        //控制台输入
//        Scanner scanner=new Scanner(System.in);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        bufferedReader.read();
        //控制台输出
        PrintWriter printWriter = new PrintWriter(System.out);
        while (true) {
            String s = bufferedReader.readLine();
            printWriter.write(s);
            if (s.equals("n")) {
                break;
            }
            printWriter.flush();
        }
        bufferedReader.close();
        printWriter.close();
    }
}
