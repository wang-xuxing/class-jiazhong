package com.jiazhong.IO流.字符流;

import java.io.*;
import java.nio.charset.Charset;

public class BufferDemo02 {
    public static void main(String[] args) throws IOException {
        bufferedWriterDemo();
    }

    /**
     * 带有缓冲区的字符输出流
     */
    public static void bufferedWriterDemo() throws IOException {
        //创建带有缓冲区的字符输出流对象

        BufferedWriter writer = new BufferedWriter(new FileWriter("D:/hello.txt"));
        writer.write("带有缓冲区的字符流111");
        writer.newLine();//输出一个换行符
        writer.flush();
        writer.close();

    }

    private static void bufferedReaderDemo() throws IOException {
        //创建带有缓冲区的字符流对象
       /* BufferedReader reader = new BufferedReader(
                                new InputStreamReader(
                                        new FileInputStream(
                                                "D:\\SoftWare\\dev\\mysql-5.7.14-winx64\\mysql5.7解压缩安装.txt"
                                                            )
                                                    ,"GBK")
                                                ,1024*10);*/
        BufferedReader reader = new BufferedReader(new FileReader("D:\\\\SoftWare\\\\dev\\\\mysql-5.7.14-winx64\\\\mysql5.7解压缩安装.txt", Charset.forName("GBK")));
        String str = null;
        //读取一行数据，返回字符串
        while((str = reader.readLine()) != null){
            System.out.println(str);
        }
        reader.close();
    }
}
