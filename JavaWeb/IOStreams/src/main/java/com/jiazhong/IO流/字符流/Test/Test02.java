package com.jiazhong.IO流.字符流.Test;

import java.io.*;

/**
 * 2.将从控制台输入的信息保存到一个文件中去。
 * （中文不能乱码，建议使用System.in、Scanner和InputStreamReader等）
 */
public class Test02 {
    public static void main(String[] args) throws IOException {
        //创建控制台输入
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        //控制台输出
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
        writer.write("请输入要保存的文本信息:");
        writer.flush();
        while (true) {
            String str = bufferedReader.readLine();
            if (str.equals("n")) break;
            sysToFile(str, new File("D:/hello.txt"));
        }bufferedReader.close();
        writer.close();

    }

    public static void sysToFile(String str, File file) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file,true));
            bufferedWriter.write(str);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
