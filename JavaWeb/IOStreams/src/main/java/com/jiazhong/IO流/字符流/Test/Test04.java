package com.jiazhong.IO流.字符流.Test;

import java.io.*;

/**
 * 4.编写Java程序，计算一个java源程序的行数并打印。
 */
public class Test04 {
    public static void main(String[] args) {
       countLineJava(new File("D:\\Java源码\\ClassJiazhong\\JavaWeb\\IOStreams\\src\\main\\java\\com\\jiazhong\\IO流\\字符流\\Test\\Test02.java"));

    }

    public static int countLineJava(File file){
        try {
            BufferedReader bufferedReader=new BufferedReader(new FileReader(file));
            int count=0;
            String str;
            while ((str=bufferedReader.readLine())!=null){
                if (str.trim().equals("")) continue;
                count++;
            }
            System.out.println(file.getCanonicalFile()+"行数:"+count);
            return count;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
