package com.jiazhong.IO流.File类;

import java.io.File;

/**
 * 扫描指定目录的所有文件
 * 使用递归调用
 */
public class testDemo04 {
    public static void main(String[] args) {
        File[] roots = File.listRoots();//获得当前电脑所有磁盘
        for (File file:roots){
            scannerFile(file);
        }

    }

    /**
     * 扫描指定文件
     * @param file
     */
    static  void scannerFile(File file){
        if (file==null){//文件无效，退出本次循环
            return;
        }
        if (file.isFile()){//如果是文件输出文件地址，退出本次调用
            String filePath=file.getAbsolutePath();
            System.out.println(filePath);
            return;
        }

        if (file.isDirectory()){
             //获得当前目录所有子文件
            File[] files = file.listFiles();
            if (files!=null&&files.length!=0){
                //遍历子文件
                for (File file1:files){
                    scannerFile(file1);
                }
            }
        }

    }
}
