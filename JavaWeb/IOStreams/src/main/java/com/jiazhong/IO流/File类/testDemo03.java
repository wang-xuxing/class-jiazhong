package com.jiazhong.IO流.File类;

import java.io.File;
import java.io.IOException;

/**
 * 编程实现创建"d:/abc/xyz/hello.java"目录及文件
 */
public class testDemo03 {
    public static void main(String[] args) throws IOException {
        File file = new File("D:/da/abc/xyz/hello.java");
//        createFile(file);
        deleteFile(file);
    }

    /**
     * 创建多级目录及文件夹
     *
     * @param file
     */
    static void createFile(File file) throws IOException {
        if (file.exists()) {//判断文件是否存在
            System.out.println("该文件存在!!!");
        } else {
            System.out.println("该文件不存在");
            //获取当前File对象最后一层目录的父目录
            File dir = file.getParentFile();
            if (dir.exists()) {//如果该目录存在创建该文件
                System.out.println("该目录存在!!!\n创建文件");
                file.createNewFile();
                System.out.println("文件" + file.getName() + "创建成功...");
            } else {//如果该目录不存在在创建该目录
                System.out.println("该目录不存在!!!\n创建目录");
                File file1 = new File(file.getParent());
                file1.mkdirs();
                System.out.println("目录" + file.getParent() + "创建成功..");
                if (file1.isDirectory()) {
                    file.createNewFile();
                    System.out.println("文件" + file.getName() + "创建成功...");
                } else {
                    System.out.println("不是目录，文件未创建成功...");
                }
            }

        }
    }

    public static void deleteFile(File file) {
        if (file == null) {//文件无效
            return;
        }
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
                System.out.println("删除文件：" + file.getName());
                deleteFile(file.getParentFile());
                return;
            }
            if (file.isDirectory()) {
                file.delete();
                System.out.println("删除目录：" + file.getName());
                deleteFile(file.getParentFile());

            }
        } else {
            System.out.println("无此文件");
        }
    }
}
