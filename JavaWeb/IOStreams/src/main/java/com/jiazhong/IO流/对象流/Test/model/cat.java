package com.jiazhong.IO流.对象流.Test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class cat implements Serializable {
    private Integer id;
    private String name;
}
