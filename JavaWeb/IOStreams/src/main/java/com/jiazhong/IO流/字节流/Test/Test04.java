package com.jiazhong.IO流.字节流.Test;

import java.io.*;

/**
 * 4.编写Java程序：实现文件夹的复制。
 */
public class Test04 {
    public static void main(String[] args) {
        dirCopy("E:\\桌面\\我爱学习\\temp\\社会实践报告\\202096094019王旭星", "E:/桌面");
    }

    /**
     * 文件夹复制
     *
     * @param srcFile  源文件
     * @param descFile 目标文件
     */
    public static void dirCopy(String srcFile, String descFile) {
        //创建源文件对象
        File src = new File(srcFile);
        if (src.exists()) {//如果源文件存在
            String fileName = src.getName();//获得要复制文件或文件夹的名字
            //创建目标文件对象
            File desc = new File(descFile + File.separator + fileName);
            if (src.isFile()) {//如果是文件复制直接即可
                fileCopy(src, desc);
            }
            if (src.isDirectory()) {//如果是文件夹
                //创建目标文件夹
                desc.mkdirs();
                //获得源文件的子文件
                File[] files = src.listFiles();
                if (files != null && files.length != 0) {
                    for (File file : files)
                        //继续从该文件夹下复制
                        dirCopy(file.getAbsolutePath(), desc.getAbsolutePath());
                }
            }
        } else {
            System.out.println("源文件不存在");
        }
    }

    /**
     * 文件复制
     *
     * @param src
     * @param desc
     */
    public static void fileCopy(File src, File desc) {
        try {
            InputStream inputStream = new FileInputStream(src);
            OutputStream outputStream = new FileOutputStream(desc);
            byte[] buf = new byte[1024 * 1024 * 10];
            int len = -1;
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
                outputStream.flush();
            }
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
