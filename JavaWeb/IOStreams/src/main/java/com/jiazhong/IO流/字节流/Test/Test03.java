package com.jiazhong.IO流.字节流.Test;

import java.io.*;

/**
 * 3.编写Java程序：读取上题文件中存储的数字，打印至控制台。
 */
public class Test03 {
    public static void main(String[] args) {
       readFileToSys(new File("D:/hello.txt"));
    }
    public static void readFileToSys(File file){
        try {
            InputStream inputStream=new FileInputStream(file);
            byte[] buf=new byte[1024];
            int len=-1;
            while ((len=inputStream.read(buf))!=-1){
                String str=new String(buf,0,len);
                System.out.println(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
