package com.jiazhong.IO流.打印流和数据流.数据流;

import java.io.*;

public class Demo01 {
    public static void main(String[] args) throws IOException {
        dataOutputStream();
        dataInputStream();
    }

    /**
     * 数据输出流
     * @throws IOException
     */
    private static void dataOutputStream() throws IOException {
        DataOutputStream dataOutputStream=new DataOutputStream(new FileOutputStream("D:/hello.num"));
        dataOutputStream.writeInt(256);
        dataOutputStream.writeInt(5256);
        dataOutputStream.writeInt(2256);
        dataOutputStream.writeInt(2526);
        dataOutputStream.writeDouble(2562.06);
        dataOutputStream.flush();
        dataOutputStream.close();
    }

    /**
     * 数据输入流
     * @throws IOException
     */
    public static void dataInputStream() throws IOException {
        DataInputStream dataInputStream=new DataInputStream(new FileInputStream("d:/hello.num"));
        try {
            while (true){
                int num = dataInputStream.readInt();
                System.out.println(num);
            }
        }catch (EOFException e){
            System.out.println("文件读取结束");
        } dataInputStream.close();


    }
}
