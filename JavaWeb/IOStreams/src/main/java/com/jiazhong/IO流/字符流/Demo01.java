package com.jiazhong.IO流.字符流;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


/**
 * 字符输出流
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {
        Writer writer=new FileWriter("d:/hello.txt");
        writer.write("你好");
        writer.flush();
        writer.close();
    }
}
