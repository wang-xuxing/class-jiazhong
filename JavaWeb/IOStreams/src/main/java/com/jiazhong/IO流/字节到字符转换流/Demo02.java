package com.jiazhong.IO流.字节到字符转换流;

import java.io.*;

/**
 * 字节到字符输出流
 */
public class Demo02 {
    public static void main(String[] args) throws IOException {
        OutputStream outputStream=new FileOutputStream("d:/hello.txt");
        OutputStreamWriter outputStreamWriter=new OutputStreamWriter(outputStream,"UTF-8");
        String str="你好世界";
        outputStreamWriter.write(str);
        outputStream.flush();
        outputStream.close();
    }
}
