package com.jiazhong.IO流.File类.Test;

import java.io.File;

/**
 * 2.编写Java程序：删除指定的文件或文件夹，如果文件夹中有文件或子文件夹也一并删除。(请用一个测试文件夹测试此方法，防止误删有用的文件)
 */
public class Test02 {
    public static void main(String[] args) {
        delFileOrDir(new File("D:\\下载文件"));
    }

    static void delFileOrDir(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                System.out.println("删除文件:" + file);
                file.delete();
                return;
            } else if (file.isDirectory()) {
                File[] files = file.listFiles();
                if (files != null && files.length > 0) {//目录不为空
                    for (File file1 : files) {
                        delFileOrDir(file1);
                    }
                }
            }
            System.out.println("删除文件夹:" + file);
            file.delete();
        } else {
            System.out.println("该文件或文件夹不存在");
        }
    }
}
