package com.jiazhong.IO流.字符流.Test;

import java.io.File;

/**
 * 5.开发一个Java程序，能够统计Java项目中代码行数，具体要求：统计每个java源文件的代码行数和所有java源文件的总行数（不含空行）
 */
public class Test05 {
    private static int sunRows=0;
    public static void main(String[] args) {
        countLineJavaProject(new File("D:\\Java源码\\ClassJiazhong\\JavaWeb\\IOStreams"));
        System.out.println("总行数:"+sunRows);

    }

    public static int countLineJavaProject(File file) {
        if (file == null) {//判断文件是否有效
            return 0;
        }

        if (file.isFile()) {//如果是文件
            if (file.getName().endsWith(".java")){
                int count = Test04.countLineJava(file);
                sunRows=sunRows+count;
            }
        }
        if (file.isDirectory()) {//如果是目录
            File[] files = file.listFiles();
            for (File file1 : files) {
                countLineJavaProject(file1);
            }
        }
        return -1;
    }
}
