package com.jiazhong.IO流.字节流.Test;

import java.io.*;
import java.util.Random;

/**
 * 2.编写Java程序：将产生20个1-100之间的随机浮点数，存入文件中。
 */
public class Test02 {
    public static void main(String[] args) {
        RandomToFile(new File("D:/hello.txt"));
    }

    /**
     * 利用数据流存入文件
     * @param file
     */
    public static void RandomToFile1(File file){
        try {
            OutputStream outputStream=new FileOutputStream(file);
            Random random=new Random();
            String str = null;
            for (int i=0;i<20;i++){
                float num=random.nextFloat()*100+1;
                str=  num+"\n";
                byte[] nums=str.getBytes();
                outputStream.write(nums);
            }
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    /**
     * 普通方法
     * @param file
     */

    public static void RandomToFile(File file){
        try {
            OutputStream outputStream=new FileOutputStream(file);
            Random random=new Random();
            String str = null;
            for (int i=0;i<20;i++){
                float num=random.nextFloat()*100+1;
                str=  num+"\n";
                byte[] nums=str.getBytes();
                outputStream.write(nums);
            }
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
