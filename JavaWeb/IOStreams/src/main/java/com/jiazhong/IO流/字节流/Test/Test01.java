package com.jiazhong.IO流.字节流.Test;

import java.io.*;

/**
 *1.编写Java程序：实现一个文本文件的拷贝和大于500M的压缩文件的拷贝并统计所耗时间（毫秒）
 */
public class Test01 {
    public static void main(String[] args) throws IOException {
        long startTime=System.currentTimeMillis();
//       txtFileCopy(new File("d:/hello.txt"),new File("e:/桌面/hello.txt"));
        zipFileCopy(new File("E:\\桌面\\我爱学习\\B-ClassJiaZhong\\齐老师讲Java\\2.前端.zip"),new File("E:\\桌面\\2.前端.zip"));
       long endTime=System.currentTimeMillis();
        System.out.println("耗时："+(endTime-startTime)+"ms");
    }

    /**
     * 文本文件拷贝
     * @param src
     * @param desc
     */
    static void txtFileCopy(File src,File desc) throws IOException {
        InputStream inputStream=new FileInputStream(src);
        OutputStream outputStream=new FileOutputStream(desc,true);
        byte[]  buf =new byte[1024];
        int len=-1;
        while ((len=inputStream.read(buf))!=-1){
            outputStream.write(buf,0,len);
            outputStream.flush();
        }
        inputStream.close();
        outputStream.close();
    }


    static void zipFileCopy(File src,File desc){
        try {
            InputStream inputStream=new FileInputStream(src);
            OutputStream outputStream=new FileOutputStream(desc);
            BufferedInputStream bufferedInputStream=new BufferedInputStream(inputStream);
            BufferedOutputStream bufferedOutputStream=new BufferedOutputStream(outputStream);
            byte[] buf=new byte[1024*1024*10];
            int len=-1;
            while ((len=bufferedInputStream.read(buf))!=-1){
                bufferedOutputStream.write(buf,0,len);
                outputStream.flush();
            }
            inputStream.close();
            outputStream.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
