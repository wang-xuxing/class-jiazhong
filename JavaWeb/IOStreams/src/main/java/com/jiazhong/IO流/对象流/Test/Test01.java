package com.jiazhong.IO流.对象流.Test;

import com.jiazhong.IO流.对象流.Test.model.cat;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * 1.编写Java程序，定义一个描述猫的JavaBean，创建若干个猫对象，储存于一个文件中。
 */
public class Test01 {
    public static void main(String[] args) throws IOException {
        ObjectOutputStream objectOutputStream=new ObjectOutputStream(new FileOutputStream("D:/catBean.java",true));
        cat cat1=new cat(1,"小花");
        cat cat2=new cat(2,"小美");
        cat cat3=new cat(3,"小丽");
        cat cat4=new cat(4,"小田");
        objectOutputStream.writeObject(cat1);
        objectOutputStream.writeObject(cat2);
        objectOutputStream.writeObject(cat3);
        objectOutputStream.writeObject(cat4);
        objectOutputStream.flush();
        objectOutputStream.close();

    }
}
