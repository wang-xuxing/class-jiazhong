package com.jiazhong.IO流.对象流;

import com.jiazhong.IO流.对象流.model.User;

import java.io.*;

public class ObjectStream {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        ObjOutputStream();
        ObjInputStream();
    }

    /**
     * 将Java对象存储到磁盘中
     *
     * @throws IOException
     */
    public static void ObjOutputStream() throws IOException {
        User user = new User(2, "校长", "123456");
        User user1 = new User(1, "校长", "123456");
        User user2 = new User(3, "校答长", "123456");
        User user3 = new User(5, "校按时长", "123456");
        User user4 = new User(4, "校达个长", "123456");
        User user5=new User(6,"dasd","123456","男");
        //创建对象输出流对象
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("D:/user.obj"));
        objectOutputStream.writeObject(user);
        objectOutputStream.writeObject(user1);
        objectOutputStream.writeObject(user2);
        objectOutputStream.writeObject(user3);
        objectOutputStream.writeObject(user4);
        objectOutputStream.writeObject(user5);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    /**
     * 从磁盘中读取数据并转化为Java对象
     */
    public static void ObjInputStream() throws IOException, ClassNotFoundException {
        //创建对象输入流对象
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("D:/user.obj"));

        try {
            while (true) {
                User user = (User) objectInputStream.readObject();
                System.out.println(user);
            }
        } catch (IOException e) {
            System.out.println("已读完");
            e.printStackTrace();
        }
        objectInputStream.close();


    }
}
