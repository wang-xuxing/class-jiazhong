package com.jiazhong.IO流.字符流.Test;

import java.io.*;
import java.nio.charset.Charset;

/**
 * 3.开发一个Java程序，实现将一个GBK编码的文件转为UTF-8编码的文件。
 */
public class Test03 {

    public static void main(String[] args) throws IOException {
        GBKToUTF(new File("D:/hello.txt"),new File("D:/hello word.txt"));
    }
    public static void GBKToUTF(File GBKSrc,File UTFDesc) throws IOException {
        InputStreamReader inputStreamReader=new InputStreamReader(new FileInputStream(GBKSrc), Charset.forName("GBK"));
        OutputStreamWriter outputStreamWriter=new OutputStreamWriter(new FileOutputStream(UTFDesc),Charset.forName("UTF-8"));
        char[]  buf=new char[1024];
        int len=-1;
        while ((len=inputStreamReader.read(buf))!=-1){
            String str=new String(buf,0,len);
            System.out.println(str);
            outputStreamWriter.write(buf,0,len);
            outputStreamWriter.flush();
        }
        outputStreamWriter.close();
        inputStreamReader.close();
    }
}
