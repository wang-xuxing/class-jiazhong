package com.jiazhong.IO流.File类.Test;

import java.io.File;

/**
 * 3.编写Java程序：扫描整个硬盘中的所有文件，将文件的对象路径打印到控制台上
 */
public class Test03 {

    public static void main(String[] args) {
      scannerFile(new File("D:/"));
    }

    public static void  scannerFile(File file){
        if (file==null){
            return;
        }
        if (file.isFile()){
            System.out.println("-"+file.getAbsolutePath());
        }
        if (file.isDirectory()){
            File[] files = file.listFiles();
            if (files!=null&&files.length!=0){
                for (File file1:files){
                    scannerFile(file1);
                }
            }
        }
    }
}
