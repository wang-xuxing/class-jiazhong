package com.jiazhong.IO流.File类.Test;

import java.io.File;

/**
 * 1.编写Java程序：打印一个文件夹下的文件和目录名，并注明是文件还是目录。
 */
public class Test01 {
    public static void main(String[] args) {
        PrintDirFile(new File("E:\\桌面\\我爱学习\\C-ClassBJUWork\\操作系统"));
    }


    public static void PrintDirFile(File file) {
        if (file == null) {//文件无效，退出循环
            return;
        }
        File[] files = file.listFiles();//或得子目录及其子文件列表
        for (File file1 : files) {//遍历子目录及其子文件
            if (files != null && files.length != 0) {
                if (file1.isFile()) {//如果是文件则输出文件名
                    System.out.println("---文件:" + file1.getParentFile() + "\\" + file1.getName());

                }
                if (file1.isDirectory()) {//如果是目录则输出目录名并递归调用该目录继续遍历该目录下内容
                    System.out.println("-目录:" + file1.getParentFile() + "\\" + file1.getName());
                    PrintDirFile(file1);
                }
            }
        }
    }
}
