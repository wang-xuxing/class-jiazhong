package com.jiazhong.IO流.字符流.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 * 1.将文本文件的内容打印至控制台上（中文不能乱码）。
 */
public class Test01 {
    public static void main(String[] args) {
       filePrintSys("D:/hello.txt");
    }
    public static void filePrintSys(String filePath){
        try {
            Reader reader=new FileReader(filePath, Charset.forName("GBK"));
            char[] buf=new char[1024];
            int len=-1;
            while ((len=reader.read(buf))!=-1){
                String str=new String(buf,0,len);
                System.out.println(str);

            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
