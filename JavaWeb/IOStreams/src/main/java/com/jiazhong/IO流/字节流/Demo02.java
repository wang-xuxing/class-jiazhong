package com.jiazhong.IO流.字节流;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

/**
 * 字节输出流
 */
public class Demo02 {
    public static void main(String[] args) throws IOException {
        OutputStream outputStream=new FileOutputStream("D:/hello.txt",true);
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入要输出的内容,输出N时结束:");
        Boolean flag=false;
        while (!flag){
            String str=scanner.nextLine();//输出一行数据
            if (str.equals("N")||str.equals("n")){
                break;
            }
            //将一个字符串转化为字节数组
            byte[] bytes = str.getBytes();
            outputStream.write(bytes);
            outputStream.write("\n".getBytes());
           outputStream.flush();//刷新流
        }
         outputStream.close();//关闭流
    }
}
