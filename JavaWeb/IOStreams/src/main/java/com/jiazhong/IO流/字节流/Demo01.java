package com.jiazhong.IO流.字节流;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 字节输入流实例
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = new FileInputStream("d:/hello java.txt");
        //读取文件内容
        //读取一个字节
       /* int num=inputStream.read();
        System.out.println((char)num);
        */
//        int num=inputStream.read();
//        System.out.println((char)num);

        //一次读取多个字节，组装到字节数组中
        byte[] buf = new byte[1024];
        int len = -1;
//        将读取到的字节存入到buf中，返回值实际读到的内容
        while ((len = inputStream.read(buf)) != -1) {
            String str = new String(buf,0,len);//从下标为0开始到下标为len结束创建字符串
            System.out.println(str);
        }

        inputStream.close();
    }
}
