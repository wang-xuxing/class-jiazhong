package com.jiazhong.IO流.File类.Test;

import java.io.File;
import java.io.FileFilter;

/**
 * 4.编写Java程序：扫描某个磁盘中的文件，将所有的world文件的完整路径打印在控制台上
 */
public class Test04 {
    public static void main(String[] args) {
        ScannerFileFound(new File("E:/桌面/我爱学习"));
    }

    public static void ScannerFileFound(File file) {
        if (file == null) {//文件是否有效
            return;
        }
        if (file.isFile()) {
            System.out.println("-" + file.getAbsolutePath());
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (file.isDirectory()) {//如果是目录直接返回
                        return true;
                    }
                    if (file.isFile()) {//如果是文件则判断是否为word文件
                        if (file.getName().endsWith(".doc") || file.getName().endsWith(".docx")) {
                            return true;
                        }
                    }
                    return false;
                }
            });
            if (files != null && files.length != 0) {//判断files是否有效
                for (File file1 : files) {//遍历子目录及其子文件
                    //递归调用此方法
                    ScannerFileFound(file1);
                }
            }
        }
    }
}


