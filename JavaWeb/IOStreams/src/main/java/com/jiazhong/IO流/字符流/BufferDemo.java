package com.jiazhong.IO流.字符流;

import java.io.*;
import java.util.Scanner;


public class BufferDemo  {
    public static void main(String[] args) throws IOException {
//        bufferReader();
        bufferWrite();
    }

    /**
     * 缓冲字符输出流
     * @throws IOException
     */
    private static void bufferWrite() throws IOException {
        BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter("d:/hello.txt"));
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        bufferedWriter.write(str);
        bufferedWriter.newLine();//输出一个换行符
        bufferedWriter.flush();

        bufferedWriter.close();
    }


    /**
     * 缓冲输入流
     */
     static void bufferReader() throws IOException {
       /**
        Reader reader=new FileReader("d:/hello java.txt");
        BufferedReader bufferedReader=new BufferedReader(reader);
        */
       //创建字节输入流对象
       InputStream inputStream=new FileInputStream("d:/hello java.txt");
       //创建字节到字符流对象
       InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
       //创建字符缓冲流对象
       BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
        String len=null;
        while ((len=bufferedReader.readLine())!=null){

            System.out.println(len);
        }
        inputStream.close();
    }
}
