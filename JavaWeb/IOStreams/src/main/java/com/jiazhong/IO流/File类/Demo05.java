package com.jiazhong.IO流.File类;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public class Demo05 {
    public static void main(String[] args) throws IOException {
        File file=new File("E:\\桌面\\我爱学习");
        if (file.isDirectory()){
            System.out.println(file.getName()+"目录中内容:");
            System.out.println(file.getParent());
            //获得所有子文件的文件名
//            String[] files=file.list();
//            for (String str:files){
//                System.out.println(str);
//            }
//          获得所有子文件的File对象
//            File[]  files=file.listFiles();
//            for (File str:files){
//                System.out.println(str);
//            }
//            System.out.println("---------------");
//            //使用文件过滤器接口，获得指定文件
            File[]  files=file.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (file.getName().endsWith("dox")||file.getName().endsWith("docx")){
                        return true;
                    }
                    return false;
                }
            });
            for (File file1:files){
                System.out.println(file1);
            }

        }

    }
}
