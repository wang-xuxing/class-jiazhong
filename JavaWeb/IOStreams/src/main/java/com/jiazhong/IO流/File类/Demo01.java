package com.jiazhong.IO流.File类;


import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Demo01 {
    public static void main(String[] args) throws IOException, ParseException, InterruptedException {
        //创建一个文件对象
        File file=new File("d:"+File.separator+"asd/abc.txt");
        if (file.exists()){//如果存在返回true如果不存在返回false
            System.out.println("文件存在");
//            file.delete();
            file.deleteOnExit();//JVM运行结束时删除，删除并退出系统
            Thread.sleep(100000);
            System.out.println("文件删除成功");
        }else {
            System.out.println("文件不存在");
           file.createNewFile();
            System.out.println("文件创建成功");
        }
        System.out.println("文件名："+file.getName());
        System.out.println("父目录:"+file.getParent());
        System.out.println("文件绝对路径："+file.getAbsolutePath());
        System.out.println("文件规范路径："+file.getCanonicalPath());
        System.out.println("文件扩展名:"+file.getName().substring(file.getName().lastIndexOf(".")+1));
        long fileSize= file.getTotalSpace();
        System.out.println("所在磁盘大小:"+fileSize/1024/1024/1024);
        System.out.println("文件大小:"+file.length());
        String date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(file.lastModified()));
        System.out.println("最后修改时间:"+date);

    }
}
