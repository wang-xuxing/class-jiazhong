package com.jiazhong.IO流.字节流;

import java.io.*;
import java.util.Date;

public class FileCopy {

    public static void main(String[] args) throws IOException {
        File src=new File("E:\\桌面\\我爱学习\\B-ClassJiaZhong\\齐老师讲Java\\4.JavaWeb开发\\20级JavaWeb.zip");
        long startTime=System.currentTimeMillis();
        File desc=new File("E:/桌面/20级JavaWeb.zip");
        fileCopy1(src,desc);
        long endTime=System.currentTimeMillis();
        System.out.println("耗时：" +((endTime-startTime)/1000));
    }
    /**
     * 文件复制
     * @param src  源文件
     * @param desc  目标文件
     * @throws FileNotFoundException
     */
    public static void fileCopy(File src,File desc) throws IOException {
        InputStream inputStream=new FileInputStream(src);
        OutputStream outputStream=new FileOutputStream(desc,true);

        byte[] buf = new byte[1024*1024*10];
        int len=-1;
        while ((len=inputStream.read(buf))!=-1){
            outputStream.write(buf,0,len);
            outputStream.flush();
        }
        inputStream.close();
        outputStream.close();
    }
    public static void fileCopy1(File src,File desc) throws IOException {
        InputStream inputStream=new FileInputStream(src);
        OutputStream outputStream=new FileOutputStream(desc,true);

        BufferedInputStream bufferedInputStream=new BufferedInputStream(inputStream);
        BufferedOutputStream bufferedOutputStream=new BufferedOutputStream(outputStream);

        byte[] buf = new byte[1024*1024*10];
        int len=-1;
        while ((len=bufferedInputStream.read(buf))!=-1){
            bufferedOutputStream.write(buf,0,len);
            outputStream.flush();
        }
        inputStream.close();
        outputStream.close();
    }
}
