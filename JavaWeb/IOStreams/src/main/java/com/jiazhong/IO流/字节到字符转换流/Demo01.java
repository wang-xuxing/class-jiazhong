package com.jiazhong.IO流.字节到字符转换流;

import java.io.*;

/**
 * 字节到字符输入流
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = new FileInputStream("D:\\hello.txt");
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        char[] buf = new char[1024];
        int len = -1;
        while ((len = inputStreamReader.read(buf)) != -1) {
              String str=new String(buf,0,len);
            System.out.println(str);
        }
        inputStream.close();
    }
}
