package com.jiazhong.servlets;

import com.jiazhong.dao.Impl.UserDaoImpl;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/userList.do")
public class userServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        UserDao userDao=new UserDaoImpl();
        List<User> users=userDao.QueryUser();

        
        request.setAttribute("userList",users);


        request.getRequestDispatcher("UserList.jsp").forward(request,response);
    }
}
