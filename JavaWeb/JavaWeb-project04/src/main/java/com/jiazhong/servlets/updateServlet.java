package com.jiazhong.servlets;

import com.jiazhong.dao.Impl.UserDaoImpl;
import com.jiazhong.dao.UserDao;
import com.jiazhong.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/update.do")
public class updateServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         request.setCharacterEncoding("UTF-8");

        try {
            Integer userid= Integer.valueOf(request.getParameter("userId"));
            String username =request.getParameter("username");
            Integer sex = Integer.valueOf(request.getParameter("sex"));
            Date date=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
            Integer age=Integer.valueOf(request.getParameter("age"));
            String address=request.getParameter("address");

            User user=new User();
            UserDao userDao=new UserDaoImpl();
            user.setUserId(userid);
            user.setUsername(username);
            user.setSex(sex);
            user.setDate(date);
            user.setAge(age);
            user.setAddress(address);

            userDao.updateUser(user);
            response.sendRedirect("userList.do");
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("errMSG2","修改失败！");
            request.getRequestDispatcher("UserList.jsp").forward(request,response);
        }
    }
}
