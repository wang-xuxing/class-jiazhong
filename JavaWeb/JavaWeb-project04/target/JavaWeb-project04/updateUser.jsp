<%@ page import="com.jiazhong.model.User" %><%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2022/10/17
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改用户</title>
</head>
<style>
    body {
        background-color: burlywood;
    }

    table {
        margin-top: 100px;
        width: 300px;
        border: 1px solid black;
        border-collapse: collapse;
    }

    td {
        border: 1px solid black;
        height: 40px;
        line-height: 30px;
        text-align: center;
    }


</style>
<body>
<h2 align="center">修改用户</h2>
<%
    User user = (User) request.getAttribute("user");
    String errMSG2=(String) request.getAttribute("errMSG2");
    if (errMSG2!=null){
%><h2 style="color: red"><%=errMSG2%></h2>
<%
    }
%>
<form action="update.do" method="post">
    <table align="center">
        <input type="hidden" name="userId" value="<%=user.getUserId()%>" >

        <tr>
            <td>用户名:</td>
            <td><input type="text" name="username" value="<%=user.getUsername()%>"></td>
        </tr>
        <tr>
            <td>性别:</td>
            <td>
                <%
                    if (user.getSex() == 1) {
                %>
                <input type="radio" name="sex" value="1" checked >男
                <input type="radio" name="sex" value="0">女
                <%
                }else {
                %>

                <input type="radio" name="sex" value="1">男
                <input type="radio" name="sex" value="0"checked>女

                <%
                    }
                %>


            </td>
        </tr>
        <tr>
            <td>出生日期:</td>
            <td><input type="date" name="date" value="<%=user.getDate()%>"></td>
        </tr>
        <tr>
            <td>年龄:</td>
            <td><input type="number" name="age" value="<%=user.getAge()%>"></td>
        </tr>
        <tr>
            <td>用户地址:</td>
            <td><input type="text" name="address" value="<%=user.getAddress()%>"></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="submit">修改</button>
                <button type="reset">重置</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
