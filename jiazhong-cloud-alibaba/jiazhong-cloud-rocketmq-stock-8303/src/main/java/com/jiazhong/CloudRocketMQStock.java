package com.jiazhong;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudRocketMQStock {
    public static void main(String[] args) {
        SpringApplication.run(CloudRocketMQStock.class,args);
    }
}