package com.jiazhong.listener;

import com.alibaba.fastjson.JSONArray;
import com.jiazhong.bean.Order;
import com.jiazhong.bean.Stock;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 生成订单后，根据用户确认，扣除库存
 *   + 用于监听订单生成后，扣减库存
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = "ORDER",consumerGroup = "huozhexiao-b",messageModel = MessageModel.BROADCASTING)
public class OrderListener implements RocketMQListener<String> {

    @Override
    public void onMessage(String s) {
        Stock stock =new Stock();
        log.info("订单消息变更,s:{}",s);
        log.info("库存数量为,num:{}",stock.getStockNum());
        Order order = JSONArray.parseObject(s, Order.class);
        stock.setStockNum(stock.getStockNum() - order.getNum());
        log.info("order:{}",order);
        log.info("扣减后库存数量为,num:{}",stock.getStockNum());
    }
}
