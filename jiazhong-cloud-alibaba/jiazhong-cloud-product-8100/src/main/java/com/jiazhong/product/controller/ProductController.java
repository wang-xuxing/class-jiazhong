package com.jiazhong.product.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/product")
public class ProductController {
    @Value("${server.port}")
    private Integer port;

    @RequestMapping("/a")
    public String a() {
        log.info("用户访问了ProductController`a 方法！");
        return "This is ProductController`a method!port:" + port;
    }
}