package com.jiazhong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudRocketMQOrder {
    public static void main(String[] args) {
        SpringApplication.run(CloudRocketMQOrder.class,args);
    }
}