package com.jiazhong.controller;


import com.alibaba.fastjson.JSONArray;
import com.jiazhong.bean.JSONResult;
import com.jiazhong.bean.Order;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping
    public JSONResult a() {
        Order order = new Order();
        order.setId(1);
        order.setName("饮料");
        order.setPrice(18188.0);
        order.setNum(4);
        //同步消息                      topic:tag
        rocketMQTemplate.syncSend("ORDER:add", JSONArray.toJSONString(order));

        return new JSONResult<>(true, 200, order, "订单加入成功!!!");
    }
}
