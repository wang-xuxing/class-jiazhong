package com.jiazhong.consumer.service.impl;

import com.jiazhong.consumer.service.ConsumerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class ConsumerServiceImpl implements ConsumerService {
    @Resource
    private DiscoveryClient client;
    @Resource
    private RestTemplate restTemplate;

    private int i;

    @Override
    public String a() {
        // 发现 找到生产者
        List<ServiceInstance> list = client.getInstances("jiazhong-cloud-product");
        // 获取到一个生产者
        ServiceInstance instance = list.get(0);
//        if (i == 3) {
//            i = 0;
//        } else {
//            i++;
//        }
        // 获取到生产者的uri
        String uri = instance.getUri().toString() + "/product/a";
        System.out.println(uri);
        // 访问网址对应的controller方法 获取返回结果
        String result = restTemplate.getForObject(uri, String.class);
        log.info("获取到了生产者的内容是：{}", result);
        return result;
    }
}
