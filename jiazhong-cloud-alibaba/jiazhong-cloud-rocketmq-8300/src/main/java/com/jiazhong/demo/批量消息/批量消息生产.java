package com.jiazhong.demo.批量消息;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class 批量消息生产 {
    @SneakyThrows
    public static void main(String[] args) {
        // 1.创建消息生产者 producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("huozhexiao-a");
        // 2.指定 NameSrv 地址
        producer.setNamesrvAddr("192.168.198.129:9876");
        // 3.启动 producer
        producer.start();
        // 4.创建消息对象，指定 Topic、Tag 和消息体
        List<Message> list = new ArrayList<>();
        for (int i = 1; i < 20; i++) {
            String body = "Hello,rocketMQ,index:" + i + "," + System.currentTimeMillis();
            String topic = "huozhexiao";
            String tag = "a";
            Message message = new Message(topic, tag, body.getBytes(StandardCharsets.UTF_8));
            list.add(message);
        }
        // 批量消息生产
        producer.send(list);
        // 6.关闭生产者 producer
        producer.shutdown();
    }
}