package com.jiazhong.demo.延时消息;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class 延时消息生产 {
    public static void main(String[] args) throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer("huozhexiao-a");
        producer.setNamesrvAddr("192.168.198.129:9876");
        producer.setSendMsgTimeout(100000);
        producer.start();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
        for (int i = 1; i < 20; i++) {
            String body = "Hello,rocketMQ,index:" + i + "," + System.currentTimeMillis();
            String topic = "huozhexiao";
            String tag = "a";
            Message message = new Message(topic, tag, body.getBytes(StandardCharsets.UTF_8));
            message.setDelayTimeSec(30);
            // 5.发送消息
            SendResult result = producer.send(message);
            log.info("回调的消息是：{}", result);
        }
        producer.shutdown();
    }
}
