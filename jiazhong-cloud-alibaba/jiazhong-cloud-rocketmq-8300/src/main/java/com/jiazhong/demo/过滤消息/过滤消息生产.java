package com.jiazhong.demo.过滤消息;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.nio.charset.StandardCharsets;

@Slf4j
public class 过滤消息生产 {
    @SneakyThrows
    public static void main(String[] args) {
        // 1.创建消息生产者 producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("huozhexiao-a");
        // 2.指定 NameSrv 地址
        producer.setNamesrvAddr("192.168.198.129:9876");
        // 3.启动 producer
        producer.start();
        // 4.创建消息对象，指定 Topic、Tag 和消息体
        for (int i = 1; i < 20; i++) {
            int random = (int) (Math.random() * 100 + 1);
            String gender = (random % 2 == 0 ? "男" : "女");
            String body = "Hello,rocketMQ,index:" + i + "," + random + "，" + gender;
            String topic = "huozhexiao";
            String tag = "a";
            Message message = new Message(topic, tag, body.getBytes(StandardCharsets.UTF_8));
            // 设置过滤条件
            message.putUserProperty("random", String.valueOf(random));
            message.putUserProperty("gender", gender);
            // 5.发送消息
            SendResult result = producer.send(message);
            log.info("回调的消息是：{}", result);
        }
        // 6.关闭生产者 producer
        producer.shutdown();
    }
}