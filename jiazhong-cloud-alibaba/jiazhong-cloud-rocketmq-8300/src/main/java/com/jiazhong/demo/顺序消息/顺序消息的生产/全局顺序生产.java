package com.jiazhong.demo.顺序消息.顺序消息的生产;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class 全局顺序生产 {
    @SneakyThrows
    public static void main(String[] args) {
        // 1.创建消息生产者 producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("huozhexiao-a");
        // 2.指定 NameSrv 地址
        producer.setNamesrvAddr("192.168.198.129:9876");
        //延时
        producer.setSendMsgTimeout(1000000);
        // 3.启动 producer
        producer.start();
        // 4.创建消息对象，指定 Topic、Tag 和消息体
        for (int i = 1; i < 20; i++) {
            String body = "Hello,rocketMQ,index:" + i;
            String topic = "huozhexiao";
            String tag = "a";
            Message message = new Message(topic, tag, body.getBytes(StandardCharsets.UTF_8));
            // 5.发送消息
            producer.send(message, new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> list, Message message, Object o) {
                    //代表只发送到一个队列中
                    return list.get(0);
                }
            },null);
        }
        // 6.关闭生产者 producer
        producer.shutdown();
    }
}
