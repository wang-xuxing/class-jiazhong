package com.jiazhong.demo.普通消息.消息生产;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

import java.nio.charset.StandardCharsets;

@Slf4j
public class 单向消息发送 {
    @SneakyThrows
    public static void main(String[] args) {
        // 1.创建消息生产者 producer，并指定生产者组名
        DefaultMQProducer producer =new DefaultMQProducer("huozhexiao-a");
        // 2.注册中心地址
        producer.setNamesrvAddr("192.168.198.129:9876");
        producer.setSendMsgTimeout(1000000);
        // 3. 启动
        producer.start();
        for (int i = 1; i < 11; i++) {
            String body = "你好，宝鸡！索引：" + i;
            String topic = "huozhexiao";
            String tag = "c";
            Message message = new Message(topic, tag, body.getBytes(StandardCharsets.UTF_8));
            // 唯一的区别，这里调用 sendOneway
            producer.sendOneway(message);
        }
        producer.shutdown();
    }
}
