package com.jiazhong.demo.顺序消息.顺序消息的生产;

import com.alibaba.fastjson.JSONArray;
import com.jiazhong.bean.Order;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class 局部顺序生产 {
    @SneakyThrows
    public static void main(String[] args) {
        // 1.创建消息生产者 producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("huozhexiao-a");
        // 2.指定 NameSrv 地址
        producer.setNamesrvAddr("192.168.198.129:9876");
        producer.setSendMsgTimeout(10000000);
        // 3.启动 producer
        producer.start();
        // 4.创建消息对象，指定 Topic、Tag 和消息体
        for (int i = 1; i < 21; i++) {
            Order order = new Order();
            order.setId(i);
            order.setName("订单:" + i);
            order.setType((int) (Math.random() * 4 + 1));
            String body = JSONArray.toJSONString(order);
            String topic = "huozhexiao";
            String tag = "a";
            Message message = new Message(topic, tag, body.getBytes(StandardCharsets.UTF_8));
            // 5.发送消息
            producer.send(message, new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> list, Message message, Object o) {
                    // 将消息发送到第一个队列中
                    return list.get(order.getType() - 1);
                }
            }, null);
        }
        // 6.关闭生产者 producer
        producer.shutdown();
    }
}