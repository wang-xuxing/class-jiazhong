package com.jiazhong.demo.过滤消息;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

@Slf4j
public class 过滤消息消费 {
    @SneakyThrows
    public static void main(String[] args) {
        //创建消费者 Consumer，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("huozhexiao-b");
        //指定 NameSrv 地址
        consumer.setNamesrvAddr("81.68.205.226:9876");
        //订阅主题 Topic 和 Tag
        consumer.subscribe("huozhexiao", MessageSelector.bySql("random>50 and gender<>'男'"));
        // 设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                // 接收消费（消费消息，订阅消息）
                list.forEach((me) -> {
                    String topic = me.getTopic();
                    String tag = me.getTags();
                    String body = new String(me.getBody());
                    log.info("topic:{},tag:{},body:{}", topic, tag, body);
                });
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        // 启动消费者 Consumer
        consumer.start();
    }
}