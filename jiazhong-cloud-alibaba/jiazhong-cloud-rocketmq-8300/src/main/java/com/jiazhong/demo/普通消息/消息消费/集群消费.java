package com.jiazhong.demo.普通消息.消息消费;

import lombok.SneakyThrows;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class 集群消费 {
    @SneakyThrows
    public static void main(String[] args) {
        //    创建消费者 Consumer，指定消费者组名
        DefaultMQPushConsumer consumer =new DefaultMQPushConsumer("huozhexiao-b");
        //    指定 NameSrv 地址
        consumer.setNamesrvAddr("192.168.198.129:9876");
        //    订阅主题 Topic 和 Tag
        consumer.subscribe("huozhexiao","*");
        // 集群消费模式 可以不设置，默认就是集群消费模式
        consumer.setMessageModel(MessageModel.CLUSTERING);
        //    设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                try {
                    // 循环接受所有消息
                    for (MessageExt message : list) {
                        // 分别获取 Topic、Tags 和 消息内容
                        String topic = message.getTopic();
                        String tags = message.getTags();
                        String msg = new String(message.getBody(), "UTF-8");
                        System.out.println("topic:" + topic + ",tags:" + tags + ",msg:" + msg);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    // 代表消息消费失败
                    return ConsumeConcurrentlyStatus.RECONSUME_LATER;
                }
                // 代表消息被成功消费
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //    启动消费者 Consumer
        consumer.start();
    }
}
