package com.jiazhong.demo.顺序消息.顺序消息的消费;

import com.alibaba.fastjson.JSONArray;
import com.jiazhong.bean.Order;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

@Slf4j
public class 局部顺序消费 {
    @SneakyThrows
    public static void main(String[] args) {

        //创建消费者 Consumer，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("huozhexiao-a");
        //指定 NameSrv 地址
        consumer.setNamesrvAddr("192.168.198.129:9876");
        //订阅主题 Topic 和 Tag
        consumer.subscribe("huozhexiao", "a");
        // 设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerOrderly() {
            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> list, ConsumeOrderlyContext consumeOrderlyContext) {
                // 接收消费（消费消息，订阅消息）
                try {
                    list.forEach((me) -> {
                        String topic = me.getTopic();
                        String tag = me.getTags();
                        String body = new String(me.getBody());
                        Order order = JSONArray.parseObject(body, Order.class);
                        if (order.getType() == 1) {
                            log.info("{}",order);
                        }
                    });
                } catch (Exception e) {
                    return ConsumeOrderlyStatus.SUSPEND_CURRENT_QUEUE_A_MOMENT;
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });

        // 启动消费者 Consumer
        consumer.start();
    }
}