package com.jiazhong.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {
    private Integer id;
    private String name;
    private Integer type;   //1:未付款  2:已付款  3:未发货 4:已发货 5:已完成
}
