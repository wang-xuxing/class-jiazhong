package com.jiazhong.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JSONResult<T> implements Serializable {

    private Boolean success;
    private Integer code;
    private T data;
    private String message;
}
