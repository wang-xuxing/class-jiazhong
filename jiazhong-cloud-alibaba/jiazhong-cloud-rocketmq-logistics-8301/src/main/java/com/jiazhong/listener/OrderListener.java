package com.jiazhong.listener;

import com.alibaba.fastjson.JSONArray;
import com.jiazhong.bean.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RocketMQMessageListener(topic = "ORDER",consumerGroup = "huozhexiao-b",messageModel = MessageModel.BROADCASTING)
public class OrderListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        Order order = JSONArray.parseObject(s,Order.class);

        log.info("生成订单信息:s{}",order);

    }
}
