package com.jiazhong;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudRocketMQLogistic {
    public static void main(String[] args) {
        SpringApplication.run(CloudRocketMQLogistic.class,args);
    }
}