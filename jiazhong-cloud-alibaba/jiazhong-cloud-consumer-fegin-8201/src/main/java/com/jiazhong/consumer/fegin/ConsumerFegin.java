package com.jiazhong.consumer.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "jiazhong-cloud-product")
public interface ConsumerFegin {
    @GetMapping("/product/a")
    String a();
}
