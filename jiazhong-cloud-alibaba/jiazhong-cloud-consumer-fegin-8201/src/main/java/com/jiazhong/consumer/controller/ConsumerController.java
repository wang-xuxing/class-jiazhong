package com.jiazhong.consumer.controller;
import com.jiazhong.consumer.fegin.ConsumerFegin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {
    @Resource
    private ConsumerFegin consumerFegin;

    @GetMapping
    public String a() {
        return consumerFegin.a();
    }
}
