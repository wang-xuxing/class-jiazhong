package com.jiazhong.jvm.引用;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class PhantomReferenceDemo {

    public static void main(String[] args) {
        A a =new A();
        a.say();
        /**
         * 设置虚引用
         * 参数1:虚引用的对象
         * 参数2:引用队列
         */
        ReferenceQueue<? super A> referenceQueue =new ReferenceQueue<>();
        PhantomReference<A>  phantomReference =new PhantomReference<A>(a,referenceQueue);
        a = null;
//        System.out.println(phantomReference.get());//无法获取
        System.gc();
        System.out.println("sssssssss");
        System.out.println(referenceQueue.poll());
    }
}
