package com.jiazhong.jvm.引用;

import java.lang.ref.SoftReference;

public class SoftReferenceDemo {

    public static void main(String[] args) {
        A a =new A();//强引用
        System.out.println(a);//查看强引用地址
        a.say();//强引用调用
        //将a对象设置为软引用
        SoftReference<A> softReference =new SoftReference<A>(a);
        System.out.println(a);//查看弱引用地址
        a=null;
        System.gc();
        softReference.get().say();//软引用未被回收
        //从软引用中获取a对象,赋值给a，对象由回到强引用状态
        a = softReference.get();
        a =null;
        System.gc();
        System.out.println(a);
    }
}
