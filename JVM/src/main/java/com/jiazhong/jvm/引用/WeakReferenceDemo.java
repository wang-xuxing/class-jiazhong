package com.jiazhong.jvm.引用;

import java.lang.ref.WeakReference;

public class WeakReferenceDemo {
    public static void main(String[] args) {
        A a =new A();
        System.out.println(a);  //强引用地址
        a.say();                //强引用调用say()
        WeakReference<A> weakReference =new WeakReference<A>(a);
        System.out.println(a);  //弱引用地址
        a = null;
        weakReference.get().say();//弱引用调用say()
        System.gc();
        weakReference.get().say();//弱引用被回收了

    }
}
