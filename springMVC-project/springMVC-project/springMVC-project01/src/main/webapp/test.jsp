<%--
  Created by IntelliJ IDEA.
  User: 活着笑
  Date: 2023/2/23
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body onload="loadUserInfo()">
<br>
<h2>test.jsp</h2>
用户名:<span id="user-Info"></span></br>
<a href="javaScript:getUserInfo()">获取数据</a></br>
用户编号:<span id="userId"></span></br>
用户名:<span id="username"></span></br>
用户密码:<span id="password"></span></br>
<script>
    function loadUserInfo() {
        //创建XMLHttpRequest对象
        let xmlHttpRequest = new XMLHttpRequest();
        //建立于服务端的连接(指定请求方式及服务器地址)
        xmlHttpRequest.open("get", "/test/test01", true);
        /**
         * 注册回调函数
         * onreadystatechange事件:请求状态改变事件
         *    当请求状态每次发生改变时，该事件对应函数都会被调用
         */
        xmlHttpRequest.onreadystatechange = function () {
            if (xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200) {
                //接受服务端回传数据
                let txt = xmlHttpRequest.responseText;
                document.getElementById("user-Info").innerHTML = txt;
            }
        }
        //发送请求
        xmlHttpRequest.send(null);
    }

    function getUserInfo() {
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get", "test/test02", true);
        xmlHttpRequest.onreadystatechange = function () {
            if (xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200) {
                let userInfo=xmlHttpRequest.responseText;
                //字符串转化为json
                let userJsonArr=eval("("+userInfo+")");
                for (const user of userJsonArr){
                    console.log(user.userId,user.username,user.password);
                    document.getElementById("userId").innerHTML=user.userId;
                    document.getElementById("username").innerHTML=user.username;
                    document.getElementById("password").innerHTML=user.password;
                }
            }
        }
        xmlHttpRequest.send(null)
    }
</script>
</body>
</html>
