package com.jiazhong.springmvc.interceptors;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.jiazhong.springmvc.utils.RequestParamWrapper;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * 当一个类实现了HandlerInterceptor该类就是一个拦截器
 */
public class MyInterceptors implements HandlerInterceptor {
    /**
     * 预处理方法
     * @param request
     * @param response
     * @param handler  :处理器方法对象(控制器对象)
     * @return  boolean:返回为true:表示放行，执行该拦截器后的控制器或其他拦截器
     *                  返回为false:表示不放行，不执行后续组件(此时需要在拦截器中进行跳转)
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String msg = request.getParameter("msg");
        //检测request是否为包装后的请求
        if (request instanceof RequestParamWrapper requestParamWrapper){
            //将request对象还原为包装后的请求
            requestParamWrapper.addParam("msg",msg+"10010");
        }
        return true ;
    }
}
