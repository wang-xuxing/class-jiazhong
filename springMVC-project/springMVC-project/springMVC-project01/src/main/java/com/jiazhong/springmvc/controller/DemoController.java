package com.jiazhong.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 使用Controller注解后，该类就成为了一个springMVC控制器类
 *   控制器和Servlet功能一样
 *   控制器的每个方法都是处理独立功能的控制器
 *   @RequestMapping:设置当前控制器的访问路径(父级路径)
 *   在方法上使用该注解，表示设置该方法的访问路径(子级路径)
 *   一个控制器的访问路径:父级路径/子级路径
 */
@Controller
@RequestMapping("/demo")
public class DemoController {
    @RequestMapping("/demo01")
    public String demo01(){
        System.out.println("执行DemoController-->demo01控制器...");
        return "/index.jsp";//默认使用请求转发
    }

    @RequestMapping("/demo02")
    public String demo02(){
        System.out.println("执行DemoController-->demo02控制器...");
        return "redirect:/index.jsp";
    }
}
