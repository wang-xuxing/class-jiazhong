package com.jiazhong.springmvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@Configuration
@ComponentScan("com.jiazhong.springmvc.controller")
public class RootConfig  {
}
