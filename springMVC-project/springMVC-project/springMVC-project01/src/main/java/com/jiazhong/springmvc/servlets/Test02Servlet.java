package com.jiazhong.springmvc.servlets;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.jiazhong.springmvc.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

//@WebServlet("/test02")
public class Test02Servlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.setAttribute("msg","我是回传的数据");
//        req.getRequestDispatcher("/test01").forward(req,resp);
        resp.setContentType("json/application;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setUserId(10 + i);
            user.setUsername("admin" + i);
            user.setPassword("123456" + i);
            userList.add(user);
        }

        Object o = JSON.toJSON(userList);
        writer.println(o);

/**        StringBuffer stringBuffer=new StringBuffer("[");
        for (User user:userList) {
            stringBuffer.append("{");
            stringBuffer.append("'userId':").append(user.getUserId()).append(",");
            stringBuffer.append("'username':'").append(user.getUsername()).append("',");
            stringBuffer.append("'password':'").append(user.getPassword()).append("'");
            stringBuffer.append("},");
        }
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("]");
        writer.println(stringBuffer);
 **/
//        writer.println("{'userId':'"+user.getUserId()+"','username':'"+user.getUsername()+"','password':'"+user.getPassword()+"'}");
//        writer.println("{'userId':10,'username':'小李','password':'123456'}");
    }
}
