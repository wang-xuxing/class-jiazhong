package com.jiazhong.springmvc.controller;

import com.jiazhong.springmvc.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ResponseBody:将方法返回值作为响应体输出到客户端
 * @RestController:@Controller+@ResponseBody两个注解组合
 */
//@Controller
@RequestMapping("/test")
//@ResponseBody
@RestController
public class TestController {

    @RequestMapping("/test01")
    public String test01(){
        return "jack";
    }
    @RequestMapping("/test02")
    public List<User> test02(){
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setUserId(10 + i);
            user.setUsername("admin" + i);
            user.setPassword("123456" + i);
            userList.add(user);
        }
        return userList;
    }
    @RequestMapping("/test03")

    public Map<String,String> test03(){
        Map map=new HashMap();
        map.put("key1","value1");
        map.put("key2","value2");
        map.put("key3","value3");
        map.put("key4","value4");
        return map;
    }
}