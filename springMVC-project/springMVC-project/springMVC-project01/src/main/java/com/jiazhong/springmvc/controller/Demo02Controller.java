package com.jiazhong.springmvc.controller;


import com.jiazhong.springmvc.model.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/demo02")
public class Demo02Controller {
    @RequestMapping("/demo01")
    public String demo01(String msg){
        System.out.println("demo02--->demo01被执行");
    return msg;
    }
    @RequestMapping("/demo02")
    public String demo02(){
        System.out.println("demo02--->demo02被执行");
        return "ok2";
    }
    @RequestMapping("/demo03")
    public String demo03(){
        System.out.println("demo02--->demo03被执行");
        return "ok3";
    }
    @RequestMapping("/demo04")
    public User demo04(String msg, HttpServletRequest request){
//        System.out.println(request.getClass());
         User user=new User();
         user.setUserId(01);
         user.setUsername(msg);
         user.setPassword(msg);
         return user;
    }
}
