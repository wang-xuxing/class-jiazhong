package com.jiazhong.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @SessionAttributes("eat"):将model中的属性存到session中
 */
@Controller
@RequestMapping("/demo01")
@SessionAttributes("eat")
public class Demo01Controller {
    /**
     * springMVC中使用request对象的方式有两种:
     *  1.与HttpServletRequest解耦方式
     *     在控制器方法参数加入Model参数
     *     Model是一个Map集合，SpringMVC会自动将该对象添加到request对象中
     *     使用Model方式只能向request中添加获取或移除属性，不能做其他操作
     *  2. 与HttpServletRequest耦合方式
     *     在控制器方法参数加入HttpServletRequest参数，或其他参数
     */
    @RequestMapping("/test01")
    public String test01(Model model) {
        //将数据添加到model中，请求转发时会将model中的数据添加到请求对象中，并转到页面使用
        model.addAttribute("name","小强");
     return "/index.jsp";
    }

    @RequestMapping("/test02")
    public String  test02(HttpServletRequest request, HttpServletResponse response){
        request.setAttribute("age",18);
        HttpSession session = request.getSession();
        session.setAttribute("hello","你好");
        return "/index.jsp";
    }

    @RequestMapping("/test03")
    public String test03(Model model){
        model.addAttribute("eat","吃了吗?");
        return "/index.jsp";
    }
}
