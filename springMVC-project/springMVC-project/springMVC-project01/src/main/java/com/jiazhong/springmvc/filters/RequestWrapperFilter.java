package com.jiazhong.springmvc.filters;


import com.jiazhong.springmvc.utils.RequestParamWrapper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebFilter("/*")
public class RequestWrapperFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //创建自定义请求封包对象
        RequestParamWrapper requestWrapperFilter=new RequestParamWrapper(request);
        //将包装好的请求和响应传递到下一个对象
        filterChain.doFilter(requestWrapperFilter,response);
    }

    @Override
    public void destroy() {

    }
}
