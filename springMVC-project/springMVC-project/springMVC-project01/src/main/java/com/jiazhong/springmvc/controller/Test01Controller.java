package com.jiazhong.springmvc.controller;


import com.jiazhong.springmvc.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test01")
public class Test01Controller {
    /**
     * 一个字符串,springMVC会将请求的参数中并进行类型转换
     * @param msg
     * @return
     */
    @RequestMapping("/test01")
    public String test01(String msg){
        System.out.println(msg);
        return msg;
    }

    /**
     * 获得一个int类型参数，springMVC会将请求的参数中并进行类型转换
     * @param num
     * @return
     */
    @RequestMapping("/test02")
    public Integer test02(int num){
        System.out.println(num+10);
        return num;
    }

    /**
     * @RequestBody:请求参数为请求体
     * @param user
     * @return
     */
    @RequestMapping("/test03")
    public Integer test03(@RequestBody User user){
        System.out.println("password:"+user.getPassword()+",username:"+user.getUsername());
        return 0;
    }

    @RequestMapping("/test04/{num}")
    public Integer test04(@PathVariable int num){
        System.out.println(num+10);
        return num;
    }

    @RequestMapping("/test05")
    public Integer test05(@RequestBody List<User> user){
        System.out.println(user);
//        System.out.println("password:"+user.getPassword()+",username:"+user.getUsername());
        return 0;
    }
}
