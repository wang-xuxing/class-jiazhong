package com.jiazhong.springmvc.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

/**
 * 自定义一个请求处理类
 * 该类会对原始request对象进行包装，并改写里面的处理方法
 * 该类自定义一个Map集合，表示所请求的参数，并重写相关操作
 *
 */
public class RequestParamWrapper extends HttpServletRequestWrapper {
    //存储request对象中所有参数
    private Map<String, String[]> paramsMap = new HashMap<>();

    /**
     * 定义一个带有参数的构造方法，方法参数为HttpServletRequest对象
     * HttpServletRequestWrapper是对request请求对象的处理，所以必须将HttpServletRequest对象传入
     *
     * @param request
     */
    public RequestParamWrapper(HttpServletRequest request) {
        //调用父类的构造方法
        super(request);
        //将request原始的参数集合转存到paramsMap中
        paramsMap.putAll(request.getParameterMap());
    }

    public RequestParamWrapper(HttpServletRequest request, Map<String, Object> extraMap) {
        //调用父类的构造方法
        this(request);
        //将request原始的参数集合转存到paramsMap中
        addParams(extraMap);
    }

    /**
     * 循环向paramMap添加参数
     *
     * @param extraMap
     */
    private void addParams(Map<String, Object> extraMap) {
        for (Map.Entry<String, Object> entry : extraMap.entrySet()) {
            //向paramMap添加一个参数
            addParam(entry.getKey(), entry.getValue());
        }
    }

    public void addParam(String name, Object value) {
        if (value instanceof String[]) {
            paramsMap.put(name, (String[]) value);
        } else if (value instanceof String) {
            paramsMap.put(name, new String[]{(String) value});
        } else {
            paramsMap.put(name, new String[]{String.valueOf(value)});
        }
    }


    /**
     * 根据参数名获得参数值
     * @param name
     * @return
     */
    public String getParameter(String name) {
        if (name == null || "".equals(name)){
            return null;
        }
        return this.paramsMap.get(name)[0];
    }

    /**
     * 根据参数名获得参数值，值为数组
     * @param name
     * @return
     */
    public String[] getParameterValues(String name){
        if (name == null || "".equals(name)){
            return null;
        }
       return this.paramsMap.get(name);
    }

    /**
     * 获得所有参数返回Map集合
     * @return
     */
    public Map<String,String[]> getParameterMap(){
        if (this.paramsMap==null||this.paramsMap.size()==0){
            return super.getParameterMap();//获得原始请求的数据
        }
        return this.paramsMap;
    }

    /**
     * 获得所有参数名
      * @return
     */
    public Enumeration<String> getParameterNames(){
        if (this.paramsMap==null||this.paramsMap.size()==0){
            return super.getParameterNames();//获得原始请求的数据
        }
        Set<String> strings = this.paramsMap.keySet();
        return Collections.enumeration(strings);
    }
}

