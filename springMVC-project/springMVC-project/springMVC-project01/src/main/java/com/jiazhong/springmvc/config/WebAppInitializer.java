package com.jiazhong.springmvc.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * springMVC初始化容器
 *   当服务器启动(tomcat)时，该类会自动创建
 *   该类包含三个初始化方法：
 *          getRootConfigClasses():获得根配置类(spring核心配置)
 *          getServletConfigClasses():获得springMVC配置类(针对SpringMVC配置)
 *             可以和spring核心配置放在一起(管理不方便)
 *          getServletConfigClasses():用于配置进入springMVC的访问路径(DispatcherServlet访问路径)
 *             "/":拦截所有请求，但排除jsp
 *             "/*":拦截所有请求
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * 初始化根容器配合类
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfig.class};
    }
    /**
     * 初始化web配置类
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{MvcConfig.class};
    }
    /**
     *
     * @return
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
