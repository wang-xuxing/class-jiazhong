package com.jiazhong.springmvc.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet("/test01")
public class TestServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.setAttribute("name","小明");
//        req.getRequestDispatcher("/test.jsp").forward(req,resp);
        resp.setContentType("json/application;charset=utf-8");
        //获得响应对象的输出流
        PrintWriter writer = resp.getWriter();
        writer.println("小李");
    }
}
