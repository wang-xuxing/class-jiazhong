package com.jiazhong.springmvc.config;

import com.jiazhong.springmvc.interceptors.MyInterceptors;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * SpringMVC配置类
 * 实现WebMvcConfigurer接口，对springMVC进行相关配置
 *
 * @EnableWebMvc:启用相关注解
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.jiazhong.springmvc.controller")
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();//放行静态资源
    }

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //向springMVC添加具体拦截器
        registry.addInterceptor(new MyInterceptors())
//                .addPathPatterns("/demo02/demo01");//设置拦截路径
                .addPathPatterns("/demo02/*");//拦截demo02下所有路径
//                .excludePathPatterns("/demo02/demo02","/demo02/demo03");//排除不拦截的路径
    }
}
