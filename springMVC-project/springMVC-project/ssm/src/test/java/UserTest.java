import com.jiazhong.ssm.config.RootConfig;
import com.jiazhong.ssm.config.WebAppInitializer;
import com.jiazhong.ssm.model.User;
import com.jiazhong.ssm.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class UserTest {


    @Test
    public void selectAllUser(){
        ApplicationContext applicationContext=new AnnotationConfigApplicationContext(RootConfig.class);
        UserService userService = (UserService) applicationContext.getBean("UserService");
        List<User> users = userService.selectAllUser();
        System.out.println(users);
    }
}
