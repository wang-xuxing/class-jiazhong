package com.jiazhong.ssm.mapper;

import com.jiazhong.ssm.model.User;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserMapper {

    @Select("select * from test.user")
    public List<User> selectAllUser();
}
