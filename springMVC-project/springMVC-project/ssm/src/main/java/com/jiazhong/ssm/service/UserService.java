package com.jiazhong.ssm.service;

import com.jiazhong.ssm.model.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService {


    public List<User> selectAllUser();

}
