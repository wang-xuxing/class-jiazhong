package com.jiazhong.ssm.controller;


import com.jiazhong.ssm.model.User;
import com.jiazhong.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/selectAllUser")
    public List<User> selectAllUser(){
        return userService.selectAllUser();
    }
}
