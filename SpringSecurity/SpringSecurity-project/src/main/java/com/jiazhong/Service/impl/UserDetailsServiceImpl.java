package com.jiazhong.Service.impl;

import com.jiazhong.mapper.UserMapper;
import com.jiazhong.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理认证逻辑的类
 * + 该类需要重写接口中的loadUserByUsername方法根据用户名获得用户对象
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    @Resource
    private UserMapper userMapper;
    /**
     * 1.根据用户名获得用户信息
     * 2.将用户信息认证需要的数据封装到UserDetails的实现类对象中(User)
     * 3.将封装好的认证信息提交给SpringSecurity进行认证
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名获得用户信息
        UserInfo userInfo = userMapper.getUserByUsername(username);
        //检测用户名是否正确
        if (userInfo==null){
              log.info("用户名不存在");
              return null;
        }
        //检测用户是否为有效用户
        if (userInfo.getUser_status()==-1){
            log.info("用户被冻结");
            return null;
        }
        List<GrantedAuthority>  authorities =new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        //将UserInfo中认证需要数据封装到User对象中(UserDetails的实现类对象)
        User user = new User(userInfo.getUser_name()//附加数据
                         ,userInfo.getUser_password()//密码
                         ,true//账户是否启用
                         ,true//账户是否过期
                         ,true//凭证是否过期
                         ,userInfo.getUser_status()==-1?false:true//账户是否被锁定
                         ,authorities);//账户拥有的权限
        return user;//将该返回值交给SpringSecurity进行认证

    }
}
