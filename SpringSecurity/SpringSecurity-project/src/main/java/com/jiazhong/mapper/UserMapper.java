package com.jiazhong.mapper;

import com.jiazhong.model.UserInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    /**
     *
     * 根据用户名获得用户对象
     * @param username
     * @return
     */
    @Select("select * from tbl_user where user_name=#{username}")
    public UserInfo getUserByUsername(String username);
}
