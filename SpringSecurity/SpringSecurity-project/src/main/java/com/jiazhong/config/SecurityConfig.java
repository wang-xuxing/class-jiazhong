package com.jiazhong.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


/**
 * SpringSecurity配置类
 */

@Configuration
public class SecurityConfig {

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * 处理需要忽略的请求
     *
     * @return
     */
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return new WebSecurityCustomizer() {
            @Override
            public void customize(WebSecurity web) {
                //配置不拦截的路径(要忽略的路径)
                web.ignoring().antMatchers("/login.html", "/fail.html");
            }
        };
    }

    /**
     * 用于拦截请求，并对请求进行处理
     *
     * @param httpSecurity
     * @return SecurityFilterChain:Security过滤器链
     */
    @Bean
    @Autowired
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()//禁用跨域请求伪造的攻击
                //请求配置
                .authorizeRequests()//获得所有认证请求
                    //防止发生重定向次数过多错误，需要将login.html放行
    //                .antMatchers("/login.html","/fail.html")//匹配指定的路径
    //                    .permitAll()//无需认证即可访问
                    //给资源设置角色
                        .antMatchers("/index.html")
                           .hasAnyRole("USER", "ADMIN")
                        .antMatchers("/test/test01")
                           .hasRole("ADMIN")
                        .antMatchers("/test/test02")
                            .hasAnyRole("USER")
                        .anyRequest()//获得任意请求
                             .authenticated()//必须认证才允许访问
                .and()
                //配置form表单
                .formLogin()//配置表单
                    .loginProcessingUrl("/login")//配置登陆处理器的url地址，该地址所对应处理类由SpringSecurity提供
                    .loginPage("/login.html")//配置登陆页
                    .failureUrl("/fail.html")//登陆失败处理页
                .usernameParameter("")
                .passwordParameter("")

        ;
        return httpSecurity.build();//获得securityFilterChain的实现类对象并返回
    }

    /**
     * 用于处理认证和授权逻辑
     * 该方法内，可以定义认证和授权相关操作
     * 此处功能:
     * -设置内存账户，根据内存账户自定义用户名和密码进行登陆
     *
     * @param builder 认证管理器的编译器对象，该对象由springSecurity自动注入
     */
    @Autowired
    public void registerProvider(AuthenticationManagerBuilder builder) throws Exception {
        //SpringSecurity要求密码必须加密,创建加密器对象
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        /**
         * 基于内置账户认证配置
         */
        /*builder
                        .inMemoryAuthentication()//设置内存账户
                            .passwordEncoder(passwordEncoder)//设置密码加密器对象
                            .withUser("admin")//设置内置账户用户名
                            .password(passwordEncoder.encode("123456"))//设置内置账户密码
                            //SpringSecurity要求每个账户必须有一个角色，此处定义账户为设置角色
                            .roles("ADMIN")//设置角色
                        ;
         */
        /**
         * 基于数据库
         */
        builder.userDetailsService(userDetailsService)//用于指定登陆的处理逻辑(认证逻辑)的对象
                .passwordEncoder(passwordEncoder)//设置密码加密器
        ;
    }
}
