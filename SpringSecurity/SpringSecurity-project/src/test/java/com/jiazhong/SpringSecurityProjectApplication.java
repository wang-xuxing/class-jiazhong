package com.jiazhong;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
public class SpringSecurityProjectApplication {

     @Test
    void contextLoads(){
         BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

         String admin = bCryptPasswordEncoder.encode("admin");//加密
         System.out.println(admin);
     }
}
