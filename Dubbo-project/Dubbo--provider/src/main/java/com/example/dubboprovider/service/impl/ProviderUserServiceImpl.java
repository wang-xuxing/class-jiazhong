package com.example.dubboprovider.service.impl;


import com.dubbo.model.User;
import com.example.dubboprovider.service.ProviderUserService;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class ProviderUserServiceImpl implements ProviderUserService {
    @Override
    public String getName() {
        System.out.println("======dubbo接口被调用了======");
        return "DubboTest";
    }

    @Override
    public User getUserById(int user_id) {
        return new User(1,"xiaowang","123456");
    }
}
