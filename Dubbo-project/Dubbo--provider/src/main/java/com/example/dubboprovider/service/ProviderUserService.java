package com.example.dubboprovider.service;

import com.dubbo.model.User;

public interface ProviderUserService {
    String getName();

        public User getUserById(int user_id);
}
