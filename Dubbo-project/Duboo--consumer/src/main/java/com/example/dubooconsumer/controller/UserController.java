package com.example.dubooconsumer.controller;

import com.dubbo.model.User;
import com.example.dubooconsumer.service.ConsumerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private ConsumerUserService consumerUserService;

    @GetMapping("/user_name")
    public String userName(){
        return consumerUserService.userName();
    }

    @GetMapping("/getUserById")
    public User getUserById(int user_id) {
        return consumerUserService.getUserById(user_id);
    }
}
