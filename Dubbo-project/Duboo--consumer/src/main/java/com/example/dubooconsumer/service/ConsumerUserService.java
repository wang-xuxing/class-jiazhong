package com.example.dubooconsumer.service;

import com.dubbo.model.User;

public interface ConsumerUserService {
    String userName();

    User getUserById(int user_id);
}
