package com.example.dubooconsumer.service.impl;

import com.dubbo.model.User;
import com.example.dubboprovider.service.ProviderUserService;
import com.example.dubooconsumer.service.ConsumerUserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class ConsumerUserServiceImpl implements ConsumerUserService {
    @DubboReference
    private ProviderUserService providerUserService;

    @Override
    public String userName() {
        return providerUserService.getName();
    }

    @Override
    public User getUserById(int user_id) {
        return providerUserService.getUserById(user_id);
    }
}
