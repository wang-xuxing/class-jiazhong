package com.jiazhong.设计模式.单例模式.enum_;

public enum EnumDemo {
    INSTANCE,INSTANCE1("实例2");
    //成员变量
    private String name ;
    //带参构造无参数
    private EnumDemo(){

    }
    //带参构造有参数
    private EnumDemo(String name){
        this.name=name;
    }
    //内部类
    class EnumInner{
        EnumDemo enumDemo =EnumDemo.INSTANCE;
        String str = enumDemo.name;
    }
}
