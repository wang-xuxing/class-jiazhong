package com.jiazhong.设计模式.单例模式;

import java.util.concurrent.TimeUnit;

/**
 * 懒汉式
 */
public class Singleton02 {

    private static  Singleton02 instance = null;

    private Singleton02() {
    }

    public synchronized static Singleton02 getInstance(){
        if (instance == null){
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            instance =new Singleton02();
        }
        return instance;
    }
}
