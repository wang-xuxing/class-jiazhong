package com.jiazhong.设计模式.单例模式;

import java.util.concurrent.TimeUnit;

/**
 * 不用同步锁关键字的懒汉式
 */
public class Singleton03 {

    private Singleton03() {
    }
    private static class Inner{
        static final Singleton03 instance = new Singleton03();
    }
    public static Singleton03 getInstance(){
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return Inner.instance;
    }
}
