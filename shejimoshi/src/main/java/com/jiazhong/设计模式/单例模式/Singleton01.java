package com.jiazhong.设计模式.单例模式;

import java.util.concurrent.TimeUnit;
/**
 * 饿汉式
 */
public class Singleton01 {
//    private static final Singleton01 INSTANCE = new Singleton01();
    private static final Singleton01 INSTANCE = InitInstance();
    private  static Singleton01 InitInstance() {

        Singleton01 object = new Singleton01();
        return object;
    }
    /**
     * 构造器私有化
     */
    private Singleton01(){
    }
    public static Singleton01 getInstance() {
        return INSTANCE;
    }
}
