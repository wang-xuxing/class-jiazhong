package com.jiazhong.设计模式.装饰者模式;

/**
 * 定义一个人类
 * 学生继承人类，学生在学习
 * 程序员继承人类，程序员在敲代码
 * 学生和程序员不单单会学习和敲代码他们其中有些人可能还具有其他能力，
 * 如唱歌、跳舞等，我们通过装饰模式的来为不同的人群添加新的能力
 * 人类-被装饰的对象
 */
public abstract class Person {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void action();
}
