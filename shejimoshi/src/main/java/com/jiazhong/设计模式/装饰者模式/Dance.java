package com.jiazhong.设计模式.装饰者模式;

/**
 * 具体装饰类-跳舞
 */
public class Dance extends Decorator{
    public Dance(Person person) {
        super(person);
    }

    public Dance() {
    }

    @Override
    public void action() {
        System.out.println("喜欢跳舞");
        person.action();
    }
}
