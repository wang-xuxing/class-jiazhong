package com.jiazhong.设计模式.装饰者模式;

/**
 * 具体装饰器-唱歌
 */
public class Sing extends Decorator{
    public Sing(Person person){
        super(person);
    }

    public Sing() {
    }

    @Override
    public void action() {
        System.out.println("喜欢唱歌");
        person.action();
    }
}
