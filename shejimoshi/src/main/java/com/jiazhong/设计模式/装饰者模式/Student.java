package com.jiazhong.设计模式.装饰者模式;

/**
 * 学生
 */
public class Student extends Person{
    @Override
    public void action() {
        System.out.println("学生:"+super.getName()+"在认真学习...");
    }
}
