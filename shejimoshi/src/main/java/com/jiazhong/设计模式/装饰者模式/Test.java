package com.jiazhong.设计模式.装饰者模式;

public class Test {
    public static void main(String[] args) {
        Student student =new Student();
        Person person =new Dance(new Sing(student));
        student.setName("小王");
        person.action();
    }
}
