package com.jiazhong.设计模式.装饰者模式;

/**
 * 程序员-具体实现类
 */
public class Programmer extends Person {
    @Override
    public void action() {
        System.out.println("程序员:"+super.getName()+"在疯狂敲代码...");
    }
}
