package com.jiazhong.设计模式.装饰者模式;

/**
 * 抽象-装饰类
 */
public abstract class Decorator extends Person{
    public Decorator() {
    }
    protected Person person;
    /**
     * 装饰器
     * @param person:装饰对象
     */
    public Decorator(Person person){
        this.person =person;
    }

}
