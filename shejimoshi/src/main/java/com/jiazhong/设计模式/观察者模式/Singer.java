package com.jiazhong.设计模式.观察者模式;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体主题:也就是关注的主播，需要有一个关注列表
 */
public class Singer implements SubObject {
    String msg;//通知信息
    private List<ObServer> obServers = new ArrayList<>();

    //关注该主播
    @Override
    public void registerObServer(ObServer obServer) {
        if (obServer != null) {
            obServers.add(obServer);
        }
    }

    //取关该主播
    @Override
    public void removeObServer(ObServer obServer) {
        if (obServers.contains(obServer)) {
            obServers.remove(obServer);
        }
    }

    /**
     * 发布信息
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
        //向观察者(抖音用户)发布信息
        if (!this.msg.equals("")){
            notifyObServer();
        }
    }
    //有主播有新信息时进行通知
    @Override
    public void notifyObServer() {
        for (ObServer obServer : obServers){
            obServer.action(msg);
        }
    }
}
