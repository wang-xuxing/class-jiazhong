package com.jiazhong.设计模式.观察者模式;

/**
 * 具体观察者-具体用户
 *
 *  主播发出新通知，用户会进行某些行为
 */
public class AUser implements  ObServer {
    private String username;
    //当前用户关注的主播
    private SubObject subObject;
    public AUser(String username, SubObject subObject) {
        this.username = username;
        this.subObject = subObject;
    }
    @Override
    public void action(String msg) {
        System.out.println(username+":主播发送新动态了!!!快去看看");
        if (msg!=null&&!msg.equals("")){
            System.out.println(msg);
        }
    }
}
