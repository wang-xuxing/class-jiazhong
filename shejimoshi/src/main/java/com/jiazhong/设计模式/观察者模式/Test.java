package com.jiazhong.设计模式.观察者模式;

    public class Test {
        public static void main(String[] args) {
            Singer singer =new Singer();
            AUser xiaoming =new AUser("小明",singer);
            xiaoming.action("关注他");
            AUser xiaohua =new AUser("小花",singer);
            xiaoming.action("点赞");
            //用户关注主题
            singer.registerObServer(xiaoming);
            singer.registerObServer(xiaohua);

        }
    }
