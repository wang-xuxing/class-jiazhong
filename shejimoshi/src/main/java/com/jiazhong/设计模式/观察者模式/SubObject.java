package com.jiazhong.设计模式.观察者模式;

/**
 * 抽象主题:类似抖音平台-有关注，取关，通知等功能
 */
public interface SubObject {
    //注册
    public void registerObServer(ObServer obServer);

    //取关
    public void removeObServer(ObServer obServer);

    //通知
    public void notifyObServer();
}
