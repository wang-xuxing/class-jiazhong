package com.jiazhong.设计模式.观察者模式;

/**
 * 观察者抽象
 */
public interface ObServer{
    //行为
    public void action(String msg);
}
