package com.jiazhong.设计模式.工厂模式.简单工厂;

import com.jiazhong.设计模式.工厂模式.Phone;

public class Test {

    public static void main(String[] args) throws Exception {
        //创建工厂
        PhoneFactory phoneFactory =new PhoneFactory();
        //制造手机
        Phone phone = phoneFactory.makePhone(PhoneType.IPHONE);
        phone.show();
    }
}
