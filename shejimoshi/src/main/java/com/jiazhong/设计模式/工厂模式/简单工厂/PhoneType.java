package com.jiazhong.设计模式.工厂模式.简单工厂;

/**
 * 手机类型枚举
 */
public enum PhoneType {
    IPHONE,
    HUAWEI,
    XIAOMI;
}
