package com.jiazhong.设计模式.工厂模式.抽象工厂;

import com.jiazhong.设计模式.工厂模式.HUAWEIPhone;
import com.jiazhong.设计模式.工厂模式.Phone;

public class HUAWEIPhoneFactory implements AbstractPhoneFactory{
    @Override
    public Phone makePhone() {
        return new HUAWEIPhone();
    }
}
