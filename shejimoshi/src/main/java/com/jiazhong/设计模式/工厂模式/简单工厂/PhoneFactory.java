package com.jiazhong.设计模式.工厂模式.简单工厂;

import com.jiazhong.设计模式.工厂模式.HUAWEIPhone;
import com.jiazhong.设计模式.工厂模式.IPhone;
import com.jiazhong.设计模式.工厂模式.Phone;
import com.jiazhong.设计模式.工厂模式.XIAOMIPhone;

/**
 * 简单手机工厂
 */
public class PhoneFactory {

    /**
     * 造手机
     * @param phoneType
     * @return
     * @throws Exception
     */
    public Phone makePhone(PhoneType phoneType)  throws Exception{

        switch (phoneType){
            case HUAWEI:
                return new HUAWEIPhone();
            case IPHONE:
                return new IPhone();
            case XIAOMI:
                return new XIAOMIPhone();
            default:
                throw  new Exception("不支持手机异常");
        }
    }
}
