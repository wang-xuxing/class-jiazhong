package com.jiazhong.设计模式.工厂模式.抽象工厂;

import com.jiazhong.设计模式.工厂模式.Phone;
import com.jiazhong.设计模式.工厂模式.XIAOMIPhone;

public class XIAOMIPhoneFactory implements AbstractPhoneFactory{
    @Override
    public Phone makePhone() {
        return new XIAOMIPhone();
    }
}
