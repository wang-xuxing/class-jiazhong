package com.jiazhong.设计模式.工厂模式.抽象工厂;

public class Test {

    public static void main(String[] args) {
        //创建华为工厂
        HUAWEIPhoneFactory huaweiPhoneFactory =new HUAWEIPhoneFactory();
        //华为工厂造手机
        huaweiPhoneFactory.makePhone().show();
        //创建苹果工厂
        IPhoneFactory iPhoneFactory =new IPhoneFactory();
        //苹果工厂造手机
        iPhoneFactory.makePhone().show();

    }
}
