package com.jiazhong.设计模式.工厂模式;

/**
 * 抽象产品
 */
public interface Phone {
    public void show();
}
