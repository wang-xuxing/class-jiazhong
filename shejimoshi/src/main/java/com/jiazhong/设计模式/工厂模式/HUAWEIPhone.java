package com.jiazhong.设计模式.工厂模式;

/**
 * 具体产品-华为手机
 */
public class HUAWEIPhone implements Phone{
    @Override
    public void show() {
        System.out.println("华为手机加工成功...........");
    }
}
