package com.jiazhong.设计模式.工厂模式.抽象工厂;

import com.jiazhong.设计模式.工厂模式.Phone;

//总工厂
public interface AbstractPhoneFactory {
    Phone makePhone();
}
