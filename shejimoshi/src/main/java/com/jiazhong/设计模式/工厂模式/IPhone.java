package com.jiazhong.设计模式.工厂模式;

/**
 * 具体产品-苹果手机
 */
public class IPhone implements Phone{
    @Override
    public void show() {
        System.out.println("苹果手机被创建");
    }
}
