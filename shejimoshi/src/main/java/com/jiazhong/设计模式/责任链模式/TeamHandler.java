package com.jiazhong.设计模式.责任链模式;

public class TeamHandler extends AbstractHandler{
    public TeamHandler(AbstractHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public void handle(int days) {
        System.out.println("项目组长通过审批...");
        if (days >1){
            super.getNextHandler().handle(days);
        }
    }
}
