package com.jiazhong.设计模式.责任链模式;

public class Test {
    public static void main(String[] args) {
        AbstractHandler handler =new TeamHandler(new PMHandler(new GMHandler()));
        System.out.println("我要请假5天");
        handler.handle(5);
    }
}
