package com.jiazhong.设计模式.责任链模式;

public class PMHandler extends AbstractHandler {
    public PMHandler(AbstractHandler nextHandler) {
        super(nextHandler);
    }
    @Override
    public void handle(int days) {
        System.out.println("项目经理通过审批....");
        if (days > 3) {
            super.getNextHandler().handle(days);
        }
    }
}
