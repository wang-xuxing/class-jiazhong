package com.jiazhong.设计模式.责任链模式;

public class GMHandler extends AbstractHandler {
    public GMHandler() {
    }

    @Override
    public void handle(int days) {
        System.out.println("总经理通过审批...");
    }
}
