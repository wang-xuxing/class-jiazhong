package com.jiazhong.设计模式.责任链模式;

public abstract class AbstractHandler {
    private AbstractHandler nextHandler;
    public AbstractHandler getNextHandler() {
        return nextHandler;
    }

    public AbstractHandler(AbstractHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public AbstractHandler() {
    }

    public AbstractHandler setNextHandler(AbstractHandler nextHandler) {
        this.nextHandler = nextHandler;
        return this;
    }
    public abstract void handle(int days);
}
