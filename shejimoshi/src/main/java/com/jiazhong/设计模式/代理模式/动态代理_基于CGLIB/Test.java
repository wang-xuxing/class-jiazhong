package com.jiazhong.设计模式.代理模式.动态代理_基于CGLIB;

import com.jiazhong.设计模式.代理模式.Boss;

public class Test {
    public static void main(String[] args) {
        Boss boss =new Boss();

        Boss boss1=(Boss) new CGlibProxy().getProxyInstance(boss);
        boss1.treat();
    }

}
