package com.jiazhong.设计模式.代理模式.动态代理_基于CGLIB;

import com.jiazhong.设计模式.代理模式.Boss;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * cglib动态代理类，该类继承MethodInterceptor接口
 * MethodInterceptor是方法拦截器接口
 */
public class CGlibProxy implements MethodInterceptor {
    private Object target;

    /**
     * 获得代理对象的实例
     * @return
     */
    public  Object getProxyInstance(Object target){
        this.target = target;
        /**
         * 通过Enhancer对象来创建目标对象
         */
        Enhancer enhancer =new Enhancer();
        //设置代理对象的父类
        enhancer.setSuperclass(target.getClass());
        //设置拦截目标对象目标方法的拦截器
        enhancer.setCallback(this);
        //创建并返回代理对象
        return  enhancer.create();
    }


    /**
     * 当拦截到目标方法被调用自动执行该方法
     * @param proxy    //代理对象
     * @param method    //目标方法
     * @param args      //方法参数
     * @param methodProxy   //代理对象方法
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("大家吃老板请客");
        //调用目标对象中的目标方法
        Object obj = method.invoke(target, args);
//        Object obj = methodProxy.invokeSuper(proxy,args);//使用代理对象访问目标对象
        System.out.println("吃完了吧，大家一路平安");
        return obj;
    }
}
