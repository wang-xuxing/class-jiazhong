package com.jiazhong.设计模式.代理模式.动态代理_基于JDK;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 自定义动态处理器
 * invoke()调用代理对象的代理方法时，代理方法会委托给invoke方法执行
 */
public class DynamicProxyHandler implements InvocationHandler {
    private Object targetObj;

    public DynamicProxyHandler(Object targetObj) {
        this.targetObj = targetObj;
    }

    /**
     * 代理对象调用目标对象方法
     * @param 代理对象
     * @param 代理对象调用的方法
     * @param 代理对象调用目标方法参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("大家吃老板请客");
        Object invoke = method.invoke(targetObj, args);
        System.out.println("吃完了吧，大家一路平安");
        return invoke;
    }
}
