package com.jiazhong.设计模式.代理模式;

/**
 * 目标对象
 */
public class Boss implements Treat {
    @Override
    public void treat() {
        System.out.println("我付钱，你们敞开吃");
    }
}
