package com.jiazhong.设计模式.代理模式.静态代理;

import com.jiazhong.设计模式.代理模式.Boss;
import com.jiazhong.设计模式.代理模式.静态代理.Assistant;

public class Test {
    public static void main(String[] args) {
        //实例化代理对象
        Assistant assistant =new Assistant();
        //实例化目标对象
        Boss boss =new Boss();
        //目标对象注入
        assistant.setBoss(boss);
        //调用代理对象方法
        assistant.treat();
    }
}
