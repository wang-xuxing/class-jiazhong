package com.jiazhong.设计模式.代理模式;

/**
 * 业务类
 */
public interface Treat {
   public void treat();
}
