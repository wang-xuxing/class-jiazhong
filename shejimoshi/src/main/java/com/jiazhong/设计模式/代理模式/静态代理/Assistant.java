package com.jiazhong.设计模式.代理模式.静态代理;

import com.jiazhong.设计模式.代理模式.Boss;
import com.jiazhong.设计模式.代理模式.Treat;

/**
 * 代理对象
 */
public class Assistant implements Treat {
    Boss boss;
    public void setBoss(Boss boss) {
        this.boss = boss;
    }
    @Override
    public void treat() {
        System.out.println("大家吃老板请客");
        boss.treat();
        System.out.println("吃完了吧，大家一路平安");
    }
}
