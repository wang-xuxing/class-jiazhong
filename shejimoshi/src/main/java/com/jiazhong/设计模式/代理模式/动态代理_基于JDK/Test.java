package com.jiazhong.设计模式.代理模式.动态代理_基于JDK;

import com.jiazhong.设计模式.代理模式.Boss;
import com.jiazhong.设计模式.代理模式.Treat;

import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {
        Boss boss =new Boss();

        Treat treat=(Treat) Proxy.newProxyInstance(
                Test.class.getClassLoader(),
                new Class[]{Treat.class},
                new DynamicProxyHandler(boss)

        );
        treat.treat();
    }

}
