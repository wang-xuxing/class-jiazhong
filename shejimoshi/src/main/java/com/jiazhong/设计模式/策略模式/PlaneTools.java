package com.jiazhong.设计模式.策略模式;

/**
 * 具体策略类
 */
public class PlaneTools implements TravelTools{
    @Override
    public void travelTools() {
        System.out.println("我们有钱，咋坐飞机去吧...");
    }
}
