package com.jiazhong.设计模式.策略模式;


/**
 * 具体策略
 */
public class TrainTools implements TravelTools{
    @Override
    public void travelTools() {
        System.out.println("哦，上周花了好多钱，我们做火车去吧");
    }
}
