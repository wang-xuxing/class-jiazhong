package com.jiazhong.设计模式.策略模式;

public class Test {
    public static void main(String[] args) {
        //坐飞机去
        PlaneTools planeTools =new PlaneTools();
        //坐火车去
        TrainTools trainTools =new TrainTools();
        Travel travel =new Travel(planeTools);
        travel.travel();
    }
}
