package com.jiazhong.设计模式.策略模式;

public class Travel {
    private TravelTools travelTools;
    Travel(TravelTools travelTools) {
        this.travelTools = travelTools;
    }
    public void travel(){
        System.out.println("一起去海南吧....");
        //怎么去
        travelTools.travelTools();
        System.out.println("到海南要好好玩玩");
    }
}
