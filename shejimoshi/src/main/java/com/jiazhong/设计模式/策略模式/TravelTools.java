package com.jiazhong.设计模式.策略模式;

/**
 * 抽象策略，可为抽象类，根据具体业务而定
 */
public interface TravelTools {
    void travelTools();
}
