package com.jiazhong.设计模式.适配器模式;

/**
 * 手机接口
 */
public interface PhoneCharge {
    //当前手机所需电压
    Integer Phone_VOLTAGE = 5 ;

    public Integer output();
}
