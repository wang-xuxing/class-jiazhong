package com.jiazhong.设计模式.适配器模式;

public class Phone {
    public void charge(PhoneCharge phoneCharge) throws Exception {
        if (phoneCharge.output()==5){
            System.out.println("手机开始充电");
        }else {
            throw new Exception("手机充电器不匹配，请更换充电器");
        }
    }
}
