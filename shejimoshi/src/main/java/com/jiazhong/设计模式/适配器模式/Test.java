package com.jiazhong.设计模式.适配器模式;

public class Test {
    public static void main(String[] args) throws Exception {
        Power power =new Power();
        PowerAdapter powerAdapter =new PowerAdapter(power);

        Phone phone =new Phone();
        phone.charge(powerAdapter);
    }
}
