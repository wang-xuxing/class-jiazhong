package com.jiazhong.设计模式.适配器模式;

public class PowerAdapter implements PhoneCharge {
    Power power = new Power();

    public PowerAdapter(Power power) {
        this.power = power;
    }

    @Override
    public Integer output() {
        int voltage = power.outPut();
        int outputVoltage = voltage/(voltage/Phone_VOLTAGE);
        System.out.println("输出电压为:"+outputVoltage+"V");
        return outputVoltage;
    }
}
