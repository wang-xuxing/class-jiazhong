package com.jiazhong.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo01")
public class DemoControl {

    @RequestMapping("/demo01")
    public String Demo01(){
        return "ok";
    }
}
