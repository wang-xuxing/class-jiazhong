package com.jiazhong;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring启动类，核心配置类，必须在项目的根包下，Springboot组件扫描器会自动扫描当前类所在的包及其子包中的类
 * @SpringBootApplication:SpringBoot启动类注解，加入该注解后，运行该类的main函数，Spring项目启动
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        //启动springboot环境
        SpringApplication.run(Application.class,args);
    }
}
