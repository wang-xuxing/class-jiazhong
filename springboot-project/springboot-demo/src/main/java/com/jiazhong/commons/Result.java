package com.jiazhong.commons;

import lombok.Generated;
import lombok.Getter;

@Getter
/**
 * 结果处理类
 */
public class Result {

    private Boolean success;//成功(true),失败(false)
    private Integer statusCode;//状态码
    private String message;//消息
    private Object data;//附加数据

    /**
     * 成功
     * @return
     */
    public static Result success() {
       return new Result(true,200,null,null);
    }
    /**
     * 成功-带有消息
     * @return
     */
    public static Result success(String message) {
        return new Result(true,200,message,null);
    }
    /**
     * 成功-带有消息和附加数据
     * @return
     */
    public static Result success(String message,Object data) {
        return new Result(true,200,message,data);
    }
    /**
     * 成功-带有附加数据
     * @return
     */
    public static Result success(Object data) {
        return new Result(true,200,null,data);
    }

    /**
     * 成功
     * @return
     */
    public static Result success(int statusCode) {
        return new Result(false,statusCode,null,null);
    }
    /**
     * 成功-带有消息
     * @return
     */
    public static Result success(int statusCode,String message) {
        return new Result(false,statusCode,message,null);
    }
    /**
     * 成功-带有消息和附加数据
     * @return
     */
    public static Result success(int statusCode,String message,Object data) {
        return new Result(false,statusCode,message,data);
    }
    /**
     * 成功-带有消息和附加数据
     * @return
     */
    public static Result success(int statusCode,Object data) {
        return new Result(false,statusCode,null,data);
    }

    private Result(Boolean success, Integer statusCode, String message, Object data) {
        this.success = success;
        this.statusCode = statusCode;
        this.message=message;
        this.data=data;
    }


}
