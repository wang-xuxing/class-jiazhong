package com.jiazhong.controller;

import com.github.pagehelper.PageInfo;
import com.jiazhong.commons.Result;
import com.jiazhong.model.User;
import com.jiazhong.servcie.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserService userService;
    @RequestMapping("/selectUser")
    public List<User> selectUser(){

        return userService.selectUser();
    }

    @RequestMapping("/selectUserByUserId")
    public User selectUserByUserId(Integer userId) {
        return userService.selectUserByUserId(userId);
    }
    @RequestMapping("/addUser")
    public Result addUser(@RequestBody User user) {
        try {
            userService.addUser(user);
            return Result.success("用户添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.success(500,"商品添加失败");
        }
    }
    @DeleteMapping("/deleteUserById")//resfull风格接受客户端请求
    public Result deleteUserById(Integer... userIds) {
        try {
            userService.deleteUserById(userIds);
            return Result.success("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.success(500,"用户删除信息失败!!!");
        }
    }
    @PutMapping("/updateUserById")
    public Result updateUserById(@RequestBody User user) {
        try {
            userService.updateUserById(user);
            return Result.success("修改信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.success(500,"用户修改信息失败!!!");
        }
    }
}
