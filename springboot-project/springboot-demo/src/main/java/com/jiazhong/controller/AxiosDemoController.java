package com.jiazhong.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/axiosDemo")
public class AxiosDemoController {
    @RequestMapping("/demo01")
      public String demo01(){
          return "demo01";
      }

}
