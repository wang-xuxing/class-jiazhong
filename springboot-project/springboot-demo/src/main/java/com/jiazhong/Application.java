package com.jiazhong;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@MapperScan("com.jiazhong.mapper")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public WebMvcConfigurer webMvcConfigurer(){

       return new WebMvcConfigurer() {
           /**
            * 进行配置
            * @param registry:跨域构造器
            */
           @Override
           public void addCorsMappings(CorsRegistry registry) {

               registry
                       .addMapping("/**")//当前应用所有资源允许被跨域访问
                       .allowedOrigins("http://localhost:8080")//设置允许访问当前应用的项目
                       .allowedMethods("GET","POST","DELETE","PUT","HEAD","OPTIONS")//设置允许哪些请求方式进行跨域访问,默认情况只允许简单请求(get,post,head)
                       .allowCredentials(true);//是否允许使用凭证，是否允许使用session后cookie存储凭证信息
           }
       };
    }
}
