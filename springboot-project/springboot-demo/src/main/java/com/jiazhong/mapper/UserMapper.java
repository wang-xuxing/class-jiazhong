package com.jiazhong.mapper;

import com.jiazhong.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    //查询所有用户
    @Select("select * from user")
    public List<User> selectUser();

    //根据id查询单个用户
    @Select("select * from user where userId=#{userId}")
    public User selectUserByUserId(Integer userId);

    //添加用户
    @Insert("insert into user values(default,#{username},#{sex},#{date},#{age},#{address})")
    public void addUser(User user);

    //根据id删除用户
    void deleteUserById(@Param("userIds") Integer... userIds);

    //根据id修改用户
    @Update("update user set username=#{username},sex=#{sex},date=#{date},age=#{age},address=#{address} where userId=#{userId}")
    void updateUserById(User user);
}
