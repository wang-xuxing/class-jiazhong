package com.jiazhong.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private Integer userId;
    private String username;
    private Integer sex;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")//将Java对象转化为JSON数据指定的日期格式
    private Date date;
    private Integer age;
    private String address;
}
