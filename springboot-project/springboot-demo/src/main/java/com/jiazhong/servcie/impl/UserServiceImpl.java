package com.jiazhong.servcie.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiazhong.mapper.UserMapper;
import com.jiazhong.model.User;
import com.jiazhong.servcie.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service

public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;
    @Override
    public List<User> selectUser() {

        return userMapper.selectUser();
    }
    @Override
    public User selectUserByUserId(Integer userId) {
        return userMapper.selectUserByUserId(userId);
    }

    @Override
    public void addUser(User user) {
         userMapper.addUser(user);
    }

    @Override
    public void deleteUserById(Integer... userIds) {
        userMapper.deleteUserById(userIds);
    }

    @Override
    public void updateUserById(User user) {
         userMapper.updateUserById(user);
    }


}
