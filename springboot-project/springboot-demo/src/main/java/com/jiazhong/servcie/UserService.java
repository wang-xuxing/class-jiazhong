package com.jiazhong.servcie;


import com.jiazhong.model.User;

import java.util.List;

public interface UserService {

    public List<User> selectUser();

    public User selectUserByUserId(Integer userId);

    public void addUser(User user) throws Exception;

    void deleteUserById( Integer... userIds) throws Exception;

    void updateUserById(User user) throws Exception;

}
