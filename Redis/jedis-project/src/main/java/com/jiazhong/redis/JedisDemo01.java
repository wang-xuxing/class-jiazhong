package com.jiazhong.redis;

import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JedisDemo01 {

    /**
     * 使用Jedis操作字符串
     */
    public static void jedisForString(){
        /**
         * 创建jedis对象并指定服务器ip地址及端口号
         */
        Jedis jedis=new Jedis("127.0.0.1",6379);
        jedis.set("num1","0");
        jedis.set("num2","1");
        String value = jedis.get("num1");//根据key获得对应的value
        jedis.mset("num3","2","num4","3");
        List<String> mget = jedis.mget("num1", "num2", "num3");
        System.out.println(mget);
        System.out.println("--------------");
        //获取redis所有元素的key
        Set<String> keys = jedis.keys("*");
        System.out.println(keys);
        System.out.println("-----------------");
        System.out.println(value);
        System.out.println("-----------------");
        jedis.setnx("num1","1");
        String num1 = jedis.get("num1");
        System.out.println(num1);
        jedis.expire("num1",1000);
        System.out.println("-----------");
        long num11 = jedis.ttl("num1");
        System.out.println(num11);
        jedis.flushAll();
        jedis.close();
    }
    public static void jedisForMap(){
        /**
         * 创建jedis对象并指定服务器ip地址及端口号
         */
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //向hash中添加数据
        Map valMap = new HashMap<String,Object>();
        valMap.put("id","001");
        valMap.put("name","小明");
        valMap.put("age","20");
        /**
         * 参数1:key
         * 参数2:value对应的是map集合
         */
        jedis.hmset("user",valMap);
        Map<String,String> user = jedis.hgetAll("user");
        System.out.println(user);
        //关闭与Redis的连接
        jedis.close();
    }
    public static void main(String[] args) {
//        JedisDemo01.jedisForString();
//          JedisDemo01.jedisForList();
    }
}
