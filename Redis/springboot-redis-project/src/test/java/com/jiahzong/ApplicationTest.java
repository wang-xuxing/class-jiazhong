package com.jiahzong;


import com.jiazhong.Application;
import com.jiazhong.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;


@SpringBootTest(classes = Application.class)
class ApplicationTest {
    private RedisTemplate redisTemplate;

    @Autowired
    public ApplicationTest(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        /**
         * 设置redisTemplate中key和value的序列化
         * SpringDateRedis默认使用JDKSerializationRedisSerializer对key和value进行序列化
         * GenericJackson2JsonRedisSerializer:将任意类型的数据转化为json串进行序列化到redis中
         * new Jackson2JsonRedisSerializer<User>(User.class):将指定类型转化json并序列化到redis中
         * new GenericToStringSerializer<User>(User.class):将任意类型数据转化为字符串
         *
         */
        //设置key序列化
        //StringRedisSerializer:只支持字符串
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //设置value序列化
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<User>(User.class));
//          redisTemplate.setValueSerializer(new GenericToStringSerializer<User>(User.class));
    }

    /**
     * 操作redis的字符串
     */
    @Test
    void operateString() {
        //获得字符串操作对象
        ValueOperations valueOperations = redisTemplate.opsForValue();
//        valueOperations.set("user",new User());//添加对象
//        valueOperations.set("num",100);
//        Integer num = (Integer) valueOperations.get("num");
//        Long num = valueOperations.increment("num");//incr命令
//        Long num = valueOperations.increment("num", 10);//incrby命令
//        User user = (User) valueOperations.get("user");
//          valueOperations.set("username","admin",100, TimeUnit.SECONDS);//添加字符串并设置过期时间
//          valueOperations.set("age",18, Duration.ofSeconds(10));
//        Map<String,Object> map=new HashMap<>();
//        map.put("username","admin");
//        map.put("age",88);
//        valueOperations.multiSet(map);//mset命令
//        ArrayList arrayList=new ArrayList();
//        Collections.addAll(arrayList,"username","age");
//        List list = valueOperations.multiGet(arrayList);//mget命令
//        System.out.println(list);
//         valueOperations.setIfAbsent("age",18);//setnx命令
//        valueOperations.setIfAbsent("age1",18);
//        System.out.println(user);
    }


    @Test
    void operateList() {
        ListOperations listOperations = redisTemplate.opsForList();
//        listOperations.leftPush("list","aaa");//lpush
//        listOperations.leftPush("list","bbb");//lpush
//        listOperations.rightPush("list","ccc");//rpush

//        listOperations.leftPushAll("list","ddd","eee","fff");
//        listOperations.leftPop("list");//lpop
//        List list = listOperations.range("list", 0, -1); //lrange key start end
//        list.forEach(System.out::println );

    }


    @Test
    void operateHash() {
        HashOperations hashOperations = redisTemplate.opsForHash();
//        hashOperations.put("hash","hashKey","value");//hset
        Map<String, Object> map = new HashMap<>();
        map.put("hashKey01", "value01");
        map.put("hashKey02", "value02");
        map.put("hashKey03", "value03");
        map.put("hashKey04", "value04");
        hashOperations.putAll("hash", map);//hmset
//        System.out.println(hashOperations.get("hash", "hashKey"));//hget
        System.out.println(hashOperations.entries("hash"));//hmget
    }


    @Test
    void operateSet() {
        SetOperations setOperations = redisTemplate.opsForSet();
//        setOperations.add("key1","value1","value2","value1");//sadd

//        System.out.println(setOperations.isMember("key1", "value"));//sismenmbers
        setOperations.add("key1", new User());
        System.out.println(setOperations.members("key1"));//smembers
    }


    @Test
    void operateZSet() {
        ZSetOperations zSetOperations = redisTemplate.opsForZSet();
//        zSetOperations.add("scores","小明",90.9);//zadd
//        zSetOperations.add("scores","花花",88.6);
//        Set scores = zSetOperations.range("scores", 0, -1);//zrange
//        Set <ZSetOperations.TypedTuple> scoreSet=new HashSet<>();
//        scoreSet.add(ZSetOperations.TypedTuple.of("张三",100d));
//        scoreSet.add(ZSetOperations.TypedTuple.of("李四",60.55));
//        zSetOperations.add("scores",scoreSet);
        /**
         * 根据z_set的key获得value和filed，filed和value被封装在TypedTuple中
         */
//        Set<ZSetOperations.TypedTuple> scores = zSetOperations.reverseRange("scores", 0, -1);//zrevrange
//        scores.forEach(System.out::println);
//        for (ZSetOperations.TypedTuple typedTuple : scores) {
//            System.out.println(typedTuple.getScore() + ":" + typedTuple.getValue());
//        }

        //获得前三名
//        Set<ZSetOperations.TypedTuple> scores = zSetOperations.reverseRangeByScoreWithScores("scores", 0, 100, 0, 3);
//        for (ZSetOperations.TypedTuple typedTuple:scores){
//            System.out.println(typedTuple.getScore()+":"+typedTuple.getValue());
//        }
//        Long rank = zSetOperations.rank("scores", "小明");//获得该值得排名
//        System.out.println(rank+1);
        Long scores = zSetOperations.zCard("scores");//获得元素数量
        System.out.println(scores);
    }

}
